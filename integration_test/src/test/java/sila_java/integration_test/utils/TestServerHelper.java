package sila_java.integration_test.utils;

import sila_java.library.server_base.utils.ArgumentHelperBuilder;
import sila_java.examples.test_server.TestServer;

import javax.annotation.Nullable;

public class TestServerHelper {
    public static TestServer build(
            final int port,
            final @Nullable String configPath,
            final @Nullable String netInterface
    ) {
        return new TestServer(
                ArgumentHelperBuilder.build(port, "TestServerTest", configPath, netInterface)
        );
    }
}
