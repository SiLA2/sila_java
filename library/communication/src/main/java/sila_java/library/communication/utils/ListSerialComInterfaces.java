package sila_java.library.communication.utils;

import com.fazecast.jSerialComm.SerialPort;

import static java.lang.System.out;

/**
 * Utility program that list series communication interfaces
 */
public class ListSerialComInterfaces {

    /**
     * main
     * @param args arguments
     */
    public static void main(String args[]) {
        ListSerialComInterfaces.display();
        System.exit(0);
    }

    /**
     * Send to standard output the connected serial communication interfaces
     */
    public static void display() {
        out.println("=====================================================");
        out.println("   Connected USB-SERIAL Communication Interfaces:");
        for (SerialPort port : SerialPort.getCommPorts())
            displayInterfaceInformation(port);
        out.println("=====================================================\n");
    }

    /**
     * Display serial port information
     */
    static void displayInterfaceInformation(SerialPort port) {
        out.printf("    System Port Name: /dev/%s\n", port.getSystemPortName());
        out.printf("    Descriptive Port Name: %s\n", port.getDescriptivePortName());
        out.printf("\nNote: \nSome OS's use port name, while others use descriptive name.\n" +
                "Usage: -s [string]\n'string' can be a substring of the name found (name must contain string).\n");
    }
}
