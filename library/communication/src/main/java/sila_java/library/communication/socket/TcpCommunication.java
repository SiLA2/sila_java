package sila_java.library.communication.socket;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

import java.io.*;
import java.net.InetSocketAddress;
import java.net.Socket;

/**
 * TCP communication
 */
@Slf4j
public class TcpCommunication extends CommunicationSocket {
    private final String ip;
    private final int port;
    private Socket socket;

    /**
     * TCP Communication Constructor
     */
    public TcpCommunication(@NonNull String ip, @NonNull int port) {
        this.ip = ip;
        this.port = port;
    }

    /**
     * @inheritDoc
     */
    @Override
    synchronized void openSocket() throws IOException {
        final InetSocketAddress inetSocketAddress = new InetSocketAddress(ip, port);
        if (inetSocketAddress.isUnresolved()) {
            throw new IOException(String.format("Inet Socket Address %s:%d NOT resolvable", ip, port));
        } else {
            /*
             * Socket needs to be recreated on reconnection
             * {@see https://docs.oracle.com/javase/6/docs/api/java/net/Socket.html}
             * Quote: "Once a socket has been closed, it is not available [...]. A new socket needs to be created."
             */
            if (socket != null) {
                throw new IllegalStateException("Socket was not properly closed.");
            } else {
                socket = new Socket();
                socket.connect(inetSocketAddress);
                if (!socket.isConnected()) {
                    throw new IOException("Connection didn't succeed");
                }
            }
        }
    }

    /**
     * @inheritDoc
     */
    @Override
    synchronized void closeSocket() throws IOException {
        if (socket != null) {
            socket.close();
            socket = null;
        }
    }

    /**
     * @inheritDoc
     */
    @Override
    public OutputStream getOutputStream() throws IOException { return socket.getOutputStream(); }

    /**
     * @inheritDoc
     */
    @Override
    public InputStream getInputStream() throws IOException { return socket.getInputStream(); }
}
