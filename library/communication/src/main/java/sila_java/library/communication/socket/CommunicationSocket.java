package sila_java.library.communication.socket;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Bidirectional Communication being managed by {@see SynchronousCommunication}
 */
public abstract class CommunicationSocket {
    private boolean isClosed = true;


    /**
     * Open socket
     * @throws IOException
     */
    public void open() throws IOException {
        if (!isClosed) return;
        openSocket();
        isClosed = false;
    }

    /**
     * Close socket
     * @throws IOException
     */
    public void close() throws IOException {
        closeSocket();
        isClosed = true;
    }

    /**
     * Get output stream
     * @return the output stream
     * @throws IOException
     */
    public abstract OutputStream getOutputStream() throws IOException;

    /**
     * Get input stream
     * @return the input stream
     * @throws IOException
     */
    public abstract InputStream getInputStream() throws IOException;

    /**
     * Open communication socket, will only be called when closed
     */
    abstract void openSocket() throws IOException;

    /**
     * Closing the socket, can be called anytime
     */
    abstract void closeSocket() throws IOException;
}

