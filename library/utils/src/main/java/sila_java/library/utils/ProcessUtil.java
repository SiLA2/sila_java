package sila_java.library.utils;

import lombok.extern.slf4j.Slf4j;
import org.jutils.jprocesses.JProcesses;
import org.jutils.jprocesses.model.ProcessInfo;

import java.io.*;
import java.time.Duration;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;

/**
 * Utility for getting, starting and stopping external processes.
 */
@Slf4j
public class ProcessUtil {
    /**
     * Interface that clients of the {@link ProcessMonitor}
     * can implement to get notified if a process is down.
     */
    public interface ProcessListener {
        void onProcessDown(String processName);
    }

    /**
     * Simple monitoring thread that can check if a process is down
     */
    public static class ProcessMonitor implements Runnable {
        private static final long MONITOR_PERIOD = Duration.ofSeconds(30).toMillis();

        private final List<String> processNames;
        private final List<ProcessListener> listeners = new CopyOnWriteArrayList<>();
        private final Thread monitorThread;
        private final JProcesses jProcesses = JProcesses.get().fastMode();

        private boolean keepRunning = true;

        /**
         * Creates a new process monitor for a list of processes to be watched.
         *
         * @param processNames a list of processes like "AutoLynx.exe" on win or "AutoLynx" on *Nix
         */
        public ProcessMonitor(List<String> processNames) {
            this.processNames = processNames;
            this.monitorThread = new Thread(this, "ProcessMonitor");
        }

        public void addListener(ProcessListener listener) {
            this.listeners.add(listener);
        }

        /**
         * Start the monitoring thread
         */
        public void startMonitoring() {
            this.monitorThread.start();
        }

        /**
         * Check if in the last run all Processes were running
         */
        public boolean allProcessesRunning() {
            return getRunningProcesses()
                    .containsAll(this.processNames);
        }

        /**
         * Checks the processes periodically and notifies listeners.
         * Runs until it is requested to stop.
         */
        @Override
        public void run() {
            keepRunning = true;
            while (keepRunning) {
                log.debug("[run] monitoring processes");
                final List<String> processes = getRunningProcesses();

                for(String processName : this.processNames) {
                    if (processes.contains(processName)) {
                        log.debug("Process {} found!", processName);
                    } else {
                        log.info("Process {} not found!", processName);
                        for (ProcessListener listener : this.listeners) {
                            listener.onProcessDown(processName);
                        }
                    }
                }
                try {
                    Thread.sleep(MONITOR_PERIOD);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                }
            }
        }

        /**
         * Request the thread to stop. This returns immediately
         * but stopping could take a while.
         */
        public void stopMonitoring() {
            this.keepRunning = false;
            Thread.currentThread().interrupt();
        }

        private List<String> getRunningProcesses() {
            return jProcesses.listProcesses()
                    .stream()
                    .map(ProcessInfo::getCommand)
                    .collect(Collectors.toList());
        }
    }

    /**
     * Find ProcessInfo by Name
     *
     * @param name like "AutoLynx.exe" or "AutoLynx" on *nix.
     * @return list processes that match this name.
     */
    public static Optional<ProcessInfo> findProcessesByName(String name) {
        return JProcesses.get().fastMode().listProcesses()
                .parallelStream()
                .filter(p->p.getCommand().contains(name))
                .findFirst();
    }

    /**
     * Start a process.
     *
     * @param command a list of command names e.g "AutoLynx.exe"
     * @return the started process
     * @throws IOException when a command could not be started
     */
    public static Process startProcess(String... command) throws IOException {
        final ProcessBuilder p = new ProcessBuilder();
        p.command(command);


        // (needs to be checked again)
        // the reason we don't just use inheritIO:
        // https://stackoverflow.com/questions/23753359/processbuilder-inheritio-sending-output-to-the-wrong-place
        final Process process = p.start();
        StreamGobbler inputGobbler = new StreamGobbler(process.getInputStream(), System.out);
        StreamGobbler errorGobbler = new StreamGobbler(process.getErrorStream(), System.err);
        inputGobbler.start();
        errorGobbler.start();

        return process;
    }

    /**
     * On windows sometimes processes block if the input stream are not read
     * therefore we make to sure to pick them up so the process can continue.
     */
    private static class StreamGobbler extends Thread {
        private InputStream in;
        private PrintStream out;

        private StreamGobbler(InputStream in, PrintStream out) {
            this.in = in;
            this.out = out;

            //finish the process when the system stops
            setDaemon(true);
        }

        @Override
        public void run() {
            try {
                BufferedReader input = new BufferedReader(new InputStreamReader(in));
                String line;
                while ((line = input.readLine()) != null)
                    out.println(line);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
