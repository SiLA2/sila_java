package sila_java.library.utils;

import com.github.sarxos.winreg.HKey;
import com.github.sarxos.winreg.RegistryException;
import com.github.sarxos.winreg.WindowsRegistry;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

/**
 * Utilities for Windows specific Registry functions
 */
@Slf4j
public class WinRegistryUtility {
    /**
     * If this is windows then this tool can actually be used otherwise it won't.
     * @return true if on windows.
     */
    public static boolean isWindows() throws RegistryException {
        final String os = System.getProperty("os.name");
        return os.toLowerCase().startsWith("win");
    }

    /**
     * Fixes the registry to avoid non-critical errors
     *
     * @implNote This can only be run in Admin, otherwise a warning is thrown
     */
    public static void fixRegistry() throws RegistryException {
        // prefs annoying error with java installation, where PREF_KEY_NAME is not present and it should
        // {@see: https://stackoverflow.com/questions/16428098/groovy-shell-warning-could-not-open-create-prefs-root-node}
        final String PREF_KEY_LOC_64 = "Software\\WOW6432Node\\JavaSoft";
        final String PREF_KEY_LOC_32 = "Software\\JavaSoft";
        final String PREF_KEY_NAME = "Prefs";
        final String result = searchForKey(HKey.HKLM, PREF_KEY_LOC_32, PREF_KEY_NAME);
        if (result == null) {
            if (isAdmin()) {
                writeKey(HKey.HKLM, PREF_KEY_LOC_32 + "\\" + PREF_KEY_NAME);
                writeKey(HKey.HKLM, PREF_KEY_LOC_64 + "\\" + PREF_KEY_NAME); // just to be sure
            } else {
                log.warn("Java Registry is missing 'Prefs' key. \n"
                        + "This is NOT critical! \n" +
                        "Please execute once as ADMIN and we will create it for you. \n" +
                        "You only need to start as an ADMIN once. \n" +
                        "Proceeding with normal execution..."
                );
            }
        }
    }

    /**
     * Checks if Admin rights are present
     *
     * @implNote Tries to create registries to check if user has admin rights
     */
    private static boolean isAdmin(){
        final Preferences prefs = Preferences.systemRoot();

        try {
            prefs.put("foo", "bar"); // SecurityException on Windows
            prefs.remove("foo");
            prefs.flush(); // BackingStoreException on Linux
            return true;
        } catch (BackingStoreException e) {
            return false;
        }
    }

    /**
     * Return true if on a 64 bit version of windows
     */
    public static boolean is64BitWindows() {
        return System.getenv("ProgramFiles(x86)") != null;
    }

    public static String readStringLM(String path, String name) throws RegistryException {
        return readString(HKey.HKLM, path, name);
    }

    /**
     * Read string from Windows registry
     * @param key the key
     * @param path the path
     * @param name the name
     * @return the read string from Windows registry
     * @throws RegistryException if the string could not be read
     */
    public static String readString(HKey key, String path, String name) throws RegistryException {
        return WindowsRegistry.getInstance().readString(key, path, name);
    }

    /**
     * Write a string to Windows registry
     * @param key the key
     * @param path the pass
     * @param valueName the value name
     * @param value the value
     * @throws RegistryException if the value could not be written
     */
    public void writeString(HKey key, String path, String valueName, String value) throws RegistryException {
        WindowsRegistry.getInstance().writeStringValue(key, path, valueName, value);
    }

    /**
     * Write a key to Windows registry
     * @param hk the H key
     * @param key the key
     * @throws RegistryException if the value could not be written
     */
    public static void writeKey(HKey hk, String key) throws RegistryException {
        WindowsRegistry.getInstance().createKey(hk, key);
    }

    /**
     * Search key at specific path and name
     * @param path the path
     * @param name the name
     * @return the key
     * @throws RegistryException if the key was not found
     */
    public static String searchForKeyLM(String path, String name) throws RegistryException {
        return searchForKey(HKey.HKLM, path, name);
    }

    /**
     * This method searches for a key recursively until it is found or returns null.
     * @param root   the toplevel branch of the registry.
     * @param path  the path to start searching for, it can be an empty string.
     * @param name  the name of the key to search for
     * @return path to the searched key or null.
     * @throws RegistryException if there were problemes reading the registry.
     */
    public static String searchForKey(HKey root, String path, String name) throws RegistryException {
        List<String> keys = WindowsRegistry.getInstance().readStringSubKeys(root, path);
        for (String folder : keys) {
            log.debug("[findFirstRecursive] check: .. {}", folder);
            String newPath = path.length() == 0 ? folder : path + "\\" + folder;
            if (folder.equals(name)) {
                return newPath;
            }
            String found = searchForKey(root, newPath, name);
            if (found != null) {
                return found;
            }
        }
        return null;
    }
}
