package sila_java.library.manager.executor.stream;

/**
 * A simple exception class thrown when we manually cancel the stream call
 */
final class StreamCancellationException extends RuntimeException { }
