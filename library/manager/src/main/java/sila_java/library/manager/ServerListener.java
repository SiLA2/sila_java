package sila_java.library.manager;

import sila_java.library.manager.models.Server;

import java.util.UUID;

/**
 * Listener for 3rd Party Applications
 */
public interface ServerListener {

    /**
     * Server was added to the server manager either manually or through discovery
     * @param uuid The server unique identifier
     */
    default void onServerAdded(UUID uuid, Server server) {}

    /**
     * Server was removed (can only happen manually)
     * @param uuid The server unique identifier
     */
    default void onServerRemoved(UUID uuid, Server server) {}

    /**
     * Server was changed
     * Example:
     * - The status from Online to Offline
     * - The server become Invalid / Valid
     *
     * @implNote The application developer is expected to track Servers by his own
     *
     * @param uuid The server unique identifier
     */
    default void onServerChange(UUID uuid, Server server) {}

    /**
     * Failed to add a server
     * @param host The server host
     * @param port The server port
     * @param reason The reason
     */
    default void onServerAdditionFail(String host, int port, String reason) {}
}