package sila_java.library.manager.models;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

/**
 * Extended SiLA Task model
 */
@Getter
@Setter
public class ExtendedSiLACall extends SiLACall {
    private boolean isGetFCPAffectedByMetadataCall = false;

    public ExtendedSiLACall(final @NonNull SiLACall siLACall) {
        super(siLACall);
    }
}
