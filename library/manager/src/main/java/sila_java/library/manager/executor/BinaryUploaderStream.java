package sila_java.library.manager.executor;

import io.grpc.stub.StreamObserver;
import lombok.Getter;
import lombok.NonNull;
import sila2.org.silastandard.SiLABinaryTransfer;

import java.io.IOException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Binary uploader stream
 */
public class BinaryUploaderStream implements StreamObserver<SiLABinaryTransfer.UploadChunkResponse> {
    @Getter
    private final CompletableFuture<Void> voidCompletableFuture = new CompletableFuture<>();
    private final String binaryTransferUUID;
    private final BinaryUploader binaryUploader;
    private final AtomicInteger nbResponseReceived = new AtomicInteger(0);
    private StreamObserver<SiLABinaryTransfer.UploadChunkRequest> responseObserver;

    /**
     * Constructor
     * @param binaryUploader the binary uploader instance
     * @param binaryTransferUUID the binary transfer UUID
     */
    public BinaryUploaderStream(@NonNull final BinaryUploader binaryUploader, @NonNull final String binaryTransferUUID) {
        this.binaryUploader = binaryUploader;
        this.binaryTransferUUID = binaryTransferUUID;
    }

    /**
     * Start upload by sending a upload request {@link BinaryUploader#getNextChunkUploadRequest(String)}
     * @param responseObserver the response observer
     */
    public void startUpload(@NonNull final StreamObserver<SiLABinaryTransfer.UploadChunkRequest> responseObserver) {
        this.responseObserver = responseObserver;
        try {
            this.responseObserver.onNext(binaryUploader.getNextChunkUploadRequest(this.binaryTransferUUID));
        } catch (IOException e) {
            this.onError(e);
        }
    }

    /**
     * Callback when a get response is received
     *
     * @inheritDoc
     * @param uploadChunkResponse the upload chunk response
     */
    @Override
    public void onNext(final SiLABinaryTransfer.UploadChunkResponse uploadChunkResponse) {
        final int requestSentCount = this.nbResponseReceived.incrementAndGet();
        if (requestSentCount < this.binaryUploader.getChunkCount()) {
            try {
                this.responseObserver.onNext(this.binaryUploader.getNextChunkUploadRequest(this.binaryTransferUUID));
            } catch (IOException e) {
                this.onError(e);
                return;
            }
        }
        if (requestSentCount > this.binaryUploader.getChunkCount()) {
            this.onError(new RuntimeException("Binary Uploader uploaded too many chunks!"));
            return;
        }
        if (requestSentCount == this.binaryUploader.getChunkCount()) {
            this.responseObserver.onCompleted();
            this.voidCompletableFuture.complete(null);
        }
    }

    /**
     * Callback when an error is received
     *
     * @inheritDoc
     * @param throwable the error
     */
    @Override
    public void onError(Throwable throwable) {
        this.voidCompletableFuture.obtrudeException(throwable);
    }

    /**
     * Callback when the stream is closed
     *
     * @inheritDoc
     */
    @Override
    public void onCompleted() {
        if (this.voidCompletableFuture.isDone()) {
            return;
        }
        if (this.nbResponseReceived.get() != binaryUploader.getChunkCount()) {
            this.voidCompletableFuture.obtrudeException(new RuntimeException("Stream completed before uploading all binary chunks"));
        } else {
            this.voidCompletableFuture.complete(null);
        }
    }
}
