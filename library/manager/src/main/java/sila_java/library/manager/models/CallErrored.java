package sila_java.library.manager.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import java.time.OffsetDateTime;

/**
 * Task errored model
 */
@Getter
@AllArgsConstructor
public class CallErrored {
    private final OffsetDateTime startDate;
    private final OffsetDateTime endDate;
    private final String error;
    private final SiLACall siLACall;
}
