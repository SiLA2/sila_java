package sila_java.library.manager.executor;

import com.google.common.util.concurrent.ThreadFactoryBuilder;
import com.google.protobuf.DynamicMessage;
import io.grpc.ManagedChannelBuilder;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.SiLAFramework;
import sila_java.library.core.sila.clients.ChannelBuilder;
import sila_java.library.manager.models.CallCompleted;
import sila_java.library.manager.models.CallErrored;
import sila_java.library.manager.models.CallStarted;
import sila_java.library.manager.models.SiLACall;

import javax.annotation.Nullable;
import java.security.cert.X509Certificate;
import java.util.*;
import java.util.concurrent.*;

/**
 * Server task manager
 *
 * @implNote Supports 32 concurrent tasks
 */
@Slf4j
public class ServerCallManager {
    private static final ExecutorService executor = new ThreadPoolExecutor(
            1,
            32, // todo check if enough
            0L,
            TimeUnit.MILLISECONDS,
            new SynchronousQueue<>(), // don't queue task
            new ThreadFactoryBuilder().setNameFormat("sila-call-exec-%d").setDaemon(true).build()
    );
    private static final CallListener NO_OP_CALL_LISTENER = new CallListener() {};
    private final Map<UUID, RunningServerCall> calls = new ConcurrentHashMap<>();
    private final List<CallListener> callManagerListeners = new CopyOnWriteArrayList<>();

    /**
     * Running server task model
     */
    @AllArgsConstructor
    @Getter
    public static class RunningServerCall {
        private final ExecutableServerCall executableServerCall;
        private final Future<String> future;
    }

    /**
     * Simple {@link CallListener} implementation that forward events to {@link ServerCallManager#callManagerListeners listeners}
     */
    @AllArgsConstructor
    private class EventForwarder implements CallListener {
        private final CallListener baseListener;

        public EventForwarder() {
            this.baseListener = new CallListener() {};
        }

        /**
         * @inheritDoc
         */
        @Override
        public void onStart(CallStarted callInProgress) {
            ServerCallManager.this.callManagerListeners
                    .forEach(l -> l.onStart(callInProgress));
            this.baseListener.onStart(callInProgress);
        }

        /**
         * @inheritDoc
         */
        @Override
        public void onComplete(CallCompleted callCompleted) {
            ServerCallManager.this.callManagerListeners
                    .forEach(l -> l.onComplete(callCompleted));
            this.baseListener.onComplete(callCompleted);
        }

        /**
         * @inheritDoc
         */
        @Override
        public void onError(CallErrored callErrored) {
            ServerCallManager.this.callManagerListeners
                    .forEach(l -> l.onError(callErrored));
            this.baseListener.onError(callErrored);
        }

        /**
         * @inheritDoc
         */
        @Override
        public void onObservablePropertyUpdate(SiLACall baseCall, String value) {
            ServerCallManager.this.callManagerListeners
                    .forEach(l -> l.onObservablePropertyUpdate(baseCall, value));
            this.baseListener.onObservablePropertyUpdate(baseCall, value);
        }

        /**
         * @inheritDoc
         */
        @Override
        public void onObservableCommandInit(SiLACall baseCall, SiLAFramework.CommandConfirmation command) {
            ServerCallManager.this.callManagerListeners
                    .forEach(l -> l.onObservableCommandInit(baseCall, command));
            this.baseListener.onObservableCommandInit(baseCall, command);
        }

        /**
         * @inheritDoc
         */
        @Override
        public void onObservableCommandExecutionInfo(SiLACall baseCall, SiLAFramework.ExecutionInfo executionInfo) {
            ServerCallManager.this.callManagerListeners
                    .forEach(l -> l.onObservableCommandExecutionInfo(baseCall, executionInfo));
            this.baseListener.onObservableCommandExecutionInfo(baseCall, executionInfo);
        }

        /**
         * @inheritDoc
         */
        @Override
        public void onObservableIntermediateResponse(SiLACall baseCall, DynamicMessage response) {
            ServerCallManager.this.callManagerListeners
                    .forEach(l -> l.onObservableIntermediateResponse(baseCall, response));
            this.baseListener.onObservableIntermediateResponse(baseCall, response);
        }
    }

    /**
     * Create a Channel builder {@link ChannelBuilder#withTLSEncryption} using {@link ServerCallManager#executor}
     *
     * @deprecated unencrypted communications are forbidden by the standard use
     *  {@link ServerCallManager#newChannelBuilderWithEncryption(String, int)}
     *  {@link ServerCallManager#newChannelBuilderWithEncryption(String, int, X509Certificate)}
     *
     * @param host the server host
     * @param port the server port
     * @return {@link ManagedChannelBuilder}
     */
    @SneakyThrows
    @Deprecated
    public ManagedChannelBuilder<?> newChannelBuilderWithoutEncryption(@NonNull final String host, final int port) {
        return ChannelBuilder.withoutEncryption(host, port).executor(executor);
    }

    /**
     * Create a Channel builder {@link ChannelBuilder#withTLSEncryption} using {@link ServerCallManager#executor}
     *
     * @param host the server host
     * @param port the server port
     * @return {@link ManagedChannelBuilder}
     */
    @SneakyThrows
    public ManagedChannelBuilder<?> newChannelBuilderWithEncryption(@NonNull final String host, final int port) {
        return ChannelBuilder.withTLSEncryption(host, port).executor(executor);
    }

    /**
     * Create a Channel builder {@link ChannelBuilder#withTLSEncryption} using {@link ServerCallManager#executor}
     *
     * @param host the server host
     * @param port the server port
     * @param certificate the server certificate
     * @return {@link ManagedChannelBuilder}
     */
    @SneakyThrows
    public ManagedChannelBuilder<?> newChannelBuilderWithEncryption(@NonNull final String host, final int port, X509Certificate certificate) {
        return ChannelBuilder.withTLSEncryption(host, port, certificate).executor(executor);
    }

    /**
     * Get running task by UUID
     * @param callUuid the task UUID
     * @return the task
     */
    public Optional<RunningServerCall> getCall(@NonNull final UUID callUuid) {
        return Optional.ofNullable(this.calls.get(callUuid));
    }

    /**
     * Get running server tasks
     * @return the running server tasks
     */
    public Collection<RunningServerCall> getCalls() {
        return this.calls.values();
    }

    /**
     * Run a task asynchronously
     *
     *
     * @param executableServerCall the executable task
     * @param notifyGlobalListener option to notify listeners globally
     * @param callRelativeListener the task listener
     *
     * @return A future gRPC encoded response String
     */
    public Future<String> runAsync(
            @NonNull final ExecutableServerCall executableServerCall,
            boolean notifyGlobalListener,
            @Nullable final CallListener callRelativeListener
    ) {
        final UUID callUuid = executableServerCall.getBaseCall().getIdentifier();
        final CallListener callListener;
        if (notifyGlobalListener) {
            callListener = (callRelativeListener == null) ? new EventForwarder() : new EventForwarder(callRelativeListener);
        } else {
            callListener = (callRelativeListener == null) ? ServerCallManager.NO_OP_CALL_LISTENER : callRelativeListener;
        }
        final Future<String> future = ServerCallManager.executor.submit(() -> {
            try (final ServerCallExecutor callExecutor = new ServerCallExecutor(executableServerCall, callListener)) {
                return callExecutor.execute();
            } finally {
                log.debug("Removing call {} from call manager", callUuid);
                this.calls.remove(callUuid);
            }
        });
        // todo check if not already in manager
        log.debug("New call {} addition into call manager", callUuid);
        this.calls.put(callUuid, new RunningServerCall(executableServerCall, future));
        return future;
    }

    /**
     * Run a task asynchronously with global listeners notification
     *
     * @param executableServerCall the executable task
     * @param callRelativeListener the task listener
     *
     * @return A future gRPC encoded response String
     */
    public Future<String> runAsync(
            @NonNull final ExecutableServerCall executableServerCall,
            @Nullable final CallListener callRelativeListener
    ) {
        return this.runAsync(executableServerCall, true, callRelativeListener);
    }


    /**
     * Run a task asynchronously with global listeners notification
     *
     * @param executableServerCall the executable task
     *
     * @return A future gRPC encoded response String
     */
    public Future<String> runAsync(@NonNull final ExecutableServerCall executableServerCall) {
        return this.runAsync(executableServerCall, null);
    }

    /**
     * Add a listener
     * @param callListener the listener to add
     * @return A runnable to unsubscribe
     */
    public Runnable addListener(@NonNull final CallListener callListener) {
        callManagerListeners.add(callListener);
        return () -> callManagerListeners.remove(callListener);
    }

    /**
     * Remove a listener
     * @param callListener the listener to remove
     */
    public void removeListener(@NonNull final CallListener callListener) {
        callManagerListeners.remove(callListener);
    }
}
