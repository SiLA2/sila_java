package sila_java.library.manager.server_management;

import lombok.NonNull;
import sila_java.library.manager.models.Server;
import javax.annotation.Nullable;

/**
 * Class representing a failed connection attempt to a server
 */
public class ServerConnectionException extends Exception {

    /**
     * Constructor
     * @param server The Server with which the connection failed
     */
    public ServerConnectionException(@NonNull final Server server) {
        super("Unable to connect to remote server: " + server.getHost() + ":" + server.getPort());
    }

    public ServerConnectionException(@NonNull final Server server, @Nullable final Exception exception) {
        super("Unable to connect to remote server: " + server.getHost() + ":" + server.getPort(), exception);
    }

    /**
     * Constructor
     * @param server The Server with which the connection failed
     * @param reason The reason
     */
    public ServerConnectionException(@NonNull final Server server, @NonNull final String reason) {
        super("Unable to connect to remote server: " + server.getHost() + ":" + server.getPort() + " because: " + reason);
    }
}