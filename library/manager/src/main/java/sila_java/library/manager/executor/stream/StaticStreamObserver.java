package sila_java.library.manager.executor.stream;

import com.google.protobuf.DynamicMessage;
import com.google.protobuf.InvalidProtocolBufferException;
import io.grpc.ClientCall;
import lombok.Getter;
import lombok.NonNull;
import sila_java.library.core.sila.mapping.grpc.ProtoMapper;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentLinkedDeque;

/**
 * Stream Observer with the capability of storing result and provide a custom callback for responses
 */
public final class StaticStreamObserver implements io.grpc.stub.StreamObserver<Object> {
    private final ConcurrentLinkedDeque<String> resultsList = new ConcurrentLinkedDeque<>();
    @Getter
    private final CompletableFuture<List<String>> future = new CompletableFuture<>();
    private final ClientCall<Object, Object> clientCall;
    private final StreamCallback callback;
    private final boolean storeResult;

    /**
     * Constructor
     * @param clientCall The call
     * @param callback Callback to be executed on every element of the Stream
     */
    public StaticStreamObserver(
            @NonNull final ClientCall<Object, Object> clientCall,
            @Nullable StreamCallback callback
    ) {
        this(clientCall, callback, true);
    }

    /**
     * Constructor
     * @param clientCall The call
     * @param callback Callback to be executed on every element of the Stream
     * @param storeResult Either or not to store results
     */
    public StaticStreamObserver(
            @NonNull final ClientCall<Object, Object> clientCall,
            @Nullable StreamCallback callback,
            final boolean storeResult
    ) {
        this.clientCall = clientCall;
        this.callback = callback;
        this.storeResult = storeResult;
    }

    /**
     * @inheritDoc
     */
    @Override
    public void onNext(Object message) {
        if (future.isDone()) {
            return;
        }
        final String json;
        try {
            json = ProtoMapper.serializeToJson((DynamicMessage)message);
            if (this.storeResult) {
                this.resultsList.add(json);
            }
            if (this.callback != null && !this.callback.onNext(json)) {
                this.clientCall.cancel(null, new StreamCancellationException());
            }
        } catch (InvalidProtocolBufferException e) {
            this.onError(new IllegalArgumentException(e.getMessage()));
        }
    }

    /**
     * @inheritDoc
     */
    @Override
    public void onError(Throwable throwable) {
        if (future.isDone()) {
            return;
        }
        if (throwable.getCause() instanceof StreamCancellationException) {
            this.onCompleted();
        } else {
            this.future.completeExceptionally(throwable);
        }
    }

    /**
     * @inheritDoc
     */
    @Override
    public void onCompleted() {
        if (future.isDone()) {
            return;
        }
        if (!this.storeResult) {
            this.future.complete(null);
        } else {
            this.future.complete(new ArrayList<>(this.resultsList));
        }
    }
}
