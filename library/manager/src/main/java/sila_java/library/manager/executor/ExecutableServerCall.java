package sila_java.library.manager.executor;

import com.google.protobuf.Descriptors;
import lombok.Getter;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import sila_java.library.manager.ServerManager;
import sila_java.library.manager.models.*;
import sila_java.library.manager.server_management.Connection;

import java.security.KeyException;
import java.time.Duration;
import java.util.Optional;
import java.util.UUID;

/**
 * Executable server call
 */
@Slf4j
@Getter
public class ExecutableServerCall {
    private static final Duration DEFAULT_CALL_TIMEOUT = Duration.ofMillis(Long.MAX_VALUE); // Infinite timeout
    private final Connection connection;
    private final SiLACall baseCall;
    private final Optional<Descriptors.ServiceDescriptor> feature;
    private final Duration timeout;
    private final Optional<BinaryUploader> binaryUploader;
    private final Optional<BinaryDownloader> binaryDownloader;
    private final Optional<UUID> binaryUuid;

    /**
     * Executable server call builder
     */
    @Getter
    public static class Builder {
        private final Connection connection;
        private final SiLACall baseCall;
        private final Optional<Descriptors.ServiceDescriptor> feature;
        private Duration timeout = DEFAULT_CALL_TIMEOUT;
        private Optional<BinaryUploader> binaryUploader = Optional.empty();
        private Optional<BinaryDownloader> binaryDownloader = Optional.empty();
        private Optional<UUID> binaryUuid = Optional.empty();

        // todo allow to update the silacall identifier UUID since it must be unique

        /**
         * Constructor
         * @param connection the connection
         * @param baseCall the base task
         */
        private Builder(@NonNull final Connection connection, @NonNull final SiLACall baseCall) {
            this.connection = connection;
            this.baseCall = baseCall;
            Descriptors.ServiceDescriptor featureService = null;
            // binary download does not have a fully qualified feature identifier
            if (this.baseCall.getType() != SiLACall.Type.DOWNLOAD_BINARY) {
                try {
                    featureService = this.connection.getFeatureService(baseCall.getFullyQualifiedFeatureId());
                } catch (KeyException e) {
                    log.warn("Unable to find feature with {}, call will be performed feature-less", baseCall.getFullyQualifiedFeatureId());
                }
            }
            this.feature = Optional.ofNullable(featureService);
        }

        /**
         * Build a new {@link ExecutableServerCall} based on the current builder configuration
         * @implNote todo the SiLACall identifier UUID should be updated if built twice?
         * @return a new {@link ExecutableServerCall} instance
         */
        public ExecutableServerCall build() {
            return new ExecutableServerCall(this);
        }

        /**
         * Set the timeout when performing a call.
         * Default value is {@link ExecutableServerCall#DEFAULT_CALL_TIMEOUT}
         *
         * @param timeout The timeout to set
         * @return the builder
         */
        public Builder withTimeout(@NonNull final Duration timeout) {
            this.timeout = timeout;
            return this;
        }

        /**
         * Add binary upload support
         * @param binaryUploader the binary uploader
         * @return the builder instance
         */
        public Builder withBinaryUploader(@NonNull final BinaryUploader binaryUploader) {
            this.binaryUploader = Optional.of(binaryUploader);
            return this;
        }

        /**
         * Add binary download support
         * @param binaryDownloader the binary downloader
         * @return the builder instance
         */
        public Builder withBinaryDownloader(@NonNull final BinaryDownloader binaryDownloader) {
            this.binaryDownloader = Optional.of(binaryDownloader);
            return this;
        }

        /**
         * Add binary UUID
         * @param binaryUUID the binary UUID
         * @return the builder instance
         */
        public Builder withBinaryUUID(@NonNull final UUID binaryUUID) {
            this.binaryUuid = Optional.of(binaryUUID);
            return this;
        }
    }

    /**
     * Create a new task builder using the specified connection and base task
     *
     * @param connection the connection
     * @param baseCall the base task
     *
     * @return a new task builder
     */
    public static Builder newBuilder(@NonNull final Connection connection, @NonNull final SiLACall baseCall) {
        return new Builder(connection, baseCall);
    }

    /**
     * Create a new {@link ExecutableServerCall} using the specified {@link SiLACall}
     *
     * @param siLACall the sila task
     * @return a new {@link ExecutableServerCall}
     */
    public static ExecutableServerCall newInstance(@NonNull final SiLACall siLACall) {
        return ExecutableServerCall.newBuilder(siLACall).build();
    }

    /**
     * Create a new {@link ExecutableServerCall.Builder} using the specified {@link SiLACall}
     *
     * @param siLACall the sila task
     * @return a new {@link ExecutableServerCall.Builder}
     */
    public static ExecutableServerCall.Builder newBuilder(@NonNull final SiLACall siLACall) {
        return ExecutableServerCall.newBuilder(
                ServerManager.getInstance().getConnections().get(siLACall.getServerId()),
                siLACall
        );
    }

    /**
     * Builder constructor
     * @param builder the builder
     */
    private ExecutableServerCall(@NonNull final Builder builder) {
        this.connection = builder.connection;
        this.baseCall = builder.baseCall;
        this.feature = builder.feature;
        this.timeout = builder.timeout;
        this.binaryUploader = builder.binaryUploader;
        this.binaryDownloader = builder.binaryDownloader;
        this.binaryUuid = builder.binaryUuid;
        if (timeout.isZero() || timeout.isNegative()) {
            throw new IllegalArgumentException("Timeout must be greater than 0");
        }
    }
}
