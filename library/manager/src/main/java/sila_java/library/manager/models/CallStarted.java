package sila_java.library.manager.models;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.time.Duration;
import java.time.OffsetDateTime;

/**
 * Task started model
 */
@Getter
@AllArgsConstructor
public class CallStarted {
    private final OffsetDateTime startDate;
    private final Duration timeout;
    private final SiLACall siLACall;
}
