package sila_java.library.manager.executor;

import com.google.protobuf.ByteString;
import lombok.Getter;
import lombok.NonNull;
import sila2.org.silastandard.SiLABinaryTransfer;

import java.io.IOException;
import java.io.InputStream;
import java.io.SequenceInputStream;
import java.util.Collections;
import java.util.List;

/**
 * Binary uploader
 */
public class BinaryUploader {
    private static final int MAX_CHUNK_SIZE = 2097152;
    private final InputStream inputStream;
    @Getter
    private final SiLABinaryTransfer.CreateBinaryRequest request;
    @Getter
    private int chunkIndex = 0;

    /**
     * Constructor
     * @param inputStream the input stream to upload
     * @param totalByteSize the total byte size
     * @param parameterIdentifier the parameter identifier
     */
    public BinaryUploader(@NonNull final InputStream inputStream, final long totalByteSize, @NonNull final String parameterIdentifier) {
        this.inputStream = inputStream;
        this.request = getBinaryRequest(totalByteSize, getChunkCount(totalByteSize), parameterIdentifier);
    }

    /**
     * Constructor
     * @param inputStream the list input stream to upload
     * @param totalByteSize the total byte size
     * @param parameterIdentifier the parameter identifier
     */
    public BinaryUploader(@NonNull final List<InputStream> inputStream, final long totalByteSize, @NonNull final String parameterIdentifier) {
        this.inputStream = new SequenceInputStream(Collections.enumeration(inputStream));
        this.request = getBinaryRequest(totalByteSize, getChunkCount(totalByteSize), parameterIdentifier);
    }

    /**
     * Get the next chunk upload request
     * @param binaryTransferUUID the binary transfer UUID
     * @return the next chunk upload request
     */
    public synchronized SiLABinaryTransfer.UploadChunkRequest getNextChunkUploadRequest(@NonNull final String binaryTransferUUID) throws IOException {
        if (this.request.getChunkCount() == chunkIndex - 1) {
            throw new IOException("End of binary upload stream");
        }
        final int chunkSizeModulo = (int) (request.getBinarySize() % MAX_CHUNK_SIZE);
        byte[] bytes = new byte[MAX_CHUNK_SIZE];
        final int lengthToRead = (chunkIndex == (this.request.getChunkCount() - 1) && chunkSizeModulo > 0) ? (chunkSizeModulo) : (MAX_CHUNK_SIZE);
        this.inputStream.read(bytes, 0, lengthToRead);
        final ByteString byteString = ByteString.copyFrom(bytes, 0, lengthToRead);
        final SiLABinaryTransfer.UploadChunkRequest uploadChunkRequest = SiLABinaryTransfer.UploadChunkRequest
                .newBuilder()
                .setPayload(byteString)
                .setChunkIndex(this.chunkIndex)
                .setBinaryTransferUUID(binaryTransferUUID)
                .build();
        ++this.chunkIndex;
        return uploadChunkRequest;
    }

    /**
     * Get the number of chunks
     * @return the number of chunks
     */
    public int getChunkCount() {
        return this.getRequest().getChunkCount();
    }

    /**
     * Get the binary size in bytes
     * @return the binary size in bytes
     */
    public long getBinarySize() {
        return this.getRequest().getBinarySize();
    }

    /**
     * Get the parameter identifier
     * @return the parameter identifier
     */
    public String getParameterIdentifier() {
        return this.getRequest().getParameterIdentifier();
    }

    /**
     * Create a create binary request
     *
     * @param totalByteSize the total size in byte
     * @param chunkCount the number of chunks
     * @param parameterIdentifier the parameter identifier
     *
     * @return a new {@link SiLABinaryTransfer.CreateBinaryRequest} instance
     */
    public static SiLABinaryTransfer.CreateBinaryRequest getBinaryRequest(
            final long totalByteSize, int chunkCount, @NonNull final String parameterIdentifier
    ) {
        return SiLABinaryTransfer.CreateBinaryRequest
                .newBuilder()
                .setBinarySize(totalByteSize)
                .setChunkCount(chunkCount)
                .setParameterIdentifier(parameterIdentifier)
                .build();
    }

    /**
     * Get the number of chunks required for a specific byte size
     * @param totalByteSize the byte size
     *
     * @return the number of chunks required
     */
    public static int getChunkCount(final long totalByteSize) {
        final int chunkSizeModulo = (int) (totalByteSize % MAX_CHUNK_SIZE);
        return (int) (totalByteSize / MAX_CHUNK_SIZE) + ((chunkSizeModulo > 0) ? 1 : 0);
    }
}
