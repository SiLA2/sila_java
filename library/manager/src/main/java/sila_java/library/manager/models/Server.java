package sila_java.library.manager.models;

import com.google.common.net.HostAndPort;
import lombok.Data;
import sila_java.library.core.models.Feature;
import sila_java.library.server_base.config.ServerConfiguration;
import sila_java.library.server_base.identification.ServerInformation;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Server Model for SiLA Servers
 */
@Data
public class Server {
    private final List<Feature> features = new ArrayList<>();
    private Date joined;
    private Status status;
    private NegotiationType negotiationType;
    private ConnectionType connectionType;
    @Nullable
    private String host;
    @Nullable
    private Integer port;
    @Nullable
    private String certificateAuthority;
    // SiLA Service parsed information
    private ServerConfiguration configuration;
    private ServerInformation information;

    public enum Status {
        ONLINE, OFFLINE
    }

    public enum NegotiationType {
        PLAIN_TEXT, TLS
    }

    public enum ConnectionType {
        CLIENT_INITIATED, SERVER_INITIATED
    }

    /**
     * Get {@link HostAndPort}
     * @return {@link HostAndPort}
     */
    public HostAndPort getHostAndPort() {
        return HostAndPort.fromParts(host, port);
    }
}
