package sila_java.library.manager.grpc;

import io.grpc.Context;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Fully Qualified Metadata Context Key modal
 * @param <T> Context Entry type
 */
@Getter
@AllArgsConstructor
public class FullyQualifiedMetadataContextKey<T> {
    private final String fullyQualifiedIdentifier;
    private final Context.Key<T> contextKey;
}
