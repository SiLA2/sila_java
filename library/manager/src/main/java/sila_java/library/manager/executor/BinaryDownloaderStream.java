package sila_java.library.manager.executor;

import io.grpc.stub.StreamObserver;
import lombok.Getter;
import lombok.NonNull;
import sila2.org.silastandard.SiLABinaryTransfer;

import java.io.IOException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Binary downloader stream
 */
public class BinaryDownloaderStream implements StreamObserver<SiLABinaryTransfer.GetChunkResponse> {
    @Getter
    private final CompletableFuture<Void> voidCompletableFuture = new CompletableFuture<>();
    private final BinaryDownloader binaryDownloader;
    private final AtomicInteger nbReceived = new AtomicInteger(0);
    private final SiLABinaryTransfer.GetBinaryInfoResponse binaryInfo;
    private final int chunkCount;
    private StreamObserver<SiLABinaryTransfer.GetChunkRequest> responseObserver;

    /**
     * Constructor
     * @param binaryDownloader the binary downloader instance
     * @param binaryInfo the binary information
     */
    public BinaryDownloaderStream(
            @NonNull final BinaryDownloader binaryDownloader,
            @NonNull final SiLABinaryTransfer.GetBinaryInfoResponse binaryInfo
    ) {
        this.binaryDownloader = binaryDownloader;
        this.binaryInfo = binaryInfo;
        final int chunkSizeModulo = (int) (binaryInfo.getBinarySize() % BinaryDownloader.MAX_CHUNK_SIZE);
        this.chunkCount = (int) (binaryInfo.getBinarySize() / BinaryDownloader.MAX_CHUNK_SIZE) + ((chunkSizeModulo > 0) ? 1 : 0);
    }

    /**
     * Start download by sending a download request {@link BinaryDownloader#getNextChunkDownloadRequest(SiLABinaryTransfer.GetBinaryInfoResponse)}
     * @param responseObserver the response observer
     */
    public void startDownload(StreamObserver<SiLABinaryTransfer.GetChunkRequest> responseObserver) {
        this.responseObserver = responseObserver;
        this.responseObserver.onNext(this.binaryDownloader.getNextChunkDownloadRequest(this.binaryInfo));
    }

    /**
     * Callback when a get response is received
     *
     * @inheritDoc
     * @param getChunkResponse the get chunk download response
     */
    @Override
    public void onNext(final SiLABinaryTransfer.GetChunkResponse getChunkResponse) {
        try {
            this.binaryDownloader.writeChunk(getChunkResponse);
        } catch (IOException e) {
            this.onError(e);
            return;
        }
        final int chunkReceived = this.nbReceived.incrementAndGet();
        if (chunkReceived > this.chunkCount) {
            this.onError(new RuntimeException("Binary downloader downloaded too many chunks!"));
            return;
        }
        if (chunkReceived == this.chunkCount) {
            try {
                this.binaryDownloader.isValidAndCompleteOrThrow();
            } catch (Exception e) {
                this.onError(e);
                return;
            }
            this.responseObserver.onCompleted();
            this.voidCompletableFuture.complete(null);
            return;
        }
        this.responseObserver.onNext(binaryDownloader.getNextChunkDownloadRequest(this.binaryInfo));
    }

    /**
     * Callback when an error is received
     *
     * @inheritDoc
     * @param throwable the error
     */
    @Override
    public void onError(Throwable throwable) {
        this.voidCompletableFuture.obtrudeException(throwable);
    }

    /**
     * Callback when the stream is closed
     *
     * @inheritDoc
     */
    @Override
    public void onCompleted() {
        if (this.voidCompletableFuture.isDone()) {
            return;
        }
        if (this.nbReceived.get() != this.chunkCount) {
            this.voidCompletableFuture.obtrudeException(new RuntimeException("Stream completed before downloading all binary chunks"));
        } else {
            this.voidCompletableFuture.complete(null);
        }
    }
}
