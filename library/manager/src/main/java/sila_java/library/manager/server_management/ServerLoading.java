package sila_java.library.manager.server_management;

import io.grpc.ClientInterceptor;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.StatusRuntimeException;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.SiLAFramework;
import sila2.org.silastandard.core.silaservice.v1.SiLAServiceGrpc;
import sila2.org.silastandard.core.silaservice.v1.SiLAServiceOuterClass;
import sila_java.library.core.models.Feature;
import sila_java.library.core.utils.Utils;
import sila_java.library.manager.ServerManager;
import sila_java.library.manager.executor.ServerCallManager;
import sila_java.library.manager.models.Server;
import sila_java.library.server_base.config.ServerConfiguration;
import sila_java.library.server_base.identification.ServerInformation;

import javax.annotation.Nullable;
import java.io.IOException;
import java.security.cert.X509Certificate;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import static sila_java.library.core.sila.mapping.feature.FeatureGenerator.generateFeature;

/**
 * Static Utilities for SiLA Server Loading
 */
@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public abstract class ServerLoading {
    private final static long MAX_SERVICE_TIMEOUT = 5; // [s]

    /**
     * Simple Exception extension representing a server loading error
     */
    public static class ServerLoadingException extends Exception {
        /**
         * @inheritDoc
         */
        ServerLoadingException(final String errorMessage) {
            super(errorMessage);
        }
    }

    /**
     * Load SiLA Server from SiLA Service
     *
     * @param server Server to provide SiLA Server information
     * @param managedChannel Connection to call the SiLA Server
     *
     * @implNote Assumes a new server created outside of this context.
     */
    public static void loadServer(
            @NonNull final Server server,
            @NonNull final ManagedChannel managedChannel
    ) throws ServerLoadingException {
        final SiLAServiceGrpc.SiLAServiceBlockingStub siLAService = SiLAServiceGrpc.newBlockingStub(managedChannel);
        try {
            // Load Server Info
            final UUID serverUUID = getServerId(siLAService);
            log.info("Got serverUUID: {}", serverUUID);
            final String serverName = getServerName(siLAService);
            final String serverType = getServerType(siLAService);
            final String serverDescription = getServerDescription(siLAService, serverName);
            final String serverVendorURL = getServerVendor(siLAService);
            final String serverVersion = getServerVersion(siLAService);

            server.setConfiguration(new ServerConfiguration(serverName, serverUUID));
            server.setInformation(new ServerInformation(serverType, serverDescription, serverVendorURL, serverVersion));
            log.info("{} Saved Server Information", serverName);

            // Get FeatureList
            final SiLAServiceOuterClass.Get_ImplementedFeatures_Responses featureList =
                    getFeatureList(siLAService, serverName);

            // Compile single Features
            loadFeatures(siLAService, server, featureList);
        } catch (StatusRuntimeException e) {
            final String errorMessage = String.format(
                    "SiLA Service doesn't return because %s. Timeout was set to %d s",
                    e.getMessage(),
                    MAX_SERVICE_TIMEOUT
            );
            throw new ServerLoadingException(errorMessage);
        } catch (final IOException e) {
            throw new ServerLoadingException(e.getMessage());
        }
    }

    /**
     * Attempt to connect to a server by trying to retrieve his UUID and return the channel used
     * @param server The server to try to connect to
     * @throws ServerConnectionException if unable to retrieve the UUID of the server
     * @return The ManagedChannel with which the connection was successful
     */
    public static ManagedChannel attemptConnectionWithServer(
            @NonNull final Server server,
            @NonNull final Optional<ClientInterceptor> clientInterceptor,
            final boolean allowInsecure,
            @Nullable X509Certificate certificate
            ) throws ServerConnectionException {
        // Attempt to Establish encrypted Connection
        final ServerCallManager serverCallManager = ServerManager.getInstance().getServerCallManager();
        ManagedChannelBuilder<?> builder = (certificate != null) ?
                serverCallManager.newChannelBuilderWithEncryption(server.getHost(), server.getPort(), certificate) :
                serverCallManager.newChannelBuilderWithEncryption(server.getHost(), server.getPort());
        clientInterceptor.ifPresent(builder::intercept);
        ManagedChannel encryptedChannel = builder.build();
        Exception exception = null;
        try {
            final UUID serverId = getServerId(SiLAServiceGrpc.newBlockingStub(encryptedChannel));
            server.setNegotiationType(Server.NegotiationType.TLS);
            log.info("Connection to remote server {} using TLS successful", serverId);
            return encryptedChannel;
        } catch (final Exception e) {
            try {
                encryptedChannel.shutdown();
            } catch (final Exception e1) {
                log.warn("Unable to shutdown managed channel!", e1);
            }

            // Note: a race condition is in principle unavoidable that when the server goes online just after,
            // a plain text channel is created
            exception = e;
            log.info("Unable to load server at [{}:{}] through TLS.", server.getHost(), server.getPort());
        }

        if (!allowInsecure) {
            throw new ServerConnectionException(server, exception);
        }

        // Attempt to Establish plain-text Connection
        builder = serverCallManager.newChannelBuilderWithoutEncryption(server.getHost(), server.getPort());
        clientInterceptor.ifPresent(builder::intercept);
        ManagedChannel plainTextChannel = builder.build();

        try {
            log.warn("Deprecated usage of insecure connection with remote server!");
            final UUID serverId = getServerId(SiLAServiceGrpc.newBlockingStub(plainTextChannel));
            server.setNegotiationType(Server.NegotiationType.PLAIN_TEXT);
            log.info("Connection to remote server {} using plain-text successful", serverId);
            return (plainTextChannel);
        } catch (final Exception e) {
            try {
                plainTextChannel.shutdown();
            } catch (final Exception e1) {
                log.warn("Unable to shutdown managed channel!", e);
            }
            log.warn("Unable to load server at [{}:{}]  through plain-text.", server.getHost(), server.getPort());
            throw new ServerConnectionException(server, e);
        }
    }

    /**
     * Get Unique Identifier from SiLA Server
     * @param siLAService SiLA Service Blocking Stub
     * @return Unique Identifier
     */
    public static UUID getServerId(@NonNull final SiLAServiceGrpc.SiLAServiceBlockingStub siLAService) {
        final String serverUUIDStr = siLAService
                .withDeadlineAfter(MAX_SERVICE_TIMEOUT, TimeUnit.SECONDS)
                .getServerUUID(SiLAServiceOuterClass.Get_ServerUUID_Parameters.newBuilder().build())
                .getServerUUID()
                .getValue();
        return UUID.fromString(serverUUIDStr);
    }

    /**
     * Gets the configurable Server Name from SiLA Server
     * @param siLAService SiLA Service Blocking Stub
     * @return Configurable Server Name
     */
    public static String getServerName(@NonNull final SiLAServiceGrpc.SiLAServiceBlockingStub siLAService) {
        final String serverName = siLAService
                .withDeadlineAfter(MAX_SERVICE_TIMEOUT, TimeUnit.SECONDS)
                .getServerName(SiLAServiceOuterClass.Get_ServerName_Parameters.newBuilder().build())
                .getServerName()
                .getValue();
        log.debug("Got serverName: {}", serverName);
        return serverName;
    }

    /**
     * Request the server features
     * @param siLAService the SiLA Service
     * @param serverName the server name
     * @return the {@link SiLAServiceOuterClass.Get_ImplementedFeatures_Responses}
     */
    private static SiLAServiceOuterClass.Get_ImplementedFeatures_Responses getFeatureList(
            @NonNull final SiLAServiceGrpc.SiLAServiceBlockingStub siLAService,
            @NonNull final String serverName
    ) {
        final long start = System.currentTimeMillis();
        final SiLAServiceOuterClass.Get_ImplementedFeatures_Responses featureList = siLAService
                .withDeadlineAfter(MAX_SERVICE_TIMEOUT, TimeUnit.SECONDS)
                .getImplementedFeatures(
                        SiLAServiceOuterClass.Get_ImplementedFeatures_Parameters
                                .newBuilder()
                                .build()
                );
        log.debug("{} Got List of Features in {} ms", serverName, (System.currentTimeMillis() - start));
        return featureList;
    }

    /**
     * Request the server version
     * @param siLAService the SiLA Service
     * @return the server version
     */
    private static String getServerVersion(@NonNull final SiLAServiceGrpc.SiLAServiceBlockingStub siLAService) {
        final String serverVersion = siLAService
                .withDeadlineAfter(MAX_SERVICE_TIMEOUT, TimeUnit.SECONDS)
                .getServerVersion(SiLAServiceOuterClass.Get_ServerVersion_Parameters.newBuilder().build())
                .getServerVersion()
                .getValue();
        log.debug("Got serverVersion: {}", serverVersion);
        return serverVersion;
    }

    /**
     * Request the server vendor URL
     * @param siLAService the SiLA Service
     * @return the server vendor URL
     */
    private static String getServerVendor(@NonNull final SiLAServiceGrpc.SiLAServiceBlockingStub siLAService) {
        final String serverVendorURL = siLAService
                .withDeadlineAfter(MAX_SERVICE_TIMEOUT, TimeUnit.SECONDS)
                .getServerVendorURL(SiLAServiceOuterClass.Get_ServerVendorURL_Parameters.newBuilder().build())
                .getServerVendorURL()
                .getValue();
        log.debug("Got serverVendorURL: {}", serverVendorURL);
        return serverVendorURL;
    }

    /**
     * Request the server description
     * @param siLAService the SiLA Service
     * @return the server description
     */
    private static String getServerDescription(
            @NonNull final SiLAServiceGrpc.SiLAServiceBlockingStub siLAService,
            @NonNull final String serverName
    ) {
        final String serverDescription = siLAService
                .withDeadlineAfter(MAX_SERVICE_TIMEOUT, TimeUnit.SECONDS)
                .getServerDescription(SiLAServiceOuterClass.Get_ServerDescription_Parameters.newBuilder().build())
                .getServerDescription()
                .getValue();
        log.debug("{} Got serverDescription: {}", serverName, serverDescription);
        return serverDescription;
    }

    /**
     * Request the server type
     * @param siLAService the SiLA Service
     * @return the server type
     */
    private static String getServerType(@NonNull final SiLAServiceGrpc.SiLAServiceBlockingStub siLAService) {
        final String serverType = siLAService
                .withDeadlineAfter(MAX_SERVICE_TIMEOUT, TimeUnit.SECONDS)
                .getServerType(SiLAServiceOuterClass.Get_ServerType_Parameters.newBuilder().build())
                .getServerType()
                .getValue();
        log.debug("Got serverType: {}", serverType);
        return serverType;
    }

    /**
     * Loading the Features into the SiLA Server Model
     *
     * @param siLAService Stub to retrieve the feature definitions
     * @param server SiLA Server model
     * @param featureList Feature List retrieved from SiLA Server
     * @throws IOException If unable to parse a feature definition
     */
    private static void loadFeatures(
            @NonNull final SiLAServiceGrpc.SiLAServiceBlockingStub siLAService,
            @NonNull final Server server,
            @NonNull final SiLAServiceOuterClass.Get_ImplementedFeatures_Responses featureList
    ) throws IOException {
        // clear previous features if reloading
        server.getFeatures().clear();
        for (final SiLAFramework.String featureIdentifier : featureList.getImplementedFeaturesList()) {
            final SiLAServiceOuterClass.GetFeatureDefinition_Parameters par =
                    SiLAServiceOuterClass.GetFeatureDefinition_Parameters.newBuilder()
                            .setFeatureIdentifier(featureIdentifier)
                            .build();

            final SiLAServiceOuterClass.GetFeatureDefinition_Responses featureDefinition = siLAService
                    .withDeadlineAfter(MAX_SERVICE_TIMEOUT, TimeUnit.SECONDS)
                    .getFeatureDefinition(par);

            final String rawFeatureDefinition = Utils.cleanupXMLString(
                    featureDefinition.getFeatureDefinition().getValue()
            );
            // Deserialize into feature
            final Feature featurePojo;
            try {
                featurePojo = generateFeature(rawFeatureDefinition);
            } catch (final IOException e) {
                throw new IOException(
                        String.format("Parsing of Feature %s failed. Reason=%s", featureIdentifier, e.getMessage())
                );
            }
            log.debug("Feature {}=", featureIdentifier);
            log.debug("{} ", featurePojo);
            server.getFeatures().add(featurePojo);
        }
    }
}
