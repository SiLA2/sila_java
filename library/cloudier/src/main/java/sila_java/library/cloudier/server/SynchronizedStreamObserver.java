package sila_java.library.cloudier.server;

import io.grpc.stub.StreamObserver;

public class SynchronizedStreamObserver<T> implements StreamObserver<T> {
    private final StreamObserver<T> streamObserver;

    public SynchronizedStreamObserver(StreamObserver<T> streamObserver) {
        this.streamObserver = streamObserver;
    }

    @Override
    public synchronized void onNext(T silaServerMessage) {
        this.streamObserver.onNext(silaServerMessage);
    }

    @Override
    public synchronized void onError(Throwable throwable) {
        this.streamObserver.onError(throwable);
    }

    @Override
    public synchronized void onCompleted() {
        this.streamObserver.onCompleted();
    }
}
