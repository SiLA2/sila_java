package sila_java.library.cloudier.server;

import sila2.org.silastandard.SiLACloudConnector;

public interface IUnobservableCommandMessageHandler {
    void onCommandExec(CloudierRequest<SiLACloudConnector.UnobservableCommandExecution> request);
}
