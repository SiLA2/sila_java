package sila_java.library.cloudier.server;

import sila2.org.silastandard.SiLACloudConnector;

public interface IMetadataMessageHandler {
    void onGetAffectedByMetadata(CloudierRequest<SiLACloudConnector.GetFCPAffectedByMetadataRequest> request);
}
