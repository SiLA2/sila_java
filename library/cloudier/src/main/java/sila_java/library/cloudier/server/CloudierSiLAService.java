package sila_java.library.cloudier.server;

import io.grpc.stub.StreamObserver;
import lombok.AllArgsConstructor;
import lombok.Getter;
import sila2.org.silastandard.core.silaservice.v1.SiLAServiceGrpc;
import sila2.org.silastandard.core.silaservice.v1.SiLAServiceOuterClass;
import sila_java.library.core.sila.types.SiLAString;

import java.util.Map;
import java.util.stream.Collectors;

@Getter
@AllArgsConstructor
public class CloudierSiLAService extends SiLAServiceGrpc.SiLAServiceImplBase {
    private final String name;
    private final String Type;
    private final String UUID;
    private final String Version;
    private final String Description;
    private final String URL;
    private final Map<String, String> features;

    @Override
    public void getFeatureDefinition(SiLAServiceOuterClass.GetFeatureDefinition_Parameters request, StreamObserver<SiLAServiceOuterClass.GetFeatureDefinition_Responses> responseObserver) {
        final String featureId = request.getFeatureIdentifier().getValue();
        String feature = this.features.get(featureId);
        final SiLAServiceOuterClass.GetFeatureDefinition_Responses.Builder builder = SiLAServiceOuterClass.GetFeatureDefinition_Responses.newBuilder();
        if (feature != null) {
            builder.setFeatureDefinition(SiLAString.from(feature));
        }
        responseObserver.onNext(builder.build());
        responseObserver.onCompleted();
    }

    @Override
    public void setServerName(SiLAServiceOuterClass.SetServerName_Parameters request, StreamObserver<SiLAServiceOuterClass.SetServerName_Responses> responseObserver) {
        responseObserver.onNext(SiLAServiceOuterClass.SetServerName_Responses.newBuilder().build());
        responseObserver.onCompleted();
    }

    @Override
    public void getServerName(SiLAServiceOuterClass.Get_ServerName_Parameters request, StreamObserver<SiLAServiceOuterClass.Get_ServerName_Responses> responseObserver) {
        responseObserver.onNext(
                SiLAServiceOuterClass.Get_ServerName_Responses
                        .newBuilder()
                        .setServerName(SiLAString.from(this.getName()))
                        .build()
        );
        responseObserver.onCompleted();
    }

    @Override
    public void getServerType(SiLAServiceOuterClass.Get_ServerType_Parameters request, StreamObserver<SiLAServiceOuterClass.Get_ServerType_Responses> responseObserver) {
        responseObserver.onNext(
                SiLAServiceOuterClass.Get_ServerType_Responses
                        .newBuilder()
                        .setServerType(SiLAString.from(this.getType()))
                        .build()
        );
        responseObserver.onCompleted();
    }

    @Override
    public void getServerUUID(SiLAServiceOuterClass.Get_ServerUUID_Parameters request, StreamObserver<SiLAServiceOuterClass.Get_ServerUUID_Responses> responseObserver) {
        responseObserver.onNext(
                SiLAServiceOuterClass.Get_ServerUUID_Responses
                        .newBuilder()
                        .setServerUUID(SiLAString.from(this.getUUID()))
                        .build()
        );
        responseObserver.onCompleted();
    }

    @Override
    public void getServerDescription(SiLAServiceOuterClass.Get_ServerDescription_Parameters request, StreamObserver<SiLAServiceOuterClass.Get_ServerDescription_Responses> responseObserver) {
        responseObserver.onNext(
                SiLAServiceOuterClass.Get_ServerDescription_Responses
                        .newBuilder()
                        .setServerDescription(SiLAString.from(this.getDescription()))
                        .build()
        );
        responseObserver.onCompleted();
    }

    @Override
    public void getServerVersion(SiLAServiceOuterClass.Get_ServerVersion_Parameters request, StreamObserver<SiLAServiceOuterClass.Get_ServerVersion_Responses> responseObserver) {
        responseObserver.onNext(
                SiLAServiceOuterClass.Get_ServerVersion_Responses
                        .newBuilder()
                        .setServerVersion(SiLAString.from(this.getVersion()))
                        .build()
        );
        responseObserver.onCompleted();
    }

    @Override
    public void getServerVendorURL(SiLAServiceOuterClass.Get_ServerVendorURL_Parameters request, StreamObserver<SiLAServiceOuterClass.Get_ServerVendorURL_Responses> responseObserver) {
        responseObserver.onNext(
                SiLAServiceOuterClass.Get_ServerVendorURL_Responses
                        .newBuilder()
                        .setServerVendorURL(SiLAString.from(this.getURL()))
                        .build()
        );
        responseObserver.onCompleted();
    }

    @Override
    public void getImplementedFeatures(SiLAServiceOuterClass.Get_ImplementedFeatures_Parameters request, StreamObserver<SiLAServiceOuterClass.Get_ImplementedFeatures_Responses> responseObserver) {
        responseObserver.onNext(SiLAServiceOuterClass.Get_ImplementedFeatures_Responses
                .newBuilder()
                        .addAllImplementedFeatures(
                                this.features.keySet().stream().map(SiLAString::from).collect(Collectors.toList())
                        )
                .build()
        );
        responseObserver.onCompleted();
    }
}
