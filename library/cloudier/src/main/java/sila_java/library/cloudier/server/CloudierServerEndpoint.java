package sila_java.library.cloudier.server;

import com.google.protobuf.*;
import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.BinaryDownloadGrpc;
import sila2.org.silastandard.BinaryUploadGrpc;
import sila2.org.silastandard.CloudClientEndpointGrpc;
import sila2.org.silastandard.SiLACloudConnector;
import sila_java.library.cloudier.server.impl.*;

import javax.annotation.Nullable;
import java.util.EnumMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.function.Consumer;

@Slf4j
public class CloudierServerEndpoint implements StreamObserver<SiLACloudConnector.SiLAClientMessage> {
    private final CloudClientEndpointGrpc.CloudClientEndpointStub clientEndpoint;
    private final StreamObserver<SiLACloudConnector.SiLAServerMessage> streamObserver;
    private final CloudierSiLAService cloudierSiLAService;
    private final CloudierConnectionConfigurationService cloudierConnectionConfigurationService;
    private final IMetadataMessageHandler metadataHandler;
    private final IBinaryMessageHandler binaryHandler;
    private final IUnobservableCommandMessageHandler unobservableCommandHandler;
    private final IObservableCommandMessageHandler observableCommandHandler;
    private final IUnobservablePropertyMessageHandler unobservablePropertyHandler;
    private final IObservablePropertyMessageHandler observablePropertyHandler;
    private final CallMessageMap callMessageMap;
    private final EnumMap<SiLACloudConnector.SiLAClientMessage.MessageCase, Consumer<SiLACloudConnector.SiLAClientMessage>> callback =
            new EnumMap<>(SiLACloudConnector.SiLAClientMessage.MessageCase.class);

    public interface CloudCallHandler<ParameterType extends GeneratedMessageV3, ResponseType> {
        void handle(ParameterType parameter, StreamObserver<ResponseType> streamObserver);
    }

    public interface AsyncCloudCallHandler<ParameterType extends GeneratedMessageV3, ResponseType> {
        StreamObserver<ParameterType> handle(StreamObserver<ResponseType> streamObserver);
    }

    public CloudierServerEndpoint(
            CloudierSiLAService cloudierSiLAService,
            CloudierConnectionConfigurationService cloudierConnectionConfigurationService,
            CloudClientEndpointGrpc.CloudClientEndpointStub clientEndpoint,
            Map<String, MessageCaseHandler> callForwarderMap
    ) {
        this(cloudierSiLAService, cloudierConnectionConfigurationService, clientEndpoint, callForwarderMap, null, null);
    }
    public CloudierServerEndpoint(
            CloudierSiLAService cloudierSiLAService,
            CloudierConnectionConfigurationService cloudierConnectionConfigurationService,
            CloudClientEndpointGrpc.CloudClientEndpointStub clientEndpoint,
            Map<String, MessageCaseHandler> callForwarderMap,
            @Nullable final BinaryUploadGrpc.BinaryUploadImplBase uploadImplBase,
            @Nullable final BinaryDownloadGrpc.BinaryDownloadImplBase downloadImplBase
    ) {
        this.cloudierConnectionConfigurationService = cloudierConnectionConfigurationService;
        this.cloudierSiLAService = cloudierSiLAService;
        this.clientEndpoint = clientEndpoint;
        this.callMessageMap = new CallMessageMap(cloudierSiLAService, cloudierConnectionConfigurationService, callForwarderMap, uploadImplBase, downloadImplBase);
        this.streamObserver = new SynchronizedStreamObserver<>(clientEndpoint.connectSiLAServer(this));
        this.metadataHandler = new MetadataMessageHandler(this.streamObserver, this.callMessageMap);
        this.binaryHandler = new BinaryMessageHandler(this.streamObserver, this.callMessageMap);
        this.unobservableCommandHandler = new UnobservableCommandMessageHandler(this.streamObserver, this.callMessageMap);
        this.observableCommandHandler = new ObservableCommandMessageHandler(this.streamObserver, this.callMessageMap);
        this.unobservablePropertyHandler = new UnobservablePropertyMessageHandler(this.streamObserver, this.callMessageMap);
        this.observablePropertyHandler = new ObservablePropertyMessageHandler(this.streamObserver, this.callMessageMap);
        this.callback.put(
                SiLACloudConnector.SiLAClientMessage.MessageCase.OBSERVABLECOMMANDINITIATION,
                (message) -> this.observableCommandHandler.onCommandInit(new CloudierRequest<>(message.getRequestUUID(), message.getObservableCommandInitiation()))
        );
        this.callback.put(
                SiLACloudConnector.SiLAClientMessage.MessageCase.OBSERVABLECOMMANDINTERMEDIATERESPONSESUBSCRIPTION,
                (message) -> this.observableCommandHandler.onIntermediate(new CloudierRequest<>(message.getRequestUUID(), message.getObservableCommandIntermediateResponseSubscription()))
        );
        this.callback.put(
                SiLACloudConnector.SiLAClientMessage.MessageCase.OBSERVABLECOMMANDEXECUTIONINFOSUBSCRIPTION,
                (message) -> this.observableCommandHandler.onCommandExecInfo(new CloudierRequest<>(message.getRequestUUID(), message.getObservableCommandExecutionInfoSubscription()))
        );
        this.callback.put(
                SiLACloudConnector.SiLAClientMessage.MessageCase.OBSERVABLECOMMANDGETRESPONSE,
                (message) -> this.observableCommandHandler.onResult(new CloudierRequest<>(message.getRequestUUID(), message.getObservableCommandGetResponse()))
        );
        this.callback.put(
                SiLACloudConnector.SiLAClientMessage.MessageCase.UNOBSERVABLECOMMANDEXECUTION,
                (message) -> this.unobservableCommandHandler.onCommandExec(new CloudierRequest<>(message.getRequestUUID(), message.getUnobservableCommandExecution()))
        );
        this.callback.put(
                SiLACloudConnector.SiLAClientMessage.MessageCase.UNOBSERVABLEPROPERTYREAD,
                (message) -> this.unobservablePropertyHandler.onUnobservableProperty(new CloudierRequest<>(message.getRequestUUID(), message.getUnobservablePropertyRead()))
        );
        this.callback.put(
                SiLACloudConnector.SiLAClientMessage.MessageCase.OBSERVABLEPROPERTYSUBSCRIPTION,
                (message) -> this.observablePropertyHandler.onObservableProperty(new CloudierRequest<>(message.getRequestUUID(), message.getObservablePropertySubscription()))
        );
        this.callback.put(
                SiLACloudConnector.SiLAClientMessage.MessageCase.CANCELOBSERVABLEPROPERTYSUBSCRIPTION,
                (message) -> this.observablePropertyHandler.onCancelObservableProperty(new CloudierRequest<>(message.getRequestUUID(), message.getCancelObservablePropertySubscription()))
        );
        this.callback.put(
                SiLACloudConnector.SiLAClientMessage.MessageCase.METADATAREQUEST,
                (message) -> this.metadataHandler.onGetAffectedByMetadata(new CloudierRequest<>(message.getRequestUUID(), message.getMetadataRequest()))
        );
        this.callback.put(
                SiLACloudConnector.SiLAClientMessage.MessageCase.CANCELOBSERVABLECOMMANDEXECUTIONINFOSUBSCRIPTION,
                (message) -> this.observableCommandHandler.onCancelExecInfo(new CloudierRequest<>(message.getRequestUUID(), message.getCancelObservableCommandExecutionInfoSubscription()))
        );
        this.callback.put(
                SiLACloudConnector.SiLAClientMessage.MessageCase.CANCELOBSERVABLECOMMANDINTERMEDIATERESPONSESUBSCRIPTION,
                (message) -> this.observableCommandHandler.onCancelIntermediate(new CloudierRequest<>(message.getRequestUUID(), message.getCancelObservableCommandIntermediateResponseSubscription()))
        );
        this.callback.put(
                SiLACloudConnector.SiLAClientMessage.MessageCase.CREATEBINARYUPLOADREQUEST,
                (message) -> this.binaryHandler.onCreateBinaryUpload(new CloudierRequest<>(message.getRequestUUID(), message.getCreateBinaryUploadRequest()))
        );
        this.callback.put(
                SiLACloudConnector.SiLAClientMessage.MessageCase.DELETEDOWNLOADEDBINARYREQUEST,
                (message) -> this.binaryHandler.onDeleteBinaryDownload(new CloudierRequest<>(message.getRequestUUID(), message.getDeleteDownloadedBinaryRequest()))
        );
        this.callback.put(
                SiLACloudConnector.SiLAClientMessage.MessageCase.DELETEUPLOADEDBINARYREQUEST,
                (message) -> this.binaryHandler.onDeleteBinaryUpload(new CloudierRequest<>(message.getRequestUUID(), message.getDeleteUploadedBinaryRequest()))
        );
        this.callback.put(
                SiLACloudConnector.SiLAClientMessage.MessageCase.UPLOADCHUNKREQUEST,
                (message) -> this.binaryHandler.onUploadChunk(new CloudierRequest<>(message.getRequestUUID(), message.getUploadChunkRequest()))
        );
        this.callback.put(
                SiLACloudConnector.SiLAClientMessage.MessageCase.GETBINARYINFOREQUEST,
                (message) -> this.binaryHandler.onBinaryInfo(new CloudierRequest<>(message.getRequestUUID(), message.getGetBinaryInfoRequest()))
        );
        this.callback.put(
                SiLACloudConnector.SiLAClientMessage.MessageCase.GETCHUNKREQUEST,
                (message) -> this.binaryHandler.onGetChunk(new CloudierRequest<>(message.getRequestUUID(), message.getGetChunkRequest()))
        );
    }

    @Override
    public void onNext(SiLACloudConnector.SiLAClientMessage silaClientMessage) {
        CompletableFuture.runAsync(() -> {
            log.warn("new message received " + silaClientMessage.getMessageCase().name());
            // todo throw property / command error if not defined
            final Consumer<SiLACloudConnector.SiLAClientMessage> callback = this.callback.get(silaClientMessage.getMessageCase());
            if (callback != null) {
                callback.accept(silaClientMessage);
            } else {
                log.info("Unhandled message " + silaClientMessage.getMessageCase().name());
            }
        });
    }

    @Override
    public void onError(Throwable throwable) {
        // todo attempt to reconnect
        log.warn("received error", throwable);
    }

    @Override
    public void onCompleted() {
        // todo attempt to reconnect
        log.warn("call ended");
    }
}
