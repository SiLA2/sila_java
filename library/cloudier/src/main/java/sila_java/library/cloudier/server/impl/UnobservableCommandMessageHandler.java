package sila_java.library.cloudier.server.impl;

import com.google.protobuf.GeneratedMessageV3;
import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.SiLACloudConnector;
import sila_java.library.cloudier.server.CallMessageMap;
import sila_java.library.cloudier.server.CloudCallForwarder;
import sila_java.library.cloudier.server.CloudierRequest;
import sila_java.library.cloudier.server.IUnobservableCommandMessageHandler;

import java.util.Optional;

@Slf4j
public class UnobservableCommandMessageHandler implements IUnobservableCommandMessageHandler {
    private final StreamObserver<SiLACloudConnector.SiLAServerMessage> response;
    private final CallMessageMap callMessageMap;
    private final CommandMessageHandler commandMessageHandler;

    public UnobservableCommandMessageHandler(StreamObserver<SiLACloudConnector.SiLAServerMessage> response, CallMessageMap callMessageMap) {
        this.response = response;
        this.callMessageMap = callMessageMap;
        this.commandMessageHandler = new CommandMessageHandler(response);
    }

    @Override
    public void onCommandExec(CloudierRequest<SiLACloudConnector.UnobservableCommandExecution> request) {
        final String fqi = request.getRequest().getFullyQualifiedCommandId();
        final Optional<CloudCallForwarder<GeneratedMessageV3, GeneratedMessageV3>> callHandler =
                this.callMessageMap.getCallHandler(fqi, SiLACloudConnector.SiLAClientMessage.MessageCase.UNOBSERVABLECOMMANDEXECUTION);
        if (callHandler.isPresent()) {
            log.info("Forwarding call for " + fqi);
            callHandler.get().forward(
                    request.getRequestUUID(),
                    request.getRequest().getCommandParameter().getMetadataList(),
                    request.getRequest().getCommandParameter().getParameters(),
                    (message) -> {
                        response.onNext(SiLACloudConnector.SiLAServerMessage
                                .newBuilder()
                                .setRequestUUID(request.getRequestUUID())
                                .setUnobservableCommandResponse(
                                        SiLACloudConnector.UnobservableCommandResponse
                                                .newBuilder()
                                                .setResponse(message.toByteString())
                                                .build()
                                )
                                .build()
                        );
                    }, (throwable -> commandMessageHandler.sendThrowableError(request.getRequestUUID(), throwable)));
        } else {
            commandMessageHandler.sendUnknownCommandError(request.getRequestUUID(), fqi);
        }
    }
}
