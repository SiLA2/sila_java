package sila_java.library.cloudier.server;

import sila2.org.silastandard.SiLACloudConnector;

public interface IObservablePropertyMessageHandler {
    void onObservableProperty(CloudierRequest<SiLACloudConnector.ObservablePropertySubscription> request);
    void onCancelObservableProperty(CloudierRequest<SiLACloudConnector.CancelObservablePropertySubscription> request);
}
