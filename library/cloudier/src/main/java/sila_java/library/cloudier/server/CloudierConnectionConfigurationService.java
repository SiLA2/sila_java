package sila_java.library.cloudier.server;

import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.core.connectionconfigurationservice.v1.ConnectionConfigurationServiceGrpc;
import sila2.org.silastandard.core.connectionconfigurationservice.v1.ConnectionConfigurationServiceOuterClass;
import sila_java.library.core.sila.types.SiLABoolean;

import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

@Slf4j
public class CloudierConnectionConfigurationService extends ConnectionConfigurationServiceGrpc.ConnectionConfigurationServiceImplBase {
    private final AtomicBoolean isServerInitiated;
    // todo make thread safe
    private final Set<Runnable> connectionModeListeners = new HashSet<>();
    // todo make thread safe
    private final Set<Runnable> clientsListeners = new HashSet<>();
    // todo make thread safe
    private final Map<String, ConnectionConfigurationServiceOuterClass.ConnectSiLAClient_Parameters> configuredClients = new HashMap<>();
    private final ConnectionConfigurationSwitch listener;

    @FunctionalInterface
    public interface ConnectionConfigurationSwitch {
        void onSwitch(boolean serverInitiatedConnectionEnabled);
    }

    public CloudierConnectionConfigurationService(boolean cloudEnabledDefault, ConnectionConfigurationSwitch listener) {
        this.isServerInitiated = new AtomicBoolean(cloudEnabledDefault);
        this.listener = listener;
    }

    public CloudierConnectionConfigurationService(
            boolean cloudEnabledDefault,
            List<ConnectionConfigurationServiceOuterClass.ConnectSiLAClient_Parameters> defaultClients,
            ConnectionConfigurationSwitch listener
    ) {
        defaultClients.forEach((c) -> this.configuredClients.put(c.getClientName().getValue(), c));
        this.isServerInitiated = new AtomicBoolean(cloudEnabledDefault);
        this.listener = listener;
    }

    private void notifyConnectionModeListeners() {
        final Set<Runnable> toRemove = new HashSet<>();
        this.connectionModeListeners.forEach(listener -> {
            try {
                listener.run();
            } catch (Exception e) {
                log.debug("Remove connection mode listener because of exception", e);
                toRemove.add(listener);
            }
        });
        this.connectionModeListeners.removeAll(toRemove);
        log.info("Connection mode listeners count {}", this.connectionModeListeners.size());
    }

    private void notifyClientsListeners() {
        final Set<Runnable> toRemove = new HashSet<>();
        this.clientsListeners.forEach(listener -> {
            try {
                listener.run();
            } catch (Exception e) {
                log.debug("Remove configured clients listener because of exception", e);
                toRemove.add(listener);
            }
        });
        this.clientsListeners.removeAll(toRemove);
        log.info("Configured clients listeners count {}", this.clientsListeners.size());
        log.info("Configured clients client count {}", this.clientsListeners.size());
    }

    @Override
    public void enableServerInitiatedConnectionMode(
            ConnectionConfigurationServiceOuterClass.EnableServerInitiatedConnectionMode_Parameters request,
            StreamObserver<ConnectionConfigurationServiceOuterClass.EnableServerInitiatedConnectionMode_Responses> responseObserver
    ) {
        responseObserver.onNext(
                ConnectionConfigurationServiceOuterClass.EnableServerInitiatedConnectionMode_Responses.newBuilder().build()
        );
        responseObserver.onCompleted();
        this.isServerInitiated.set(true);
        this.listener.onSwitch(true);
    }

    @Override
    public void disableServerInitiatedConnectionMode(
            ConnectionConfigurationServiceOuterClass.DisableServerInitiatedConnectionMode_Parameters request,
            StreamObserver<ConnectionConfigurationServiceOuterClass.DisableServerInitiatedConnectionMode_Responses> responseObserver
    ) {
        responseObserver.onNext(
                ConnectionConfigurationServiceOuterClass.DisableServerInitiatedConnectionMode_Responses.newBuilder().build()
        );
        responseObserver.onCompleted();
        this.isServerInitiated.set(false);
        this.listener.onSwitch(false);
    }

    @Override
    public void connectSiLAClient(
            ConnectionConfigurationServiceOuterClass.ConnectSiLAClient_Parameters request,
            StreamObserver<ConnectionConfigurationServiceOuterClass.ConnectSiLAClient_Responses> responseObserver
    ) {
        final String clientName = request.getClientName().getValue();
        this.configuredClients.put(clientName, request);
        this.notifyClientsListeners();
        responseObserver.onNext(ConnectionConfigurationServiceOuterClass.ConnectSiLAClient_Responses.newBuilder().build());
        responseObserver.onCompleted();
    }

    @Override
    public void disconnectSiLAClient(
            ConnectionConfigurationServiceOuterClass.DisconnectSiLAClient_Parameters request,
            StreamObserver<ConnectionConfigurationServiceOuterClass.DisconnectSiLAClient_Responses> responseObserver
    ) {
        final String clientName = request.getClientName().getValue();
        final boolean toRemove = request.getRemove().getValue();
        // todo disconnect
        if (toRemove) {
            this.configuredClients.remove(clientName);
            this.notifyClientsListeners();
        }
        responseObserver.onNext(ConnectionConfigurationServiceOuterClass.DisconnectSiLAClient_Responses.newBuilder().build());
        responseObserver.onCompleted();
    }

    @Override
    public void getServerInitiatedConnectionModeStatus(ConnectionConfigurationServiceOuterClass.Get_ServerInitiatedConnectionModeStatus_Parameters request, StreamObserver<ConnectionConfigurationServiceOuterClass.Get_ServerInitiatedConnectionModeStatus_Responses> responseObserver) {
        responseObserver.onNext(
                ConnectionConfigurationServiceOuterClass.Get_ServerInitiatedConnectionModeStatus_Responses
                        .newBuilder()
                        .setServerInitiatedConnectionModeStatus(SiLABoolean.from(this.isServerInitiated.get()))
                        .build()
        );
        responseObserver.onCompleted();
    }

    @Override
    public void getConfiguredSiLAClients(ConnectionConfigurationServiceOuterClass.Get_ConfiguredSiLAClients_Parameters request, StreamObserver<ConnectionConfigurationServiceOuterClass.Get_ConfiguredSiLAClients_Responses> responseObserver) {
        Set<ConnectionConfigurationServiceOuterClass.Get_ConfiguredSiLAClients_Responses.ConfiguredSiLAClients_Struct> clients =
                this.configuredClients.values()
                        .stream()
                        .map((client) -> ConnectionConfigurationServiceOuterClass.Get_ConfiguredSiLAClients_Responses.ConfiguredSiLAClients_Struct
                                .newBuilder()
                                .setClientName(client.getClientName())
                                .setSiLAClientHost(client.getSiLAClientHost())
                                .setSiLAClientPort(client.getSiLAClientPort())
                                .build()
                        ).collect(Collectors.toSet());
        responseObserver.onNext(
                ConnectionConfigurationServiceOuterClass.Get_ConfiguredSiLAClients_Responses
                        .newBuilder()
                        .addAllConfiguredSiLAClients(clients)
                        .build()
        );
        responseObserver.onCompleted();
    }
}
