package sila_java.library.cloudier.client;

import com.google.protobuf.ByteString;
import com.google.protobuf.GeneratedMessageV3;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Parser;
import io.grpc.stub.StreamObserver;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.SiLABinaryTransfer;
import sila2.org.silastandard.SiLACloudConnector;
import sila2.org.silastandard.SiLACloudConnector.Metadata;
import sila2.org.silastandard.SiLAFramework;
import sila2.org.silastandard.core.silaservice.v1.SiLAServiceOuterClass;
import sila_java.library.core.sila.errors.SiLAErrorException;
import sila_java.library.core.sila.types.SiLAString;

import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Function;

@Slf4j
@AllArgsConstructor
public class CloudierClientObserver implements StreamObserver<SiLACloudConnector.SiLAServerMessage>, AutoCloseable {
    private final StreamObserver<SiLACloudConnector.SiLAClientMessage> responseObserver;
    private final CloudierClientEndpoint.CloudServerListener cloudServerListener;
    private final Map<String, CloudierClientEndpoint.MessageListener> messageListeners = new ConcurrentHashMap<>();
    private Optional<UUID> optionalServerUUID = Optional.empty();

    public CloudierClientObserver(
            StreamObserver<SiLACloudConnector.SiLAClientMessage> responseObserver,
            CloudierClientEndpoint.CloudServerListener cloudServerListener
    ) {
        this.responseObserver = responseObserver;
        this.cloudServerListener = cloudServerListener;
    }

    @Override
    public void close() {
        // todo check if there are anything else to cleanup
        this.responseObserver.onCompleted();
        this.cloudServerListener.onEnd(this.optionalServerUUID, Optional.empty());
    }

    public static class CompletableCancelableFuture<T> extends CompletableFuture<T> {
        private final Runnable onCancel;

        public CompletableCancelableFuture(@NonNull final Runnable onCancel) {
            super();
            this.onCancel = onCancel;
        }

        @Override
        public boolean cancel(boolean mayInterruptIfRunning) {
            try {
                this.onCancel.run();
            } catch (Throwable e) {
                log.warn("Exception occurred while cancelling future", e);
            }
            return super.cancel(mayInterruptIfRunning);
        }
    }

    public CompletableFuture<ByteString> runObservableCommand(
            SiLACloudConnector.CommandParameter parameter,
            String fullyQualifiedCommandId,
            boolean hasIntermediateResponse,
            CloudierClientEndpoint.CallListener callListener
    ) {
        final String uuid = UUID.randomUUID().toString();
        final CompletableFuture<ByteString> result = new CompletableCancelableFuture<>(() -> {
            this.messageListeners.remove(uuid);
            if (hasIntermediateResponse) {
                this.responseObserver.onNext(
                        SiLACloudConnector.SiLAClientMessage
                                .newBuilder()
                                .setRequestUUID(uuid)
                                .setCancelObservableCommandIntermediateResponseSubscription(
                                        SiLACloudConnector.CancelObservableCommandIntermediateResponseSubscription.newBuilder().build()
                                )
                                .build()
                );
            }
            this.responseObserver.onNext(
                    SiLACloudConnector.SiLAClientMessage
                            .newBuilder()
                            .setRequestUUID(uuid)
                            .setCancelObservableCommandExecutionInfoSubscription(
                                    SiLACloudConnector.CancelObservableCommandExecutionInfoSubscription.newBuilder().build()
                            )
                            .build()
            );
        });
        final AtomicReference<SiLAFramework.CommandExecutionUUID> commandExecUUID = new AtomicReference<>(null);
        this.messageListeners.put(uuid, message -> {
            if (message.hasObservableCommandConfirmation()) {
                commandExecUUID.set(message.getObservableCommandConfirmation().getCommandConfirmation().getCommandExecutionUUID());
                if (hasIntermediateResponse) {
                    this.responseObserver.onNext(
                            SiLACloudConnector.SiLAClientMessage
                                    .newBuilder()
                                    .setRequestUUID(uuid)
                                    .setObservableCommandIntermediateResponseSubscription(
                                            SiLACloudConnector.ObservableCommandIntermediateResponseSubscription.newBuilder().setCommandExecutionUUID(
                                                    commandExecUUID.get()
                                            ).build()
                                    )
                                    .build()
                    );
                }
                this.responseObserver.onNext(
                        SiLACloudConnector.SiLAClientMessage
                                .newBuilder()
                                .setRequestUUID(uuid)
                                .setObservableCommandExecutionInfoSubscription(
                                        SiLACloudConnector.ObservableCommandExecutionInfoSubscription.newBuilder().setCommandExecutionUUID(
                                                commandExecUUID.get()
                                        ).build()
                                )
                                .build()
                );
                callListener.onCommandInit(message.getObservableCommandConfirmation());
            } else if (message.hasCommandError()) {
                this.messageListeners.remove(uuid);
                result.obtrudeException(new SiLAErrorException(message.getCommandError()));
                callListener.onError(message.getCommandError());
            } else if (message.hasObservableCommandExecutionInfo()) {
                SiLAFramework.ExecutionInfo.CommandStatus status = message.getObservableCommandExecutionInfo().getExecutionInfo().getCommandStatus();
                callListener.onCommandExecutionInfo(message.getObservableCommandExecutionInfo());
                if (status == SiLAFramework.ExecutionInfo.CommandStatus.finishedWithError || status == SiLAFramework.ExecutionInfo.CommandStatus.finishedSuccessfully) {
                    this.responseObserver.onNext(
                            SiLACloudConnector.SiLAClientMessage
                                    .newBuilder()
                                    .setRequestUUID(uuid)
                                    .setObservableCommandGetResponse(
                                            SiLACloudConnector.ObservableCommandGetResponse.newBuilder().setCommandExecutionUUID(
                                                    commandExecUUID.get()
                                            ).build()
                                    )
                                    .build()
                    );
                }
            } else if (message.hasObservableCommandIntermediateResponse()) {
                callListener.onIntermediateResponse(message.getObservableCommandIntermediateResponse());
            } else if (message.hasObservableCommandResponse()) {
                this.messageListeners.remove(uuid);
                result.complete(message.getObservableCommandResponse().getResponse());
            } else {
                log.warn("Unknown response {} for observable command {}", message.getMessageCase(), uuid);
            }
        });
        this.responseObserver.onNext(
                SiLACloudConnector.SiLAClientMessage
                        .newBuilder()
                        .setRequestUUID(uuid)
                        .setObservableCommandInitiation(SiLACloudConnector.ObservableCommandInitiation.newBuilder()
                                .setCommandParameter(parameter)
                                .setFullyQualifiedCommandId(fullyQualifiedCommandId)
                                .build()
                        )
                        .build()
        );
        return result;
    }

    public <ParameterType extends GeneratedMessageV3, ResponseType> CompletableFuture<ResponseType> runUnobservableCommand(
            String fullyQualifiedId,
            ParameterType parameter,
            Parser<ResponseType> parser
    ) {
        return runUnobservableCommand(
                SiLACloudConnector.UnobservableCommandExecution
                        .newBuilder()
                        .setFullyQualifiedCommandId(fullyQualifiedId)
                        .setCommandParameter(
                                SiLACloudConnector.CommandParameter
                                        .newBuilder()
                                        .setParameters(parameter.toByteString())
                                        .build()
                        ).build(),
                parser
        );
    }

    public <ResponseType> CompletableFuture<ResponseType>  runUnobservableCommand(
            SiLACloudConnector.UnobservableCommandExecution commandExecution,
            Parser<ResponseType> parser
    ) {
        return runUnobservableCommand(commandExecution).thenApply((byteString) -> {
            try {
                return parser.parseFrom(byteString);
            } catch (InvalidProtocolBufferException e) {
                // todo make sure that the exception is not swallowed
                throw new RuntimeException("Invalid message received", e);
            }
        });
    }

    public CompletableFuture<ByteString> runUnobservableCommand(
            SiLACloudConnector.UnobservableCommandExecution commandExecution
    ) {
        final String uuid = UUID.randomUUID().toString();
        final CompletableFuture<ByteString> result = new CompletableCancelableFuture<>(() -> {
            this.messageListeners.remove(uuid);
        });
        this.messageListeners.put(uuid, message -> {
            if (message.hasUnobservableCommandResponse()) {
                this.messageListeners.remove(uuid);
                result.complete(message.getUnobservableCommandResponse().getResponse());
            } else if (message.hasCommandError()) {
                this.messageListeners.remove(uuid);
                result.obtrudeException(new SiLAErrorException(message.getCommandError()));
            } else {
                log.warn("Unknown response {} for unobservable command {}", message.getMessageCase(), uuid);
            }
        });
        this.responseObserver.onNext(
                SiLACloudConnector.SiLAClientMessage
                        .newBuilder()
                        .setRequestUUID(uuid)
                        .setUnobservableCommandExecution(commandExecution)
                        .build()
        );
        return result;
    }

    public CompletableFuture<ByteString> readObservableProperty(SiLACloudConnector.ObservablePropertySubscription propertySubscription, Function<ByteString, Boolean> responseCallback) {
        final String uuid = UUID.randomUUID().toString();
        final CompletableFuture<ByteString> result = new CompletableCancelableFuture<>(() -> {
            this.messageListeners.remove(uuid);
            this.responseObserver.onNext(
                    SiLACloudConnector.SiLAClientMessage
                            .newBuilder()
                            .setRequestUUID(uuid)
                            .setCancelObservablePropertySubscription(
                                    SiLACloudConnector.CancelObservablePropertySubscription.newBuilder().build()
                            )
                            .build()
            );
        });
        this.messageListeners.put(uuid, message -> {
            if (message.hasObservablePropertyValue()) {
                final boolean keepReading = responseCallback.apply(message.getObservablePropertyValue().getValue());
                if (!keepReading) {
                    result.complete(message.getObservablePropertyValue().getValue());
                }
            } else if (message.hasPropertyError()) {
                this.messageListeners.remove(uuid);
                result.obtrudeException(new SiLAErrorException(message.getPropertyError()));
            } else {
                log.warn("Unknown response for observable property " + uuid);
            }
        });
        this.responseObserver.onNext(
                SiLACloudConnector.SiLAClientMessage
                        .newBuilder()
                        .setRequestUUID(uuid)
                        .setObservablePropertySubscription(propertySubscription)
                        .build()
        );
        return result;
    }

    public <ResponseType> CompletableFuture<ResponseType> readUnobservableProperty(
            String fullyQualifiedId,
            Parser<ResponseType> parser
    ) {
        return readUnobservableProperty(
                SiLACloudConnector.UnobservablePropertyRead.newBuilder().setFullyQualifiedPropertyId(fullyQualifiedId).build(),
                parser
        );
    }

    public <ResponseType> CompletableFuture<ResponseType> readUnobservableProperty(
            SiLACloudConnector.UnobservablePropertyRead propertyRead,
            Parser<ResponseType> parser
    ) {
        return readUnobservableProperty(propertyRead).thenApply((byteString) -> {
            try {
                return parser.parseFrom(byteString);
            } catch (InvalidProtocolBufferException e) {
                // todo make sure that the exception is not swallowed
                throw new RuntimeException("Invalid message received", e);
            }
        });
    }

    public CompletableFuture<ByteString> readUnobservableProperty(
            SiLACloudConnector.UnobservablePropertyRead propertyRead
    ) {
        final String uuid = UUID.randomUUID().toString();
        final CompletableFuture<ByteString> result = new CompletableCancelableFuture<>(() -> {
            this.messageListeners.remove(uuid);
        });
        this.messageListeners.put(uuid, message -> {
            if (message.hasUnobservablePropertyValue()) {
                this.messageListeners.remove(uuid);
                result.complete(message.getUnobservablePropertyValue().getValue());
            } else if (message.hasPropertyError()) {
                this.messageListeners.remove(uuid);
                result.obtrudeException(new SiLAErrorException(message.getPropertyError()));
            } else {
                log.warn("Unknown response for unobservable property " + uuid);
            }
        });
        this.responseObserver.onNext(
                SiLACloudConnector.SiLAClientMessage
                        .newBuilder()
                        .setRequestUUID(uuid)
                        .setUnobservablePropertyRead(propertyRead)
                        .build()
        );
        return result;
    }

    public CompletableFuture<SiLACloudConnector.GetFCPAffectedByMetadataResponse> getFCPAffectedByMetadata(String fullyQualifiedMetadataId) {
        final String uuid = UUID.randomUUID().toString();
        // todo implement CompletableCancelableFuture
        final CompletableFuture<SiLACloudConnector.GetFCPAffectedByMetadataResponse> result = new CompletableFuture<>();
        this.messageListeners.put(uuid, message -> {
            if (message.hasGetFCPAffectedByMetadataResponse()) {
                this.messageListeners.remove(uuid);
                result.complete(message.getGetFCPAffectedByMetadataResponse());
            } else if (message.hasCommandError()) {
                this.messageListeners.remove(uuid);
                result.obtrudeException(new SiLAErrorException(message.getCommandError()));
            } else {
                log.warn("Unknown response for command " + uuid);
            }
        });
        this.responseObserver.onNext(
                SiLACloudConnector.SiLAClientMessage
                        .newBuilder()
                        .setRequestUUID(uuid)
                        .setMetadataRequest(
                                SiLACloudConnector.GetFCPAffectedByMetadataRequest
                                        .newBuilder()
                                        .setFullyQualifiedMetadataId(fullyQualifiedMetadataId)
                                        .build()
                        )
                        .build()
        );
        return result;
    }

    public CompletableFuture<SiLABinaryTransfer.CreateBinaryResponse> createBinaryUploadRequest(
            SiLABinaryTransfer.CreateBinaryRequest createBinaryRequest,
            Set<Metadata> metadata
    ) {
        final String uuid = UUID.randomUUID().toString();
        // todo implement CompletableCancelableFuture
        final CompletableFuture<SiLABinaryTransfer.CreateBinaryResponse> result = new CompletableFuture<>();
        this.messageListeners.put(uuid, message -> {
            if (message.hasCreateBinaryResponse()) {
                this.messageListeners.remove(uuid);
                result.complete(message.getCreateBinaryResponse());
            } else if (message.hasBinaryTransferError()) {
                this.messageListeners.remove(uuid);
                result.obtrudeException(new SiLAErrorException(message.getBinaryTransferError()));
            } else {
                log.warn("Unknown response for command " + uuid);
            }
        });
        this.responseObserver.onNext(
                SiLACloudConnector.SiLAClientMessage
                        .newBuilder()
                        .setRequestUUID(uuid)
                        .setCreateBinaryUploadRequest(
                                SiLACloudConnector.CreateBinaryUploadRequest
                                        .newBuilder()
                                        .addAllMetadata(metadata)
                                        .setCreateBinaryRequest(createBinaryRequest)
                                        .build()
                        )
                        .build()
        );
        return result;
    }

    public CompletableFuture<SiLABinaryTransfer.DeleteBinaryResponse> deleteDownloadedBinaryRequest(
            SiLABinaryTransfer.DeleteBinaryRequest deleteBinaryRequest
    ) {
        final String uuid = UUID.randomUUID().toString();
        // todo implement CompletableCancelableFuture
        final CompletableFuture<SiLABinaryTransfer.DeleteBinaryResponse> result = new CompletableFuture<>();
        this.messageListeners.put(uuid, message -> {
            if (message.hasDeleteBinaryResponse()) {
                this.messageListeners.remove(uuid);
                result.complete(message.getDeleteBinaryResponse());
            } else if (message.hasBinaryTransferError()) {
                this.messageListeners.remove(uuid);
                result.obtrudeException(new SiLAErrorException(message.getBinaryTransferError()));
            } else {
                log.warn("Unknown response for command " + uuid);
            }
        });
        this.responseObserver.onNext(
                SiLACloudConnector.SiLAClientMessage
                        .newBuilder()
                        .setRequestUUID(uuid)
                        .setDeleteDownloadedBinaryRequest(
                                deleteBinaryRequest
                        )
                        .build()
        );
        return result;
    }

    public CompletableFuture<SiLABinaryTransfer.DeleteBinaryResponse> deleteUploadedBinaryRequest(
            SiLABinaryTransfer.DeleteBinaryRequest deleteBinaryRequest
    ) {
        final String uuid = UUID.randomUUID().toString();
        // todo implement CompletableCancelableFuture
        final CompletableFuture<SiLABinaryTransfer.DeleteBinaryResponse> result = new CompletableFuture<>();
        this.messageListeners.put(uuid, message -> {
            if (message.hasDeleteBinaryResponse()) {
                this.messageListeners.remove(uuid);
                result.complete(message.getDeleteBinaryResponse());
            } else if (message.hasBinaryTransferError()) {
                this.messageListeners.remove(uuid);
                result.obtrudeException(new SiLAErrorException(message.getBinaryTransferError()));
            } else {
                log.warn("Unknown response for command " + uuid);
            }
        });
        this.responseObserver.onNext(
                SiLACloudConnector.SiLAClientMessage
                        .newBuilder()
                        .setRequestUUID(uuid)
                        .setDeleteUploadedBinaryRequest(
                                deleteBinaryRequest
                        )
                        .build()
        );
        return result;
    }

    public CompletableFuture<SiLABinaryTransfer.UploadChunkResponse> uploadBinaryChunkRequest(
            SiLABinaryTransfer.UploadChunkRequest uploadChunkRequest
    ) {
        final String uuid = UUID.randomUUID().toString();
        // todo implement CompletableCancelableFuture
        final CompletableFuture<SiLABinaryTransfer.UploadChunkResponse> result = new CompletableFuture<>();
        this.messageListeners.put(uuid, message -> {
            if (message.hasUploadChunkResponse()) {
                this.messageListeners.remove(uuid);
                result.complete(message.getUploadChunkResponse());
            } else if (message.hasBinaryTransferError()) {
                this.messageListeners.remove(uuid);
                result.obtrudeException(new SiLAErrorException(message.getBinaryTransferError()));
            } else {
                log.warn("Unknown response for command " + uuid);
            }
        });
        this.responseObserver.onNext(
                SiLACloudConnector.SiLAClientMessage
                        .newBuilder()
                        .setRequestUUID(uuid)
                        .setUploadChunkRequest(
                                uploadChunkRequest
                        )
                        .build()
        );
        return result;
    }

    public CompletableFuture<SiLABinaryTransfer.GetBinaryInfoResponse> getBinaryInfoResponseRequest(
            SiLABinaryTransfer.GetBinaryInfoRequest binaryInfoRequest
    ) {
        final String uuid = UUID.randomUUID().toString();
        // todo implement CompletableCancelableFuture
        final CompletableFuture<SiLABinaryTransfer.GetBinaryInfoResponse> result = new CompletableFuture<>();
        this.messageListeners.put(uuid, message -> {
            if (message.hasGetBinaryResponse()) {
                this.messageListeners.remove(uuid);
                result.complete(message.getGetBinaryResponse());
            } else if (message.hasBinaryTransferError()) {
                this.messageListeners.remove(uuid);
                result.obtrudeException(new SiLAErrorException(message.getBinaryTransferError()));
            } else {
                log.warn("Unknown response for command " + uuid);
            }
        });
        this.responseObserver.onNext(
                SiLACloudConnector.SiLAClientMessage
                        .newBuilder()
                        .setRequestUUID(uuid)
                        .setGetBinaryInfoRequest(
                                binaryInfoRequest
                        )
                        .build()
        );
        return result;
    }

    public CompletableFuture<SiLABinaryTransfer.GetChunkResponse> getBinaryChunkRequest(
            SiLABinaryTransfer.GetChunkRequest chunkRequest
    ) {
        final String uuid = UUID.randomUUID().toString();
        // todo implement CompletableCancelableFuture
        final CompletableFuture<SiLABinaryTransfer.GetChunkResponse> result = new CompletableFuture<>();
        this.messageListeners.put(uuid, message -> {
            if (message.hasGetChunkResponse()) {
                this.messageListeners.remove(uuid);
                result.complete(message.getGetChunkResponse());
            } else if (message.hasBinaryTransferError()) {
                this.messageListeners.remove(uuid);
                result.obtrudeException(new SiLAErrorException(message.getBinaryTransferError()));
            } else {
                log.warn("Unknown response for command " + uuid);
            }
        });
        this.responseObserver.onNext(
                SiLACloudConnector.SiLAClientMessage
                        .newBuilder()
                        .setRequestUUID(uuid)
                        .setGetChunkRequest(
                                chunkRequest
                        )
                        .build()
        );
        return result;
    }

    public CompletableFuture<SiLAServiceOuterClass.Get_ServerUUID_Responses> getOptionalServerUUID() {
        return this.readUnobservableProperty(
                "org.silastandard/core/SiLAService/v1/Property/ServerUUID",
                SiLAServiceOuterClass.Get_ServerUUID_Responses.parser()
        );
    }

    public CompletableFuture<SiLAServiceOuterClass.Get_ServerType_Responses> getServerType() {
        return this.readUnobservableProperty(
                "org.silastandard/core/SiLAService/v1/Property/ServerType",
                SiLAServiceOuterClass.Get_ServerType_Responses.parser()
        );
    }

    public CompletableFuture<SiLAServiceOuterClass.Get_ServerName_Responses> getServerName() {
        return this.readUnobservableProperty(
                "org.silastandard/core/SiLAService/v1/Property/ServerName",
                SiLAServiceOuterClass.Get_ServerName_Responses.parser()
        );
    }

    public CompletableFuture<SiLAServiceOuterClass.Get_ServerDescription_Responses> getServerDescription() {
        return this.readUnobservableProperty(
                "org.silastandard/core/SiLAService/v1/Property/ServerDescription",
                SiLAServiceOuterClass.Get_ServerDescription_Responses.parser()
        );
    }

    public CompletableFuture<SiLAServiceOuterClass.Get_ServerVersion_Responses> getServerVersion() {
        return this.readUnobservableProperty(
                "org.silastandard/core/SiLAService/v1/Property/ServerVersion",
                SiLAServiceOuterClass.Get_ServerVersion_Responses.parser()
        );
    }

    public CompletableFuture<SiLAServiceOuterClass.Get_ServerVendorURL_Responses> getServerVendorUrl() {
        return this.readUnobservableProperty(
                "org.silastandard/core/SiLAService/v1/Property/ServerVendorURL",
                SiLAServiceOuterClass.Get_ServerVendorURL_Responses.parser()
        );
    }

    public CompletableFuture<SiLAServiceOuterClass.Get_ImplementedFeatures_Responses> getServerImplementedFeatures() {
        return this.readUnobservableProperty(
                "org.silastandard/core/SiLAService/v1/Property/ImplementedFeatures",
                SiLAServiceOuterClass.Get_ImplementedFeatures_Responses.parser()
        );
    }

    public CompletableFuture<SiLAServiceOuterClass.GetFeatureDefinition_Responses> getServerImplementedFeatures(String featureId) {
        return this.runUnobservableCommand(
                "org.silastandard/core/SiLAService/v1/Command/GetFeatureDefinition",
                SiLAServiceOuterClass.GetFeatureDefinition_Parameters
                        .newBuilder()
                        .setFeatureIdentifier(SiLAString.from(featureId)).build(),
                SiLAServiceOuterClass.GetFeatureDefinition_Responses.parser()
        );
    }

    @Override
    public void onNext(SiLACloudConnector.SiLAServerMessage silaServerMessage) {
        log.debug("Cloud message received " + silaServerMessage.getMessageCase().name());
        this.messageListeners.getOrDefault(silaServerMessage.getRequestUUID(), message -> {
            log.warn("Unhandled response received for request " + message.getRequestUUID());
        }).onMessage(silaServerMessage);
    }

    @Override
    public void onError(Throwable throwable) {
        cloudServerListener.onEnd(this.optionalServerUUID, Optional.ofNullable(throwable));
    }

    @Override
    public void onCompleted() {
        cloudServerListener.onEnd(this.optionalServerUUID,Optional.empty());
    }

    public void setServerUUID(UUID serverUUIDValue) {
        this.optionalServerUUID = Optional.ofNullable(serverUUIDValue);
    }
}
