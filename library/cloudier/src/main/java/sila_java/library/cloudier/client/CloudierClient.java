package sila_java.library.cloudier.client;

import com.google.protobuf.ByteString;
import io.grpc.Server;
import io.grpc.ServerBuilder;
import lombok.Getter;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.SiLACloudConnector;
import sila2.org.silastandard.SiLAFramework;
import sila_java.library.core.encryption.EncryptionUtils;
import sila_java.library.core.encryption.SelfSignedCertificate;
import sila_java.library.core.sila.types.SiLAString;

import java.io.IOException;
import java.security.cert.CertificateEncodingException;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import static sila_java.library.cloudier.server.CloudierServer.*;
import static sila_java.library.core.utils.Utils.blockUntilShutdown;

@Slf4j
public class CloudierClient implements AutoCloseable {
    public static final int DEFAULT_PORT = 50051;
    private final Server server;
    @Getter
    private final CloudierClientEndpoint endpointService;

    public CloudierClient(final CloudierClientEndpoint.CloudServerListener cloudServerListener)
            throws IOException, CertificateEncodingException, SelfSignedCertificate.CertificateGenerationException {
        this.endpointService = new CloudierClientEndpoint(cloudServerListener);
        final SelfSignedCertificate selfSignedCertificate = SelfSignedCertificate.newBuilder().build();
        final ServerBuilder serverBuilder = ServerBuilder
                .forPort(DEFAULT_PORT)
                .useTransportSecurity(
                        EncryptionUtils.certificateToStream(selfSignedCertificate.getCertificate()),
                        EncryptionUtils.keyToStream(selfSignedCertificate.getPrivateKey())
                )
                .addService(endpointService);
        server = serverBuilder.build();
        server.start();
        log.info("Cloudier client listening on port: " + DEFAULT_PORT);
    }

    public static void main(String[] args) throws ExecutionException, InterruptedException, TimeoutException, CertificateEncodingException, IOException, SelfSignedCertificate.CertificateGenerationException {
        final CloudierClient cloudierClient = new CloudierClient(new CloudierClientEndpoint.CloudServerListener() {
            @Override
            public void onAdd(UUID serverUUID, CloudierClientObserver cloudierClientObserver) {

            }

            @Override
            public void onEnd(Optional<UUID> serverUUID, Optional<Throwable> t) {

            }
        });
        while (cloudierClient.endpointService.getResponseObservers().get(serverUUID) == null) {
            Thread.sleep(100);
        }
        final CloudierClientObserver clientObserver = cloudierClient.endpointService.getResponseObservers().get(serverUUID);
        CompletableFuture<SiLAFramework.String> stringCompletableFuture =
                clientObserver.runUnobservableCommand("", SiLAString.from(""), SiLAFramework.String.parser());
        SiLAFramework.String s = stringCompletableFuture.get(60, TimeUnit.SECONDS);
        log.info("received " + s.getValue());
        CompletableFuture<SiLAFramework.String> property =
                clientObserver.readUnobservableProperty("", SiLAFramework.String.parser());
        s = property.get(60, TimeUnit.SECONDS);
        log.info("received " + s.getValue());
        CompletableFuture<ByteString> obs =
                clientObserver.runObservableCommand(SiLACloudConnector.CommandParameter.newBuilder().build(), "", true, new CloudierClientEndpoint.CallListener() {
                    @Override
                    public void onCommandInit(SiLACloudConnector.ObservableCommandConfirmation observableCommandConfirmation) {
                        log.info("Command init " + observableCommandConfirmation.getCommandConfirmation().getCommandExecutionUUID().getValue());
                    }

                    @Override
                    public void onCommandExecutionInfo(SiLACloudConnector.ObservableCommandExecutionInfo observableCommandExecutionInfo) {
                        log.info("Command status " + observableCommandExecutionInfo.getExecutionInfo().getCommandStatus().name());
                    }

                    @SneakyThrows
                    @Override
                    public void onIntermediateResponse(SiLACloudConnector.ObservableCommandIntermediateResponse observableCommandExecutionInfo) {
                        log.info("Intermediate response: " + SiLAFramework.String.parseFrom(observableCommandExecutionInfo.getResponse()).getValue());
                    }

                    @Override
                    public void onError(SiLAFramework.SiLAError siLAError) {
                        log.info("On error " + siLAError.toString());
                    }
                });
        ByteString b = obs.get(60, TimeUnit.SECONDS);
        log.info("received " + SiLAFramework.String.parseFrom(b).getValue());
        CompletableFuture<ByteString> obsproperty =
                clientObserver.readObservableProperty(SiLACloudConnector.ObservablePropertySubscription.newBuilder().setFullyQualifiedPropertyId("").build(), (r) -> {
                    return false; // stop reading
                    // todo handle response
                });
        b = obsproperty.get(60, TimeUnit.SECONDS);
        log.info("received " + SiLAFramework.String.parseFrom(b).getValue());
        if (!clientObserver.getOptionalServerUUID().get(60, TimeUnit.SECONDS).getServerUUID().getValue().equals(serverUUID)) {
            throw new RuntimeException("UUID does not match");
        }
        if (!clientObserver.getServerDescription().get(60, TimeUnit.SECONDS).getServerDescription().getValue().equals(serverDescription)) {
            throw new RuntimeException("Description does not match");
        }
        if (!clientObserver.getServerName().get(60, TimeUnit.SECONDS).getServerName().getValue().equals(serverName)) {
            throw new RuntimeException("Name does not match");
        }
        if (!clientObserver.getServerType().get(60, TimeUnit.SECONDS).getServerType().getValue().equals(serverType)) {
            throw new RuntimeException("Type does not match");
        }
        if (!clientObserver.getServerVendorUrl().get(60, TimeUnit.SECONDS).getServerVendorURL().getValue().equals(serverURL)) {
            throw new RuntimeException("URL does not match");
        }
        if (!clientObserver.getServerVersion().get(60, TimeUnit.SECONDS).getServerVersion().getValue().equals(serverVersion)) {
            throw new RuntimeException("Version does not match");
        }
        log.info("To stop the client press CTRL + C.");
        blockUntilShutdown(cloudierClient::close);
    }

    @Override
    public void close() {
        try {
            server.shutdown().awaitTermination(5, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            log.warn("Server closing interrupted", e);
        }
    }
}
