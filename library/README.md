# Library
Java library to facilitate the implementation of both SiLA Servers and SiLA Clients.

The dependencies are setup that only lower layers can depend on upper layers with the following order:
1. sila_base
2. core
3. maven_plugin
4. server_base
5. manager
6. cloudier

## Components

### sila_base
Maven wrapper around the `sila_base` repository, to allow the use of the base definitions in all
packages.

### core
The core library components including discovery, gRPC Mapping and various utilities.

### maven_plugin
Compile time plugin to automate mapping and code generation in the future.

### server_base
Common components to facilitate the implementation of SiLA Servers. Includes the implementation of
generic features such as SiLAService.

## manager
Utility component to manage SiLA Servers from the client side, mainly for late-bound applications.

## cloudier
Components for SILA cloud connectivity