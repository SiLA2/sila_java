package sila_java.library.server_base.config;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

/**
 * Non persistent server configuration container
 */
@Slf4j
public class NonPersistentServerConfigWrapper implements IServerConfigWrapper {
    private ServerConfiguration configuration;

    /**
     * Create a non persistent server configuration container
     * @param serverType The server name to use
     */
    public NonPersistentServerConfigWrapper(@NonNull final String serverType) {
        this.configuration = new ServerConfiguration(serverType);
    }

    /**
     * Create a non persistent server configuration container
     * @param configuration The server configuration to contain
     */
    public NonPersistentServerConfigWrapper(@NonNull final ServerConfiguration configuration) {
        this.configuration = configuration;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ServerConfiguration getCacheConfig() {
        return configuration;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setConfig(@NonNull final ServerConfiguration serverConfiguration) {
        this.configuration = serverConfiguration;
    }
}