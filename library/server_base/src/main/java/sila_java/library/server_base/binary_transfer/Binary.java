package sila_java.library.server_base.binary_transfer;

import lombok.AllArgsConstructor;
import lombok.Getter;
import sila_java.library.core.sila.binary_transfer.BinaryInfo;

import java.sql.Blob;

/**
 * Binary modal containing a blob of data and binary info
 */
@Getter
@AllArgsConstructor
public class Binary {
    private final Blob data;
    private final BinaryInfo info;
}
