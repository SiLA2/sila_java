package sila_java.library.server_base;

import io.grpc.Context;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Fully qualified metadata context key model
 *
 * @param <T> Context type
 */
@Getter
@AllArgsConstructor
public class FullyQualifiedMetadataContextKey<T> {
    private final String fullyQualifiedIdentifier;
    private final Context.Key<T> contextKey;
}
