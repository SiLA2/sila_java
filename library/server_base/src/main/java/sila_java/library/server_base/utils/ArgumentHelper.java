package sila_java.library.server_base.utils;

import lombok.Getter;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.impl.Arguments;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.Namespace;
import sila_java.library.core.utils.GitRepositoryState;
import sila_java.library.core.utils.ListNetworkInterfaces;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;

/**
 * Argument helper to parse CLI arguments
 */
@Slf4j
public class ArgumentHelper {
    @Getter private final String[] args;
    @Getter private final Namespace ns;

    /**
     * Parse the main arguments
     * If the list network flag is specified, a list of internet interface will be displayed and the program will exit
     * @param args The main arguments
     * @param serverName The server name
     */
    public ArgumentHelper(@NonNull final String[] args, @NonNull final String serverName) {
        this(args, serverName, null);
    }

    /**
     * Parse the main arguments
     * If the list network flag is specified, a list of internet interface will be displayed and the program will exit
     * @param args The main arguments
     * @param serverName The server name
     * @param baseParser If you want to extend the default parser with other flags
     */
    public ArgumentHelper(@NonNull final String[] args,
                          @NonNull final String serverName,
                          final ArgumentParser baseParser) {
        this.args = args;

        final ArgumentParser parser;
        // Argument Parser
        if (baseParser == null) {
            parser = ArgumentParsers.newFor(serverName).build()
                    .defaultHelp(true)
                    .description("SiLA Server.");
        } else {
            parser = baseParser;
        }
        // Port Argument (optional)
        parser.addArgument("-p", "--port")
                .type(Integer.class)
                .help("Specify port to use.");
        // Host Argument (optional)
        parser.addArgument("-H", "--host")
                .type(String.class)
                .help("Specify the host on which the SiLA Server will be running.");
        // Network Interface Argument
        parser.addArgument("-n", "--networkInterface")
                .type(String.class)
                .help(
                    "Specify network interface. Check using the \"ifconfig\" command on Linux or MacOS and \"ipconfig\" on Windows."
                );
        // Network discovery
        parser.addArgument("-d", "--discovery")
                .type(Arguments.booleanType("yes", "no"))
                .setDefault(true)
                .help("Enable or disable network discovery. ");

        // Server config file holding UUID and ServerName
        parser.addArgument("-c", "--configFile")
                .type(String.class)
                .help("Specify the file name to use to read/store server information.");
        // Only list System Information
        parser.addArgument("-l", "--listNetworks")
                .type(Arguments.booleanType("yes", "no"))
                .setDefault(false)
                .help("List names of network interfaces found.");
        // Encrypt communication or not
        parser.addArgument("-u", "--unsafeCommunication")
            .type(Arguments.booleanType("yes", "no"))
            .setDefault(false)
            .help("Use non-standard plain-text insecure communication.");
        parser.addArgument("-s", "--simulation")
                .type(Arguments.booleanType("yes", "no"))
                .setDefault(false)
                .help("Specify (if supported) to start the server with simulation mode enabled.");
        parser.addArgument("-v", "--version")
                .type(Arguments.booleanType("yes", "no"))
                .setDefault(false)
                .help("Display server version.");
        parser.addArgument("-k", "--privateKey")
                .type(String.class)
                .setDefault("./private.pem")
                .help("Path to Private key (e.g. 'private.pem')");
        parser.addArgument("-cr", "--certificate")
                .type(Arguments.booleanType("yes", "no"))
                .type(String.class)
                .setDefault("./cert.pem")
                .help("Path to Certificate (e.g. 'cert.pem')");
        parser.addArgument("-crp", "--certificatePassword")
                .type(Arguments.booleanType("yes", "no"))
                .type(String.class)
                .help("Password to unlock the certificate.");
        this.ns = parser.parseArgsOrFail(args);
        if (ns.getBoolean("listNetworks")) {
            ListNetworkInterfaces.display();
            System.exit(0);
        }
        if (ns.getBoolean("version")) {
            try {
                System.out.println(new GitRepositoryState().generateVersion());
                System.exit(0);
            } catch (IOException e) {
                log.warn("Unable to retrieve version", e);
                System.out.println("Unknown version");
                System.exit(0);
            }
        }
    }

    /**
     * Get server port
     * @return the server port
     */
    public Optional<Integer> getPort() {
        final Integer port = ns.getInt("port");
        return (port == null) ? Optional.empty() : Optional.of(port);
    }

    /**
     * Get network interface (used for discovery)
     * @return the network interface
     */
    public Optional<String> getInterface() {
        final String networkInterface = ns.getString("networkInterface");
        return (networkInterface == null) ? Optional.empty() : Optional.of(networkInterface);
    }

    /**
     * Get server configuration file
     * @return the server configuration file
     */
    public Optional<Path> getConfigFile() {
        final String configFile = ns.getString("configFile");
        return (configFile == null) ? Optional.empty() : Optional.of(Paths.get(configFile));
    }

    /**
     * Get server private key file
     * @return the server private key file
     */
    public Path getPrivateKeyFile() {
        final String privateKey = ns.getString("privateKey");
        return Paths.get(privateKey);
    }

    /**
     * Get certificate password
     * @return the certiricate password
     */
    public String getCertificatePassword() {
        return ns.getString("certificatePassword");
    }

    /**
     * Get server certificate file
     * @return the server certificate file
     */
    public Path getCertificateFile() {
        final String certificate = ns.getString("certificate");
        return Paths.get(certificate);
    }

    /**
     * Get server host
     * @return the server host
     */
    public Optional<String> getHost() {
        final String host = ns.getString("host");
        return (host == null) ? Optional.empty() : Optional.of(host);
    }

    /**
     * Get server simulation mode
     * @return true to enable simulation mode
     */
    public boolean isSimulationEnabled() {
        return ns.getBoolean("simulation");
    }

    /**
     * Whether network discovery is enabled or not
     * @return true to enable network discovery
     */
    public boolean hasNetworkDiscovery() {
        return ns.getBoolean("discovery");
    }

    /**
     * Use unsafe communication
     * @return true to enable unsafe communication
     */
    public boolean useUnsafeCommunication() {
        return ns.getBoolean("unsafeCommunication");
    }
}
