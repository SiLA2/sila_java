package sila_java.library.server_base.binary_transfer.database;

/**
 * Exception class describing binary database exception
 */
public class BinaryDatabaseException extends Exception {

    /**
     * @inheritDoc
     */
    public BinaryDatabaseException() {
        super();
    }

    /**
     * @inheritDoc
     */
    public BinaryDatabaseException(String message) {
        super(message);
    }

    /**
     * @inheritDoc
     */
    public BinaryDatabaseException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * @inheritDoc
     */
    public BinaryDatabaseException(Throwable cause) {
        super(cause);
    }
}
