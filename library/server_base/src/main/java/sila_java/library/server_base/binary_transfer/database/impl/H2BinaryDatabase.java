package sila_java.library.server_base.binary_transfer.database.impl;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.h2.jdbcx.JdbcDataSource;
import sila_java.library.core.sila.binary_transfer.BinaryInfo;
import sila_java.library.server_base.binary_transfer.Binary;
import sila_java.library.server_base.binary_transfer.database.BinaryDatabaseException;
import sila_java.library.server_base.binary_transfer.database.BinaryDatabase;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.time.Duration;
import java.time.OffsetDateTime;
import java.util.UUID;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Persisted implementation of {@link BinaryDatabase} using H2 database
 */
@Slf4j
public class H2BinaryDatabase implements BinaryDatabase {
    /**
     * SQL query to create a "BINARIES" table inside a database
     */
    private static final String SQL_INIT = "CREATE TABLE IF NOT EXISTS BINARIES\n" +
            "(\n" +
            "    ID UUID NOT NULL PRIMARY KEY,\n" +
            "    PARAMETER_ID VARCHAR NOT NULL,\n" +
            "    EXPIRATION TIMESTAMP WITH TIME ZONE NOT NULL ,\n" +
            "    DATA BLOB NOT NULL\n" +
            ");\n";

    private static final String ADD_PARAMETER_ID_IF_MISSING =
            "ALTER TABLE BINARIES ADD COLUMN IF NOT EXISTS PARAMETER_ID VARCHAR;\n";
    private static final String DB_USER = "sa";
    private static final String DB_PASS = "sa";
    private static final int CLEANUP_CHECK_INTERVAL_SEC = 300;
    private static final Duration BINARY_EXTEND_DURATION = Duration.ofMinutes(10);
    private final ScheduledExecutorService cleaner = Executors.newSingleThreadScheduledExecutor();
    private final Connection connection;

    /**
     * Construct a {@link BinaryDatabase} for a specific server.
     *
     * The default credentials are {@link H2BinaryDatabase#DB_USER database user} {@link H2BinaryDatabase#DB_PASS database password}
     * The default binary expiration is {@link H2BinaryDatabase#BINARY_EXTEND_DURATION seconds}
     *
     * @param serverId The server UUID
     * @throws SQLException raised if the {@link H2BinaryDatabase#SQL_INIT} query fails
     */
    public H2BinaryDatabase(@NonNull final UUID serverId) throws SQLException {
        final JdbcDataSource ds = new JdbcDataSource();
        final String binaryDbPath = "jdbc:h2:~/.sila/binary/" + serverId + ";DB_CLOSE_ON_EXIT=FALSE";
        ds.setURL(binaryDbPath);
        log.info("binary DB persisted at: {}", binaryDbPath);
        ds.setUser(DB_USER);
        ds.setPassword(DB_PASS);
        this.connection = ds.getConnection();
        try (final Statement statement = connection.createStatement()) {
            statement.addBatch(SQL_INIT);
            statement.addBatch(ADD_PARAMETER_ID_IF_MISSING);
            statement.executeBatch();
        } catch (SQLException e) {
            connection.close();
            throw e;
        }
        this.cleaner.scheduleAtFixedRate(() -> {
            try {
                purgeExpiredBinaries();
            } catch (BinaryDatabaseException e) {
                log.warn("Following exception occurred while purging expired binaries: {}", e.getMessage(), e);
            }
        }, 0, CLEANUP_CHECK_INTERVAL_SEC, TimeUnit.SECONDS);
    }

    /**
     * Cleanup method
     *
     * Delete expired binaries, close the database and shutdown the {@link H2BinaryDatabase#cleaner cleaner worker}
     */
    @Override
    public void close() {
        if (!this.cleaner.isTerminated() && !this.cleaner.isShutdown())  {
            this.cleaner.shutdownNow();
        }
        try {
            if (this.connection.isClosed()) {
                log.info("Connection is already closed");
                return;
            }
        } catch (SQLException e) {
            log.warn("Failed to check connection state", e);
        }
        try {
            this.purgeExpiredBinaries();
        } catch (BinaryDatabaseException e) {
            log.warn("Error occurred while removing binaries from database: {}", e.getMessage(), e);
        }
        try {
            this.connection.close();
        } catch (SQLException e) {
            log.warn("Exception occurred while closing db connection", e);
        }
    }

    /**
     * Retrieve a binary from the database
     *
     * @param binaryTransferUuid the binary identifier
     * @return The binary if it exists
     *
     * @throws BinaryDatabaseException if the binary does not exist or the database connection failed
     */
    @Override
    public Binary getBinary(@NonNull final UUID binaryTransferUuid) throws BinaryDatabaseException {
        final String query = "SELECT ID, PARAMETER_ID, EXPIRATION, DATA, octet_length(BINARIES.DATA) AS BYTE_SIZE FROM BINARIES WHERE ID = ?";
        try (final PreparedStatement preparedStatement = this.connection.prepareStatement(query)) {
            preparedStatement.setString(1, binaryTransferUuid.toString());
            preparedStatement.execute();
            final ResultSet resultSet = preparedStatement.getResultSet();
            if (!resultSet.first()) {
                throw new BinaryDatabaseException("No binary found with id " + binaryTransferUuid);
            }
            return new Binary(resultSet.getBlob("DATA"), getBinaryInfo(resultSet));
        } catch (SQLException e) {
            throw new BinaryDatabaseException(e);
        }
    }

    /**
     * Check if the specified binary transfer UUID exists
     *
     * @param binaryTransferUuid the binary identifier
     * @return The binary if it exists
     *
     * @throws BinaryDatabaseException if the binary does not exist or the database connection failed
     */
    @Override
    public boolean hasBinary(UUID binaryTransferUuid) throws BinaryDatabaseException {
        final String query = "SELECT ID FROM BINARIES WHERE ID = ?";
        try (final PreparedStatement preparedStatement = this.connection.prepareStatement(query)) {
            preparedStatement.setString(1, binaryTransferUuid.toString());
            preparedStatement.execute();
            return preparedStatement.getResultSet().first();
        } catch (SQLException e) {
            throw new BinaryDatabaseException(e);
        }
    }

    /**
     * Retrieve a binary {@link BinaryInfo binary info}
     *
     * @param binaryTransferUuid the binary identifier
     * @return The binary if it exists
     *
     * @throws BinaryDatabaseException if the binary does not exist or the database connection failed
     */
    @Override
    public BinaryInfo getBinaryInfo(@NonNull final UUID binaryTransferUuid) throws BinaryDatabaseException {
        final String query = "SELECT ID, PARAMETER_ID, EXPIRATION, octet_length(BINARIES.DATA) AS BYTE_SIZE FROM BINARIES WHERE ID = ?";
        try (final PreparedStatement preparedStatement = this.connection.prepareStatement(query)) {
            preparedStatement.setString(1, binaryTransferUuid.toString());
            preparedStatement.execute();
            final ResultSet resultSet = preparedStatement.getResultSet();
            if (!resultSet.first()) {
                throw new BinaryDatabaseException("No blob found with id " + binaryTransferUuid);
            }
            return getBinaryInfo(resultSet);
        } catch (SQLException e) {
            throw new BinaryDatabaseException(e);
        }
    }

    /**
     * Save a binary stream to the database
     *
     * @param binaryTransferUuid the binary identifier
     * @param bytes The data stream to save in the database
     * @param parameterIdentifier The fully qualified parameter identifier
     * @return The duration left until the binary expires
     *
     * @throws BinaryDatabaseException if the binary could not be saved
     */
    @Override
    public Duration addBinary(
            @NonNull final UUID binaryTransferUuid,
            final byte[] bytes,
            @NonNull final String parameterIdentifier
    ) throws BinaryDatabaseException {
        try (final InputStream byteStream = new ByteArrayInputStream(bytes)) {
            return addBinary(binaryTransferUuid, byteStream, parameterIdentifier);
        } catch (IOException e) {
            // should never happen, ByteArrayInputStream does not perform I/O
            throw new RuntimeException(e);
        }
    }

    /**
     * Save a binary stream to the database
     *
     * @param binaryTransferUuid the binary identifier
     * @param stream The data stream to save in the database
     * @param parameterIdentifier The fully qualified parameter identifier
     * @return The duration left until the binary expires
     *
     * @throws BinaryDatabaseException if the binary could not be saved
     */
    @Override
    public Duration addBinary(
            @NonNull final UUID binaryTransferUuid,
            @NonNull final InputStream stream,
            @NonNull final String parameterIdentifier
    ) throws BinaryDatabaseException {
        final String query = "INSERT INTO BINARIES (ID, PARAMETER_ID, EXPIRATION, DATA) VALUES (?, ?, ?, ?)";
        try (final PreparedStatement preparedStatement = this.connection.prepareStatement(query)) {
            preparedStatement.setObject(1, binaryTransferUuid);
            final OffsetDateTime expirationDate = OffsetDateTime.now().plus(BINARY_EXTEND_DURATION);
            preparedStatement.setObject(2, parameterIdentifier);
            preparedStatement.setObject(3, expirationDate);
            preparedStatement.setBinaryStream(4, stream);
            preparedStatement.execute();
            return Duration.between(OffsetDateTime.now(), expirationDate);
        } catch (SQLException e) {
            throw new BinaryDatabaseException(e);
        }
    }

    /**
     * Extend the binary expiration duration by {@link H2BinaryDatabase#BINARY_EXTEND_DURATION}
     * @param binaryTransferUuid The binary identifier
     * @return The extended duration left until the binary expires
     *
     * @throws BinaryDatabaseException if the binary could not be extended
     */
    @Override
    public Duration extendBinaryExpiration(@NonNull final UUID binaryTransferUuid) throws BinaryDatabaseException {
        final String query = "UPDATE BINARIES SET EXPIRATION = ? WHERE ID = ?";
        try (final PreparedStatement preparedStatement = this.connection.prepareStatement(query)) {
            final OffsetDateTime newExpiration = getBinaryInfo(binaryTransferUuid).getExpiration().plus(BINARY_EXTEND_DURATION);
            preparedStatement.setObject(1, newExpiration);
            preparedStatement.setObject(2, binaryTransferUuid);
            preparedStatement.execute();
            return Duration.between(OffsetDateTime.now(), newExpiration);
        } catch (SQLException e) {
            throw new BinaryDatabaseException(e);
        }
    }

    /**
     * Remove the BINARIES table and hence all binaries from the database
     *
     * @throws BinaryDatabaseException If the removal of the binaries failed
     */
    @Override
    public void removeAllBinaries() throws BinaryDatabaseException {
        final String query = "delete from BINARIES";
        try (final PreparedStatement preparedStatement = this.connection.prepareStatement(query)) {
            preparedStatement.execute();
        } catch (SQLException e) {
            throw new BinaryDatabaseException(e);
        }
    }

    /**
     * Remove the specified binary from the database
     * @param binaryTransferUuid The binary identifier to remove
     *
     * @throws BinaryDatabaseException If to binary removal failed
     */
    @Override
    public void removeBinary(@NonNull final UUID binaryTransferUuid) throws BinaryDatabaseException {
        final String query = "delete from BINARIES where ID = ?";
        try (final PreparedStatement preparedStatement = this.connection.prepareStatement(query)) {
            preparedStatement.setObject(1, binaryTransferUuid);
            preparedStatement.execute();
        } catch (SQLException e) {
            throw new BinaryDatabaseException(e);
        }
    }

    /**
     * Remove expired binaries from the database
     *
     * @throws BinaryDatabaseException If the removal of the expired binaries failed
     */
    private void purgeExpiredBinaries() throws BinaryDatabaseException {
        final String query = "delete from BINARIES where EXPIRATION < ?";
        try (final PreparedStatement preparedStatement = this.connection.prepareStatement(query)) {
            preparedStatement.setObject(1, OffsetDateTime.now());
            final int nbRemoved = preparedStatement.executeUpdate();
            if (nbRemoved > 0) {
                log.info("Purged {} expired binary.", nbRemoved);
            }
        } catch (SQLException e) {
            throw new BinaryDatabaseException(e);
        }
    }

    /**
     * Get the binary information from a {@link ResultSet result set}
     * @param resultSet The result set to retrieve the {@link BinaryInfo binary information}
     *
     * @return The binary information of the specified result set if present
     *
     * @throws SQLException If the binary info could be not be retrieved
     */
    private static BinaryInfo getBinaryInfo(ResultSet resultSet) throws SQLException {
        final UUID id = UUID.fromString(resultSet.getString("ID"));
        final OffsetDateTime expiration = resultSet.getObject("EXPIRATION", OffsetDateTime.class);
        final String parameterId = resultSet.getObject("PARAMETER_ID", String.class);
        final long byteSize = resultSet.getLong("BYTE_SIZE");
        return new BinaryInfo(id, expiration, parameterId, byteSize);
    }
}
