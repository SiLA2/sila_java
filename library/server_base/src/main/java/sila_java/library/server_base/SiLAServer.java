package sila_java.library.server_base;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import io.grpc.*;
import lombok.Getter;
import lombok.NonNull;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import sila_java.library.core.discovery.SiLAServerRegistration;
import sila_java.library.core.encryption.SelfSignedCertificate;
import sila_java.library.core.sila.mapping.feature.FeatureGenerator;
import sila_java.library.core.sila.mapping.feature.MalformedSiLAFeature;
import sila_java.library.core.sila.utils.FullyQualifiedIdentifierUtils;
import sila_java.library.server_base.binary_transfer.database.BinaryDatabaseInjector;
import sila_java.library.server_base.config.IServerConfigWrapper;
import sila_java.library.server_base.config.NonPersistentServerConfigWrapper;
import sila_java.library.server_base.config.PersistentServerConfigWrapper;
import sila_java.library.server_base.config.ServerConfiguration;
import sila_java.library.server_base.identification.ServerInformation;
import sila_java.library.server_base.binary_transfer.database.BinaryDatabase;
import sila_java.library.server_base.binary_transfer.database.impl.H2BinaryDatabase;
import sila_java.library.server_base.binary_transfer.download.DownloadService;
import sila_java.library.server_base.binary_transfer.upload.UploadService;
import sila_java.library.server_base.metadata.MetadataExtractingInterceptor;
import sila_java.library.server_base.standard_features.v1.AuthorizationController;
import sila_java.library.server_base.standard_features.v1.AuthorizationController.Authorize;
import sila_java.library.server_base.standard_features.FeatureImplementation;
import sila_java.library.server_base.standard_features.v1.SiLAServiceServer;
import sila_java.library.server_base.utils.TransmitThrowableInterceptor;

import javax.annotation.Nullable;
import java.io.*;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.*;
import java.security.cert.*;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

import static sila_java.library.core.encryption.EncryptionUtils.*;
import static sila_java.library.core.utils.SocketUtils.getAvailablePortInRange;

/**
 * SiLA Server
 */
@Slf4j
public class SiLAServer implements AutoCloseable {
    private final static int SHUTDOWN_TIMEOUT = 20; // [s]
    private final static String DEFAULT_NETWORK_INTERFACE = "local";
    private final SiLAServerRegistration serverRegistration;
    private final Server server;
    @Getter
    private final BinaryDatabase binaryDatabaseImpl;
    /**
     * Whether the server owns the binary database and hence is responsible for closing it
     */
    @Getter
    private final boolean ownsBinaryDatabase;

    @Getter
    private final Optional<DownloadService> downloadService;
    @Getter
    private final Optional<UploadService> uploadService;

    @Getter
    private final Authorize authorize;

    private final Thread shutdownHook = new Thread(this::close);
    private final Multimap<String, String> affectedByMetadata;

    /**
     * Create and start a Base Server to expose the SiLA Features
     *
     * @param builder The builder
     *
     * @implNote Instance can only withoutConfig one server at a time.
     */
    private SiLAServer(@NonNull final Builder builder) throws IOException {
        final IServerConfigWrapper serverConfigurationWrapper = builder.getNewServerConfigurationWrapper();
        this.affectedByMetadata = builder.affectedByMetadata;
        this.authorize = builder.authorize == null ? AuthorizationController.DEFAULT_AUTHORIZE : builder.authorize;

        final SiLAServiceServer siLAServiceServer = new SiLAServiceServer(
                serverConfigurationWrapper,
                builder.serverInformation,
                builder.featureDefinitions
        );
        final InetAddress inetAddress = (builder.host != null) ? InetAddress.getByName(builder.host) : null;
        log.info("Server registered on SiLAService with features={}", builder.featureDefinitions.keySet());

        final UUID serverUuid = serverConfigurationWrapper.getCacheConfig().getUuid();

        if (builder.discovery && builder.interfaceName == null && builder.host == null) {
            builder.interfaceName = DEFAULT_NETWORK_INTERFACE;
        }

        if (builder.discovery && builder.interfaceName != null && builder.host != null) {
            throw new RuntimeException("Discovery requires either a host or interface name but both were provided");
        }

        if (builder.privateKeyFile != null && builder.certificateFile != null) {
            final File privateKeyFile = builder.privateKeyFile.toFile();
            final File certificateFile = builder.certificateFile.toFile();
            final boolean privateKeyFileExist = Files.exists(builder.privateKeyFile);
            final boolean certificateFileExist = Files.exists(builder.certificateFile);
            if (!privateKeyFileExist && certificateFileExist) {
                throw new RuntimeException("Certificate was provided, but couldn't find Private Key");
            }
            if (!certificateFileExist && privateKeyFileExist) {
                throw new RuntimeException("Private Key was provided, but couldn't find Certificate");
            }
            if (privateKeyFileExist && certificateFileExist) {
                builder.privateKey = readPrivateKey(privateKeyFile, builder.certificatePassword);
                builder.certificate = readCertificate(certificateFile);
            } else {
                privateKeyFile.getParentFile().mkdirs();
                privateKeyFile.createNewFile();
                certificateFile.getParentFile().mkdirs();
                certificateFile.createNewFile();
            }
        }

        if (builder.unsafeCommunication) {
            log.warn("Using plain-text communication forbidden by the SiLA 2 Standard !!");
        }
        if (builder.certificate == null && builder.privateKey == null) {
            log.info("No certificate provided, creating and using self signed certificate");
            final SelfSignedCertificate selfSignedCertificate;
            try {
                String serverIp = null;
                if (inetAddress != null) {
                    serverIp = inetAddress.getHostAddress();
                } else if (builder.interfaceName != null) {
                    try (final SiLAServerRegistration tmpRegsitration = SiLAServerRegistration.newInstanceFromInterface(
                            serverUuid,
                            builder.interfaceName,
                            builder.port,
                            null
                    )) {
                        serverIp = tmpRegsitration.findInetAddress().getHostAddress();
                    } catch (Exception e) {
                        log.warn("Failed to retrieve server ip for discovery!", e);
                    }
                }
                log.info("Creating self signed certificate for server uuid {} and ip {}", serverUuid, serverIp);
                selfSignedCertificate = SelfSignedCertificate
                        .newBuilder()
                        .withServerUUID(serverUuid)
                        .withServerIP(serverIp)
                        .build();
            } catch (SelfSignedCertificate.CertificateGenerationException e) {
                throw new RuntimeException(e);
            }
            builder.certificate = selfSignedCertificate.getCertificate();
            builder.privateKey = selfSignedCertificate.getPrivateKey();
            if (builder.certificateFile != null) {
                writeCertificateToFile(builder.certificateFile.toFile(), builder.certificate);
                log.info("Wrote certificate to {}", builder.certificateFile);
            }
            if (builder.privateKeyFile != null) {
                log.info("Wrote private key to {}", builder.privateKeyFile);
                writePrivateKeyToFile(builder.privateKeyFile.toFile(), builder.privateKey);
            }
        }

        final ServerCredentials serverCredentials;
        // If cert and key we use transport security
        if (builder.certificate != null && builder.privateKey != null && !builder.unsafeCommunication) {
            try (InputStream certChainStream = certificateToStream(builder.certificate);
                 InputStream keyStream = keyToStream(builder.privateKey)) {
                serverCredentials = TlsServerCredentials.create(certChainStream, keyStream);
            } catch (final CertificateException e) {
                throw new IOException(e);
            }
            log.info("Server will use safe encrypted communication.");
        } else {
            serverCredentials = InsecureServerCredentials.create();
            log.warn("Server will use deprecated unsafe plain-text communication.");
        }

        final ServerBuilder<?> serverBuilder = Grpc.newServerBuilderForPort(builder.port, serverCredentials)
                .addService(siLAServiceServer.getService());

        this.ownsBinaryDatabase = builder.customBinaryDatabaseProvider == null;

        if (builder.binaryTransferSupport) {
            log.info("Server will support binary transfer");
            this.binaryDatabaseImpl = builder.getNewBinaryDatabase(serverUuid);
            this.downloadService = Optional.of(new DownloadService(this.binaryDatabaseImpl, this.authorize));
            this.uploadService = Optional.of(
                    new UploadService(
                            this.binaryDatabaseImpl,
                            siLAServiceServer.getFeatureDefinitions().values(),
                            this.affectedByMetadata,
                            this.authorize
                    )
            );
            serverBuilder.addService(this.downloadService.get());
            serverBuilder.addService(this.uploadService.get());
            if (builder.addBinaryDatabaseInjector) {
                serverBuilder.intercept(new BinaryDatabaseInjector(this.binaryDatabaseImpl));
            }
        } else {
            this.binaryDatabaseImpl = null;
            log.warn("Server will not support binary transfer");
            this.downloadService = Optional.empty();
            this.uploadService = Optional.empty();
        }

        builder.bindableServices.forEach(serverBuilder::addService);
        builder.serverServices.forEach(serverBuilder::addService);

        if (builder.discovery) {
            final String certificate = (builder.certificate != null && !builder.unsafeCommunication) ? writeCertificateToString(builder.certificate) : null;
            if (builder.interfaceName != null) {
                serverRegistration = SiLAServerRegistration.newInstanceFromInterface(
                        serverUuid,
                        builder.interfaceName,
                        builder.port,
                        certificate
                );
                log.info("Server registering with discovery on interface {}.", builder.interfaceName);
            } else if (inetAddress != null) {
                serverRegistration = SiLAServerRegistration.newInstanceFromHost(
                        serverUuid,
                        inetAddress,
                        builder.port,
                        certificate
                );
                log.info("Server registering with discovery on host {}.", builder.host);
            } else {
                throw new RuntimeException("Discovery requires either a valid host or interface!");
            }
        } else {
            serverRegistration = null;
            log.warn("Server starting without specifying a network interface, discovery is not enabled.");
        }

        builder.interceptors.forEach(serverInterceptor -> {
            serverBuilder.intercept(serverInterceptor);
            log.info("Added interceptor of type " + serverInterceptor.getClass().getTypeName());
        });

        this.server = serverBuilder.build().start();
        Runtime.getRuntime().addShutdownHook(this.shutdownHook);

        log.info("Server started on {}:{}", builder.host == null ? "*" : builder.host, builder.port);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void close() {
        try {
            Runtime.getRuntime().removeShutdownHook(this.shutdownHook);
        } catch (IllegalStateException | SecurityException e) {
            log.debug("Failed to remove shutdown hook", e);
        }

        log.info("[stop] stopping server...");
        if (this.serverRegistration != null) {
            this.serverRegistration.close();
        }

        // Stop server
        if (!this.server.isTerminated() && !this.server.isShutdown()) {
            try {
                log.info("[stop] stopping the server ...");
                this.server.shutdownNow().awaitTermination(SHUTDOWN_TIMEOUT, TimeUnit.SECONDS);
                log.info("[stop] the server was stopped");
            } catch (InterruptedException e) {
                log.warn("[stop] could not shutdown the server within {} seconds", SHUTDOWN_TIMEOUT);
            }
            log.info("[stop] stopped");
        } else {
            log.info("[stop] server already stopped");
        }
        if (this.binaryDatabaseImpl != null && this.ownsBinaryDatabase) {
            try {
                this.binaryDatabaseImpl.close();
            } catch (Exception e) {
                log.warn("Error occurred while closing binary database {}", e.getMessage(), e);
            }
        }
    }

    /**
     * Await termination of the server.
     */
    @SneakyThrows
    public void blockUntilShutdown() {
        if (!this.server.isShutdown() && !this.server.isTerminated()) {
            this.server.awaitTermination();
        }
    }

    /**
     * Server Builder
     */
    public static class Builder {
        private static int[] defaultPortRange = {50052, 50052+256};

        // Mandatory Arguments
        private final ServerInformation serverInformation;

        private final Map<String, String> featureDefinitions = new HashMap<>();
        private final List<BindableService> bindableServices = new ArrayList<>();
        private final List<ServerServiceDefinition> serverServices = new ArrayList<>();

        // Optional Arguments
        private String interfaceName = null;
        private boolean discovery = true;
        private Integer port = null;
        private String host = null;

        // Config
        @Getter
        private ConfigWrapperProvider customServerConfigProvider = null;
        private Path persistentConfigFile = Paths.get("./server.cfg");
        private boolean persistedConfig = false;
        private String name = null;
        private UUID uuid = null;

        private ServerConfiguration defaultServerConfiguration;

        // Binary Transfer
        private boolean binaryTransferSupport = false;
        @Getter
        private BinaryDatabaseProvider customBinaryDatabaseProvider = null;

        // Authorization

        @Getter
        private Authorize authorize = null;


        // TLS
        private Path certificateFile = null;
        private String certificatePassword;

        private Path privateKeyFile = null;
        private X509Certificate certificate = null;
        private PrivateKey privateKey = null;

        private boolean unsafeCommunication = false;
        private final List<ServerInterceptor> interceptors = new ArrayList<>();
        private boolean addBinaryDatabaseInjector;

        private final Multimap<String, String> affectedByMetadata = ArrayListMultimap.create();

        @FunctionalInterface
        public interface ConfigWrapperProvider {
            IServerConfigWrapper get(
                    @NonNull final Path configPath, @NonNull final ServerConfiguration serverConfiguration
            );
        }

        @FunctionalInterface
        public interface BinaryDatabaseProvider {
            BinaryDatabase get(@NonNull final UUID serverUUID) throws SQLException;
        }

        public Map<String, String> getFeatureDefinitions() {
            return Collections.unmodifiableMap(this.featureDefinitions);
        }

        /**
         * Enables or disables Network Discovery
         * @param isEnabled true to enable network discovery
         */
        public Builder withDiscovery(@NonNull final boolean isEnabled) {
            this.discovery = isEnabled;
            return this;
        }

        /**
         * Disables Network Discovery
         */
        public Builder withoutDiscovery() {
            return this.withDiscovery(false);
        }

        /**
         * Enables Discovery on a certain interface
         * @param interfaceName  Name of network interface to use discovery
         */
        public Builder withNetworkInterface(@NonNull final String interfaceName) {
            this.interfaceName = interfaceName;
            return this;
        }

        /**
         * Define Specific Port for the services (otherwise a default range will be chosen)
         * @param port Port on which the server runs.
         */
        public Builder withPort(int port) {
            this.port = port;
            return this;
        }

        /**
         * Define the host or IP address on which the services will be exclusively served on.
         * @param host host or IP address on which the server runs.
         */
        public Builder withHost(@Nullable final String host) {
            this.host = host;
            return this;
        }

        /**
         * Define the server name. Setting this will override previously persisted server name.
         * @return this builder
         */
        public Builder withName(@Nullable final String name) {
            this.name = name;
            return this;
        }

        /**
         * Define the server UUID. Setting this will override previously persisted server UUID.
         * @return this builder
         */
        public Builder withUUID(@Nullable final UUID uuid) {
            this.uuid = uuid;
            return this;
        }

        /**
         * Define the default server configuration. Setting this will not override previously persisted server configuration.
         * @return this builder
         */
        public Builder withDefaultServerConfiguration(@Nullable final ServerConfiguration defaultServerConfiguration) {
            this.defaultServerConfiguration = defaultServerConfiguration;
            return this;
        }

        /**
         * @deprecated Unsafe plain-text communication is forbidden by the SiLA 2 Standard
         */
        public Builder withUnsafeCommunication(final boolean unsafeCommunication) {
            this.unsafeCommunication = unsafeCommunication;
            return this;
        }

        /**
         * Enable or disable support for binary transfer
         * @return this builder
         */
        @SneakyThrows
        public Builder withBinaryTransferSupport(final boolean binaryTransferSupport) {
            this.binaryTransferSupport = binaryTransferSupport;
            return this;
        }

        /**
         * Add a {@link BinaryDatabaseInjector} to the server so the {@link BinaryDatabase} is easily accessible to
         * feature implementations
         *
         * @return this builder
         */
        public Builder withBinaryDatabaseInjector() {
            this.addBinaryDatabaseInjector = true;
            return this;
        }

        /**
         * Enable support for binary transfer
         * @return this builder
         */
        @SneakyThrows
        public Builder withBinaryTransferSupport() {
            return this.withBinaryTransferSupport(true);
        }

        /**
         * Allows to set a custom binary database instead of the default one.
         * The provider is responsible for closing the database.
         *
         * @param customBinaryDatabaseProvider The {@link BinaryDatabaseProvider} to set
         * @return this builder
         */
        public Builder withCustomBinaryDatabaseProvider(@Nullable final BinaryDatabaseProvider customBinaryDatabaseProvider) {
            this.customBinaryDatabaseProvider = customBinaryDatabaseProvider;
            return this;
        }

        /**
         * Allows to set a custom authorize to authorize access to the server feature(s) instead of the default one.
         *
         * @param authorize The {@link Authorize} to set
         * @return this builder
         */
        public Builder withCustomAuthorizer(@Nullable final Authorize authorize) {
            this.authorize = authorize;
            return this;
        }

        /**
         * Add a Feature to be exposed
         * @param featureDescription Feature Description as String Content in XML
         * @param featureService Feature Service implemented with gRPC
         *
         * @implNote Unfortunately there is no straightforward way to validate the pure protobuf
         * descriptions and the generated gRPC descriptions.
         */
        @SneakyThrows({IOException.class, MalformedSiLAFeature.class})
        public Builder addFeature(
                @NonNull final String featureDescription,
                @NonNull final BindableService featureService
        ) {
            featureDefinitions.put(
                    FeatureGenerator.generateFullyQualifiedIdentifier(
                            FeatureGenerator.generateFeature(featureDescription)
                    ),
                    featureDescription
            );
            bindableServices.add(featureService);
            return this;
        }

        /**
         * Very early stage API to specify FCP Affected by Metadata will most likely be changed in the future
         * @param FCPAffectedByMetadata The fully qualified feature or command or property identifier affected
         * @param FQIMetadata The fully qualified metadata affecting the feature or command or property
         * @return this builder
         */
        public Builder addFCPAffectedByMetadata(
                @NonNull final String FCPAffectedByMetadata,
                @NonNull final String ...FQIMetadata
        ) {
            boolean isValidFQI = Stream.of(
                    FullyQualifiedIdentifierUtils.FullyQualifiedFeatureIdentifierPattern,
                    FullyQualifiedIdentifierUtils.FullyQualifiedCommandIdentifierPattern,
                    FullyQualifiedIdentifierUtils.FullyQualifiedPropertyIdentifierPattern
            ).anyMatch(p -> p.matcher(FCPAffectedByMetadata).matches());
            if (!isValidFQI) {
                throw new RuntimeException("A valid FCP must be provided.");
            }
            for (final String metadata : FQIMetadata) {
                if (!FullyQualifiedIdentifierUtils.FullyQualifiedMetadataIdentifierPattern.matcher(metadata).matches()) {
                    throw new RuntimeException("A valid fully qualified metadate identifier must be provided.");
                }
                this.affectedByMetadata.put(FCPAffectedByMetadata, metadata);
            }
            return this;
        }

        /**
         * Very early stage API to remove FCP Affected by Metadata will most likely be changed in the future
         * @param FCPAffectedByMetadata The fully qualified feature or command or property identifier affected
         * @param FQIMetadata The fully qualified metadata affecting the feature or command or property
         * @return this builder
         */
        public Builder removeFCPAffectedByMetadata(
                @NonNull final String FCPAffectedByMetadata,
                @NonNull final String ...FQIMetadata
        ) {
            for (final String metadata : FQIMetadata) {
                this.affectedByMetadata.remove(FCPAffectedByMetadata,metadata);
            }
            return this;
        }


        /**
         * Add a Feature to be exposed
         * @param featureImplementation Exposing both the description and implementation
         */
        @SneakyThrows({IOException.class, MalformedSiLAFeature.class})
        public Builder addFeature(@NonNull final FeatureImplementation featureImplementation) {
            featureDefinitions.put(
                    FeatureGenerator.generateFullyQualifiedIdentifier(
                            FeatureGenerator.generateFeature(featureImplementation.getFeatureDescription())
                    ),
                    featureImplementation.getFeatureDescription()
            );
            bindableServices.add(featureImplementation.getService());
            return this;
        }

        /**
         * Add a Feature to be exposed
         * @param featureDescription Feature Description as String Content in XML
         * @param serverServiceFeatureDefinition Server service definition feature implemented with gRPC
         *
         */
        @SneakyThrows({IOException.class, MalformedSiLAFeature.class})
        public Builder addFeature(
                @NonNull final String featureDescription,
                @NonNull final ServerServiceDefinition serverServiceFeatureDefinition
        ) {
            featureDefinitions.put(
                    FeatureGenerator.generateFullyQualifiedIdentifier(
                            FeatureGenerator.generateFeature(featureDescription)
                    ),
                    featureDescription
            );
            serverServices.add(serverServiceFeatureDefinition);
            return this;
        }

        /**
         * Starts and Creates the SiLA Server
         * @return SiLA Server
         */
        public SiLAServer start() throws IOException {
            if (this.port == null) {
                this.port = getAvailablePortInRange(defaultPortRange[0], defaultPortRange[1]);
            }

            return new SiLAServer(this);
        }

        /**
         * Use TLS certification
         * @param certChain InputStream certification
         * @param privateKey InputStream private key
         */
        public Builder withTLS(
                @NonNull final X509Certificate certChain,
                @NonNull final PrivateKey privateKey
        ) {
            if (this.certificateFile != null || this.privateKeyFile != null) {
                throw new RuntimeException("Cannot use Persistent TLS and Runtime TLS");
            }
            this.certificate = certChain;
            this.privateKey = privateKey;
            return this;
        }

        /**
         * Use persistent TLS credentials. Will create a self-signed certificate if both file are missing
         * @param certificateFile Path to the private key PEM file
         * @param privateKeyFile Path to the certificate PEM file
         * @param certificatePassword Password of the certificate if any
         */
        public Builder withPersistentTLS(
                @NonNull final Path privateKeyFile,
                @NonNull final Path certificateFile,
                @Nullable final String certificatePassword
        ) {
            if (this.certificate != null || this.privateKey != null) {
                throw new RuntimeException("Cannot use Runtime TLS and Persisted TLS");
            }
            this.privateKeyFile = privateKeyFile;
            this.certificateFile = certificateFile;
            this.certificatePassword = certificatePassword;
            return this;
        }

        /**
         * Use persistent TLS credentials. Will create a self-signed certificate if both file are missing
         * @param certificateFile Path to the private key PEM file
         * @param privateKeyFile Path to the certificate PEM file
         */
        public Builder withPersistentTLS(@NonNull final Path privateKeyFile, @NonNull final Path certificateFile) {
            return this.withPersistentTLS(privateKeyFile, certificateFile, null);
        }

        /**
         * Add a gRPC {@link ServerInterceptor} to the server
         *
         * @param interceptor The {@link ServerInterceptor} to add
         * @return this builder
         */
        public Builder addInterceptor(@NonNull final ServerInterceptor interceptor) {
            this.interceptors.add(interceptor);
            return this;
        }

        /**
         * Allows to set a custom configuration wrapper instead of the default one
         *
         * @param customConfigWrapperProvider The {@link ConfigWrapperProvider} to set
         * @return this builder
         */
        public Builder withCustomConfigWrapperProvider(@Nullable final ConfigWrapperProvider customConfigWrapperProvider) {
            this.customServerConfigProvider = customConfigWrapperProvider;
            return this;
        }

        /**
         * Allows to specify whether the server configuration should be persisted.
         * If no config file is set via {@link SiLAServer.Builder#withPersistentConfigFile(Path)}, the file "server.cfg"
         * will be used.
         *
         * @param usePersistentConfig The boolean
         * @return this builder
         */
        public Builder withPersistentConfig(final boolean usePersistentConfig) {
            this.persistedConfig = usePersistentConfig;
            return this;
        }

        /**
         * Allows to specify whether the server configuration should be persisted.
         * If no config file is set via {@link SiLAServer.Builder#withPersistentConfigFile(Path)}, the file "server.cfg"
         * will be used.
         *
         * @return this builder
         */
        public Builder withPersistentConfig() {
            return this.withPersistentConfig(true);
        }

        /**
         * Allows to specify the file to save and or load the server configuration
         *
         * @param persistentConfigFile The path to save and or load the server configuration
         * @return this builder
         */
        public Builder withPersistentConfigFile(final Path persistentConfigFile) {
            this.persistentConfigFile = persistentConfigFile;
            this.persistedConfig = true;
            return this;
        }

        /**
         * Add a {@link MetadataExtractingInterceptor} to the server
         *
         * @return this builder
         *
         * @implNote The order of added interceptors is preserved. Thus, interceptors added with
         * {@link SiLAServer.Builder#addInterceptor(ServerInterceptor)} before this method precede the
         * {@link MetadataExtractingInterceptor} in the call handling chain.
         */
        public Builder withMetadataExtractingInterceptor() {
            return addInterceptor(new MetadataExtractingInterceptor());
        }

        /**
         * Create builder for a Server
         *
         * @param serverInformation     Meta server information defined by the server implementer
         */
        public static Builder newBuilder(@NonNull final ServerInformation serverInformation) throws IOException {
            return new Builder(serverInformation);
        }

        /**
         * Get a new server configuration wrapper instance
         * @return a new server configuration wrapper instance
         * @throws IOException
         */
        public IServerConfigWrapper getNewServerConfigurationWrapper() throws IOException {
            final ServerConfiguration serverConfiguration = new ServerConfiguration(
                    (this.name != null) ? this.name : this.defaultServerConfiguration.getName(),
                    (this.uuid != null) ? this.uuid : this.defaultServerConfiguration.getUuid()
            );

            final IServerConfigWrapper serverConfigWrapper;
            if (this.customServerConfigProvider != null) {
                serverConfigWrapper = this.customServerConfigProvider.get(
                        this.persistentConfigFile, serverConfiguration);
            } else if (this.persistedConfig) {
                serverConfigWrapper = new PersistentServerConfigWrapper(
                        this.persistentConfigFile, serverConfiguration);
            }
            else {
                serverConfigWrapper = new NonPersistentServerConfigWrapper(serverConfiguration);
            }

            if (this.persistedConfig && (this.uuid != null || this.name != null)) {
                // force override
                serverConfigWrapper.setConfig(serverConfiguration);
            }
            return serverConfigWrapper;
        }

        private BinaryDatabase getNewBinaryDatabase(@NonNull final UUID serverUUID) {
            try {
                if (this.customBinaryDatabaseProvider != null) {
                    return this.customBinaryDatabaseProvider.get(serverUUID);
                }
                return new H2BinaryDatabase(serverUUID);
            } catch (SQLException e) {
                log.warn("Error while setting BinaryDatabase: {}", e.getMessage(), e);
                throw new RuntimeException(e);
            }
        }

        @SneakyThrows
        private Builder(@NonNull final ServerInformation serverInformation) {
            this.serverInformation = serverInformation;
            this.interceptors.add(TransmitThrowableInterceptor.instance());
            this.defaultServerConfiguration = new ServerConfiguration(serverInformation.getType());
        }
    }
}
