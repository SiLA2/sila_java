package sila_java.library.server_base.binary_transfer.upload;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import io.grpc.StatusRuntimeException;
import io.grpc.stub.StreamObserver;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.BinaryUploadGrpc;
import sila2.org.silastandard.SiLABinaryTransfer;
import sila2.org.silastandard.SiLAFramework;
import sila_java.library.core.models.BasicType;
import sila_java.library.core.models.DataTypeType;
import sila_java.library.core.models.Feature;
import sila_java.library.core.models.SiLAElement;
import sila_java.library.core.sila.binary_transfer.BinaryInfo;
import sila_java.library.core.sila.errors.SiLAErrors;
import sila_java.library.core.sila.mapping.feature.FeatureGenerator;
import sila_java.library.core.sila.mapping.feature.MalformedSiLAFeature;
import sila_java.library.core.sila.types.SiLABinaryTransferUUID;
import sila_java.library.core.sila.types.SiLADuration;
import sila_java.library.core.sila.utils.FullyQualifiedIdentifierUtils;
import sila_java.library.server_base.binary_transfer.database.BinaryDatabase;
import sila_java.library.server_base.binary_transfer.database.BinaryDatabaseException;
import sila_java.library.core.sila.binary_transfer.BinaryTransferErrorHandler;
import sila_java.library.server_base.metadata.ServerMetadataContainer;
import sila_java.library.server_base.standard_features.v1.AuthorizationController;

import javax.annotation.Nullable;
import java.io.IOException;
import java.io.InputStream;
import java.time.Duration;
import java.util.*;
import java.util.regex.Matcher;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static sila_java.library.core.sila.mapping.feature.FeatureGenerator.generateFullyQualifiedIdentifier;
import static sila_java.library.server_base.metadata.MetadataExtractingInterceptor.SILA_METADATA_KEY;

/**
 * Service responsible for uploading binaries
 */
@Slf4j
public class UploadService extends BinaryUploadGrpc.BinaryUploadImplBase {
    static final int MAX_CHUNK_SIZE = 2097152;

    /**
     * The maximum lifetime of a binary upload
     */
    private static final Duration MAX_UPLOAD_DURATION = Duration.ofMinutes(10);
    private final UploadManager uploadManager = new UploadManager();
    private final BinaryDatabase binaryDatabase;
    private final AuthorizationController.Authorize authorize;


    private final Map<String, Feature> features;
    private final Multimap<String, String> affectedByMetadata;

    public UploadService(
            @NonNull final BinaryDatabase binaryDatabase,
            @NonNull final Collection<String> featuresDefinitions,
            @NonNull final AuthorizationController.Authorize authorize
    ) {
        this(binaryDatabase, featuresDefinitions, ArrayListMultimap.create(), authorize);
    }

    public UploadService(
            @NonNull final BinaryDatabase binaryDatabase,
            @NonNull final Collection<String> featuresDefinitions,
            @NonNull final Multimap<String, String> affectedByMetadata,
            @NonNull final AuthorizationController.Authorize authorize
    ) {
        this.binaryDatabase = binaryDatabase;
        this.affectedByMetadata = affectedByMetadata;
        this.features = featuresDefinitions.stream().map(f -> {
            try {
                return FeatureGenerator.generateFeature(f);
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        }).collect(Collectors.toMap(
                f -> {
                    try {
                        return generateFullyQualifiedIdentifier(f);
                    } catch (MalformedSiLAFeature e) {
                        throw new RuntimeException(e);
                    }
                },
                f -> f
        ));
        this.authorize = authorize;
    }

    /**
     * Create a binary upload.
     *
     * Add an upload entry in {@link UploadService#uploadManager}
     *
     * @implNote todo support metadata
     *
     * @param request the request
     * @param responseObserver the response observer
     */
    @Override
    public void createBinary(
            final SiLABinaryTransfer.CreateBinaryRequest request,
            final StreamObserver<SiLABinaryTransfer.CreateBinaryResponse> responseObserver
    ) {
        try {
            this.authorize.authorize(request.getParameterIdentifier());
            final UUID blobId = UUID.randomUUID();
            final Duration expiration = MAX_UPLOAD_DURATION;

            final Matcher parameterMatcher =
                    FullyQualifiedIdentifierUtils.FullyQualifiedCommandParameterIdentifierPattern
                            .matcher(request.getParameterIdentifier());

            if (!parameterMatcher.matches()) {
                throw BinaryTransferErrorHandler.generateBinaryTransferError(
                        SiLABinaryTransfer.BinaryTransferError.ErrorType.BINARY_UPLOAD_FAILED,
                        "Invalid parameter identifier."
                );
            }

            final String[] featureMatcher = FullyQualifiedIdentifierUtils.FullyQualifiedFeatureIdentifierPattern
                    .split(request.getParameterIdentifier());

            if (featureMatcher.length < 2) {
                throw BinaryTransferErrorHandler.generateBinaryTransferError(
                        SiLABinaryTransfer.BinaryTransferError.ErrorType.BINARY_UPLOAD_FAILED,
                        "Invalid parameter identifier."
                );
            }
            final String fqiFeature = request.getParameterIdentifier()
                    .substring(0, request.getParameterIdentifier().length() - (featureMatcher[1].length()));

            final Feature feature = this.features.get(fqiFeature);

            if (feature == null) {
                throw BinaryTransferErrorHandler.generateBinaryTransferError(
                        SiLABinaryTransfer.BinaryTransferError.ErrorType.BINARY_UPLOAD_FAILED,
                        "Invalid parameter identifier."
                );
            }

            final String[] split = featureMatcher[1].split("/");
            final String commandId = split[2];
            final String parameterId = split[4];

            final Feature.Command command = feature.getCommand().stream().filter(c -> c.getIdentifier().equals(commandId)).findAny().orElseThrow(() -> BinaryTransferErrorHandler.generateBinaryTransferError(
                    SiLABinaryTransfer.BinaryTransferError.ErrorType.BINARY_UPLOAD_FAILED,
                    "Invalid parameter identifier."
            ));

            final SiLAElement parameter = command.getParameter().stream().filter(c -> c.getIdentifier().equals(parameterId)).findAny().orElseThrow(() -> BinaryTransferErrorHandler.generateBinaryTransferError(
                    SiLABinaryTransfer.BinaryTransferError.ErrorType.BINARY_UPLOAD_FAILED,
                    "Invalid parameter identifier."
            ));

            if (!typeContainsBinary(feature, parameter.getDataType())) {
                throw BinaryTransferErrorHandler.generateBinaryTransferError(
                        SiLABinaryTransfer.BinaryTransferError.ErrorType.BINARY_UPLOAD_FAILED,
                        "Invalid parameter identifier."
                );
            }

            final String fqiCommand;
            try {
                fqiCommand = generateFullyQualifiedIdentifier(feature, command);
            } catch (MalformedSiLAFeature e) {
                throw BinaryTransferErrorHandler.generateBinaryTransferError(
                        SiLABinaryTransfer.BinaryTransferError.ErrorType.BINARY_UPLOAD_FAILED,
                        "Invalid parameter identifier."
                );
            }

            final Set<String> requiredMetadata = Stream.concat(
                    this.affectedByMetadata.get(fqiFeature).stream(), this.affectedByMetadata.get(fqiCommand).stream()
            ).collect(Collectors.toSet());

            final ServerMetadataContainer serverMetadataContainer = SILA_METADATA_KEY.get();
            for (final String metadata : requiredMetadata) {
                if (serverMetadataContainer == null || !serverMetadataContainer.hasFQIMetadata(metadata)) {
                    throw SiLAErrors.generateFrameworkError(
                            SiLAFramework.FrameworkError.ErrorType.INVALID_METADATA,
                            "Missing required metadata."
                    );
                }
            }

            uploadManager.addUpload(blobId, request.getBinarySize(), request.getChunkCount(), request.getParameterIdentifier(), expiration);
            responseObserver.onNext(
                    SiLABinaryTransfer.CreateBinaryResponse
                            .newBuilder()
                            .setLifetimeOfBinary(SiLADuration.from(expiration))
                            .setBinaryTransferUUID(blobId.toString())
                            .build()
            );
        } catch (StatusRuntimeException e) {
            responseObserver.onError(e);
            return;
        }

        responseObserver.onCompleted();
    }

    /**
     * Upload chunk stream
     *
     * @param responseObserver the stream response observer
     * @return the stream request observer
     */
    @Override
    public StreamObserver<SiLABinaryTransfer.UploadChunkRequest> uploadChunk(
            final StreamObserver<SiLABinaryTransfer.UploadChunkResponse> responseObserver
    ) {
        return new StreamObserver<SiLABinaryTransfer.UploadChunkRequest>() {
            @Override
            public void onNext(final SiLABinaryTransfer.UploadChunkRequest request) {
                uploadChunk(request, responseObserver);
            }

            @Override
            public void onError(final Throwable t) {
                log.warn("Upload chunk stream exception: {}", t.getMessage(), t);
            }

            @Override
            public void onCompleted() {
                responseObserver.onCompleted();
            }
        };
    }

    /**
     * Upload a chunk
     *
     * @param request the chunk upload request
     * @param responseObserver the stream response observer
     *
     * If successful return {@link SiLABinaryTransfer.UploadChunkResponse}
     * If binary does not exist or cannot be removed return {@link SiLABinaryTransfer.BinaryTransferError.ErrorType#INVALID_BINARY_TRANSFER_UUID}
     */
    private void uploadChunk(
            final SiLABinaryTransfer.UploadChunkRequest request,
            final StreamObserver<SiLABinaryTransfer.UploadChunkResponse> responseObserver
    ) {
        try {
            Duration expiration = MAX_UPLOAD_DURATION;
            final UUID blobId = SiLABinaryTransferUUID.from(request.getBinaryTransferUUID());

            if (request.getPayload().toByteArray().length > MAX_CHUNK_SIZE) {
                throw BinaryTransferErrorHandler.generateBinaryTransferError(
                        SiLABinaryTransfer.BinaryTransferError.ErrorType.BINARY_UPLOAD_FAILED,
                        "Chunk size is too large."
                );
            }

            throwIfBinaryUploadNotExist(blobId);

            try {
                final UploadCompletion uploadCompletion = this.uploadManager.updateUpload(
                        blobId,
                        request.getChunkIndex(),
                        request.getPayload().toByteArray(),
                        expiration
                );
                if (uploadCompletion.isComplete()) {
                    log.info("Upload {} from client complete", blobId);
                    // Close the upload, remove it and add to database
                    this.uploadManager.removeUpload(blobId);
                    try (InputStream chunkStream = uploadCompletion.chunksToStream()) {
                        expiration = this.binaryDatabase.addBinary(blobId, chunkStream, uploadCompletion.getParameterIdentifier());
                    }
                    uploadCompletion.close();
                    uploadChunkResponse(request, responseObserver, expiration);
                    responseObserver.onCompleted();
                    return;
                }
            } catch (final Exception e) {
                log.warn("The following error occurred when attempting to save received chunk: {}", e.getMessage(), e);
                throw BinaryTransferErrorHandler.generateBinaryTransferError(
                        SiLABinaryTransfer.BinaryTransferError.ErrorType.BINARY_UPLOAD_FAILED,
                        e.getMessage()
                );
            }
            uploadChunkResponse(request, responseObserver, expiration);
        } catch (StatusRuntimeException e) {
            responseObserver.onError(e);
        }
    }

    /**
     * Upload chunk response
     *
     * @param request The request
     * @param responseObserver The response stream observer
     * @param newExpiration The new expiration
     */
    private void uploadChunkResponse(SiLABinaryTransfer.UploadChunkRequest request, final StreamObserver<SiLABinaryTransfer.UploadChunkResponse> responseObserver, Duration newExpiration) {
        responseObserver.onNext(
                SiLABinaryTransfer.UploadChunkResponse
                        .newBuilder()
                        .setBinaryTransferUUID(request.getBinaryTransferUUID())
                        .setChunkIndex(request.getChunkIndex())
                        .setLifetimeOfBinary(SiLADuration.from(newExpiration))
                        .build()
        );
    }

    /**
     *  Delete binary
     *
     * @param request the request
     * @param responseObserver the response observer
     *
     * If successful return {@link SiLABinaryTransfer.DeleteBinaryResponse}
     * If binary does not exist or cannot be removed return {@link SiLABinaryTransfer.BinaryTransferError.ErrorType#INVALID_BINARY_TRANSFER_UUID}
     */
    @Override
    public void deleteBinary(
            final SiLABinaryTransfer.DeleteBinaryRequest request,
            final StreamObserver<SiLABinaryTransfer.DeleteBinaryResponse> responseObserver
    ) {
        final UUID binaryToDelete = SiLABinaryTransferUUID.from(request.getBinaryTransferUUID());

        // check for invalid UUID
        boolean fullUploadExists;
        BinaryInfo binaryInfo = null;
        try {
            binaryInfo = binaryDatabase.getBinaryInfo(binaryToDelete);
            fullUploadExists = true;
        } catch (BinaryDatabaseException ex) {
            fullUploadExists = false;
        }
        if (binaryInfo != null) {
            try {
                this.authorize.authorize(binaryInfo.getParameterIdentifier());
            } catch (StatusRuntimeException e) {
                responseObserver.onError(e);
                return;
            }
        }
        boolean partialUploadExists = uploadManager.getUpload(binaryToDelete) != null;
        if (!fullUploadExists && !partialUploadExists) {
            responseObserver.onError(BinaryTransferErrorHandler.generateBinaryTransferError(
                    SiLABinaryTransfer.BinaryTransferError.ErrorType.INVALID_BINARY_TRANSFER_UUID,
                    String.format("No binary found with UUID %s", binaryToDelete)
            ));
            return;
        }

        // try deletion
        if (fullUploadExists) {
            try {
                binaryDatabase.removeBinary(binaryToDelete);
            } catch (BinaryDatabaseException ex) {
                responseObserver.onError(BinaryTransferErrorHandler.generateBinaryTransferError(
                        SiLABinaryTransfer.BinaryTransferError.ErrorType.BINARY_UPLOAD_FAILED,
                        String.format("Failed to delete binary with UUID %s", binaryToDelete)));
                return;
            }
        }
        if (partialUploadExists && uploadManager.removeUpload(binaryToDelete) == null) {
            responseObserver.onError(BinaryTransferErrorHandler.generateBinaryTransferError(
                    SiLABinaryTransfer.BinaryTransferError.ErrorType.BINARY_UPLOAD_FAILED,
                    String.format("Failed to delete binary with UUID %s", binaryToDelete)));
            return;
        }

        // success
        responseObserver.onNext(SiLABinaryTransfer.DeleteBinaryResponse.newBuilder().build());
        responseObserver.onCompleted();
    }

    private void throwIfBinaryUploadNotExist(@Nullable final UUID binaryTransferUUID) throws StatusRuntimeException {
        if (binaryTransferUUID == null || !this.uploadManager.hasUpload(binaryTransferUUID)) {
            throw BinaryTransferErrorHandler.generateBinaryTransferError(
                    SiLABinaryTransfer.BinaryTransferError.ErrorType.INVALID_BINARY_TRANSFER_UUID,
                    "Invalid Binary Transfer UUID."
            );
        }
    }

    private static boolean typeContainsBinary(@NonNull final Feature feature, @NonNull final DataTypeType dataType) {
        if (dataType.getBasic() != null) {
            return dataType.getBasic().name().equals(BasicType.BINARY.name());
        }
        if (dataType.getList() != null) {
            return typeContainsBinary(feature, dataType.getList().getDataType());
        }
        if (dataType.getConstrained() != null) {
            return typeContainsBinary(feature, dataType.getConstrained().getDataType());
        }
        if (dataType.getStructure() != null) {
            for (final SiLAElement structureType : dataType.getStructure().getElement()) {
                if (typeContainsBinary(feature, structureType.getDataType())) {
                    return true;
                }
            }
            return false;
        }
        if (dataType.getDataTypeIdentifier() != null) {
            final SiLAElement dataTypeDef = feature.getDataTypeDefinition()
                    .stream().filter(d -> d.getIdentifier().equals(dataType.getDataTypeIdentifier()))
                    .findAny()
                    .orElseThrow(() -> new RuntimeException("Data Type Identifier is not present in the feature."));
            return typeContainsBinary(feature, dataTypeDef.getDataType());
        }
        throw new RuntimeException("DataTypeType does not contain any type.");

    }
}