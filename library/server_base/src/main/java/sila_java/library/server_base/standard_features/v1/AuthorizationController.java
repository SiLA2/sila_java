package sila_java.library.server_base.standard_features.v1;

import sila_java.library.core.sila.errors.SiLAErrorException;

public class AuthorizationController {
    public static final String FULLY_QUALIFIED_FEATURE_IDENTIFIER = "org.silastandard/core/AuthorizationService/v1";

    public static final String INVALID_ACCESS_TOKEN = "InvalidAccessToken";
    public static Authorize DEFAULT_AUTHORIZE = (fqi) -> {};

    /**
     * This functional interface represents an authorization mechanism for a given Fully Qualified Identifier (FQI).
     * It is intended to be used in conjunction with lambda expressions or method references for providing
     * a simple way to implement an authorization mechanism.
     *
     */
    @FunctionalInterface
    public interface Authorize {

        /**
         * Authorizes the specified Fully Qualified Identifier (FQI) by implementing the desired authorization
         * mechanism. This method may throw a SiLAErrorException if any errors occur during the authorization process
         * or if the request is not authorized.
         *
         * @param fqi The Fully Qualified Identifier (FQI) to be authorized.
         * @throws SiLAErrorException If any errors occur during the authorization process
         * or the authorization is not granted.
         */
        public void authorize(String fqi) throws SiLAErrorException;
    }

}
