package sila_java.library.server_base.standard_features.v2;

import io.grpc.BindableService;
import io.grpc.StatusRuntimeException;
import io.grpc.stub.StreamObserver;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import sila2.org.silastandard.core.lockcontroller.v2.LockControllerGrpc;
import sila2.org.silastandard.core.lockcontroller.v2.LockControllerOuterClass;
import sila_java.library.core.sila.errors.SiLAErrorException;
import sila_java.library.core.sila.errors.SiLAErrors;
import sila_java.library.core.sila.types.SiLABoolean;
import sila_java.library.core.sila.types.SiLAString;
import sila_java.library.server_base.standard_features.FeatureImplementation;
import sila_java.library.server_base.standard_features.v1.AuthorizationController;
import sila_java.library.sila_base.EmptyClass;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import static sila_java.library.core.utils.FileUtils.getFileContent;

/**
 * Lock controller core feature implementation
 *
 * @implNote todo make thread safe
 */
@Slf4j
@RequiredArgsConstructor
public class LockController extends LockControllerGrpc.LockControllerImplBase implements FeatureImplementation, AutoCloseable {
    private static final String FULLY_QUALIFIED_FEATURE_IDENTIFIER = "org.silastandard/core/LockController/v2";

    private static final String INVALID_LOCK_IDENTIFIER = "InvalidLockIdentifier";
    private static final String SERVER_NOT_LOCKED = "ServerNotLocked";
    private static final String SERVER_ALREADY_LOCKED = "ServerAlreadyLocked";
    private final Timer lockTimer = new Timer();
    private final AuthorizationController.Authorize authorize;
    private String lockIdentifier;

    /**
     * Lock the server
     * @param request the request
     * @param responseObserver the response observer
     */
    @Override
    public void lockServer(LockControllerOuterClass.LockServer_Parameters request, StreamObserver<LockControllerOuterClass.LockServer_Responses> responseObserver) {
        try {
            this.authorize.authorize(FULLY_QUALIFIED_FEATURE_IDENTIFIER);
        } catch (SiLAErrorException e) {
            responseObserver.onError(e);
            return;
        }
        if (StringUtils.isNotEmpty(lockIdentifier)) {
            respondWithError(
                    responseObserver,
                    SiLAErrors.generateDefinedExecutionError(
                            FULLY_QUALIFIED_FEATURE_IDENTIFIER + "/DefinedExecutionError/" + SERVER_ALREADY_LOCKED,
                            "server already locked"
                    )
            );
            return;
        }

        if (!request.hasLockIdentifier()) {
            respondWithError(
                    responseObserver,
                    SiLAErrors.generateDefinedExecutionError(
                            FULLY_QUALIFIED_FEATURE_IDENTIFIER + "/DefinedExecutionError/" + INVALID_LOCK_IDENTIFIER,
                            "lockIdentifier required"
                    )
            );
            return;
        }

        if (request.hasTimeout()) {
            handleLockServer(request.getLockIdentifier().getValue(), request.getTimeout().getValue());
        } else {
            handleLockServer(request.getLockIdentifier().getValue());
        }

        responseObserver.onCompleted();

    }

    /**
     * Unlock the server
     * @param request the request
     * @param responseObserver the response obvserver
     */
    @Override
    public void unlockServer(LockControllerOuterClass.UnlockServer_Parameters request, StreamObserver<LockControllerOuterClass.UnlockServer_Responses> responseObserver) {
        try {
            this.authorize.authorize(FULLY_QUALIFIED_FEATURE_IDENTIFIER);
        } catch (SiLAErrorException e) {
            responseObserver.onError(e);
            return;
        }
        if (StringUtils.isEmpty(lockIdentifier)) {
            respondWithError(
                    responseObserver,
                    SiLAErrors.generateDefinedExecutionError(
                            FULLY_QUALIFIED_FEATURE_IDENTIFIER + "/DefinedExecutionError/" + SERVER_NOT_LOCKED,
                            "server not locked"
                    )
            );
            return;
        }

        if (!request.hasLockIdentifier()) {
            respondWithError(
                    responseObserver,
                    SiLAErrors.generateDefinedExecutionError(
                            FULLY_QUALIFIED_FEATURE_IDENTIFIER + "/DefinedExecutionError/" + INVALID_LOCK_IDENTIFIER,
                            "lockIdentifier required"
                    )
            );
            return;
        }

        if (!StringUtils.equals(request.getLockIdentifier().getValue(), lockIdentifier)) {
            respondWithError(
                    responseObserver,
                    SiLAErrors.generateDefinedExecutionError(
                            FULLY_QUALIFIED_FEATURE_IDENTIFIER + "/DefinedExecutionError/" + INVALID_LOCK_IDENTIFIER,
                            "invalid lockIdentifier"
                    )
            );
            return;
        }

        handleUnlockServer();

        responseObserver.onNext(LockControllerOuterClass.UnlockServer_Responses.getDefaultInstance());
        responseObserver.onCompleted();
    }

    /**
     * Subscription mechanism for lock status
     *
     * @param request the request
     * @param responseObserver the response observer
     *
     * @implNote todo make observable
     */
    @Override
    public void subscribeIsLocked(LockControllerOuterClass.Subscribe_IsLocked_Parameters request, StreamObserver<LockControllerOuterClass.Subscribe_IsLocked_Responses> responseObserver) {
        responseObserver.onNext(LockControllerOuterClass.Subscribe_IsLocked_Responses.newBuilder().setIsLocked(SiLABoolean.from(StringUtils.isNotEmpty(lockIdentifier))).build());
        responseObserver.onCompleted();
    }

    /**
     * Get feature / command / property affected by metadata lock identifier
     * @param request the request
     * @param responseObserver the response observer
     */
    @Override
    public void getFCPAffectedByMetadataLockIdentifier(LockControllerOuterClass.Get_FCPAffectedByMetadata_LockIdentifier_Parameters request, StreamObserver<LockControllerOuterClass.Get_FCPAffectedByMetadata_LockIdentifier_Responses> responseObserver) {
        LockControllerOuterClass.Get_FCPAffectedByMetadata_LockIdentifier_Responses fcpaResponse = LockControllerOuterClass.Get_FCPAffectedByMetadata_LockIdentifier_Responses.newBuilder()
                .addAffectedCalls(SiLAString.from(FULLY_QUALIFIED_FEATURE_IDENTIFIER + "/Command/LockServer"))
                .addAffectedCalls(SiLAString.from(FULLY_QUALIFIED_FEATURE_IDENTIFIER + "/Command/UnlockServer"))
                .build();
        responseObserver.onNext(fcpaResponse);
        responseObserver.onCompleted();
    }

    /**
     * Cleanup and unlock server
     * @inheritDoc
     */
    @Override
    public void close(){
        handleUnlockServer();
    }

    /**
     * Get lock controller feature definition
     * @return lock controller feature definition
     */
    @Override
    @SneakyThrows(IOException.class)
    public String getFeatureDescription() {
        return getFileContent(EmptyClass.class.getResourceAsStream(
                "/sila_base/feature_definitions/org/silastandard/core/LockController-v2_0.sila.xml"
        ));
    }

    /**
     * @inheritDoc
     */
    @Override
    public BindableService getService() {
        return this;
    }

    /**
     * Lock the server
     * @param identifier
     */
    private void handleLockServer(String identifier) {
        handleLockServer(identifier, 0);
    }

    /**
     * Lock the server for the specified amount of time in millisecond
     * @param identifier the lock identifier
     * @param timeout the duration of the lock in millisecond
     */
    private void handleLockServer(String identifier, long timeout) {
        this.lockIdentifier = identifier;
        if (timeout > 0) {
            lockTimer.schedule(new ResetLockTask(), timeout * 1_000);
        }
    }

    /**
     * Unlock server
     */
    private void handleUnlockServer() {
        lockIdentifier = null;
        lockTimer.cancel();
    }

    /**
     * Send exception to stream observer and close it
     * @param streamObserver the stream observer
     * @param exception the exception
     */
    private void respondWithError(StreamObserver<?> streamObserver, StatusRuntimeException exception) {
        streamObserver.onError(exception);
        streamObserver.onCompleted();
    }

    /**
     * Simple {@link TimerTask} extension that nullify {@link LockController#lockIdentifier} when run or cancel
     */
    class ResetLockTask extends TimerTask {

        /**
         * @inheritDoc
         */
        @Override
        public void run() {
            log.info("lockTimer#run");
            LockController.this.lockIdentifier = null;
        }

        /**
         * @inheritDoc
         */
        @Override
        public boolean cancel() {
            LockController.this.lockIdentifier = null;
            log.info("lockTimer#cancel");
            return super.cancel();
        }
    }
}
