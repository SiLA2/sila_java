package sila_java.library.server_base.command.observable;

import io.grpc.StatusRuntimeException;

import java.util.UUID;

/**
 * Runnable command task
 * @param <ParamType> The command parameter type
 * @param <ResultType> The command result type
 */
public interface RunnableCommandTask<ParamType, ResultType> extends AutoCloseable {
    /**
     * Run the command task
     * @param command The command object linked to this task
     * @return The task result
     * @throws StatusRuntimeException If an error occur during the task
     */
    ResultType run(ObservableCommandWrapper<ParamType, ResultType> command) throws StatusRuntimeException;

    default void validate(ParamType parameters) throws StatusRuntimeException {}

    default void preRun(ParamType parameters, UUID commandExecutionUUID) {}

    default void onFinish(ObservableCommandWrapper<ParamType, ResultType> command, boolean withError) {}

    /**
     * Called when the command id becomes invalid
     */
    @Override
    default void close() {}

    /**
     * Called when a new command is added in the command manager
     * @param newCommand The new command added
     */
    default void onNewCommand(final ObservableCommandWrapper<ParamType, ResultType> newCommand) {}
}
