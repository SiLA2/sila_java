package sila_java.library.server_base.standard_features.v1;

import io.grpc.stub.StreamObserver;
import lombok.Getter;
import lombok.NonNull;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import sila2.org.silastandard.SiLAFramework;
import sila2.org.silastandard.core.silaservice.v1.SiLAServiceGrpc;
import sila2.org.silastandard.core.silaservice.v1.SiLAServiceOuterClass;
import sila_java.library.core.sila.errors.SiLAErrors;
import sila_java.library.core.sila.mapping.feature.FeatureGenerator;
import sila_java.library.core.sila.types.SiLAString;
import sila_java.library.core.sila.utils.FullyQualifiedIdentifierUtils;
import sila_java.library.server_base.config.IServerConfigWrapper;
import sila_java.library.server_base.config.ServerConfiguration;
import sila_java.library.server_base.identification.ServerInformation;
import sila_java.library.server_base.metadata.ServerMetadataContainer;
import sila_java.library.server_base.standard_features.FeatureImplementation;
import sila_java.library.sila_base.EmptyClass;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static sila_java.library.core.utils.FileUtils.getFileContent;
import static sila_java.library.core.utils.Utils.validateFeatureXML;
import static sila_java.library.server_base.metadata.MetadataExtractingInterceptor.SILA_METADATA_KEY;

/**
 * SiLAService Implementation used by other SiLA servers
 */
@Slf4j
public class SiLAServiceServer implements FeatureImplementation {
    @Getter
    private final Map<String, String> featureDefinitions = new HashMap<>();

    private final IServerConfigWrapper serverConfigurationContainer;

    @Getter
    private final ServerInformation serverInformation;

    /**
     * Constructor that create a SiLA Service server and validate the provided server information/configuration/features definitions
     * @param serverConfigurationContainer the server configuration
     * @param serverInformation the server information
     * @param featureDefinitions the server feature definitions
     */
    @SneakyThrows
    public SiLAServiceServer(
            @NonNull final IServerConfigWrapper serverConfigurationContainer,
            @Nonnull final ServerInformation serverInformation,
            @Nonnull final Map<String, String> featureDefinitions) {
        this.serverConfigurationContainer = serverConfigurationContainer;
        this.serverInformation = serverInformation;
        featureDefinitions.put(
                FeatureGenerator.generateFullyQualifiedIdentifier(
                        FeatureGenerator.generateFeature(this.getFeatureDescription())
                ),
                this.getFeatureDescription()
        );

        log.info(
                "[SiLAService] create service for: {}\nfeatureDefinitions={} ...",
                serverInformation,
                featureDefinitions.keySet()
        );

        // Get serialised feature definition
        for (Map.Entry<String, String> entry : featureDefinitions.entrySet()) {
            String key = entry.getKey();
            try {
                final String featureDefinition = entry.getValue();

                validateFeatureXML(featureDefinition);
                this.featureDefinitions.put(key, featureDefinition);
                log.info(
                        "[registerServer] type={} feature={} is read from XML string.",
                        serverInformation.getType(),
                        key
                );
            } catch (IOException e) {
                throw new IllegalArgumentException(
                        "Error registering feature definition for server = " + serverInformation.getType() +
                                "& feature = '" + key + "' is not valid", e);
            }
        }
        log.info(
                "[registerServer] server type = {} with features successfully registered!",
                serverInformation.getType()
        );
    }

    /**
     * @inheritDoc
     */
    @Override
    @SneakyThrows(IOException.class)
    public String getFeatureDescription() {
        return getFileContent(
                Objects.requireNonNull(EmptyClass.class.getResourceAsStream(
                        "/sila_base/feature_definitions/org/silastandard/core/SiLAService-v1_0.sila.xml"
                ))
        );
    }

    /**
     * @inheritDoc
     */
    @Override
    public io.grpc.BindableService getService() {
        return new ServiceImpl();
    }

    /**
     * gRPC Service Implementation
     */
    private class ServiceImpl extends SiLAServiceGrpc.SiLAServiceImplBase {

        /**
         * Get a feature definition
         * @param req the request
         * @param responseObserver the response observer
         */
        @Override
        public void getFeatureDefinition(
                @NonNull final SiLAServiceOuterClass.GetFeatureDefinition_Parameters req,
                @NonNull final StreamObserver<SiLAServiceOuterClass.GetFeatureDefinition_Responses> responseObserver
        ) {
            if (rejectMetadata(responseObserver)) {
                return;
            }
            if (!req.hasFeatureIdentifier()) {
                responseObserver.onError(SiLAErrors.generateValidationError(
                        "org.silastandard/core/SiLAService/v1/Command/GetFeatureDefinition/Parameter/FeatureIdentifier",
                        "Missing feature identifier."
                ));
            }
            final boolean isValidFeatureId =
                    FullyQualifiedIdentifierUtils.FullyQualifiedFeatureIdentifierPattern
                            .matcher(req.getFeatureIdentifier().getValue())
                            .matches();
            if (!isValidFeatureId) {
                responseObserver.onError(SiLAErrors.generateValidationError(
                        "org.silastandard/core/SiLAService/v1/Command/GetFeatureDefinition/Parameter/FeatureIdentifier",
                        "Invalid feature identifier."
                ));
            }

            final String featureIdentifier = req
                    .getFeatureIdentifier()
                    .getValue();

            final String serverName = serverConfigurationContainer.getCacheConfig().getName();

            log.debug(
                    "[{}][getFeatureDefinition] feature={} entered.",
                    serverName,
                    featureIdentifier
            );

            final String featureDefinitionContent =
                    SiLAServiceServer.this.featureDefinitions.get(featureIdentifier);

            if (featureDefinitionContent == null) {
                responseObserver.onError(SiLAErrors.generateDefinedExecutionError(
                        "org.silastandard/core/SiLAService/v1/DefinedExecutionError/UnimplementedFeature",
                        String.format("The server does not implement the feature '%s'", featureIdentifier)));
                return;
            }
            responseObserver.onNext(
                    SiLAServiceOuterClass.GetFeatureDefinition_Responses
                            .newBuilder()
                            .setFeatureDefinition(SiLAString.from(featureDefinitionContent))
                            .build()
            );
            responseObserver.onCompleted();
            log.debug(
                    "[{}][getFeatureDefinition] feature={} returned={}.",
                    serverName,
                    featureIdentifier,
                    featureDefinitionContent
            );
        }

        /**
         * Set server name
         * @param req the request
         * @param responseObserver the response observer
         */
        @Override
        public void setServerName(
                SiLAServiceOuterClass.SetServerName_Parameters req,
                StreamObserver<SiLAServiceOuterClass.SetServerName_Responses> responseObserver
        ) {
            if (rejectMetadata(responseObserver)) {
                return;
            }
            val serverName = req.getServerName().getValue();

            if (serverName.length() > 255) {
                responseObserver.onError(SiLAErrors.generateValidationError(
                        "org.silastandard/core/SiLAService/v1/Command/SetServerName/Parameter/ServerName",
                        "Server name must not be longer than 255 characters")
                );
                return;
            }
            if (serverName.isEmpty()) {
                responseObserver.onError(SiLAErrors.generateValidationError(
                        "org.silastandard/core/SiLAService/v1/Command/SetServerName/Parameter/ServerName",
                        "Server name must not be empty")
                );
                return;
            }

            log.info("setServerName being called, Server Name: {}", serverName);

            try {
                serverConfigurationContainer.setConfig(
                        new ServerConfiguration(serverName, serverConfigurationContainer.getCacheConfig().getUuid())
                );
                responseObserver.onNext(SiLAServiceOuterClass.SetServerName_Responses.newBuilder().build());
                responseObserver.onCompleted();
            } catch (IOException e) {
                val error = SiLAErrors.generateGenericExecutionError(e);
                responseObserver.onError(error);
            }
        }


        /**
         * Get server name
         * @param req the request
         * @param responseObserver the response observer
         */
        @Override
        public void getServerName(
                SiLAServiceOuterClass.Get_ServerName_Parameters req,
                StreamObserver<SiLAServiceOuterClass.Get_ServerName_Responses> responseObserver
        ) {
            log.debug("getServerName being called");
            if (rejectMetadata(responseObserver)) {
                return;
            }

            responseObserver.onNext(
                    SiLAServiceOuterClass.Get_ServerName_Responses
                            .newBuilder()
                            .setServerName(SiLAString.from(serverConfigurationContainer.getCacheConfig().getName()))
                            .build()
            );
            responseObserver.onCompleted();
        }

        /**
         * Get server type
         * @param req the request
         * @param responseObserver the response observer
         */
        @Override
        public void getServerType(
                SiLAServiceOuterClass.Get_ServerType_Parameters req,
                StreamObserver<SiLAServiceOuterClass.Get_ServerType_Responses> responseObserver
        ) {
            log.debug("getServerType being called");
            if (rejectMetadata(responseObserver)) {
                return;
            }

            responseObserver.onNext(
                    SiLAServiceOuterClass.Get_ServerType_Responses
                            .newBuilder()
                            .setServerType(SiLAString.from(serverInformation.getType()))
                            .build()
            );
            responseObserver.onCompleted();
        }

        /**
         * Get server UUID
         * @param req the request
         * @param responseObserver the response observer
         */
        @Override
        public void getServerUUID(
                SiLAServiceOuterClass.Get_ServerUUID_Parameters req,
                StreamObserver<SiLAServiceOuterClass.Get_ServerUUID_Responses> responseObserver
        ) {
            if (rejectMetadata(responseObserver)) {
                return;
            }

            responseObserver.onNext(
                    SiLAServiceOuterClass.Get_ServerUUID_Responses
                            .newBuilder()
                            .setServerUUID(
                                    SiLAString.from(serverConfigurationContainer.getCacheConfig().getUuid().toString())
                            )
                            .build()
            );
            responseObserver.onCompleted();
        }

        /**
         * Get server description
         * @param req the request
         * @param responseObserver the response observer
         */
        @Override
        public void getServerDescription(
                @NonNull final SiLAServiceOuterClass.Get_ServerDescription_Parameters req,
                @NonNull final StreamObserver<SiLAServiceOuterClass.Get_ServerDescription_Responses> responseObserver
        ) {
            log.debug("getServerDescription being called");
            if (rejectMetadata(responseObserver)) {
                return;
            }

            responseObserver.onNext(
                    SiLAServiceOuterClass.Get_ServerDescription_Responses
                            .newBuilder()
                            .setServerDescription(SiLAString.from(serverInformation.getDescription()))
                            .build()
            );
            responseObserver.onCompleted();
        }

        /**
         * Get server version
         * @param req the request
         * @param responseObserver the response observer
         */
        @Override
        public void getServerVersion(
                @NonNull final SiLAServiceOuterClass.Get_ServerVersion_Parameters req,
                @NonNull final StreamObserver<SiLAServiceOuterClass.Get_ServerVersion_Responses> responseObserver
        ) {
            log.debug("getServerVersion being called");
            if (rejectMetadata(responseObserver)) {
                return;
            }

            responseObserver.onNext(
                    SiLAServiceOuterClass.Get_ServerVersion_Responses
                            .newBuilder()
                            .setServerVersion(SiLAString.from(serverInformation.getVersion()))
                            .build()
            );
            responseObserver.onCompleted();
        }

        /**
         * Get server vendor URL
         * @param req the request
         * @param responseObserver the response observer
         */
        @Override
        public void getServerVendorURL(
                @NonNull final SiLAServiceOuterClass.Get_ServerVendorURL_Parameters req,
                @NonNull final StreamObserver<SiLAServiceOuterClass.Get_ServerVendorURL_Responses> responseObserver
        ) {
            log.debug("getServerVendorName being called");
            if (rejectMetadata(responseObserver)) {
                return;
            }

            responseObserver.onNext(SiLAServiceOuterClass.Get_ServerVendorURL_Responses
                    .newBuilder()
                    .setServerVendorURL(SiLAString.from(serverInformation.getVendorURL()))
                    .build());
            responseObserver.onCompleted();
        }

        /**
         * Get server implemented features
         * @param req the request
         * @param responseObserver the response observer
         *
         * @implNote todo use fully qualified id
         */
        @Override
        public void getImplementedFeatures(
                @NonNull final SiLAServiceOuterClass.Get_ImplementedFeatures_Parameters req,
                @NonNull final StreamObserver<SiLAServiceOuterClass.Get_ImplementedFeatures_Responses> responseObserver
        ) {
            log.debug("getImplementedFeatures being called");
            if (rejectMetadata(responseObserver)) {
                return;
            }

            final SiLAServiceOuterClass.Get_ImplementedFeatures_Responses.Builder idBuilder =
                    SiLAServiceOuterClass.Get_ImplementedFeatures_Responses.newBuilder();

            featureDefinitions.keySet().forEach(featureId -> idBuilder.addImplementedFeatures(
                    SiLAString.from(featureId)
            ));

            responseObserver.onNext(idBuilder.build());
            responseObserver.onCompleted();
        }

        private boolean rejectMetadata(StreamObserver<?> responseObserver) {
            final ServerMetadataContainer serverMetadataContainer = SILA_METADATA_KEY.get();
            if (serverMetadataContainer != null && !serverMetadataContainer.isEmpty()) {
                responseObserver.onError(SiLAErrors.generateFrameworkError(
                        SiLAFramework.FrameworkError.ErrorType.NO_METADATA_ALLOWED,
                        "No metadata must be provided to the SiLA Service."
                ));
                return true;
            }
            return false;
        }
    }
}
