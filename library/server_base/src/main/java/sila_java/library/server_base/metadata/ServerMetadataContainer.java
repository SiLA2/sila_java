package sila_java.library.server_base.metadata;

import com.google.protobuf.Message;
import io.grpc.Context;
import io.grpc.Metadata;
import lombok.NonNull;
import sila_java.library.core.sila.utils.MetadataUtils;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import static sila_java.library.core.sila.utils.MetadataUtils.metadataProtobufMessageClassToGrpcMetadataKey;
import static sila_java.library.server_base.metadata.MetadataExtractingInterceptor.SILA_METADATA_KEY;

/**
 * Provides SiLA feature implementations access to SiLA Client Metadata
 */
public class ServerMetadataContainer {
    private static final String grpcMetadataKeyPrefix = "sila-";
    private static final String grpcMetadataKeySuffix = "-bin";

    /**
     * Metadata as transmitted via grpc: Keys are like
     * {@code "sila-org.silastandard-core-silaservice-v1-metadata-authorizationtoken-bin"}, values are Base64-encoded
     * Protobuf messages containing SiLA Client Metadata
     */
    private final Map<String, byte[]> rawMetadata;

    private ServerMetadataContainer(Map<String, byte[]> rawMetadata) {
        this.rawMetadata = rawMetadata;
    }

    public static ServerMetadataContainer fromHeaders(Metadata headers) {
        HashMap<String, byte[]> rawMetadata = new HashMap<>();
        headers.keys()
               .stream()
               .filter(key -> key.startsWith(grpcMetadataKeyPrefix) && key.endsWith(grpcMetadataKeySuffix))
               .forEach(key -> rawMetadata.put(
                       key,
                       headers.get(Metadata.Key.of(key, Metadata.BINARY_BYTE_MARSHALLER))
               ));
        return new ServerMetadataContainer(rawMetadata);
    }

    /**
     * Get all SiLA Client Metadata from {@link Context#current()}
     *
     * @implNote The {@link ServerMetadataContainer} is attached to the context by
     *         {@link MetadataExtractingInterceptor}. If the current call was not (yet) handled by this interceptor,
     *         this method will return {@code null}.
     */
    public static ServerMetadataContainer current() {
        return SILA_METADATA_KEY.get(Context.current());
    }

    /**
     * Get the received SiLA Client Metadata Protobuf message of the given type
     *
     * @param metadataMessageClass Protobuf message class of the desired SiLA Client Metadata
     *         ({@code <FeatureIdentifier>OuterClass.Metadata_<MetadataIdentifier>})
     * @param <T> Type of the desired SiLA Client Metadata Protobuf message
     *
     * @return The desired Protobuf message, or {@code null} if no such metadata was received
     */
    public <T extends Message> T get(Class<T> metadataMessageClass) {
        // e.g. sila-org.silastandard-core-authorizationservice-v1-metadata-accesstoken-bin
        final String grpcMetadataKey = metadataProtobufMessageClassToGrpcMetadataKey(metadataMessageClass);

        // get message bytes from gRPC metadata
        final byte[] messageBytes = rawMetadata.get(grpcMetadataKey);

        if (messageBytes == null) {
            // no such metadata received
            return null;
        }

        // parse as Protobuf message
        try {
            final Method parseMethod = metadataMessageClass.getMethod("parseFrom", byte[].class);
            //noinspection unchecked,PrimitiveArrayArgumentToVarargsMethod
            return (T) parseMethod.invoke(null, messageBytes);
        } catch (NoSuchMethodException e) {
            throw new RuntimeException("Given message class does not have a method parseFrom(byte[])");
        } catch (InvocationTargetException e) {
            throw new RuntimeException("Method parseFrom(byte[]) threw an exception", e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException("Runtime cannot access method parseFrom(byte[])");
        }
    }

    public boolean isEmpty() {
        return this.rawMetadata.isEmpty();
    }

    public boolean hasFQIMetadata(@NonNull final String FQIMetadata) {
        return this.rawMetadata.containsKey(
                MetadataUtils.fullyQualifiedMetadataIdentifierToGrpcMetadataKey(FQIMetadata)
        );
    }

    public boolean hasFQIMetadataHeader(@NonNull final String FQIMetadataHeader) {
        return this.rawMetadata.containsKey(FQIMetadataHeader);
    }
}
