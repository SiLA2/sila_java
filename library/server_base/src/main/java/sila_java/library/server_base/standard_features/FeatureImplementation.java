package sila_java.library.server_base.standard_features;

import io.grpc.BindableService;

/**
 * Feature implementation interface
 */
public interface FeatureImplementation {
    /**
     * Gets Feature Description in XML Form as String
     *
     * @return XML String
     */
    String getFeatureDescription();
    /**
     * Gets gRPC Service to serve
     *
     * @return bindable service
     */
    BindableService getService();
}
