package sila_java.library.server_base.binary_transfer.upload;


import io.grpc.ForwardingServerCallListener;
import io.grpc.Metadata;
import io.grpc.ServerCall;
import io.grpc.ServerCallHandler;
import io.grpc.ServerInterceptor;
import sila2.org.silastandard.SiLABinaryTransfer;
import sila_java.library.core.sila.binary_transfer.BinaryTransferErrorHandler;
import sila_java.library.core.sila.utils.FullyQualifiedIdentifierUtils;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Server-side interceptor which blocks all attempts to use Binary Upload for parameters which are not explicitly
 * whitelisted.
 */
public class BinaryUploadParameterWhitelistInterceptor implements ServerInterceptor {
    private final Set<String> allowedParameters;

    /**
     * Create a new {@link BinaryUploadParameterWhitelistInterceptor}
     *
     * @param allowedParameters Fully qualified parameter identifiers for which Binary Upload is allowed
     */
    public BinaryUploadParameterWhitelistInterceptor(List<String> allowedParameters) {
        for (final String allowedParameter : allowedParameters) {
            if (!FullyQualifiedIdentifierUtils.FullyQualifiedCommandParameterIdentifierPattern.matcher(allowedParameter)
                    .matches()) {
                throw new IllegalArgumentException(String.format(
                        "Not a fully qualified parameter identifier: '%s'",
                        allowedParameter
                ));
            }
        }
        this.allowedParameters = new HashSet<>(allowedParameters);
    }

    /**
     * Create a new {@link BinaryUploadParameterWhitelistInterceptor}
     *
     * @param allowedParameters Fully qualified parameter identifiers for which Binary Upload is allowed
     */
    public BinaryUploadParameterWhitelistInterceptor(String... allowedParameters) {
        this(Arrays.asList(allowedParameters));
    }

    @Override
    public <ReqT, RespT> ServerCall.Listener<ReqT> interceptCall(
            final ServerCall<ReqT, RespT> call, final Metadata headers, final ServerCallHandler<ReqT, RespT> next) {
        // only intercept CreateBinary RPC
        if (!call.getMethodDescriptor().getFullMethodName()
                .equals("sila2.org.silastandard.BinaryUpload/CreateBinary")) {
            return next.startCall(call, headers);
        }

        // inject message handler
        return new ForwardingServerCallListener<ReqT>() {
            /**
             * Listener which does nothing with the received call, so it won't fail if the call was closed
             */
            private final ServerCall.Listener<ReqT> NOOP_LISTENER = new ServerCall.Listener<ReqT>() {};
            private ServerCall.Listener<ReqT> delegate = next.startCall(call, headers);

            /**
             * {@inheritDoc}
             */
            @Override
            protected ServerCall.Listener<ReqT> delegate() {
                return delegate;
            }

            /**
             * {@inheritDoc}
             */
            @Override
            public void onMessage(final ReqT message) {
                SiLABinaryTransfer.CreateBinaryRequest request = (SiLABinaryTransfer.CreateBinaryRequest) message;
                String parameterIdentifier = request.getParameterIdentifier();
                if (!allowedParameters.contains(parameterIdentifier)) {
                    call.close(BinaryTransferErrorHandler.generateBinaryTransferError(
                            SiLABinaryTransfer.BinaryTransferError.ErrorType.BINARY_UPLOAD_FAILED,
                            String.format(
                                    "Binary upload is not allowed for parameter '%s'",
                                    parameterIdentifier
                            )
                    ).getStatus(), headers);
                    delegate = NOOP_LISTENER;
                    return;
                }
                super.onMessage(message);
            }
        };
    }
}
