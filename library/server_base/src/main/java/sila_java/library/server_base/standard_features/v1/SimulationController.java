package sila_java.library.server_base.standard_features.v1;

import io.grpc.BindableService;
import io.grpc.stub.StreamObserver;
import lombok.*;
import sila2.org.silastandard.core.simulationcontroller.v1.SimulationControllerGrpc;
import sila2.org.silastandard.core.simulationcontroller.v1.SimulationControllerOuterClass;
import sila_java.library.core.sila.errors.SiLAErrorException;
import sila_java.library.core.sila.types.SiLABoolean;
import sila_java.library.server_base.standard_features.FeatureImplementation;
import sila_java.library.sila_base.EmptyClass;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Supplier;

import static sila_java.library.core.utils.FileUtils.getFileContent;

/**
 * Class responsible for handling multiple simulated and real contexts
 */
@AllArgsConstructor
@RequiredArgsConstructor
public class SimulationController extends SimulationControllerGrpc.SimulationControllerImplBase
        implements FeatureImplementation, AutoCloseable
{
    public static final String FULLY_QUALIFIED_FEATURE_IDENTIFIER = "org.silastandard/core/SimulationController/v1";
    private final List<ContextSwitcher> contextSwitchers = Collections.synchronizedList(new ArrayList<>());
    private final AuthorizationController.Authorize authorize;
    private boolean simulation;


    /**
     * Interface representing a closeable Context
     */
    public interface Context extends AutoCloseable {
        @Override
        default void close() {}
    }

    /**
     * Class responsible for managing two context, a real and simulated one
     * @param <T> The context type
     */
    @Getter
    public final class ContextSwitcher<T extends Context> implements AutoCloseable {
        private final Supplier<T> realProvider;
        private final T simulation;
        private T real;

        /**
         * Constructor
         * @param realProvider the real context provider
         * @param simulation the simulation context
         */
        private ContextSwitcher(@NonNull final Supplier<T> realProvider, @NonNull final T simulation) {
            this.realProvider = realProvider;
            this.simulation = simulation;
        }

        /**
         * Get the active context
         * @implNote When calling this function for the first time with real mode enabled,
         * the real context will be retrieved from the Provider and cached
         * @return The active context
         */
        public T getActive() {
            synchronized (this) {
                if (!SimulationController.this.simulation) {
                    if (real == null) {
                        real = realProvider.get();
                    }
                }
                return SimulationController.this.simulation ? simulation : real;
            }
        }

        /**
         * Close the real and simulation context
         */
        public void close() {
            if (real != null) {
                real.close();
                real = null;
            }
            simulation.close();
        }
    }

    /**
     * Factory method that creates a ContextSwitcher
     * @param real The real context provider
     * @param sim The simulated context
     * @param <T> The context type
     * @return a new ContextSwitcher
     */
    public <T extends Context> ContextSwitcher<T> createContextSwitcher(
            @NonNull final Supplier<T> real,
            @NonNull final T sim
    ) {
        final ContextSwitcher<T> wrapper = new ContextSwitcher<>(real, sim);
        contextSwitchers.add(wrapper);
        return wrapper;
    }

    /**
     * Closes all the context switcher created automatically
     */
    @Override
    public void close() {
        contextSwitchers.forEach(ContextSwitcher::close);
    }

    /**
     * @inheritDoc
     */
    @Override
    @SneakyThrows(IOException.class)
    public String getFeatureDescription() {
        return getFileContent(EmptyClass.class.getResourceAsStream(
                "/sila_base/feature_definitions/org/silastandard/core/SimulationController-v1_0.sila.xml"
        ));
    }

    /**
     * @inheritDoc
     */
    @Override
    public BindableService getService() {
        return this;
    }


    /**
     * Start simulation mode
     * @param request the request
     * @param responseObserver the response
     */
    @Override
    public void startSimulationMode(
            final SimulationControllerOuterClass.StartSimulationMode_Parameters request,
            final StreamObserver<SimulationControllerOuterClass.StartSimulationMode_Responses> responseObserver
    ) {
        try {
            this.authorize.authorize(FULLY_QUALIFIED_FEATURE_IDENTIFIER);
        } catch (SiLAErrorException e) {
            responseObserver.onError(e);
            return;
        }
        simulation = true;
        responseObserver.onNext(SimulationControllerOuterClass.StartSimulationMode_Responses.newBuilder().build());
        responseObserver.onCompleted();
    }

    /**
     * Start real mode
     * @param request the request
     * @param responseObserver the response
     */
    @Override
    public void startRealMode(
            final SimulationControllerOuterClass.StartRealMode_Parameters request,
            final StreamObserver<SimulationControllerOuterClass.StartRealMode_Responses> responseObserver
    ) {
        try {
            this.authorize.authorize(FULLY_QUALIFIED_FEATURE_IDENTIFIER);
        } catch (SiLAErrorException e) {
            responseObserver.onError(e);
            return;
        }
        simulation = false;
        responseObserver.onNext(SimulationControllerOuterClass.StartRealMode_Responses.newBuilder().build());
        responseObserver.onCompleted();
    }

    /**
     * Get simulation mode
     * @param request the request
     * @param responseObserver the response
     */
    @Override
    public void getSimulationMode(
            final SimulationControllerOuterClass.Get_SimulationMode_Parameters request,
            final StreamObserver<SimulationControllerOuterClass.Get_SimulationMode_Responses> responseObserver
    ) {
        try {
            this.authorize.authorize(FULLY_QUALIFIED_FEATURE_IDENTIFIER);
        } catch (SiLAErrorException e) {
            responseObserver.onError(e);
            return;
        }
        responseObserver.onNext(
                SimulationControllerOuterClass.Get_SimulationMode_Responses
                        .newBuilder()
                        .setSimulationMode(SiLABoolean.from(simulation))
                        .build()
        );
        responseObserver.onCompleted();
    }
}
