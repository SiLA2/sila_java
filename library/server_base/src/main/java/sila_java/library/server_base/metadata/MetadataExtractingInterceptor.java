package sila_java.library.server_base.metadata;

import io.grpc.*;

import java.util.HashMap;

/**
 * gRPC server interceptor which captures incoming SiLA Client Metadata message bytes and attaches it to the
 * {@link Context#current()} as {@link ServerMetadataContainer}
 */
public class MetadataExtractingInterceptor implements ServerInterceptor {
    public static final Context.Key<ServerMetadataContainer> SILA_METADATA_KEY = Context.key("SILA_METADATA_KEY");

    /**
     * Capture incoming SiLA Client Metadata message bytes in an {@link ServerMetadataContainer} and attach it to
     * {@link Context#current()}, then continue handling the call
     *
     * @param call object to receive response messages
     * @param headers which can contain extra call metadata from {@link ClientCall#start}, e.g. authentication
     *         credentials.
     * @param next next processor in the interceptor chain
     * @param <ReqT> Request type
     * @param <RespT> Response type
     *
     * @return The incoming call, with SiLA Client Metadata message bytes attached to {@link Context#current()}
     *
     * @implNote Does not parse the SiLA Client Metadata messages
     */
    @Override
    public <ReqT, RespT> ServerCall.Listener<ReqT> interceptCall(
            ServerCall<ReqT, RespT> call, Metadata headers, ServerCallHandler<ReqT, RespT> next) {
        final Context context = Context.current()
                .withValue(SILA_METADATA_KEY, ServerMetadataContainer.fromHeaders(headers));

        return Contexts.interceptCall(context, call, headers, next);
    }
}
