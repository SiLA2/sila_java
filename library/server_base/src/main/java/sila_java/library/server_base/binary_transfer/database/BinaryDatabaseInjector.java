package sila_java.library.server_base.binary_transfer.database;

import io.grpc.Context;
import io.grpc.Contexts;
import io.grpc.Metadata;
import io.grpc.ServerCall;
import io.grpc.ServerCallHandler;
import io.grpc.ServerInterceptor;

/**
 * Server-side interceptor which appends a reference to the current {@link BinaryDatabase} to the current context
 */
public class BinaryDatabaseInjector implements ServerInterceptor {
    /**
     * The {@link Context.Key} to which the database reference is bound
     */
    public static final Context.Key<BinaryDatabase> BINARY_DATABASE_KEY = Context.key("BINARY_DATABASE_KEY");
    private final BinaryDatabase binaryDatabase;

    /**
     * @param binaryDatabase The database to inject
     */
    public BinaryDatabaseInjector(BinaryDatabase binaryDatabase) {
        this.binaryDatabase = binaryDatabase;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <ReqT, RespT> ServerCall.Listener<ReqT> interceptCall(
            final ServerCall<ReqT, RespT> call,
            final Metadata headers,
            final ServerCallHandler<ReqT, RespT> next
    ) {
        return Contexts.interceptCall(
                Context.current().withValue(BINARY_DATABASE_KEY, this.binaryDatabase), call, headers, next
        );
    }
}
