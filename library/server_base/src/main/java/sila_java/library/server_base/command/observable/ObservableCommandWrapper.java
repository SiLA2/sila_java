package sila_java.library.server_base.command.observable;

import io.grpc.StatusRuntimeException;
import io.grpc.stub.StreamObserver;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.SiLAFramework;
import sila_java.library.core.sila.errors.SiLAErrors;
import sila_java.library.core.sila.types.SiLAReal;

import javax.annotation.Nullable;
import java.time.Duration;
import java.time.Instant;
import java.util.Iterator;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.*;

/**
 * Wrapper for all observable commands
 * @param <ParamType>
 * @param <ResultType>
 */
@Slf4j
public class ObservableCommandWrapper<ParamType, ResultType> implements AutoCloseable {
    @Getter private final UUID executionId = UUID.randomUUID();
    @Getter private final ParamType parameter;
    @Getter private final Set<StreamObserver> stateObservers = ConcurrentHashMap.newKeySet();
    @Getter private final Set<StreamObserver> intermediateResponseObservers = ConcurrentHashMap.newKeySet();

    @Getter(AccessLevel.PACKAGE)
    private final ExecutionInfo executionInfo = new ExecutionInfo();
    @Getter
    private final Duration lifeTimeOfExecution;

    @Getter(AccessLevel.PACKAGE)
    private final RunnableCommandTask<ParamType, ResultType> task;

    @Getter(AccessLevel.PACKAGE)
    private final Future<ResultType> future;

    @Getter
    @Nullable
    private Instant expireAt = null;

    static final class ExecutionInfo {
        @Getter(AccessLevel.PACKAGE)
        private SiLAFramework.ExecutionInfo.CommandStatus state = SiLAFramework.ExecutionInfo.CommandStatus.waiting;
        private double progressionPercent = 0d;
        private Duration estimatedRemainingTime = null;
    }

    /**
     * Constructor
     * @param parameter The command parameter
     * @param task The task
     * @param runner The runner
     */
    ObservableCommandWrapper(
            @NonNull final ParamType parameter,
            @NonNull final RunnableCommandTask<ParamType, ResultType> task,
            @NonNull final ObservableCommandTaskRunner runner
    ) {
        this(parameter, task, runner, null);
    }

    /**
     * Constructor
     * @param param The command parameter
     * @param task The task
     * @param runner The runner
     * @param lifeTimeOfExecution The life time of execution of the command
     *                            If null the command will never expire unless removed manually
     */
    ObservableCommandWrapper(
            @NonNull final ParamType param,
            @NonNull final RunnableCommandTask<ParamType, ResultType> task,
            @NonNull final ObservableCommandTaskRunner runner,
            @Nullable final Duration lifeTimeOfExecution
    ) {
        this.parameter = param;
        this.task = task;
        this.lifeTimeOfExecution = lifeTimeOfExecution;
        this.setStateAndNotify(SiLAFramework.ExecutionInfo.CommandStatus.waiting);
        this.future = runner.enqueueTask(() -> {
            this.task.preRun(param, this.executionId);
            return this.call();
        });
    }

    /**
     * Retrieving Initial Command Confirmation
     * @return Command Confirmation
     */
    public SiLAFramework.CommandConfirmation getCommandConfirmation() {
        final SiLAFramework.CommandConfirmation.Builder builder = SiLAFramework.CommandConfirmation
                .newBuilder()
                .setCommandExecutionUUID(SiLAFramework.CommandExecutionUUID
                        .newBuilder()
                        .setValue(this.executionId.toString())
                        .build()
                );
        if (this.lifeTimeOfExecution != null) {
            builder.setLifetimeOfExecution(SiLAFramework.Duration
                    .newBuilder()
                    .setSeconds(this.lifeTimeOfExecution.getSeconds())
                    .setNanos(this.lifeTimeOfExecution.getNano())
                    .build()
            );
        }
        return builder.build();
    }

    /**
     * Wraps the command task with appropriate state notifications
     * @return Result if successful
     */
    public ResultType call() throws StatusRuntimeException {
        setStateAndNotify(SiLAFramework.ExecutionInfo.CommandStatus.running);
        if (this.lifeTimeOfExecution != null) {
            this.expireAt = Instant.now().plus(this.lifeTimeOfExecution);
        }
        try {
            return finishCommand(this.task.run(this), null);
        } catch (final StatusRuntimeException e) {
            finishCommand(null, e);
        } catch (final Throwable e) {
            finishCommand(null, SiLAErrors.generateGenericExecutionError(e));
        }
        return finishCommand(null,
                SiLAErrors.generateUndefinedExecutionError(
                        "No result was returned after end of command execution"
                )
        );
    }

    public ResultType finishCommand(@Nullable final ResultType result, @Nullable final StatusRuntimeException e) throws StatusRuntimeException {
        // todo check if empty result are allowed by the standard
        if (e != null) {
            log.warn("Error occurred during observable command " + executionId + " task execution", e);
            setStateAndNotify(SiLAFramework.ExecutionInfo.CommandStatus.finishedWithError);
            closeObservers(this.stateObservers);
            closeObservers(this.intermediateResponseObservers);
            this.task.onFinish(this, true);
            throw e;
        }
        setStateAndNotify(SiLAFramework.ExecutionInfo.CommandStatus.finishedSuccessfully);
        closeObservers(this.stateObservers);
        closeObservers(this.intermediateResponseObservers);
        this.task.onFinish(this, false);
        return result;
    }

    /**
     * Add a command state observer
     * @param streamObserver The observer
     */
    public void addStateObserver(@NonNull final StreamObserver<SiLAFramework.ExecutionInfo> streamObserver) {
        streamObserver.onNext(getSiLAExecutionInfo());

        if (this.isDone()) {
            streamObserver.onNext(getSiLAExecutionInfo()); // prevent race condition
            streamObserver.onCompleted();
            return;
        }
        this.stateObservers.add(streamObserver);
    }

    /**
     * Add an intermediate response observer
     * @param streamObserver The observer
     */
    public <IntermediateResultType> void addIntermediateResponseObserver(
            @NonNull final StreamObserver<IntermediateResultType> streamObserver
    ) {
        if (this.isDone()) {
            streamObserver.onCompleted();
            return;
        }
        this.intermediateResponseObservers.add(streamObserver);
    }

    /**
     * Send command result to an observer or an error if command execution was not successful
     * @param streamObserver the observer
     */
    public void sendResult(@NonNull final StreamObserver<ResultType> streamObserver) {
        if (future.isDone() &&
                (
                        this.executionInfo.state == SiLAFramework.ExecutionInfo.CommandStatus.finishedSuccessfully ||
                        this.executionInfo.state == SiLAFramework.ExecutionInfo.CommandStatus.finishedWithError
                )
        ) {
            try {
                streamObserver.onNext(future.get());
            } catch (final InterruptedException | ExecutionException | StatusRuntimeException e) {
                streamObserver.onError(e);
            }
        } else {
            streamObserver.onError(SiLAErrors.generateFrameworkError(
                    SiLAFramework.FrameworkError.ErrorType.COMMAND_EXECUTION_NOT_FINISHED,
                    "Command Execution is not finished. The result is not yet available."
            ));
        }
        streamObserver.onCompleted();
    }

    /**
     * Notify intermediate response observers an intermediate response
     * @param intermediateResponse The intermediate response to notify
     */
    public <IntermediateResultType>  void notifyIntermediateResponse(
            @NonNull final IntermediateResultType intermediateResponse
    ) {
        ObservableCommandWrapper.notifyIntermediateObservers(this.intermediateResponseObservers, intermediateResponse);
    }

    private static <IntermediateResultType> void notifyIntermediateObservers(
            @NonNull final Set<StreamObserver> intermediateResponseObservers,
            @NonNull final IntermediateResultType intermediateResponse
    ) {
        final Iterator<StreamObserver> it = intermediateResponseObservers.iterator();
        while (it.hasNext()) {
            final StreamObserver<IntermediateResultType> element = it.next();
            try {
                element.onNext(intermediateResponse);
            } catch (final Exception e) {
                element.onError(e);
                it.remove();
            }
        }
    }

    /**
     * Dispose all resources owned by the wrapper
     */
    @Override
    public void close() {
        this.future.cancel(true);
        this.task.close();
        if (!isDone()) {
            this.setStateAndNotify(SiLAFramework.ExecutionInfo.CommandStatus.finishedWithError);
        }
        ObservableCommandWrapper.closeObservers(this.stateObservers);
        this.stateObservers.clear();
        ObservableCommandWrapper.closeObservers(this.intermediateResponseObservers);
        this.intermediateResponseObservers.clear();
        this.expireAt = Instant.now();
    }

    /**
     * Return whether or not the command is done
     * @return true if the command is done, otherwise false
     */
    public boolean isDone() {
        return (this.executionInfo.state == SiLAFramework.ExecutionInfo.CommandStatus.finishedWithError ||
                this.executionInfo.state == SiLAFramework.ExecutionInfo.CommandStatus.finishedSuccessfully);
    }

    public boolean isExpired() {
        return (this.expireAt != null && this.expireAt.isBefore(Instant.now()));
    }

    /**
     * Set the execution information and notify observers
     * @param progressionPercent The progression percentage
     * @param estimatedRemainingTime The estimated remaining time
     */
    public void setExecutionInfoAndNotify(
            final double progressionPercent,
            @Nullable final Duration estimatedRemainingTime
    ) {
        this.executionInfo.progressionPercent = progressionPercent;
        this.executionInfo.estimatedRemainingTime = estimatedRemainingTime;
        this.notifyState();
    }

    /**
     * Set the state and notify observers of the change
     * @param state The state to set
     */
    private void setStateAndNotify(@NonNull final SiLAFramework.ExecutionInfo.CommandStatus state) {
        this.executionInfo.state = state;
        this.notifyState();
    }

    /**
     * Notify state observers with the current state
     */
    private void notifyState() {
        ObservableCommandWrapper.notifyObservers(this.stateObservers, getSiLAExecutionInfo());
    }

    /**
     * Utility Function to Notify all observers with a value
     * @param observers The observers
     * @param valueToNotify The value to notify
     * @param <NotifyType> The type of the value to notify
     */
    private static <NotifyType> void notifyObservers(
            @NonNull final Set<StreamObserver> observers,
            @NonNull final NotifyType valueToNotify
    ) {
        final Iterator<StreamObserver> it = observers.iterator();
        while (it.hasNext()) {
            final StreamObserver<NotifyType> element = it.next();
            try {
                element.onNext(valueToNotify);
            } catch (final Exception e) {
                log.debug("Error occurred while notifying observer");
                element.onError(e);
                it.remove();
            }
        }
    }

    /**
     * Utility Function to close all observers of a set
     * @param observers the observers
     * @param <NotifyType> The type observers are observing
     */
    private static <NotifyType> void closeObservers(@NonNull final Set<StreamObserver> observers) {
        final Iterator<StreamObserver> it = observers.iterator();
        while (it.hasNext()) {
            final StreamObserver<NotifyType> element = it.next();
            try {
                element.onCompleted();
            } catch (final Exception e) {
                it.remove();
            }
        }
    }

    /**
     * Return the current execution information
     * @return the current execution information
     */
    private SiLAFramework.ExecutionInfo getSiLAExecutionInfo() {
        final Duration remainingTime = this.executionInfo.estimatedRemainingTime;
        final SiLAFramework.ExecutionInfo.Builder builder = SiLAFramework.ExecutionInfo
                .newBuilder()
                .setCommandStatus(this.executionInfo.state)
                .setProgressInfo(SiLAReal.from(this.executionInfo.progressionPercent));
        if (this.lifeTimeOfExecution != null) {
            final Duration lifetimeLeft = this.expireAt == null ?
                    this.lifeTimeOfExecution :
                    Duration.between(Instant.now(), this.expireAt);
            if (lifetimeLeft.isNegative()) {
                builder.setUpdatedLifetimeOfExecution(SiLAFramework.Duration
                        .newBuilder()
                        .setSeconds(0)
                        .setNanos(0)
                        .build()
                );
            } else {
                builder.setUpdatedLifetimeOfExecution(SiLAFramework.Duration
                        .newBuilder()
                        .setSeconds(lifetimeLeft.getSeconds())
                        .setNanos(lifetimeLeft.getNano())
                        .build()
                );
            }
        }
        if (remainingTime != null) {
            builder.setEstimatedRemainingTime(SiLAFramework.Duration
                    .newBuilder()
                    .setSeconds(remainingTime.getSeconds())
                    .setNanos(remainingTime.getNano())
                    .build()
            );
        }
        return builder.build();
    }
}
