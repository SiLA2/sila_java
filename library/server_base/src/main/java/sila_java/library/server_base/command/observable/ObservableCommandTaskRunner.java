package sila_java.library.server_base.command.observable;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.*;

/**
 * Observable command task runner
 */
@Slf4j
public class ObservableCommandTaskRunner implements AutoCloseable {
    private final ThreadPoolExecutor executorService;

    /**
     * Constructor
     * @param maxQueueSize the maximum size of the queue
     * @param maxConcurrentCalls the maximum number of concurrent calls
     */
    public ObservableCommandTaskRunner(final int maxQueueSize, final int maxConcurrentCalls) {
        if (maxQueueSize < 0) {
            throw new IllegalArgumentException("Max Queue size cannot be lower than 0");
        }
        if (maxConcurrentCalls < 1) {
            throw new IllegalArgumentException("Max Concurrent call cannot be lower than 1");
        }

        final BlockingQueue<Runnable> queueImpl = (maxQueueSize == 0) ?
                (new SynchronousQueue<>(true)) :
                (new ArrayBlockingQueue<>(maxQueueSize, true));
        this.executorService = new ThreadPoolExecutor(
                maxConcurrentCalls,
                maxConcurrentCalls,
                0L,
                TimeUnit.MILLISECONDS,
                queueImpl
        );
    }

    /**
     * Add a new task to the queue
     * @param callable The calling task
     * @return A future result
     * @throws RejectedExecutionException if the queue cannot accept anymore task
     */
    <ResultType> Future<ResultType> enqueueTask(
            @NonNull final Callable<ResultType> callable
    ) throws RejectedExecutionException {
        return this.executorService.submit(callable);
    }

    /**
     * Dispose the runner resources
     * After this call the runner won't be usable anymore
     */
    @Override
    public void close() {
        this.executorService.shutdownNow();
    }
}
