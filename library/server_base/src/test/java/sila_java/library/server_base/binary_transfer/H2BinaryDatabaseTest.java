package sila_java.library.server_base.binary_transfer;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import sila_java.library.core.sila.binary_transfer.BinaryInfo;
import sila_java.library.server_base.binary_transfer.database.BinaryDatabase;
import sila_java.library.server_base.binary_transfer.database.BinaryDatabaseException;
import sila_java.library.server_base.binary_transfer.database.impl.H2BinaryDatabase;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.UUID;

public class H2BinaryDatabaseTest {
    private BinaryDatabase db;

    @BeforeEach
    public void setUp() throws SQLException {
        db = new H2BinaryDatabase(UUID.randomUUID());
    }

    @AfterEach
    public void tearDown() throws Exception {
        db.removeAllBinaries();
        db.close();
    }

    @Test
    void testExceptionsThrowsForUnknownUUIDs() {
        Assertions.assertThrows(BinaryDatabaseException.class, () -> db.getBinary(UUID.randomUUID()));
        Assertions.assertThrows(BinaryDatabaseException.class, () -> db.getBinaryInfo(UUID.randomUUID()));
    }

    @Test
    void testValidDataFlow() throws IOException, BinaryDatabaseException, SQLException {
        byte[] bytes = Utils.generateRandomLargeBinary();
        UUID binaryUUID = UUID.randomUUID();

        // add binary
        try (InputStream byteStream = new ByteArrayInputStream(bytes)) {
            db.addBinary(binaryUUID, byteStream, "ch.unitelabs/test/UnobservableCommandTest/v1/Command/XOREncipher/Parameter/Data");
        }

        // get binary info
        BinaryInfo binaryInfoFromDb = db.getBinaryInfo(binaryUUID);
        Assertions.assertEquals(bytes.length, binaryInfoFromDb.getLength());
        Assertions.assertEquals(binaryUUID, binaryInfoFromDb.getId());

        // get binary
        Binary binaryFromDb = db.getBinary(binaryUUID);
        Assertions.assertEquals(bytes.length, binaryFromDb.getData().length());
        Assertions.assertEquals(bytes.length, binaryFromDb.getInfo().getLength());
        Assertions.assertArrayEquals(bytes, binaryFromDb.getData().getBytes(1, bytes.length));
        Assertions.assertEquals(binaryUUID, binaryFromDb.getInfo().getId());

        // remove binary
        db.removeBinary(binaryUUID);
        Assertions.assertThrows(BinaryDatabaseException.class, () -> db.getBinary(binaryUUID));
        Assertions.assertThrows(BinaryDatabaseException.class, () -> db.getBinaryInfo(binaryUUID));
    }

    @Test
    public void testRemoveAll() throws BinaryDatabaseException {
        UUID uuid1 = UUID.randomUUID();
        UUID uuid2 = UUID.randomUUID();
        db.addBinary(uuid1, Utils.generateRandomLargeBinary(), "ch.unitelabs/test/UnobservableCommandTest/v1/Command/XOREncipher/Parameter/Data");
        db.addBinary(uuid2, Utils.generateRandomLargeBinary(), "ch.unitelabs/test/UnobservableCommandTest/v1/Command/XOREncipher/Parameter/Data");
        db.removeAllBinaries();

        Assertions.assertThrows(BinaryDatabaseException.class, () -> db.getBinary(uuid1));
        Assertions.assertThrows(BinaryDatabaseException.class, () -> db.getBinaryInfo(uuid1));
        Assertions.assertThrows(BinaryDatabaseException.class, () -> db.getBinary(uuid1));
        Assertions.assertThrows(BinaryDatabaseException.class, () -> db.getBinaryInfo(uuid2));
    }
}
