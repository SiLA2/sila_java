package sila_java.library.server_base.binary_transfer;

import io.grpc.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import sila2.org.silastandard.SiLABinaryTransfer;
import sila_java.library.core.sila.binary_transfer.BinaryDownloadHelper;
import sila_java.library.core.sila.binary_transfer.BinaryInfo;
import sila_java.library.core.sila.binary_transfer.BinaryTransferErrorHandler;
import sila_java.library.core.sila.binary_transfer.BinaryUploadHelper;
import sila_java.library.server_base.binary_transfer.database.BinaryDatabaseException;
import sila_java.library.server_base.binary_transfer.database.impl.H2BinaryDatabase;
import sila_java.library.server_base.binary_transfer.download.DownloadService;
import sila_java.library.server_base.binary_transfer.upload.UploadService;
import sila_java.library.server_base.standard_features.v1.AuthorizationController;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.SQLException;
import java.util.Collections;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

import static sila_java.library.core.utils.FileUtils.getFileContent;

public class BinaryTransferTest {
    private Server server;
    private BinaryUploadHelper uploader;
    private BinaryDownloadHelper downloader;
    private final String parameterIdentifier
            = "org.silastandard/test/BinaryTransferTest/v1/Command/EchoBinaryAndMetadataString/Parameter/Binary";
    private H2BinaryDatabase db;

    private static final String SERVER_HOSTNAME = "127.0.0.1";
    private static final int SERVER_PORT = 50052;

    @BeforeEach
    public void setUp() throws SQLException, IOException, InterruptedException {
        db = new H2BinaryDatabase(UUID.randomUUID());
        final String featureStr = getFileContent(
                Objects.requireNonNull(
                        this.getClass().getResourceAsStream("BinaryTransferTest.sila.xml")
                )
        );
        server = Grpc.newServerBuilderForPort(SERVER_PORT, InsecureServerCredentials.create())
                .addService(new UploadService(db, Collections.singleton(featureStr), AuthorizationController.DEFAULT_AUTHORIZE))
                .addService(new DownloadService(db, AuthorizationController.DEFAULT_AUTHORIZE)).build();
        server.start();

        Thread.sleep(100);

        final Channel channel = ManagedChannelBuilder.forAddress(SERVER_HOSTNAME, SERVER_PORT).usePlaintext().build();
        uploader = new BinaryUploadHelper(channel);
        downloader = new BinaryDownloadHelper(channel);
    }

    @AfterEach
    public void tearDown() {
        server.shutdown();
    }

    @Test
    void testUploadFromByteArray() throws BinaryDatabaseException {
        final byte[] bytesToUpload = Utils.generateRandomLargeBinary();
        final BinaryInfo uploadInfo = uploader.upload(bytesToUpload, parameterIdentifier);
        Assertions.assertEquals(bytesToUpload.length, uploadInfo.getLength(), "Upload info reports wrong size");
        Assertions.assertTrue(db.hasBinary(uploadInfo.getId()));
        Assertions.assertEquals(
                bytesToUpload.length,
                db.getBinaryInfo(uploadInfo.getId()).getLength(),
                "Database reports wrong size"
        );
    }

    @Test
    void testUploadFromFile() throws BinaryDatabaseException, IOException {
        final byte[] bytesToUpload = Utils.generateRandomLargeBinary();
        final Path tempUploadFile = Files.createTempFile("sila-download-test-", ".bin");
        tempUploadFile.toFile().deleteOnExit();
        Files.write(tempUploadFile, bytesToUpload);

        BinaryInfo uploadInfo = uploader.upload(
                Files.newInputStream(tempUploadFile),
                bytesToUpload.length,
                parameterIdentifier
        );
        Assertions.assertEquals(bytesToUpload.length, uploadInfo.getLength(), "Upload info reports wrong size");
        Assertions.assertTrue(db.hasBinary(uploadInfo.getId()));
        Assertions.assertEquals(
                bytesToUpload.length,
                db.getBinaryInfo(uploadInfo.getId()).getLength(),
                "Database reports wrong size"
        );
    }

    @Test
    void testDownloadToByteArray() {
        final byte[] bytesToUpload = Utils.generateRandomLargeBinary();
        BinaryInfo uploadInfo = uploader.upload(bytesToUpload, parameterIdentifier);

        Assertions.assertArrayEquals(bytesToUpload, downloader.download(uploadInfo.getId()));
    }

    @Test
    void testDownloadToFile() throws IOException {
        final byte[] bytesToUpload = Utils.generateRandomLargeBinary();
        BinaryInfo uploadInfo = uploader.upload(bytesToUpload, parameterIdentifier);

        Path tempDownloadFile = Files.createTempFile("sila-download-test-", ".bin");
        tempDownloadFile.toFile().deleteOnExit();

        downloader.downloadToFile(uploadInfo.getId(), tempDownloadFile);
        Assertions.assertArrayEquals(bytesToUpload, Files.readAllBytes(tempDownloadFile));
    }

    @Test
    void testUploaderCanDeleteBinary() throws BinaryDatabaseException {
        final byte[] bytesToUpload = Utils.generateRandomLargeBinary();
        BinaryInfo uploadInfo = uploader.upload(bytesToUpload, parameterIdentifier);
        UUID uploadId = uploadInfo.getId();

        Assertions.assertTrue(db.hasBinary(uploadInfo.getId()));
        Assertions.assertEquals(bytesToUpload.length, downloader.getBinaryInfo(uploadId).getLength());
        uploader.deleteBinary(uploadId);
        Assertions.assertFalse(db.hasBinary(uploadInfo.getId()));
        assertThrowsBinaryTransferError(() -> downloader.getBinaryInfo(uploadId));
    }

    @Test
    void testDownloaderCanDeleteBinary() throws BinaryDatabaseException {
        final byte[] bytesToUpload = Utils.generateRandomLargeBinary();
        BinaryInfo uploadInfo = uploader.upload(bytesToUpload, parameterIdentifier);
        UUID uploadId = uploadInfo.getId();

        Assertions.assertTrue(db.hasBinary(uploadInfo.getId()));
        Assertions.assertEquals(bytesToUpload.length, downloader.getBinaryInfo(uploadId).getLength());
        downloader.deleteBinary(uploadId);
        Assertions.assertFalse(db.hasBinary(uploadInfo.getId()));
        assertThrowsBinaryTransferError(() -> downloader.getBinaryInfo(uploadId));
    }

    private void assertThrowsBinaryTransferError(Executable call) {
        StatusRuntimeException ex = Assertions.assertThrows(
                StatusRuntimeException.class,
                call,
                "Didn't throw StatusRuntimeException"
        );
        Assertions.assertEquals(Status.Code.ABORTED, ex.getStatus().getCode(), "Call failed with wrong status");
        Optional<SiLABinaryTransfer.BinaryTransferError> maybeErr
                = BinaryTransferErrorHandler.retrieveBinaryTransferError(ex);
        Assertions.assertTrue(maybeErr.isPresent(), "No BinaryTransferError");
        Assertions.assertEquals(
                SiLABinaryTransfer.BinaryTransferError.ErrorType.INVALID_BINARY_TRANSFER_UUID,
                maybeErr.get().getErrorType(),
                "Error has wrong type"
        );
        Assertions.assertTrue(maybeErr.get().getMessage().length() > 0, "Error has no error message");
    }
}
