package sila_java.library.server_base.command.observable;

import io.grpc.StatusRuntimeException;
import io.grpc.stub.StreamObserver;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import sila2.org.silastandard.SiLAFramework;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.atomic.AtomicBoolean;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class ObservableCommandTest {

    @Test
    void erroredTask() throws InterruptedException {
        final ObservableCommandTaskRunner runner = new ObservableCommandTaskRunner(1, 1);
        ErroredObservableCommand<Integer, Integer> command = new ErroredObservableCommand<>(0, 42, runner);
        Thread.sleep(TestableObservableCommand.SLEEP_MS);
        Assertions.assertEquals(
                SiLAFramework.ExecutionInfo.CommandStatus.finishedWithError,
                command.cmd.getExecutionInfo().getState()
        );
        Assertions.assertThrows(ExecutionException.class, () -> command.cmd.getFuture().get());
    }

    @Test
    void resultLessTask() throws InterruptedException {
        // todo check if allowed by standard
        final ObservableCommandTaskRunner runner = new ObservableCommandTaskRunner(1, 1);
        ResultLessObservableCommand<Integer, Integer> command = new ResultLessObservableCommand<>(0, 42, runner);
        command.running.set(false);
        Thread.sleep(TestableObservableCommand.SLEEP_MS);
        Assertions.assertEquals(
                SiLAFramework.ExecutionInfo.CommandStatus.finishedSuccessfully,
                command.cmd.getExecutionInfo().getState()
        );
    }

    @Test
    void sequentialTasks() throws InterruptedException, ExecutionException {
        final ObservableCommandTaskRunner runner = new ObservableCommandTaskRunner(1, 1);
        TestableObservableCommand<Integer, Integer> cmd1 =
                new TestableObservableCommand<>(0, 42, runner);
        TestableObservableCommand<Integer, Integer> cmd2 =
                new TestableObservableCommand<>(0, 1337, runner);
        Assertions.assertThrows(
                RejectedExecutionException.class,
                () -> new TestableObservableCommand<>(0, 0xDE4D, runner)
        );
        Thread.sleep(TestableObservableCommand.SLEEP_MS);
        Assertions.assertEquals(SiLAFramework.ExecutionInfo.CommandStatus.running, cmd1.cmd.getExecutionInfo().getState());
        Assertions.assertEquals(SiLAFramework.ExecutionInfo.CommandStatus.waiting, cmd2.cmd.getExecutionInfo().getState());
        Assertions.assertEquals(0, cmd1.nbCommandAdded);
        cmd1.cmd.getTask().onNewCommand(cmd1.cmd);
        Assertions.assertEquals(1, cmd1.nbCommandAdded);
        cmd1.cmd.getTask().onNewCommand(cmd2.cmd);
        Assertions.assertEquals(2, cmd1.nbCommandAdded);
        cmd1.running.set(false);
        cmd1.cmd.getFuture().get();
        Assertions.assertEquals(SiLAFramework.ExecutionInfo.CommandStatus.finishedSuccessfully, cmd1.cmd.getExecutionInfo().getState());
        Assertions.assertEquals(cmd1.expectedResult, cmd1.cmd.getFuture().get());
        Thread.sleep(TestableObservableCommand.SLEEP_MS);
        Assertions.assertEquals(SiLAFramework.ExecutionInfo.CommandStatus.running, cmd2.cmd.getExecutionInfo().getState());
        cmd2.running.set(false);
        cmd1.cmd.getTask().onNewCommand(cmd2.cmd);
        Assertions.assertEquals(3, cmd1.nbCommandAdded);
        Assertions.assertEquals(0, cmd2.nbCommandAdded);
        cmd2.cmd.getFuture().get();
        Assertions.assertEquals(SiLAFramework.ExecutionInfo.CommandStatus.finishedSuccessfully, cmd2.cmd.getExecutionInfo().getState());
        Assertions.assertEquals(cmd2.expectedResult, cmd2.cmd.getFuture().get());
        cmd1.cmd.close();
        cmd2.cmd.close();
    }

    @Test
    void concurrentTasks() throws InterruptedException, ExecutionException {
        final ObservableCommandTaskRunner runner = new ObservableCommandTaskRunner(0, 2);
        final TestableObservableCommand<Integer, Integer> cmd1 =
                new TestableObservableCommand<>(0, 42, runner);
        final TestableObservableCommand<Integer, Integer> cmd2 =
                new TestableObservableCommand<>(0, 1337, runner);
        Assertions.assertThrows(
                RejectedExecutionException.class,
                () -> new TestableObservableCommand<>(0, 0xDE4D, runner)
        );
        Thread.sleep(TestableObservableCommand.SLEEP_MS);
        Assertions.assertEquals(SiLAFramework.ExecutionInfo.CommandStatus.running, cmd1.cmd.getExecutionInfo().getState());
        Assertions.assertEquals(SiLAFramework.ExecutionInfo.CommandStatus.running, cmd2.cmd.getExecutionInfo().getState());
        cmd1.running.set(false);
        cmd1.cmd.getFuture().get();
        Assertions.assertEquals(SiLAFramework.ExecutionInfo.CommandStatus.finishedSuccessfully, cmd1.cmd.getExecutionInfo().getState());
        Assertions.assertEquals(cmd1.expectedResult, cmd1.cmd.getFuture().get());
        Assertions.assertEquals(SiLAFramework.ExecutionInfo.CommandStatus.running, cmd2.cmd.getExecutionInfo().getState());
        cmd2.running.set(false);
        cmd2.cmd.getFuture().get();
        Assertions.assertEquals(SiLAFramework.ExecutionInfo.CommandStatus.finishedSuccessfully, cmd2.cmd.getExecutionInfo().getState());
        Assertions.assertEquals(cmd2.expectedResult, cmd2.cmd.getFuture().get());
        cmd1.cmd.close();
        cmd2.cmd.close();
    }

    static class TestableObservableCommand<ParamType, ResultType> implements RunnableCommandTask<ParamType, ResultType>
    {
        protected static final int SLEEP_MS = 100;
        protected final AtomicBoolean running = new AtomicBoolean(true);
        protected final ParamType param;
        protected final ResultType expectedResult;
        protected final ObservableCommandWrapper<ParamType, ResultType> cmd;
        protected int nbCommandAdded = 0;

        TestableObservableCommand(
                final ParamType param,
                final ResultType expectedResult,
                final ObservableCommandTaskRunner runner
        ) {
            this.param = param;
            this.expectedResult = expectedResult;
            this.cmd = new ObservableCommandWrapper<>(param, this, runner);
        }

        @Override
        public ResultType run(ObservableCommandWrapper<ParamType, ResultType> command) throws StatusRuntimeException {
            while (running.get()) {
                try {
                    Thread.sleep(SLEEP_MS);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                }
            }
            return expectedResult;
        }

        @Override
        public void onNewCommand(ObservableCommandWrapper<ParamType, ResultType> observableCommandWrapper) {
            ++this.nbCommandAdded;
        }

        protected StreamObserver<SiLAFramework.CommandConfirmation> getStreamObserver() {
            return new StreamObserver<SiLAFramework.CommandConfirmation>() {
                public void onNext(SiLAFramework.CommandConfirmation confirmation) { }
                public void onError(Throwable throwable) { }
                public void onCompleted() { }
            };
        }
    }

    static class ErroredObservableCommand<ParamType, ResultType> extends TestableObservableCommand<ParamType, ResultType> {
        public ErroredObservableCommand(ParamType param, ResultType expectedResult, ObservableCommandTaskRunner runner) {
            super(param, expectedResult, runner);
        }

        @Override
        public ResultType run(ObservableCommandWrapper<ParamType, ResultType> command) throws StatusRuntimeException {
            throw new IllegalStateException();
        }
    }

    static class ResultLessObservableCommand<ParamType, ResultType> extends TestableObservableCommand<ParamType, ResultType> {
        public ResultLessObservableCommand(ParamType param, ResultType expectedResult, ObservableCommandTaskRunner runner) {
            super(param, expectedResult, runner);
        }

        @Override
        public ResultType run(ObservableCommandWrapper<ParamType, ResultType> command) throws StatusRuntimeException {
            super.run(command);
            return null;
        }
    }
}
