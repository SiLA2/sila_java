package sila_java.library.server_base.binary_transfer;

import java.util.Random;

public class Utils {
    private static final Random rng = new Random();

    /**
     * Generate a large binary
     * @return A large binary
     */
    public static byte[] generateRandomLargeBinary() {
        byte[] bytes = new byte[3 * 999 * 999];  // > 2MB, probably not a multiple of any used chunk size
        rng.nextBytes(bytes);
        return bytes;
    }
}
