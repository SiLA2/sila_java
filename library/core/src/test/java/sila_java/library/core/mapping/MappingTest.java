package sila_java.library.core.mapping;

import com.google.protobuf.Descriptors;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import sila_java.library.core.models.Feature;
import sila_java.library.core.sila.mapping.feature.FeatureGenerator;
import sila_java.library.core.sila.mapping.feature.MalformedSiLAFeature;
import sila_java.library.core.sila.mapping.grpc.ProtoMapper;
import sila_java.library.core.utils.FileUtils;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static sila_java.library.core.sila.mapping.grpc.DynamicProtoBuilder.generateProtoFile;
import static sila_java.library.core.utils.Utils.validateFeatureXML;

@DisplayName("Test SiLA Protobuf Mapping")
class MappingTest {
    private static final String featureName = "TestFeature";
    private static String EmptyLinesRegex = "[\n\r]+";
    private final File featureFile;
    private String protoFileContent;

    MappingTest() throws URISyntaxException, IOException {
        final String xmlFilePath = "TestFeature.sila.xml";
        final String protoFilePath = "TestFeature.proto";

        featureFile = new File(getURL(xmlFilePath).toURI());
        final URL protoURL = getURL(protoFilePath);
        final byte[] encoded = Files.readAllBytes(Paths.get(protoURL.toURI()));
        protoFileContent = new String(encoded);
        // Remove carriage return for platform neutrality
        protoFileContent = protoFileContent.replaceAll(MappingTest.EmptyLinesRegex, "");
    }

    private URL getURL(String filePath) {
        java.net.URL url = getClass().getClassLoader().getResource(filePath);
        if (url == null) {
            throw new NullPointerException(filePath + " couldn't be found!");
        }
        return url;
    }

    @Test
    void testXML() throws IOException {
        // Test if XML can be validated
        validateFeatureXML(FileUtils.getFileContent(this.featureFile.getAbsolutePath()));
    }

    @Test
    void testFeatureUtilities() throws MalformedSiLAFeature, IOException {
        final Feature feature = FeatureGenerator.generateFeature(
                FileUtils.getFileContent(featureFile.getAbsolutePath())
        );

        assertEquals(
                "ch.unitelabs/none/TestFeature/v1",
                FeatureGenerator.generateFullyQualifiedIdentifier(feature)
        );
    }

    @Test
    void testProto() throws IOException, MalformedSiLAFeature {
        // Create Proto Descriptor and check obvious parameters
        Descriptors.FileDescriptor protoFileDesc = ProtoMapper
                .usingFeature(FileUtils.getFileContent(this.featureFile.getAbsolutePath()))
                .generateProto();

        assertTrue(protoFileDesc.getFullName().matches(featureName));
        assertTrue(protoFileDesc.getName().matches(featureName));

        // @Note: It needs to be exactly the same...
        final String genProtoFile = generateProtoFile(protoFileDesc)
                .replaceAll(MappingTest.EmptyLinesRegex, "");
        assertEquals(protoFileContent, genProtoFile);
    }

    @Test
    void testNestedDataTypes() throws URISyntaxException, IOException, MalformedSiLAFeature {
        final String xmlFilePath = "NestedDataTypesFeature.sila.xml";
        final File nestedDataTypesFeatureFile = new File(getURL(xmlFilePath).toURI());

        ProtoMapper
                .usingFeature(FileUtils.getFileContent(nestedDataTypesFeatureFile.getAbsolutePath()))
                .generateProto();

        // @Note: No exception means success. The InstrumentId data type is successfully resolved when
        //        generating the DeviceId even though it appears below.
    }
}
