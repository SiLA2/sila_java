package sila_java.library.core.asynchronous;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import static java.util.concurrent.Executors.newSingleThreadScheduledExecutor;

/**
 * Wrapper Class to have re-usable Scheduling Timers
 */
@Slf4j
public class TaskScheduler implements AutoCloseable {
    private final ScheduledExecutorService scheduledExecutorService = newSingleThreadScheduledExecutor();
    private final long period;
    private final Runnable runnableWrap;
    private ScheduledFuture<?> scheduledFuture = null;

    /**
     * Constructor
     * @param runnable runnable to be scheduled.
     * @param period time in milliseconds between successive task executions.
     * @param name for this scheduler, for association purposes
     */
    public TaskScheduler(final @NonNull Runnable runnable, final long period, final @NonNull String name) {
        // Runnable needs to be wrapped as the ScheduledExecutorService silently swallows the errors
        this.runnableWrap = ()->{
            try {
                runnable.run();
            } catch (Throwable e) {
                log.error("Task Scheduler " + name + " failed", e);
                throw new RuntimeException(e);
            }
        };
        this.period = period;
    }

    /**
     * Start the scheduler
     */
    public synchronized void start() {
        if (scheduledFuture != null && !scheduledFuture.isDone()) {
            log.warn("An attempt was made to start the TaskScheduler when it was already running");
        }

        // Note: This will block until thread becomes available
        scheduledFuture = scheduledExecutorService.scheduleAtFixedRate(runnableWrap, 0, period, TimeUnit.MILLISECONDS);
    }

    /**
     * Close the scheduler
     */
    @Override
    public synchronized void close() {
        if (scheduledFuture == null || scheduledFuture.isDone()) {
            log.info("An attempt was made to stop the TaskScheduler when it was already stopped");
        } else {
            scheduledFuture.cancel(true);
        }

        scheduledExecutorService.shutdownNow();
    }
}
