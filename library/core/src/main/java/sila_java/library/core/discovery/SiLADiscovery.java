package sila_java.library.core.discovery;

import lombok.NonNull;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.xbill.DNS.*;
import sila_java.library.core.discovery.networking.service_discovery.*;

import java.security.cert.X509Certificate;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static sila_java.library.core.encryption.EncryptionUtils.readCertificate;

/**
 * SiLA Discovery
 */
@Slf4j
public class SiLADiscovery implements AutoCloseable, ResponseListener.InstanceListener {
    private static final int QUERY_RATE = 1000; // [ms]
    private static final int DEFAULT_ITERATIONS = 3;

    private final static Pattern GUID_PATTERN = Pattern.compile(
            "([0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12})",
            Pattern.CASE_INSENSITIVE | Pattern.DOTALL | Pattern.MULTILINE
    );

    private final Map<UUID, MdnsServerInfo> serverCache = new ConcurrentHashMap<>();
    private final Set<ServerListener> serverListeners = ConcurrentHashMap.newKeySet();
    private final SenderReceiver senderReceiver;
    private final Message queryQuestion;

    /**
     * Discovery Class to get DNS-SD records via mDNS
     */
    @SneakyThrows
    public SiLADiscovery() {
        this.queryQuestion = Message.newQuery(new PTRRecord(new Name(Constants.SILA_MDNS_TARGET), DClass.ANY, 0, new Name(Constants.SILA_MDNS_TARGET)));
        this.senderReceiver = new SenderReceiver(new ResponseListener(this));
        this.senderReceiver.enable();
    }

    /**
     * @inheritDoc
     */
    @Override
    public void close() {
        this.senderReceiver.close();
        this.serverListeners.clear();
    }

    /**
     * Callback when an instance is added.
     * Add the server if missing and notify listeners
     *
     * @inheritDoc
     */
    @Override
    public void instanceAdded(Instance instance) {
        log.debug("Processing instance: {}", instance.getName());

        final String instanceName = instance.getName();
        UUID serverUUID;
        try {
            serverUUID = parseUUID(instanceName);
        } catch (final InvalidInstanceNameException e) {
            log.warn("Failed to process instance because: {}", e.getMessage());
            return;
        }
        final Optional<X509Certificate> certificateAuthority = getCertificateAuthority(instance.getAttributes());

        MdnsServerInfo serverInfo = serverCache.get(serverUUID);
        if (serverInfo == null) {
            if (serverCache.size() >= Constants.MAX_SERVERS) {
                log.warn("Maximum number of servers reached. Ignoring new server: {}", serverUUID);
                return;
            }
            serverInfo = new MdnsServerInfo(instance.getHostAddress(), instance.getPort(), certificateAuthority, instance.getTtl());
            serverCache.put(serverUUID, serverInfo);
            notifyListenersAdded(serverUUID, serverInfo);
        } else {
            boolean updated = false;
            if (!serverInfo.getHostAddress().equals(instance.getHostAddress())) {
                serverInfo.setHostAddress(instance.getHostAddress());
                updated = true;
            }
            if (serverInfo.getPort() != instance.getPort()) {
                serverInfo.setPort(instance.getPort());
                updated = true;
            }
            if (!serverInfo.getCertificateAuthority().equals(certificateAuthority)) {
                serverInfo.setCertificateAuthority(certificateAuthority);
                updated = true;
            }
            serverInfo.updateExpirationTime(instance.getTtl());
            if (updated) {
                notifyListenersUpdated(serverUUID, serverInfo);
            }
        }

        log.debug(
                "[instanceAdded/Updated] Service Instance {} on {}:{}",
                serverUUID,
                serverInfo.getHostAddress(),
                serverInfo.getPort()
        );
    }

    public void instanceRemoved(String instanceName) {
        try {
            UUID serverUUID = parseUUID(instanceName);
            MdnsServerInfo removedServer = serverCache.remove(serverUUID);
            if (removedServer != null) {
                notifyListenersRemoved(serverUUID, removedServer);
            }
        } catch (InvalidInstanceNameException e) {
            log.warn("Failed to process removed instance because: {}", e.getMessage());
        }
    }

    private void notifyListenersAdded(UUID serverUUID, MdnsServerInfo serverInfo) {
        serverListeners.forEach(listener ->
                listener.serverAdded(serverUUID, serverInfo.getHostAddress(), serverInfo.getPort(), serverInfo.getCertificateAuthority())
        );
    }

    private void notifyListenersUpdated(UUID serverUUID, MdnsServerInfo serverInfo) {
        serverListeners.forEach(listener ->
                listener.serverUpdated(serverUUID, serverInfo.getHostAddress(), serverInfo.getPort(), serverInfo.getCertificateAuthority())
        );
    }

    private void notifyListenersRemoved(UUID serverUUID, MdnsServerInfo serverInfo) {
        serverListeners.forEach(listener ->
                listener.serverRemoved(serverUUID, serverInfo.getHostAddress(), serverInfo.getPort(), serverInfo.getCertificateAuthority())
        );
    }

    /**
     * Get certificate authority from map as described in SiLA Standard part B SiLA Server Discovery Support section
     * @param attributes the attributes
     * @return the parsed certificate
     */
    public Optional<X509Certificate> getCertificateAuthority(@NonNull final Map<String, String> attributes) {
        final String regex = "^ca([0-9]+?)$";
        final Pattern pattern = Pattern.compile(regex);
        final Map<Integer, String> caParts = new Hashtable<>();

        try {
            for (final Map.Entry<String, String> attribute : attributes.entrySet()) {
                final Matcher matcher = pattern.matcher(attribute.getKey());
                if (matcher.find() && matcher.groupCount() > 0) {
                    caParts.put(Integer.parseInt(matcher.group(1)), attribute.getValue());
                }
            }
            final StringBuilder ca = new StringBuilder();
            for (int i = 0; i < caParts.size(); i++) {
                ca.append(caParts.get(i)).append("\r\n");
            }
            return Optional.of(readCertificate(ca.toString()));
        } catch (Exception e) {
            log.debug("mDNS Attributes does contain a valid CA", e);
        }
        return Optional.empty();
    }

    /**
     * Refresh the discovery by cleaning all the elements from the cache
     */
    public void clearCache() {
        serverCache.clear();
    }

    /**
     * Scan the network for several iterations
     *
     * @implNote Clears the cache so it re-adds server previously found for all listeners
     */
    public void scanNetwork() {
        scanNetwork(DEFAULT_ITERATIONS);
    }

    /**
     * Scan the network for x iterations
     * The number of iteration must be greater than or equal to 1
     * This call is blocking for: (iteration * (iteration + 1) / 2) * QUERY_RATE milliseconds
     * @param iteration The number of scan iteration to perform
     */
    public synchronized void scanNetwork(final int iteration) {
        final int totalTimeMs = (iteration * (iteration + 1) / 2) * QUERY_RATE;
        int timeToWaitMs = QUERY_RATE;

        log.info("Performing network discovery scan for {} seconds...", (totalTimeMs / 1000));
        clearCache();

        for (int i = 0; i < iteration; ++i) {
            try {
                this.query();
                Thread.sleep(timeToWaitMs);
                timeToWaitMs *= 2;
            } catch (final InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }
        log.info("End of network discovery scan.");
    }

    /**
     * Add Listener
     */
    public void addListener(@NonNull final ServerListener serverListener) {
        this.serverListeners.add(serverListener);
    }

    /**
     * Remove Listener
     */
    public void removeListener(@NonNull final ServerListener serverListener) {
        this.serverListeners.remove(serverListener);
    }

    /**
     * Issue a query on all specified Network Interfaces
     */
    private void query() {
        this.senderReceiver.send(queryQuestion);
    }

    /**
     * Parse UUID from the service instance name
     *
     * @param instanceName instance name of discovered service
     * @return unique identifier of that service instance
     */
    private static UUID parseUUID(@NonNull final String instanceName) throws InvalidInstanceNameException {
        final Matcher matcher = GUID_PATTERN.matcher(instanceName);
        if (!matcher.matches()) {
            throw new InvalidInstanceNameException(
                    "The service instance: " + instanceName + " does not follow the pattern of an UUID."
            );
        }
        return UUID.fromString(matcher.group(1));
    }
}