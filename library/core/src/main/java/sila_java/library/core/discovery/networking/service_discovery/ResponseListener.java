package sila_java.library.core.discovery.networking.service_discovery;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.xbill.DNS.Message;
import org.xbill.DNS.Record;
import org.xbill.DNS.Section;
import sila_java.library.core.discovery.Constants;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Listens to DNS Responses and Updates instance Cache
 */
@Slf4j
public class ResponseListener implements Consumer<Message>, AutoCloseable {
    private final InstanceListener instanceListener;
    private final Map<String, Set<Record>> recordCache = new ConcurrentHashMap<>();
    private final Timer cacheCleanupTimer;

    /**
     * Instance listener
     */
    public interface InstanceListener {
        /**
         * Callback when an instance is added
         * @param instance the added instance
         */
        void instanceAdded(Instance instance);
        void instanceRemoved(String instanceName);
    }

    /**
     * Constructor
     * @param instanceListener the instance listener
     */
    public ResponseListener(@NonNull final InstanceListener instanceListener) {
        this.instanceListener = instanceListener;
        this.cacheCleanupTimer = new Timer("RecordCacheCleanupTimer", true);
        this.cacheCleanupTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                cleanupCache();
            }
        }, Constants.CACHE_CLEANUP_INTERVAL, Constants.CACHE_CLEANUP_INTERVAL);
    }

    /**
     * Listen for responses and notify listeners when an instance is added
     * @inheritDoc
     */
    @Override
    public void accept(@NonNull final Message response) {
        log.debug("Listener response: {}", response);

        // Collect all records from the response
        final Set<Record> newRecords = Stream
                .concat(response.getSection(Section.ANSWER).stream(), response.getSection(Section.ADDITIONAL).stream())
                .collect(Collectors.toSet());

        if (!newRecords.isEmpty()) {
            // Update the cache with new records
            updateCache(newRecords);

            // Try to create an instance using all cached records
            try {
                final Optional<Instance> optionalInstance = Instance.createFromRecords(getAllCachedRecords());
                if (optionalInstance.isPresent()) {
                    Instance instance = optionalInstance.get();
                    if (instance.getTtl() > 0) {
                        instanceListener.instanceAdded(instance);
                    } else {
                        log.debug("Instance {} TTL is 0", instance.getName());
                    }
                }
            } catch (final Exception e) {
                log.debug("Error creating instance: {}", e.getMessage());
            }
        }
    }

    private void updateCache(Set<Record> newRecords) {
        for (Record record : newRecords) {
            String key = record.getName().toString();
            recordCache.compute(key, (k, v) -> {
                if (v == null) {
                    v = new HashSet<>();
                }
                v.add(record);
                if (v.size() > Constants.MAX_RECORDS_PER_SERVER) {
                    v = v.stream()
                            .sorted(Comparator.comparingLong(Record::getTTL).reversed())
                            .limit(Constants.MAX_RECORDS_PER_SERVER)
                            .collect(Collectors.toSet());
                }
                return v;
            });
        }
    }

    private Set<Record> getAllCachedRecords() {
        return recordCache.values().stream()
                .flatMap(Set::stream)
                .collect(Collectors.toSet());
    }

    private void cleanupCache() {
        if (recordCache.size() > Constants.MAX_SERVERS) {
            List<String> keysToRemove = recordCache.entrySet().stream()
                    .sorted(Comparator.comparingLong(e -> e.getValue().stream().mapToLong(Record::getTTL).min().orElse(0)))
                    .limit(recordCache.size() - Constants.MAX_SERVERS)
                    .map(Map.Entry::getKey)
                    .collect(Collectors.toList());
            keysToRemove.forEach(key -> {
                recordCache.remove(key);
                instanceListener.instanceRemoved(key);
            });
        }
    }

    @Override
    public void close() {
        cacheCleanupTimer.cancel();
    }
}