package sila_java.library.core.sila.types;

import io.grpc.StatusRuntimeException;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import sila2.org.silastandard.SiLABinaryTransfer;
import sila2.org.silastandard.SiLAFramework;
import sila_java.library.core.sila.binary_transfer.BinaryTransferErrorHandler;

import javax.annotation.Nullable;
import java.util.UUID;

/**
 * SiLA Binary Trasnfer UUID utility class
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public abstract class SiLABinaryTransferUUID {

    /**
     * Create a {@link UUID} from a {@link String}
     * @param binaryTransferUuid the String
     * @return a {@link UUID}
     */
    public static UUID from(@Nullable final String binaryTransferUuid) throws StatusRuntimeException {
        try {
            if (binaryTransferUuid == null) {
                throw new NullPointerException();
            }
            return UUID.fromString(binaryTransferUuid);
        } catch (Exception e) {
            throw BinaryTransferErrorHandler.generateBinaryTransferError(
                    SiLABinaryTransfer.BinaryTransferError.ErrorType.INVALID_BINARY_TRANSFER_UUID,
                    "Invalid Binary Transfer UUID."
            );
        }
    }

    /**
     * Create a {@link UUID} from a {@link SiLAFramework.Binary}
     * @param binary the binary
     * @return a {@link UUID}
     */
    public static UUID from(@Nullable final SiLAFramework.Binary binary) throws StatusRuntimeException {
        try {
            if (binary == null || !binary.hasBinaryTransferUUID()) {
                throw new NullPointerException();
            }
            return UUID.fromString(binary.getBinaryTransferUUID());
        } catch (Exception e) {
            throw BinaryTransferErrorHandler.generateBinaryTransferError(
                    SiLABinaryTransfer.BinaryTransferError.ErrorType.INVALID_BINARY_TRANSFER_UUID,
                    "Invalid Binary Transfer UUID."
            );
        }
    }
}
