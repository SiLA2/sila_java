package sila_java.library.core.sila.types;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import sila2.org.silastandard.SiLAFramework;

import java.time.OffsetDateTime;

/**
 * SiLA Boolean utility class
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public abstract class SiLABoolean {
    /**
     * Create a {@link SiLAFramework.Boolean} from a {@link Boolean}
     * @param bool the boolean
     * @return a {@link SiLAFramework.Boolean}
     */
    public static SiLAFramework.Boolean from(final boolean bool) {
        return SiLAFramework.Boolean.newBuilder().setValue(bool).build();
    }
}
