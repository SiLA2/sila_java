package sila_java.library.core.sila.types;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import sila2.org.silastandard.SiLAFramework;

import java.time.Duration;

/**
 * SiLA Duration utility class
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public abstract class SiLADuration {
    /**
     * Create a {@link SiLAFramework.Duration} from a {@link Duration}
     * @param duration the duration
     * @return a {@link SiLAFramework.Duration}
     */
    public static SiLAFramework.Duration from(final Duration duration) {
        return SiLAFramework.Duration
                .newBuilder()
                .setNanos(duration.getNano())
                .setSeconds(duration.getSeconds())
                .build();
    }
}
