package sila_java.library.core.sila.types;

import com.google.protobuf.*;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import sila2.org.silastandard.SiLAFramework;
import sila_java.library.core.models.DataTypeType;
import sila_java.library.core.models.SiLAElement;
import sila_java.library.core.models.StructureType;
import sila_java.library.core.sila.mapping.feature.MalformedSiLAFeature;
import sila_java.library.core.sila.mapping.grpc.ProtoMapper;
import sila_java.library.core.utils.XMLMarshaller;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * SiLA Any utility class
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public abstract class SiLAAny {

    private static final String VOID_TYPE_XML = "<DataType>\n" +
            "  <Constrained>\n" +
            "    <DataType>\n" +
            "      <Basic>String</Basic>\n" +
            "    </DataType>\n" +
            "    <Constraints>\n" +
            "      <Length>0</Length>\n" +
            "    </Constraints>\n" +
            "  </Constrained>\n" +
            "</DataType>\n";

    private static final String FIELD_NAME = "Any";

    private static final SiLAFramework.Any ANY_VOID = from(VOID_TYPE_XML, SiLAString.from(""));


    public static SiLAFramework.Any getVoid() {
        return SiLAAny.ANY_VOID;
    }

    public static DynamicMessage toMessage(SiLAFramework.Any any) throws IOException, MalformedSiLAFeature {
        return DynamicMessage.parseFrom(createDescriptor(any.getType()), any.getPayload());
    }

    public static List<DynamicMessage> toMessageList(SiLAFramework.Any any) throws IOException, MalformedSiLAFeature {
        final Descriptors.Descriptor descriptor = createDescriptor(any.getType());
        final Descriptors.FieldDescriptor fieldDescriptor = descriptor.findFieldByName(FIELD_NAME);
        final DynamicMessage outerMessage = DynamicMessage.parseFrom(descriptor, any.getPayload());

        return IntStream.range(0, outerMessage.getRepeatedFieldCount(fieldDescriptor))
                .mapToObj(i -> (DynamicMessage) outerMessage.getRepeatedField(fieldDescriptor, i))
                .collect(Collectors.toList());
    }

    public static SiLAFramework.Any from(final String typeXml, final Message value) {
        return createAnyMessage(typeXml, value);
    }

    public static SiLAFramework.Any from(final DataTypeType dataTypeType, final Message silaTypeValue) {
        return from(XMLMarshaller.convertToXML(dataTypeType), silaTypeValue);
    }

    public static SiLAFramework.Any fromList(final DataTypeType dataTypeType, final List<Message> silaTypeValues) {
        return fromList(XMLMarshaller.convertToXML(dataTypeType), silaTypeValues);
    }

    public static SiLAFramework.Any fromList(final String typeXml, final List<Message> values){
        return createAnyMessage(typeXml, values);
    }

    private static Descriptors.Descriptor createDescriptor(final String typeXml) throws IOException, MalformedSiLAFeature {
        final DataTypeType dataType = XMLMarshaller.convertFromXML(DataTypeType.class, typeXml);

        final DataTypeType anonymousStructureType = new DataTypeType();
        final StructureType structureType = new StructureType();
        final SiLAElement element = new SiLAElement();
        element.setDataType(dataType);
        element.setDescription("Any");
        element.setDisplayName("Any");
        element.setIdentifier(FIELD_NAME);
        structureType.getElement().add(element);
        anonymousStructureType.setStructure(structureType);

        return ProtoMapper.dataTypeToDescriptor(anonymousStructureType);
    }

    @SneakyThrows
    private static SiLAFramework.Any createAnyMessage(final String typeXml, final Object value) {
        final boolean isList = value instanceof List<?>;
        if (!(value instanceof Message || isList)) {
            throw new IllegalArgumentException("Value must be a Message or a List<Message>");
        }

        final Descriptors.Descriptor descriptor = createDescriptor(typeXml);
        final DynamicMessage.Builder builder = DynamicMessage.newBuilder(descriptor);
        final Descriptors.FieldDescriptor fieldDescriptor = descriptor.findFieldByName(FIELD_NAME);

        if (isList) {
            @SuppressWarnings("unchecked")
            List<Message> messageList = (List<Message>) value;
            messageList.forEach(v -> builder.addRepeatedField(fieldDescriptor, v));
        } else {
            Message message = (Message) value;
            builder.setField(fieldDescriptor, message);
        }

        return SiLAFramework.Any.newBuilder().setType(typeXml).setPayload(builder.build().toByteString()).build();
    }
}