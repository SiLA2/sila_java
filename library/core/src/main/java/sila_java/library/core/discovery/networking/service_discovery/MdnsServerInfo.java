package sila_java.library.core.discovery.networking.service_discovery;

import lombok.Getter;
import lombok.Setter;

import java.security.cert.X509Certificate;
import java.util.Optional;

@Getter
public class MdnsServerInfo {
    @Setter private String hostAddress;
    @Setter private int port;
    @Setter private Optional<X509Certificate> certificateAuthority;
    private long expirationTime;

    public MdnsServerInfo(String hostAddress, int port, Optional<X509Certificate> certificateAuthority, long ttl) {
        this.hostAddress = hostAddress;
        this.port = port;
        this.certificateAuthority = certificateAuthority;
        updateExpirationTime(ttl);
    }

    public void updateExpirationTime(long ttl) {
        this.expirationTime = System.currentTimeMillis() + (ttl * 1000);
    }

    public boolean isExpired() {
        return System.currentTimeMillis() > expirationTime;
    }
}