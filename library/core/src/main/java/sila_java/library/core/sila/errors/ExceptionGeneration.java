package sila_java.library.core.sila.errors;

import io.grpc.Status;
import io.grpc.StatusRuntimeException;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import org.apache.commons.lang3.time.DurationFormatUtils;
import sila2.org.silastandard.SiLAFramework;

import javax.annotation.Nullable;
import java.time.Duration;
import java.util.Optional;
import java.util.concurrent.CancellationException;

import static sila_java.library.core.sila.errors.SiLAErrors.retrieveSiLAError;

/**
 * Exception Generation Utility related to SiLA Calls from a SiLA Client perspective
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public abstract class ExceptionGeneration {
    /**
     * Generate the most meaningful error message from an error
     * @param e The throwable error
     * @return The error string
     */
    public static String generateMessage(@NonNull final Throwable e) {
        return generateMessage(e, null);
    }

    /**
     * Generate the most meaningful error message from an error and timeout information
     * @param e The throwable error
     * @param timeout The timeout related to the SiLA call
     * @return The error string
     */
    public static String generateMessage(@NonNull final Throwable e, @Nullable Duration timeout) {
        final Optional<String> siLAMessage;
        if (e instanceof StatusRuntimeException) {
            final StatusRuntimeException statusRuntimeException = (StatusRuntimeException) e;
            if (timeout != null && statusRuntimeException.getStatus().getCode() == Status.DEADLINE_EXCEEDED.getCode()) {
                siLAMessage = Optional.of(
                        "Call exceeded the timeout after running for " +
                                DurationFormatUtils.formatDurationHMS(timeout.toMillis())
                );
            } else {
                siLAMessage = retrieveSiLAError(statusRuntimeException).map(ExceptionGeneration::generateVerboseMessage);
            }
        } else {
            siLAMessage = Optional.empty();
        }

        if (e instanceof CancellationException || e instanceof InterruptedException) {
            return "Task was cancelled.";
        }

        return siLAMessage.orElse(getErrorMessage(e).orElse(
                "An error occurred without further information (" + e.getClass().getCanonicalName() + ")"
        ));
    }

    /**
     * Extract error message from a {@link Throwable}
     * @param throwable the throwable
     * @return the extracted error message
     */
    private static Optional<String> getErrorMessage(@NonNull final Throwable throwable) {
        if (throwable.getMessage() != null && !throwable.getMessage().isEmpty()) {
            return Optional.of(throwable.getMessage());
        } else {
            return Optional.empty();
        }
    }

    /**
     * More verbose error message
     *
     * @param error SiLA Error retrieved from protobuf serialisation
     * @return SiLA Error Message
     */
    private static String generateVerboseMessage(@NonNull final SiLAFramework.SiLAError error) {
        final StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append(getSiLAErrorType(error)).append(" error: ");
        if (error.hasFrameworkError()) {
            stringBuilder.append(error.getFrameworkError().getErrorType().getValueDescriptor().getName());
            if (!error.getFrameworkError().getMessage().isEmpty()) {
                stringBuilder.append(" caused by: ").append(error.getFrameworkError().getMessage());
            }
        } else if (error.hasUndefinedExecutionError() && !error.getUndefinedExecutionError().getMessage().isEmpty()) {
            stringBuilder.append(" caused by: ").append(error.getUndefinedExecutionError().getMessage());
        } else if (error.hasDefinedExecutionError()) {
            stringBuilder.append(" with error identifier: ");
            if (error.getDefinedExecutionError().getErrorIdentifier().isEmpty()) {
                stringBuilder.append("Unknown");
            } else {
                stringBuilder.append("'").append(error.getDefinedExecutionError().getErrorIdentifier()).append("'");
            }
            if (!error.getDefinedExecutionError().getMessage().isEmpty()) {
                stringBuilder.append(" caused by : ").append(error.getDefinedExecutionError().getMessage());
            }
        } else if (error.hasValidationError()) {
            if (error.getValidationError().getParameter().isEmpty()) {
                stringBuilder.append("Unknown parameter: ");
            } else {
                stringBuilder.append("for parameter: '").append(error.getValidationError().getParameter()).append("'");
            }
            stringBuilder.append(" is invalid because: ");
            if (error.getValidationError().getMessage().isEmpty()) {
                stringBuilder.append(" of an unknown error");
            } else {
                stringBuilder.append(error.getValidationError().getMessage());
            }
        } else {
            stringBuilder.append("No information about SiLA Error");
        }
        return stringBuilder.toString();
    }

    /**
     * Purely informational function that return a String representing the SiLA Error kind.
     * Usage is deprecated for anything else than logging or related.
     * @param error the SiLA Error
     * @return A string representing the SiLA error kind
     */
    private static String getSiLAErrorType(@NonNull final SiLAFramework.SiLAError error) {
        if (error.hasDefinedExecutionError())
            return ("Execution");
        if (error.hasFrameworkError())
            return ("Framework");
        if (error.hasUndefinedExecutionError())
            return ("Undefined Execution");
        if (error.hasValidationError())
            return ("Validation " );
        return ("Unknown (code: " + error.getErrorCase().getNumber() + ")");
    }
}
