package sila_java.library.core.sila.binary_transfer;

import lombok.AllArgsConstructor;
import lombok.Getter;

import javax.annotation.Nullable;
import java.time.OffsetDateTime;
import java.util.UUID;

/**
 * Binary info model
 */
@Getter
@AllArgsConstructor
public class BinaryInfo {
    private final UUID id;
    private final OffsetDateTime expiration;
    @Nullable
    private final String parameterIdentifier;
    /**
     * Length in byte
     */
    private final long length;

    public BinaryInfo(UUID id, OffsetDateTime expiration, long length) {
        this.id = id;
        this.expiration = expiration;
        this.parameterIdentifier = null;
        this.length = length;
    }
}
