package sila_java.library.core.sila.binary_transfer;

import io.grpc.Channel;
import io.grpc.stub.StreamObserver;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.BinaryDownloadGrpc;
import sila2.org.silastandard.SiLABinaryTransfer;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.file.Path;
import java.time.Duration;
import java.time.OffsetDateTime;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.IntStream;

/**
 * Client-side utility class for SiLA Binary Download
 */
@Slf4j
public class BinaryDownloadHelper {
    private static final int BINARY_CHUNK_LENGTH = (int) Math.pow(1024, 2);
    private final BinaryDownloadGrpc.BinaryDownloadBlockingStub downloadBlockingStub;
    private final BinaryDownloadGrpc.BinaryDownloadStub downloadStreamStub;

    /**
     * Create a {@link BinaryDownloadHelper} using a provided gRPC channel
     *
     * @param channel The gRPC channel
     */
    public BinaryDownloadHelper(@NonNull final Channel channel) {
        this.downloadBlockingStub = BinaryDownloadGrpc.newBlockingStub(channel);
        this.downloadStreamStub = BinaryDownloadGrpc.newStub(channel);
    }

    /**
     * Download a binary to a file
     *
     * @param binaryTransferUUID {@link UUID} of the binary to download
     * @param outputFile {@link Path} to the output file
     * @throws IOException In case of issues during file access
     *
     * @implNote Use this instead of {@link BinaryDownloadHelper#download(UUID)} if the downloaded binary is too
     *         large to fit in memory.
     */
    public void downloadToFile(
            @NonNull final UUID binaryTransferUUID,
            @NonNull final Path outputFile
    ) throws IOException {
        try (final FileOutputStream fileOutputStream = new FileOutputStream(outputFile.toFile())) {
            download(binaryTransferUUID, (offset, length, payload) -> fileOutputStream.write(payload));
        }
    }

    /**
     * Download a binary
     *
     * @param binaryTransferUUID {@link UUID} of the binary to download
     * @return Downloaded binary
     */
    public byte[] download(@NonNull final UUID binaryTransferUUID) {
        final long numBytes = getBinaryInfo(binaryTransferUUID).getLength();
        if (numBytes > Integer.MAX_VALUE) {
            throw new RuntimeException(String.format(
                    "Byte arrays can only store %s bytes, requested binary has %s bytes", Integer.MAX_VALUE, numBytes));
        }

        final byte[] rawBuffer = new byte[(int) numBytes];
        final ByteBuffer buffer = ByteBuffer.wrap(rawBuffer);
        download(binaryTransferUUID, (offset, length, payload) -> buffer.put(payload, 0, payload.length));
        return rawBuffer;
    }

    /**
     * Request information about a binary available for download
     *
     * @param binaryTransferUUID {@link UUID} of the binary
     * @return Information about the binary (UUID, size, lifetime)
     */
    public BinaryInfo getBinaryInfo(@NonNull final UUID binaryTransferUUID) {
        final SiLABinaryTransfer.GetBinaryInfoResponse rawInfo = downloadBlockingStub.getBinaryInfo(
                SiLABinaryTransfer.GetBinaryInfoRequest
                        .newBuilder()
                        .setBinaryTransferUUID(binaryTransferUUID.toString())
                        .build()
        );

        OffsetDateTime expiration = OffsetDateTime
                .now()
                .plus(
                        Duration.ofNanos(
                                rawInfo.getLifetimeOfBinary().getSeconds()
                                        * (long) Math.pow(10, 9)
                                        + rawInfo.getLifetimeOfBinary().getNanos()
                        )
        );
        return new BinaryInfo(binaryTransferUUID, expiration, rawInfo.getBinarySize());
    }

    /**
     * Request that the server deletes the binary associated with the given {@link UUID}
     * @param binaryTransferUUID {@link UUID} of the binary to delete
     */
    public void deleteBinary(@NonNull final UUID binaryTransferUUID) {
        //noinspection ResultOfMethodCallIgnored
        downloadBlockingStub.deleteBinary(
                SiLABinaryTransfer.DeleteBinaryRequest.newBuilder()
                        .setBinaryTransferUUID(binaryTransferUUID.toString())
                        .build()
        );
    }

    @FunctionalInterface
    private interface ChunkReceiver<Offset, Length, Payload> {
        void apply(Offset offset, Length length, Payload payload) throws IOException;
    }

    private void download(@NonNull final UUID binaryTransferUUID, @NonNull final ChunkReceiver<Long, Long, byte[]> chunkReceiver) {
        log.debug("Requesting information of binary {}", binaryTransferUUID);
        final long numBytes = (int) downloadBlockingStub.getBinaryInfo(
                SiLABinaryTransfer.GetBinaryInfoRequest
                        .newBuilder()
                        .setBinaryTransferUUID(binaryTransferUUID.toString())
                        .build())
                .getBinarySize();
        final int numChunks = (int) Math.ceil(numBytes / (double) BINARY_CHUNK_LENGTH);

        log.info(
                "Starting download of {} bytes ({} chunks) of binary {}",
                numBytes,
                numChunks,
                binaryTransferUUID
        );

        final Set<Integer> chunksToDownload = ConcurrentHashMap.newKeySet();
        IntStream.range(0, numChunks).forEach(chunksToDownload::add);
        final CompletableFuture<Void> voidCompletableFuture = new CompletableFuture<>();

        final StreamObserver<SiLABinaryTransfer.GetChunkRequest> requestStreamObserver = downloadStreamStub.getChunk(
                new StreamObserver<SiLABinaryTransfer.GetChunkResponse>() {
                    @Override
                    public void onNext(final SiLABinaryTransfer.GetChunkResponse value) {
                        final int chunkIndex = (int) value.getOffset() / BINARY_CHUNK_LENGTH;

                        final byte[] payload = value.getPayload().toByteArray();
                        try {
                            chunkReceiver.apply(value.getOffset(), (long) payload.length, payload);
                        } catch (IOException e) {
                            onError(e);
                            return;
                        }

                        chunksToDownload.remove(chunkIndex);
                        log.debug("Downloaded chunk {}/{} of binary {}", chunkIndex, numChunks, binaryTransferUUID);
                    }

                    @Override
                    public void onError(Throwable t) {
                        log.error("GetChunk failed", t);
                        voidCompletableFuture.completeExceptionally(t);
                    }

                    @Override
                    public void onCompleted() {
                        if (!chunksToDownload.isEmpty()) {
                            throw new RuntimeException("Chunk download ended before all chunks were downloaded");
                        }
                        log.debug("GetChunk RPC completed");
                        voidCompletableFuture.complete(null);
                    }
                });

        for (int chunkIndex = 0; chunkIndex < numChunks; chunkIndex++) {
            final int offset = chunkIndex * BINARY_CHUNK_LENGTH;
            final int length = (int) Math.min(numBytes - offset, BINARY_CHUNK_LENGTH);

            log.debug("Downloading chunk {}/{} of binary {}", chunkIndex, numChunks, binaryTransferUUID);
            requestStreamObserver.onNext(
                    SiLABinaryTransfer.GetChunkRequest.newBuilder()
                            .setBinaryTransferUUID(binaryTransferUUID.toString())
                            .setOffset(offset)
                            .setLength(length)
                            .build()
            );
        }

        requestStreamObserver.onCompleted();
        voidCompletableFuture.join();

        log.info("Binary download {} completed", binaryTransferUUID);
    }
}
