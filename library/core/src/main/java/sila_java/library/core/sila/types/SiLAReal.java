package sila_java.library.core.sila.types;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import sila2.org.silastandard.SiLAFramework;

/**
 * SiLA Real utility class
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public abstract class SiLAReal {
    /**
     * Create a {@link SiLAFramework.Real} from a {@link Float}
     * @param real the float
     * @return a {@link SiLAFramework.Real}
     */
    public static SiLAFramework.Real from(final float real) {
        return SiLAReal.from((double)real);
    }

    /**
     * Create a {@link SiLAFramework.Real} from a {@link Double}
     * @param real the double
     * @return a {@link SiLAFramework.Real}
     */
    public static SiLAFramework.Real from(final double real) {
        return SiLAFramework.Real.newBuilder().setValue(real).build();
    }
}
