package sila_java.library.core.sila.types;

import com.google.protobuf.ByteString;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import sila2.org.silastandard.SiLAFramework;

import java.util.UUID;

/**
 * SiLA Binary utility class
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public abstract class SiLABinary {
    /**
     * Create a {@link SiLAFramework.Binary} from a {@link Byte} array
     * @param bytes the bytes
     * @return a {@link SiLAFramework.Binary}
     */
    public static SiLAFramework.Binary fromBytes(final byte[] bytes) {
        return SiLABinary.fromBytes(ByteString.copyFrom(bytes));
    }

    /**
     * Create a {@link SiLAFramework.Binary} from a {@link ByteString} array
     * @param byteString the byteString
     * @return a {@link SiLAFramework.Binary}
     */
    public static SiLAFramework.Binary fromBytes(final ByteString byteString) {
        return SiLAFramework.Binary.newBuilder().setValue(byteString).build();
    }

    /**
     * Create a {@link SiLAFramework.Binary} from a {@link String binary transfer UUID}
     * @param binaryTransferUUID the binary transfer UUID
     * @return a {@link SiLAFramework.Binary}
     */
    public static SiLAFramework.Binary fromBinaryTransferUUID(final String binaryTransferUUID) {
        return SiLAFramework.Binary.newBuilder().setBinaryTransferUUID(binaryTransferUUID).build();
    }

    /**
     * Create a {@link SiLAFramework.Binary} from a {@link UUID binary transfer UUID}
     * @param binaryTransferUUID the binary transfer UUID
     * @return a {@link SiLAFramework.Binary}
     */
    public static SiLAFramework.Binary fromBinaryTransferUUID(final UUID binaryTransferUUID) {
        return SiLAFramework.Binary.newBuilder().setBinaryTransferUUID(binaryTransferUUID.toString()).build();
    }
}
