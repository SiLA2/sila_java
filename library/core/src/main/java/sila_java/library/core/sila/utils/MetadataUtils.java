package sila_java.library.core.sila.utils;

import com.google.protobuf.Message;
import io.grpc.ClientInterceptor;
import io.grpc.Metadata;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Utility class for handling SiLA Client Metadata
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public abstract class MetadataUtils {
    /**
     * Regex matching the class name of SiLA Client Metadata Protobuf messages, e.g.
     * {@code
     * "sila2.org.silastandard.core.authorizationservice.v1.AuthorizationServiceOuterClass$Metadata_AccessToken"}
     */
    private static final Pattern metadataMessageClassNamePattern = Pattern.compile(
            "sila2\\.([a-z.]+)\\.([a-z]+)\\.[a-z]+\\.(v\\d+)\\.([A-Z][a-zA-Z\\d]*)OuterClass\\$Metadata_([A-Z][a-zA-Z\\d]*)");

    /**
     * Convert a fully qualified metadata identifier to the corresponding gRPC metadata key
     *
     * @param fullyQualifiedMetadataIdentifier the fully qualified metadata identifier, e.g.
     *         {@code "org.silastandard/core/LockController/v1/Metadata/LockIdentifier"}
     *
     * @return the metadata header, e.g.
     *         {@code "sila-org.silastandard-core-lockcontroller-v1-metadata-lockidentifier-bin"}
     */
    public static String fullyQualifiedMetadataIdentifierToGrpcMetadataKey(
            @NonNull final String fullyQualifiedMetadataIdentifier
    ) {
        return "sila-" + fullyQualifiedMetadataIdentifier.replace("/", "-").toLowerCase() + "-bin";
    }

    /**
     * Create a {@link ClientInterceptor} which injects the provided SiLA Client Metadata messages into all calls
     *
     * @param metadataMessages SiLA Client Metadata messages to send
     * @return new {@link ClientInterceptor}
     */
    public static ClientInterceptor newMetadataInjector(@NonNull final List<Message> metadataMessages) {
        final Metadata metadata = new Metadata();

        metadataMessages.forEach(m ->
                metadata.put(
                        Metadata.Key.of(
                                metadataProtobufMessageClassToGrpcMetadataKey(m.getClass()),
                                Metadata.BINARY_BYTE_MARSHALLER
                        ), m.toByteArray()
                )
        );

        return io.grpc.stub.MetadataUtils.newAttachHeadersInterceptor(metadata);
    }

    /**
     * Create a {@link ClientInterceptor} which injects the provided SiLA Client Metadata messages into all calls
     *
     * @param metadataMessages SiLA Client Metadata messages to send
     * @return new {@link ClientInterceptor}
     */
    public static ClientInterceptor newMetadataInjector(@NonNull final Message... metadataMessages) {
        return newMetadataInjector(Arrays.asList(metadataMessages));
    }

    /**
     * Provides the gRPC metadata key corresponding to a provided SiLA Client Metadata protobuf message
     *
     * @param metadataProtobufMessageClass Class for which to provide the gRPC metadata key
     * @return The corresponding gRPC metadata key
     */
    public static String metadataProtobufMessageClassToGrpcMetadataKey(
            @NonNull final Class<? extends Message> metadataProtobufMessageClass) {
        final Matcher matcher = metadataMessageClassNamePattern.matcher(metadataProtobufMessageClass.getName());
        if (!matcher.find()) {
            throw new RuntimeException("Provided class is not a SiLA Metadata Protobuf message type");
        }

        final String originator = matcher.group(1);
        final String category = matcher.group(2);
        final String majorVersion = matcher.group(3);
        final String featureIdentifier = matcher.group(4);
        final String metadataIdentifier = matcher.group(5);

        return fullyQualifiedMetadataIdentifierToGrpcMetadataKey(String.join(
                "/",
                originator,
                category,
                featureIdentifier,
                majorVersion,
                "Metadata",
                metadataIdentifier
        ));
    }
}
