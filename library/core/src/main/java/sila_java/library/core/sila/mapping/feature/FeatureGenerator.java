package sila_java.library.core.sila.mapping.feature;

import lombok.NonNull;
import sila_java.library.core.models.Feature;
import sila_java.library.core.utils.XMLMarshaller;

import javax.xml.transform.stream.StreamSource;
import java.io.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static sila_java.library.core.utils.Utils.validateFeatureXML;

/**
 * Converts Feature xml to Feature object representation.
 */
public class FeatureGenerator {
    /**
     * Maps from XML Field definitions to JAVA Class Definition
     *
     * @param xmlContent XML Document content as String
     *
     * @return Feature POJO for Feature Definition
     */
    public static Feature generateFeature(@NonNull final String xmlContent) throws IOException {
        try {
            validateFeatureXML(xmlContent);
        } catch (IOException e) {
            throw new IOException("Schema Validation failed: " + e.getMessage());
        }

        return XMLMarshaller.convertFromXML(Feature.class, xmlContent);
    }

    /**
     * Generate a FQI from a feature definition according to the standard
     */
    public static String generateFullyQualifiedIdentifier(@NonNull final Feature feature) throws MalformedSiLAFeature {
        return Stream.of(
                feature.getOriginator(),
                feature.getCategory(),
                feature.getIdentifier(),
                "v" + getMajorVersion(feature))
                .filter(s -> s != null && !s.isEmpty())
                .collect(Collectors.joining("/"));
    }

    /**
     * Generate a FQI from a feature and command definition according to the standard
     */
    public static String generateFullyQualifiedIdentifier(
            @NonNull final Feature feature,
            @NonNull final Feature.Command command
    ) throws MalformedSiLAFeature {
        return String.join(
                "/", generateFullyQualifiedIdentifier(feature), "Command", command.getIdentifier()
        );
    }


    /**
     * Get Major Version of Feature
     */
    public static int getMajorVersion(@NonNull final Feature feature) throws MalformedSiLAFeature{
        final int firstDotPos = feature.getFeatureVersion().indexOf('.');
        if (firstDotPos > 0) {
            return Integer.parseInt(feature.getFeatureVersion().substring(0, firstDotPos));
        } else {
            throw new MalformedSiLAFeature("Invalid SiLA Feature Version number (" + feature.getFeatureVersion() + ")");
        }
    }
}
