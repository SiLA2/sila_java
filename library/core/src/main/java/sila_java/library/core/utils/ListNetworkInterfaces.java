package sila_java.library.core.utils;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Collections;
import java.util.Enumeration;

import static java.lang.System.out;

/**
 * Utility program that list network interfaces
 */
public class ListNetworkInterfaces {

    /**
     * main
     * @param args arguments
     */
    public static void main(String args[]) {
        ListNetworkInterfaces.display();
        System.exit(0);
    }

    /**
     * Send to standard output the network interfaces
     */
    public static void display() {
        out.println("=====================================================");
        out.println("           System Network Interfaces:");
        Enumeration<NetworkInterface> nets = null;
        try {
            nets = NetworkInterface.getNetworkInterfaces();
        } catch (SocketException e) {
            out.printf("Socket Exception trying to get Network Interfaces: %s\n", e.getMessage());
        }
        for (NetworkInterface netint : Collections.list(nets))
            displayInterfaceInformation(netint);
        out.println("=====================================================\n");
    }

    /**
     * Display network interface information
     */
    static void displayInterfaceInformation(NetworkInterface netint) {
        out.printf("\nDisplay name: %s\n", netint.getDisplayName());
        out.printf("Name: %s\n", netint.getName());
        Enumeration<InetAddress> inetAddresses = netint.getInetAddresses();
        for (InetAddress inetAddress : Collections.list(inetAddresses)) {
            out.printf("InetAddress: %s\n", inetAddress);
        }
    }
}
