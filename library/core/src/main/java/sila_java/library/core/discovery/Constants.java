package sila_java.library.core.discovery;

public class Constants {
    public static final String SILA_MDNS_DOMAIN = "local.";
    public static final String SILA_MDNS_TYPE = "_sila._tcp.";
    public static final String SILA_MDNS_TARGET = SILA_MDNS_TYPE + SILA_MDNS_DOMAIN;

    public static final int MAX_SERVERS = 512; // Maximum number of servers in the cache
    public static final int MAX_RECORDS_PER_SERVER = 64; // Maximum number of records per server
    public static final long CACHE_CLEANUP_INTERVAL = 60000; // 1 minute
}