package sila_java.library.core.discovery.networking.service_discovery;

/**
 * Invalid instance name exception
 */
public class InvalidInstanceNameException extends Exception {
    /**
     * @inheritDoc
     */
    public InvalidInstanceNameException(String message) {
        super(message);
    }
}
