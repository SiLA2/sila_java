package sila_java.library.core.encryption;

import lombok.Getter;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x500.X500NameBuilder;
import org.bouncycastle.asn1.x500.style.RFC4519Style;
import org.bouncycastle.asn1.x509.*;
import org.bouncycastle.cert.CertIOException;
import org.bouncycastle.cert.X509v3CertificateBuilder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.cert.jcajce.JcaX509v3CertificateBuilder;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;
import org.bouncycastle.pqc.jcajce.spec.SPHINCS256KeyGenParameterSpec;
import org.bouncycastle.pqc.jcajce.spec.XMSSMTParameterSpec;
import org.bouncycastle.pqc.jcajce.spec.XMSSParameterSpec;

import javax.annotation.Nullable;
import java.io.ByteArrayInputStream;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.spec.RSAKeyGenParameterSpec;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Self signed certificate
 */
@Slf4j
@Getter
public class SelfSignedCertificate {
    private static final String CN = "SiLA2"; // Common name as specified in SiLA2 Standard Part B
    private static final String DEFAULT_IP_ADDRESS = "127.0.0.1";
    private static final String LOCALHOST = "localhost";
    private static final String DOCKER_INTERNAL_HOST = "host.docker.internal";

    private final X509Certificate certificate;
    private final PrivateKey privateKey;
    private final PublicKey publicKey;

    public enum EncryptionAlgorithm {
        XMSS, XMSSMT, SPHINCS256, RSA
    }

    public enum DigestAlgorithm {
        SHA256, SHA512
    }

    public enum KeySize {
        SIZE_1024(1024),
        SIZE_2048(2048),
        SIZE_4096(4096);

        public final int value;

        KeySize(final int value) {
            this.value = value;
        }
    }

    /**
     * Used to encapsulate another exception linked to the generation of a self signed certificate
     */
    public static class CertificateGenerationException extends Exception {

        /**
         * Constructor
         * @param cause The exception to encapsulate
         */
        public CertificateGenerationException(@NonNull final Throwable cause) {
            super(cause);
        }
    }

    /**
     * Create a new {@link Builder} instance
     * @return a new {@link Builder} instance
     */
    public static Builder newBuilder() {
        return new Builder();
    }

    /**
     * Self signed certificate builder
     */
    public static class Builder {
        private EncryptionAlgorithm encryptionAlgorithm = EncryptionAlgorithm.RSA;
        private DigestAlgorithm digestAlgorithm = DigestAlgorithm.SHA256;
        private KeySize keySize = KeySize.SIZE_2048;
        private int days = 365;
        private String payload = "SiLA Standard now allows an encryption layer.";
        private UUID serverUuid = null;
        private String serverIp = null;

        /**
         * Set the encryption algorithm to use
         * @param encryptionAlgorithm The encryption algorithm
         * @return The builder instance
         */
        public Builder withEncryptionAlgorithm(@NonNull final EncryptionAlgorithm encryptionAlgorithm) {
            this.encryptionAlgorithm = encryptionAlgorithm;
            return this;
        }

        /**
         * Set the digest algorithm to use
         * @param digestAlgorithm The digest algorithm
         * @return The builder instance
         */
        public Builder withDigestAlgorithm(@NonNull final DigestAlgorithm digestAlgorithm) {
            this.digestAlgorithm = digestAlgorithm;
            return this;
        }

        /**
         * Set the size of the key
         * @param keySize The key size
         * @return The builder instance
         */
        public Builder withKeySize(@NonNull final KeySize keySize) {
            this.keySize = keySize;
            return this;
        }

        /**
         * Set the number of days the certificate will be valid
         * @param days The number of days
         * @return The builder instance
         */
        public Builder withDays(final int days) {
            this.days = days;
            return this;
        }

        /**
         * Set the server UUID
         * @param serverUuid The server UUID
         * @return The builder instance
         */
        public Builder withServerUUID(@Nullable final UUID serverUuid) {
            this.serverUuid = serverUuid;
            return this;
        }

        /**
         * Set the server IP
         * @param serverIp The server IP
         * @return The builder instance
         */
        public Builder withServerIP(@Nullable final String serverIp) {
            this.serverIp = serverIp;
            return this;
        }

        /**
         * Set the certificate signature payload
         * @param payload The payload
         * @return The builder instance
         */
        public Builder withPayload(@NonNull final String payload) {
            this.payload = payload;
            return this;
        }

        /**
         * Build a new SelfSignedCertificate instance
         * @return A new SelfSignedCertificate instance
         * @throws CertificateGenerationException if an exception during the creation of the certificate
         */
        public SelfSignedCertificate build() throws CertificateGenerationException {
            return new SelfSignedCertificate(
                    this.encryptionAlgorithm,
                    this.digestAlgorithm,
                    this.keySize,
                    this.days,
                    this.payload,
                    this.serverIp,
                    this.serverUuid
            );
        }
    }

    /**
     * Initialize the {@link KeyPairGenerator}
     * @param kpg the key pair generator
     * @param alg the encryption algorithm
     * @param digest the digest algorithm
     * @param keySize the key size
     * @throws InvalidAlgorithmParameterException if the encryption algorithm is invalid
     */
    private static void initialize(
            @NonNull final KeyPairGenerator kpg,
            @NonNull final EncryptionAlgorithm alg,
            @NonNull final DigestAlgorithm digest,
            @NonNull final KeySize keySize
    ) throws InvalidAlgorithmParameterException {
        switch (alg) {
            case XMSS:
                kpg.initialize(new XMSSParameterSpec(4, digest.toString()));
                break;
            case XMSSMT:
                kpg.initialize(new XMSSMTParameterSpec(4, 2, digest.toString()));
                break;
            case SPHINCS256:
                kpg.initialize(new SPHINCS256KeyGenParameterSpec());
                break;
            case RSA:
                kpg.initialize(new RSAKeyGenParameterSpec(keySize.value, new BigInteger("65537")));
                break;
            default:
                throw new InvalidAlgorithmParameterException("Cannot initialize with specified algorithm: " + alg);
        }
    }

    /**
     * Sign payload
     * @param pk private key
     * @param sigAlg signature algorithm
     * @param provider provider
     * @param payload payload
     * @throws NoSuchProviderException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeyException
     * @throws SignatureException
     */
    private static void sign(
            @NonNull final PrivateKey pk,
            @NonNull final String sigAlg,
            @NonNull final String provider,
            @NonNull final String payload
    ) throws NoSuchProviderException, NoSuchAlgorithmException, InvalidKeyException, SignatureException {
        final Signature signer = Signature.getInstance(sigAlg, provider);
        signer.initSign(pk);
        signer.update(payload.getBytes());
        signer.sign();
        log.info("Successfully signed");
    }

    /**
     * Create a X509Certificate
     * @param privKey the private key
     * @param pubKey the public key
     * @param sigAlg the signature algorithm
     * @param days the days of validity of the certificate
     * @param isSelfSigned if self signed
     * @param ipAddress the server ip address
     * @param serverId the server UUID
     * @return
     * @throws OperatorCreationException
     * @throws CertificateException
     * @throws NoSuchProviderException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeyException
     * @throws SignatureException
     * @throws CertIOException
     */
    private static X509Certificate generate(
            @NonNull final PrivateKey privKey,
            @NonNull final PublicKey pubKey,
            @NonNull final String sigAlg,
            final int days,
            final boolean isSelfSigned,
            @Nullable String ipAddress,
            @Nullable UUID serverId
    ) throws OperatorCreationException, CertificateException, NoSuchProviderException, NoSuchAlgorithmException,
            InvalidKeyException, SignatureException, CertIOException {
        final Provider bouncyCastle = new BouncyCastleProvider();
        // distinguished name table.
        final X500Name subject = createSubjectBuilder().build();
        final X500Name issuer = subject;
        // create the certificate
        final ContentSigner sigGen = new JcaContentSignerBuilder(sigAlg).build(privKey);
        final SecureRandom secureRandom = new SecureRandom();
        final ExtendedKeyUsage extendedKeyUsage = new ExtendedKeyUsage( new KeyPurposeId[]{KeyPurposeId.id_kp_serverAuth, KeyPurposeId.id_kp_clientAuth} );
        final KeyUsage keyUsage = new KeyUsage(KeyUsage.keyCertSign | KeyUsage.digitalSignature | KeyUsage.keyEncipherment);
        final long validFromMs = System.currentTimeMillis() - 50000;
        final long validToMs = System.currentTimeMillis() + (long) days * 24 * 60 * 60 * 1000;
        X509v3CertificateBuilder certGen = new JcaX509v3CertificateBuilder(
                issuer,
                new BigInteger(64, secureRandom),
                new Date(validFromMs),
                new Date(validToMs),
                subject,
                pubKey // PublicKey
        )
                .addExtension(Extension.basicConstraints, true, new BasicConstraints(false))
                .addExtension(Extension.extendedKeyUsage, false, extendedKeyUsage)
                .addExtension(Extension.keyUsage, true, keyUsage)
                .addExtension(Extension.subjectKeyIdentifier, true, new SubjectKeyIdentifier(pubKey.getEncoded()));
        final GeneralNames subjectAlternativeName = createSubjectAlternativeNames(ipAddress);
        certGen = certGen.addExtension(Extension.subjectAlternativeName, false, subjectAlternativeName);
        if (serverId != null) {
            certGen = certGen.addExtension(new ASN1ObjectIdentifier("1.3.6.1.4.1.58583").intern(), false, serverId.toString().getBytes(StandardCharsets.US_ASCII));
        }

        final X509Certificate cert = new JcaX509CertificateConverter()
                .setProvider(bouncyCastle)
                .getCertificate(certGen.build(sigGen));
        cert.checkValidity(new Date());
        if (isSelfSigned) {
            // check verifies in general
            cert.verify(pubKey);
            // check verifies with contained key
            cert.verify(cert.getPublicKey());
        }
        final ByteArrayInputStream bIn = new ByteArrayInputStream(cert.getEncoded());
        final CertificateFactory fact = CertificateFactory.getInstance("X.509", bouncyCastle);
        log.info("Certificate constructed and valid for " + days + " days.");
        return (X509Certificate) fact.generateCertificate(bIn);
    }

    private static GeneralNames createSubjectAlternativeNames(@Nullable String ipAddress) {
        List<GeneralName> names = new ArrayList<>();
        names.add(new GeneralName(GeneralName.iPAddress, ipAddress != null ? ipAddress : DEFAULT_IP_ADDRESS));
        names.add(new GeneralName(GeneralName.dNSName, LOCALHOST));
        names.add(new GeneralName(GeneralName.dNSName, DOCKER_INTERNAL_HOST));
        return new GeneralNames(names.toArray(new GeneralName[0]));
    }


    /**
     * Create a new SiLA self signed {@link X500NameBuilder}
     * @return a new SiLA self signed {@link X500NameBuilder}
     */
    private static X500NameBuilder createSubjectBuilder() {
        final X500NameBuilder builder = new X500NameBuilder(RFC4519Style.INSTANCE);
        builder.addRDN(RFC4519Style.c, "CH") // Country
                .addRDN(RFC4519Style.o, "Association Consortium Standardization in Lab Automation (SiLA)") // Organisation
                .addRDN(RFC4519Style.l, "Rapperswil-Jona") // Locality name
                .addRDN(RFC4519Style.st, "SG") // Stare or province
                .addRDN(RFC4519Style.cn, CN); // Common name
        return builder;
    }

    /**
     * Create a new self signed certificate
     *
     * @param alg The encryption algorithm to use
     * @param digest The digest algorithm to use
     * @param keySize The size of the key
     * @param days The days of validity of the certificate
     * @param payload The signature payload
     *
     * @throws CertificateGenerationException if one of the following exception occur:
     * CertificateException, NoSuchAlgorithmException, InvalidKeyException, SignatureException,
     * InvalidAlgorithmParameterException, OperatorCreationException, NoSuchProviderException
     */
    private SelfSignedCertificate(
            @NonNull final EncryptionAlgorithm alg,
            @NonNull final DigestAlgorithm digest,
            @NonNull final KeySize keySize,
            final int days,
            @NonNull final String payload,
            @Nullable String ipAddress,
            @Nullable UUID serverId
    ) throws CertificateGenerationException {
        final String sigAlg = digest + "with" + alg;
        final String provider = "BC";
        // add providers
        Security.addProvider(new BouncyCastleProvider()); // because PROVIDER = "BC"

        try {
            // create keypair
            final KeyPairGenerator kpg = KeyPairGenerator.getInstance(alg.toString(), provider);
            initialize(kpg, alg, digest, keySize);
            final KeyPair myKp = kpg.generateKeyPair();

            // sign
            sign(myKp.getPrivate(), sigAlg, provider, payload);

            this.privateKey = myKp.getPrivate();
            this.publicKey = myKp.getPublic();
            this.certificate = generate(this.privateKey, this.publicKey, sigAlg, days, true, ipAddress, serverId);
        } catch (final CertificateException
                | NoSuchAlgorithmException
                | InvalidKeyException
                | SignatureException
                | InvalidAlgorithmParameterException
                | OperatorCreationException
                | CertIOException
                | NoSuchProviderException e
        ) {
            throw new CertificateGenerationException(e);
        }
    }
}