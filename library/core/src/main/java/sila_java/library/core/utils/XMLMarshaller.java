package sila_java.library.core.utils;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

import javax.xml.bind.JAXB;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.sax.SAXSource;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;

/**
 * XML Marshalling between XML Content as String and Java class
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public abstract class XMLMarshaller {
    /**
     * Converts any XML content to a defined class, if possible
     *
     * @implNote Node name is assumed to be unique as child of root node
     *
     * @param clazz Class to convert XML to
     * @param xmlContent Content of XML
     * @param <T> Type inferred from Class
     * @return converted class from XML content
     */
    public static <T> T convertFromXML(
            @NonNull final Class<T> clazz,
            @NonNull final String xmlContent
    ) throws IOException {
        final XMLReader xmlReader = createNewXMLReader();
        try {
            final Unmarshaller unmarshaller = JAXBContext.newInstance(clazz).createUnmarshaller();
            final SAXSource source = new SAXSource(xmlReader, new InputSource(new StringReader(xmlContent)));
            return unmarshaller.unmarshal(source, clazz).getValue();
        } catch (JAXBException e) {
            throw new IOException(e);
        }
    }

    /**
     * Converts any JAXB Object to String
     */
    public static String convertToXML(@NonNull final Object jaxbObject) {
        final StringWriter sw = new StringWriter();
        JAXB.marshal(jaxbObject, sw);
        return sw.toString();
    }

    /**
     * Create a new {@link XMLReader} instance
     * @return a new {@link XMLReader} instance
     * @throws IOException if could not create a new {@link XMLReader} instance
     */
    private static XMLReader createNewXMLReader() throws IOException {
        final SAXParserFactory sax = SAXParserFactory.newInstance();
        sax.setNamespaceAware(false);
        try {
            return sax.newSAXParser().getXMLReader();
        } catch (ParserConfigurationException | SAXException e) {
            throw new IOException(e);
        }
    }
}
