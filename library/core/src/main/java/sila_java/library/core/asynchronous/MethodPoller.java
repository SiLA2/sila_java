package sila_java.library.core.asynchronous;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

import java.time.Duration;
import java.util.Optional;
import java.util.concurrent.*;

/**
 * Simple MethodPoller Utility to wait until a condition is met
 *
 * @implNote Default is 1 Second Interval forever
 */
@Slf4j
public class MethodPoller {
    private final static ExecutorService executor = Executors.newCachedThreadPool(r -> {
        final Thread t = Executors.defaultThreadFactory().newThread(r);
        t.setDaemon(true);
        return t;
    });
    private Duration pollInterval = Duration.ofSeconds(1);
    private Duration maxDuration = Duration.ofSeconds(Long.MAX_VALUE);

    /**
     * Create a new {@link MethodPoller} instance
     * @return a new {@link MethodPoller} instance
     */
    public static MethodPoller await() {
        return new MethodPoller();
    }

    /**
     * Set polling interval
     * @param pollInterval the polling interval
     * @return the {@link MethodPoller} instance
     */
    public MethodPoller withInterval(@NonNull Duration pollInterval) {
        this.pollInterval = pollInterval;
        return this;
    }

    /**
     * Set maximum polling
     * @param maxDuration the maximum polling duration
     * @return the {@link MethodPoller} instance
     */
    public MethodPoller atMost(@NonNull Duration maxDuration) {
        this.maxDuration = maxDuration;
        return this;
    }

    /**
     * Set callable condition to stop polling
     * @param conditionEvaluator the callable condition to stop polling
     */
    public void until(@NonNull Callable<Boolean> conditionEvaluator) throws ExecutionException, TimeoutException {
        this.execute(
                executor.submit(
                        ()->{
                            while(!conditionEvaluator.call()) {
                                Thread.sleep(pollInterval.toMillis());
                                log.trace("Checking condition evaluator...");
                            }
                            return null;
                        }
                )
        );
    }

    /**
     * Set callable optional to stop polling
     * @param valueProvider the callable optional to stop polling
     * @return the {@link MethodPoller} instance
     */
    public <T> T untilPresent(@NonNull Callable<Optional<T>> valueProvider) throws ExecutionException, TimeoutException {
        return this.execute(
                executor.submit(
                        ()->{
                            Optional<T> value = Optional.empty();

                            while(!value.isPresent()) {
                                value = valueProvider.call();
                                Thread.sleep(pollInterval.toMillis());
                                log.trace("Checking value provider...");
                            }

                            return value.get();
                        }
                )
        );
    }

    /**
     * Execute
     *
     * @param future the future
     * @return the completed future result
     * @param <T> the result type
     * @throws ExecutionException if an error occur during polling
     * @throws TimeoutException if the polling did not complete in time
     */
    private <T> T execute(@NonNull Future<T> future) throws ExecutionException, TimeoutException {
        try {
            return future.get(maxDuration.toMillis(), TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            throw new RuntimeException(e);
        } catch (TimeoutException e) {
            future.cancel(true);
            if (!future.isCancelled()) {
                throw new ExecutionException("Condition Evaluator can not be cancelled", e.getCause());
            }
            throw e;
        }
    }
}
