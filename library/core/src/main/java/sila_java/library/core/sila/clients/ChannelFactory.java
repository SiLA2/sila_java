package sila_java.library.core.sila.clients;

import io.grpc.ManagedChannel;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.io.InputStream;
import java.security.cert.X509Certificate;

/**
 * Helper for creating {@link io.grpc.ManagedChannel}s
 *
 * @apiNote Use {@link ChannelBuilder} instead if further channel configuration is required
 */
@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public abstract class ChannelFactory {
    /**
     * Create a gRPC channel for TLS-encrypted communication with a SiLA Server
     *
     * @param host SiLA Server host
     * @param port SiLA Server port
     *
     * @return gRPC channel for TLS-encrypted communication
     *
     * @apiNote If the SiLA Server does not use a trusted certificate, use one of the overloaded methods to
     *         provide the CA certificate, e.g.
     *         {@link ChannelFactory#getTLSEncryptedChannel(String, int, X509Certificate)}.
     */
    public static ManagedChannel getTLSEncryptedChannel(@NonNull String host, int port) {
        return ChannelBuilder.withTLSEncryption(host, port).build();
    }

    /**
     * Create a gRPC channel builder for TLS-encrypted communication with a SiLA Server using a custom CA certificate
     *
     * @param host SiLA Server host
     * @param port SiLA Server port
     * @param customCertificateAuthority Custom CA certificate
     *
     * @return gRPC channel builder for TLS-encrypted communication
     *
     * @throws IOException If the provided certificate could not be parsed
     * @apiNote Use {@link ChannelFactory#getTLSEncryptedChannel(String, int)} if the SiLA Server uses a trusted
     *         certificate
     */
    public static ManagedChannel getTLSEncryptedChannel(
            @NonNull String host, int port, @NonNull InputStream customCertificateAuthority) throws IOException {
        return ChannelBuilder.withTLSEncryption(host, port, customCertificateAuthority).build();
    }

    /**
     * Create a gRPC channel builder for TLS-encrypted communication with a SiLA Server using a custom CA certificate
     *
     * @param host SiLA Server host
     * @param port SiLA Server port
     * @param customCertificateAuthority PEM-encoded custom CA certificate
     *
     * @return gRPC channel builder for TLS-encrypted communication
     *
     * @throws IOException If the provided certificate could not be parsed
     * @apiNote Use {@link ChannelFactory#getTLSEncryptedChannel(String, int)} if the SiLA Server uses a trusted
     *         certificate
     */
    public static ManagedChannel getTLSEncryptedChannel(
            @NonNull String host, int port, @NonNull String customCertificateAuthority) throws IOException {
        return ChannelBuilder.withTLSEncryption(host, port, customCertificateAuthority).build();
    }

    /**
     * Create a gRPC channel builder for TLS-encrypted communication with a SiLA Server using a custom CA certificate
     *
     * @param host SiLA Server host
     * @param port SiLA Server port
     * @param customCertificateAuthority PEM-encoded custom CA certificate
     *
     * @return gRPC channel builder for TLS-encrypted communication
     *
     * @apiNote Use {@link ChannelFactory#getTLSEncryptedChannel(String, int)} if the SiLA Server uses a trusted
     *         certificate
     */
    public static ManagedChannel getTLSEncryptedChannel(
            @NonNull String host, int port, @NonNull X509Certificate customCertificateAuthority) {
        return ChannelBuilder.withTLSEncryption(host, port, customCertificateAuthority).build();
    }

    /**
     * Create a gRPC channel builder for unencrypted communication with a SiLA Server
     *
     * @param host SiLA Server host
     * @param port SiLA Server port
     *
     * @return gRPC channel builder for unencrypted communication
     *
     * @deprecated Unencrypted communication violates the SiLA 2 specification. Use
     *         {@link ChannelFactory#getTLSEncryptedChannel(String, int)} or
     *         {@link ChannelFactory#getTLSEncryptedChannel(String, int, InputStream)} instead.
     */
    @Deprecated
    public static ManagedChannel getUnencryptedChannel(@NonNull String host, int port) {
        return ChannelBuilder.withoutEncryption(host, port).build();
    }
}
