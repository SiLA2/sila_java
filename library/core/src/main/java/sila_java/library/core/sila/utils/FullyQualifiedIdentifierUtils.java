package sila_java.library.core.sila.utils;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.regex.Pattern;

/**
 * Provides utilities for working with SiLA Fully Qualified Identifiers
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public abstract class FullyQualifiedIdentifierUtils {
    private static final String ORIGINATOR_REGEX = "[a-z][a-z.]*";
    private static final String CATEGORY_REGEX = "[a-z][a-z.]*";
    private static final String IDENTIFIER_REGEX = "[A-Z][a-zA-Z0-9]*";
    private static final String MAJOR_VERSION_REGEX = "v\\d+";

    private static final String FULLY_QUALIFIED_FEATURE_IDENTIFIER_REGEX = String.join(
            "/", ORIGINATOR_REGEX, CATEGORY_REGEX, IDENTIFIER_REGEX, MAJOR_VERSION_REGEX
    );
    private static final String FULLY_QUALIFIED_COMMAND_IDENTIFIER_REGEX = String.join(
            "/", FULLY_QUALIFIED_FEATURE_IDENTIFIER_REGEX, "Command", IDENTIFIER_REGEX
    );

    /**
     * Pattern for matching fully qualified feature identifiers
     *
     * Example: {@code "org.silastandard/core/SiLAService/v1"}
     */
    public static final Pattern FullyQualifiedFeatureIdentifierPattern = Pattern.compile(
            FULLY_QUALIFIED_FEATURE_IDENTIFIER_REGEX
    );
    /**
     * Pattern for matching fully qualified command identifiers
     *
     * Example: {@code "org.silastandard/core/SiLAService/v1/Command/GetFeatureDefinition"}
     */
    public static final Pattern FullyQualifiedCommandIdentifierPattern = Pattern.compile(
            FULLY_QUALIFIED_COMMAND_IDENTIFIER_REGEX
    );
    /**
     * Pattern for matching fully qualified command parameter identifiers
     *
     * Example: {@code "org.silastandard/core/SiLAService/v1/Command/GetFeatureDefinition/Parameter/FeatureIdentifier"}
     */
    public static final Pattern FullyQualifiedCommandParameterIdentifierPattern = Pattern.compile(
            String.join("/", FULLY_QUALIFIED_COMMAND_IDENTIFIER_REGEX, "Parameter", IDENTIFIER_REGEX)
    );
    /**
     * Pattern for matching fully qualified command response identifiers
     *
     * Example: {@code "org.silastandard/core/SiLAService/v1/Command/GetFeatureDefinition/Response/FeatureDefinition"}
     */
    public static final Pattern FullyQualifiedCommandResponseIdentifierPattern = Pattern.compile(
            String.join("/", FULLY_QUALIFIED_COMMAND_IDENTIFIER_REGEX, "Response", IDENTIFIER_REGEX)
    );
    /**
     * Pattern for matching fully qualified intermediate response identifiers
     *
     * Example:
     * {@code
     * "org.silastandard/core/SiLAService/v1/Command/GetFeatureDefinition/IntermediateResponse/MadeUpIntermediateResponse"}
     */
    public static final Pattern FullyQualifiedIntermediateCommandResponseIdentifierPattern = Pattern.compile(
            String.join(
                    "/",
                    FULLY_QUALIFIED_COMMAND_IDENTIFIER_REGEX,
                    "IntermediateResponse",
                    IDENTIFIER_REGEX
            )
    );
    /**
     * Pattern for matching fully qualified defined execution error identifiers
     *
     * Example: {@code "org.silastandard/core/SiLAService/v1/DefinedExecutionErrors/UnimplementedFeature"}
     */
    public static final Pattern FullyQualifiedDefinedExecutionErrorIdentifierPattern = Pattern.compile(
            String.join(
                    "/",
                    FULLY_QUALIFIED_FEATURE_IDENTIFIER_REGEX,
                    "DefinedExecutionError",
                    IDENTIFIER_REGEX
            )
    );
    /**
     * Pattern for matching fully qualified property identifiers
     *
     * Example: {@code "org.silastandard/core/SiLAService/v1/Property/ImplementedFeatures"}
     */
    public static final Pattern FullyQualifiedPropertyIdentifierPattern = Pattern.compile(
            String.join(
                    "/",
                    FULLY_QUALIFIED_FEATURE_IDENTIFIER_REGEX,
                    "Property",
                    IDENTIFIER_REGEX
            )
    );
    /**
     * Pattern for matching fully qualified custom data type identifiers
     *
     * Example: {@code "org.silastandard/core/ErrorRecoveryService/v1/DataType/Timeout"}
     */
    public static final Pattern FullyQualifiedCustomDataTypeIdentifierPattern = Pattern.compile(
            String.join("/", FULLY_QUALIFIED_FEATURE_IDENTIFIER_REGEX, "DataType", IDENTIFIER_REGEX)
    );
    /**
     * Pattern for matching fully qualified metadata identifiers
     *
     * Example: {@code "org.silastandard/core/LockController/v1/Metadata/LockIdentifier"}
     */
    public static final Pattern FullyQualifiedMetadataIdentifierPattern = Pattern.compile(
            String.join("/", FULLY_QUALIFIED_FEATURE_IDENTIFIER_REGEX, "Metadata", IDENTIFIER_REGEX)
    );
}
