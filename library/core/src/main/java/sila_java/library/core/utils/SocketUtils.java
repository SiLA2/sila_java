package sila_java.library.core.utils;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import javax.annotation.Nonnull;
import javax.net.ServerSocketFactory;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketAddress;

/**
 * Utilities for Analysing Socket States
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public abstract class SocketUtils {
    /**
     * Check if there if socket is open by trying to connect
     *
     * @param host of endpoint / socket
     * @param port of endpoint /socket
     * @param connectTimeout timeout for the ping
     * @return Boolean to indicate if connection is up or not
     */
    public static Boolean isSocketOpen(
            @Nonnull String host,
            int port,
            int connectTimeout) throws IOException {
        final SocketAddress socketAddress = new InetSocketAddress(host, port);
        try (final Socket socket = new Socket()) {
            socket.connect(socketAddress, connectTimeout);
            return true;
        } catch (IOException e) {
            return false;
        }
    }

    /**
     * Test whether a port is available or not.
     *
     * @param port the port to test
     * @return true if the port is available, false otherwise
     */
    public static boolean isPortAvailable(int port) {
        try (ServerSocket s = ServerSocketFactory.getDefault().createServerSocket(port)) {
            s.setReuseAddress(false);
            s.close();
            return true;
        } catch (IOException ignored) {
            return false;
        }
    }

    /**
     * Gets an available port in provided range.
     *
     * @param minPort the lower boundary of the range
     * @param maxPort the higher boundary of the range
     * @return the first available tcp port in the provided range
     * @throws IllegalStateException thrown if no available port is found
     */
    public static int getAvailablePortInRange(int minPort, int maxPort) throws IllegalStateException {
        for (int i = minPort; i <= maxPort; i++) {
            if (isPortAvailable(i))
                return i;
        }
        throw new IllegalStateException(String.format(
                "Could not find an available TCP port in the range [%d, %d]",
                minPort,
                maxPort
        ));
    }
}
