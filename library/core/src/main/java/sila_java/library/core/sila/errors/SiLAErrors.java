package sila_java.library.core.sila.errors;

import com.google.protobuf.InvalidProtocolBufferException;
import io.grpc.Status;
import io.grpc.StatusRuntimeException;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import sila2.org.silastandard.SiLAFramework;

import javax.annotation.Nonnull;
import javax.xml.bind.DatatypeConverter;
import java.security.InvalidParameterException;
import java.util.Optional;

import static sila_java.library.core.sila.utils.FullyQualifiedIdentifierUtils.FullyQualifiedCommandParameterIdentifierPattern;
import static sila_java.library.core.sila.utils.FullyQualifiedIdentifierUtils.FullyQualifiedDefinedExecutionErrorIdentifierPattern;

/**
 * Utility Functions to handle SiLA Errors
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public abstract class SiLAErrors {
    /**
     * Catching a generic Java Throwable and generating a SiLA conforming Execution Error
     */
    public static StatusRuntimeException generateGenericExecutionError(@Nonnull final Throwable e) {
        final SiLAFramework.UndefinedExecutionError.Builder builder = SiLAFramework.UndefinedExecutionError
                .newBuilder()
                .setMessage(
                        getErrorDescriptionFromThrowable(e)
                );
        return new SiLAErrorException(SiLAFramework.SiLAError.newBuilder().setUndefinedExecutionError(builder).build());
    }

    public static String getErrorDescriptionFromThrowable(Throwable e) {
        return e.getClass().getName() + ": " + ((e.getMessage() == null) ? "Unknown reason" : e.getMessage());
    }

    /**
     * Generate a SiLA conforming Validation Error
     * @param fullyQualifiedParameterIdentifier The fully qualified parameter identifier concerned by the error
     * @param message The error message
     * @return {@link StatusRuntimeException} according to SiLA
     */
    public static StatusRuntimeException generateValidationError(
            @NonNull final String fullyQualifiedParameterIdentifier,
            @NonNull final String message
    ) {
        if (!FullyQualifiedCommandParameterIdentifierPattern.matcher(fullyQualifiedParameterIdentifier).matches()) {
            throw new InvalidParameterException("A valid fully qualified parameter identifier must be provided.");
        }
        return new SiLAErrorException(SiLAFramework.SiLAError.newBuilder()
                .setValidationError(SiLAFramework.ValidationError.newBuilder()
                        .setParameter(fullyQualifiedParameterIdentifier)
                        .setMessage(message)
                        .build())
                .build());
    }

    /**
     * Generate a SiLA conforming Execution Error
     * @param fullyQualifiedErrorIdentifier The fully qualified error identifier
     * @param message The error message
     * @return {@link StatusRuntimeException} according to SiLA
     */
    public static StatusRuntimeException generateDefinedExecutionError(
            @NonNull final String fullyQualifiedErrorIdentifier,
            @NonNull final String message
    ) {
        if (!FullyQualifiedDefinedExecutionErrorIdentifierPattern.matcher(fullyQualifiedErrorIdentifier).matches()) {
            throw new InvalidParameterException("A valid fully qualified error identifier must be provided.");
        }
        return new SiLAErrorException(SiLAFramework.SiLAError.newBuilder()
                .setDefinedExecutionError(SiLAFramework.DefinedExecutionError.newBuilder()
                        .setErrorIdentifier(fullyQualifiedErrorIdentifier)
                        .setMessage(message)
                        .build())
                .build());
    }

    /**
     * Generate a SiLA conforming Execution Error
     * @param message The error message
     * @return {@link StatusRuntimeException} according to SiLA
     */
    public static StatusRuntimeException generateUndefinedExecutionError(@NonNull final String message) {
        return new SiLAErrorException(SiLAFramework.SiLAError.newBuilder()
                .setUndefinedExecutionError(SiLAFramework.UndefinedExecutionError.newBuilder()
                        .setMessage(message)
                        .build())
                .build());
    }

    /**
     * Generate a SiLA conforming Framework Error
     * @param errorType the framework error type
     * @param message The error message
     * @return {@link StatusRuntimeException} according to SiLA
     */
    public static StatusRuntimeException generateFrameworkError(
            @NonNull final SiLAFramework.FrameworkError.ErrorType errorType,
            @NonNull final String message
    ) {
        return new SiLAErrorException(SiLAFramework.SiLAError.newBuilder()
                .setFrameworkError(SiLAFramework.FrameworkError
                        .newBuilder()
                        .setErrorType(errorType)
                        .setMessage(message))
                .build());
    }

    /**
     * Retrieve SiLAError from gRPC Exception
     *
     * @param statusRuntimeException gRPC Exception occuring during gRPC call
     * @return Optional SiLAError if well-formed
     */
    public static Optional<SiLAFramework.SiLAError> retrieveSiLAError(
            @Nonnull final StatusRuntimeException statusRuntimeException
    ) {
        final Status status = statusRuntimeException.getStatus();

        if (!status.getCode().equals(Status.Code.ABORTED) || status.getDescription() == null) {
            return Optional.empty();
        }

        try {
            return Optional.of(SiLAFramework.SiLAError.parseFrom(DatatypeConverter.parseBase64Binary(status.getDescription())));
        } catch (InvalidProtocolBufferException e) {
            return Optional.empty();
        }
    }

    /**
     * Convert a throwable into a {@link SiLAFramework.SiLAError}
     * @param throwable the throwable
     * @return The converted {@link SiLAFramework.SiLAError}
     */
    public static SiLAFramework.SiLAError throwableToSiLAError(Throwable throwable) {
        SiLAFramework.SiLAError siLAError = null;
        if (throwable instanceof SiLAErrorException) {
            siLAError = ((SiLAErrorException) throwable).getSiLAError();
        } else if (throwable instanceof StatusRuntimeException) {
            Optional<SiLAFramework.SiLAError> error = retrieveSiLAError((StatusRuntimeException) throwable);
            if (error.isPresent()) {
                siLAError = error.get();
            }
        } else {
            final String message = ExceptionGeneration.generateMessage(throwable);
            siLAError = retrieveSiLAError(SiLAErrors.generateUndefinedExecutionError(message)).get();
        }
        return siLAError;
    }
}
