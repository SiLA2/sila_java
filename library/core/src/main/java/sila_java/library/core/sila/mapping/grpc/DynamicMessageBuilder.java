package sila_java.library.core.sila.mapping.grpc;

import com.google.protobuf.DescriptorProtos;
import lombok.*;

import java.util.List;

/**
 * Helper Class to create nessage descriptors
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
abstract class DynamicMessageBuilder {
    /**
     * Field description model
     */
    @Data
    @AllArgsConstructor
    static class FieldDescription {
        final String identifier;
        final String typeName;
        boolean repeated;
    }

    /**
     * Message description model
     */
    @Data
    @Builder
    static class MessageDescription {
        final String identifier;
        final List<FieldDescription> fields;
        final List<MessageDescription> nestedMessages;
    }

    /**
     * Create message type DescriptorProto
     * @param messageDescription the message description
     * @return {@link DescriptorProtos.DescriptorProto}
     */
    static DescriptorProtos.DescriptorProto createMessageType(
            MessageDescription messageDescription) {
        DescriptorProtos.DescriptorProto.Builder msgType = DescriptorProtos.DescriptorProto.newBuilder();
        msgType.setName(messageDescription.identifier);

        for (MessageDescription nestedMessage : messageDescription.nestedMessages) {
            msgType.addNestedType(createMessageType(nestedMessage));
        }

        int fieldIndex = 0;
        for (FieldDescription field : messageDescription.fields) {
            msgType.addField(createField(field, ++fieldIndex));
        }

        return msgType.build();
    }

    /**
     * Create a {@link DescriptorProtos.FieldDescriptorProto}
     * @param field the field description
     * @param index the field index
     * @return a {@link DescriptorProtos.FieldDescriptorProto}
     */
    private static DescriptorProtos.FieldDescriptorProto createField(
            @NonNull final FieldDescription field,
            final int index
    ) {
        final DescriptorProtos.FieldDescriptorProto.Builder fieldBuilder =
                DescriptorProtos.FieldDescriptorProto.newBuilder();
        fieldBuilder.setName(field.getIdentifier());
        fieldBuilder.setNumber(index);
        if (field.repeated) {
            fieldBuilder.setLabel(DescriptorProtos.FieldDescriptorProto.Label.LABEL_REPEATED);
        }
        fieldBuilder.setTypeName(field.getTypeName());
        return fieldBuilder.build();
    }
}
