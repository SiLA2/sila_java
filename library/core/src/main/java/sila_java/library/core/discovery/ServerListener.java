package sila_java.library.core.discovery;


import java.security.cert.X509Certificate;
import java.util.Optional;
import java.util.UUID;

/**
 * Listener interface that gets notified when a SiLA Server is added to mdns.
 *
 * @implNote The query of the server actually being active has to be done on a higher level.
 */
public interface ServerListener {
    /**
     * Server added, note that the same Server might be added several times
     * @param instanceId Service Instance ID, SiLA Server UUID for SiLA Servers
     * @param host Host of Service
     * @param port Port of service
     * @param certificate Optionally the server certificate authority
     */
    void serverAdded(UUID instanceId, String host, int port, Optional<X509Certificate> certificate);
    default void serverUpdated(UUID instanceId, String host, int port, Optional<X509Certificate> certificate) {};
    default void serverRemoved(UUID instanceId, String host, int port, Optional<X509Certificate> certificate) {};
}
