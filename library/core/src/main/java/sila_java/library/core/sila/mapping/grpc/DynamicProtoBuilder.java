package sila_java.library.core.sila.mapping.grpc;

import com.google.protobuf.DescriptorProtos;
import com.google.protobuf.Descriptors;

import javax.annotation.Nonnull;

import static sila_java.library.core.sila.mapping.grpc.DynamicMessageBuilder.createMessageType;

/**
 * Wrapper for programmatic Proto description, avoids using SiLA Terminology
 *
 * @implNote The builder only works with one service per proto definition
 */
public class DynamicProtoBuilder {
    private static final String tab = "    "; // 4 Spaces

    private final DescriptorProtos.FileDescriptorProto.Builder protoDescription;
    private final DescriptorProtos.ServiceDescriptorProto.Builder serviceDescription;

    /**
     * Constructor
     *
     * @param name Name of the proto file and service
     * @param packageName Package of the created definition
     */
    DynamicProtoBuilder(String name, String packageName) {
        protoDescription = DescriptorProtos.FileDescriptorProto.newBuilder();
        protoDescription.setName(name);

        protoDescription.setPackage(packageName);

        // Only one service definition is assumed
        serviceDescription = DescriptorProtos.ServiceDescriptorProto.newBuilder();
        serviceDescription.setName(name);
    }

    /**
     * Add Message Type to Proto Definition
     *
     * @param messageDescription Custom Message Definition
     */
    void addMessage(DynamicMessageBuilder.MessageDescription messageDescription) {
        protoDescription.addMessageType(createMessageType(messageDescription));
    }

    /**
     * Add Service Call to Proto Definition
     *
     * @param name Service Call Name
     * @param input Input Message Name (Parameters)
     * @param clientStreaming If input is streamed from client to server
     * @param output Output Message Name (Responses)
     * @param serverStreaming If input is streamed from server to client
     */
    void addCall(String name,
            String input, boolean clientStreaming,
            String output, boolean serverStreaming) {
        // rpc call
        final DescriptorProtos.MethodDescriptorProto.Builder methodDescription =
                DescriptorProtos.MethodDescriptorProto.newBuilder();
        methodDescription.setName(name);

        methodDescription.setInputType(input);
        methodDescription.setClientStreaming(clientStreaming);

        methodDescription.setOutputType(output);
        methodDescription.setServerStreaming(serverStreaming);

        serviceDescription.addMethod(methodDescription);
    }

    /**
     * Proto description to generate File and issue calls
     *
     * @param dependencies Other proto definition this proto depends on
     */
    Descriptors.FileDescriptor generateDescriptor(
            Descriptors.FileDescriptor... dependencies
    ) throws Descriptors.DescriptorValidationException {
        protoDescription.addService(serviceDescription);
        return Descriptors.FileDescriptor.buildFrom(
                protoDescription.build(),
                dependencies
        );
    }

    /**
     * Convert FileDescriptor to Protofile String
     *
     * @implNote This probably doesn't work for all protos, but is tested with
     *        generated protos complying to SiLA.
     *
     * @param fileDescriptor Proto file descriptor generated e.g. with
     *                       {@link DynamicProtoBuilder#generateDescriptor(Descriptors.FileDescriptor...)}
     * @return String containing the content of the proto file.
     */
    public static String generateProtoFile(@Nonnull Descriptors.FileDescriptor fileDescriptor) {
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("syntax = \"proto3\";\n\n");

        for (Descriptors.FileDescriptor dependency : fileDescriptor.getDependencies()) {
            stringBuilder.append("import \"" + dependency.getName() + "\";\n\n");
        }

        stringBuilder.append("package " + fileDescriptor.getPackage() + ";\n\n");

        /* Write Services */
        for (Descriptors.ServiceDescriptor service : fileDescriptor.getServices()) {
            stringBuilder.append("service " + service.getName() + " {\n");
            for (Descriptors.MethodDescriptor method : service.getMethods()) {
                stringBuilder.append(tab + "rpc " + method.getName() + "(" +
                        (method.toProto().getClientStreaming() ? "stream " : "") +
                        method.getInputType().getFullName() + ") returns (" +
                        (method.toProto().getServerStreaming() ? "stream " : "") +
                        method.getOutputType().getFullName() + ") {}\n"
                );
            }
            stringBuilder.append("}\n\n");
        }

        /* Write Messages */
        for (Descriptors.Descriptor message : fileDescriptor.getMessageTypes()) {
            stringBuilder.append(createMessageDefinition(message, ""));
            stringBuilder.append("\n");
        }

        return stringBuilder.toString();
    }

    /**
     * Private Utility to create Message Definition
     *
     * @param message Message Descriptor
     * @param nestedTab Tab to nest the message
     * @return Message Definition in String format
     */
    private static String createMessageDefinition(Descriptors.Descriptor message, String nestedTab) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(nestedTab + "message " + message.getName() + " {\n");

        for (Descriptors.Descriptor nestedMessage : message.getNestedTypes()) {
            stringBuilder.append(createMessageDefinition(nestedMessage, nestedTab + tab));
        }

        int index = 1; // proto starts at 1
        for (Descriptors.FieldDescriptor field : message.getFields()) {
            final Descriptors.OneofDescriptor oneofDescriptor =
                    field.getContainingOneof();

            String oneOfOpen = "";
            String endingCurl = "";

            if (oneofDescriptor != null) {
                oneOfOpen = "oneof " + oneofDescriptor.getName() + " { ";
                endingCurl = " }";
            }

            final String repeated = field.isRepeated() ? "repeated " : "";
            final String typeName = field.getMessageType().getFullName();

            stringBuilder.append(nestedTab + tab + oneOfOpen + repeated + typeName +
                    " " + field.getName() + " = " + index + ";" + endingCurl + "\n");
            index++;
        }

        stringBuilder.append(nestedTab + "}\n");
        return stringBuilder.toString();
    }
}