package sila_java.library.core.discovery.networking.service_discovery;

import lombok.NonNull;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.xbill.DNS.Message;
import sila_java.library.core.asynchronous.TaskScheduler;

import java.io.IOException;
import java.net.*;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Consumer;

import static sila_java.library.core.discovery.networking.helpers.MulticastDns.*;

/**
 * Manages Datagram Channels to send UDP mDNS Messages and receive them
 */
@Slf4j
public class SenderReceiver implements AutoCloseable {
    // Rate to check if Network is up and create interfaces
    private static final int CHECK_NETWORK_PERIOD = 1000; // [ms]
    private static final int CHANNEL_READ_DELAY = 500; // [ms]

    // Continuously running receiver thread
    private final TaskScheduler readScheduler;
    private final ByteBuffer readBuffer = ByteBuffer.allocate(MAX_DNS_MESSAGE_SIZE);

    // Scheduled task to create datagrams on network interfaces
    private final TaskScheduler networkScheduler;

    // listener to be invoked whenever a new message is received.
    private final Consumer<Message> listener;

    // mDNS channels to send and receive from (continuously updated)
    private final Map<NetworkInterface, List<mDNSChannel>> channelsMap = new ConcurrentHashMap<>();

    /**
     * Utility Class that combines the channel with the target
     * address for mDNS
     */
    @Value
    private static class mDNSChannel implements AutoCloseable {
        final DatagramChannel datagramChannel;
        final InetSocketAddress target;

        @Override
        public void close() {
            try {
                this.datagramChannel.close();
            } catch (final IOException e) {
                log.warn(e.getMessage());
            }
        }
    }

    /**
     * Constructor
     *
     * @implNote Currently only possible on all found network interfaces
     *
     * @param listener Listener Listening to incoming mDNS messages
     */
    public SenderReceiver(final Consumer<Message> listener) {
        this.readScheduler = new TaskScheduler(
                this::readChannels,
                CHANNEL_READ_DELAY,
                "Read Channel Scheduler"
        );
        this.networkScheduler = new TaskScheduler(
                this::assignNetworks,
                CHECK_NETWORK_PERIOD,
                "Check Network Scheduler"
        );
        this.listener = listener;
    }

    /**
     * Cleanup {@link SenderReceiver#readScheduler} {@link SenderReceiver#networkScheduler} {@link SenderReceiver#channelsMap}
     * @inheritDoc
     */
    @Override
    public final synchronized void close() {
        log.info("Closing channel");
        readScheduler.close();
        networkScheduler.close();
        channelsMap.values().forEach(list->list.forEach(mDNSChannel::close));
    }

    /**
     * Enables receiving DNS messages.
     */
    public final synchronized void enable() {
        log.info("Enable Receiving DNS Messages");
        networkScheduler.start();
        readScheduler.start();
    }

    /**
     * Send the given DNS Message on all Network Interfaces
     *
     * @param questionQuery question to send
     */
    public final void send(@NonNull final Message questionQuery) {
        log.debug("Sending {}", questionQuery);

        channelsMap.values().stream().flatMap(List::stream).forEach(mDNSChannel -> send(mDNSChannel, ByteBuffer.wrap(questionQuery.toWire(MAX_DNS_MESSAGE_SIZE))));
    }

    /**
     * Asynchronously reading all channel buffers and informing listeners
     */
    private void readChannels() {
        channelsMap.values()
                .stream()
                .flatMap(List::stream)
                .filter(mDNSChannel -> mDNSChannel.getDatagramChannel().isOpen())
                .map(mDNSChannel::getDatagramChannel)
                .forEach(datagramChannel -> {
                    readBuffer.clear();
                    try {
                        final SocketAddress address = datagramChannel.receive(readBuffer);
                        if (address != null && readBuffer.position() != 0) {
                            readBuffer.flip();
                            final byte[] bytes = new byte[readBuffer.remaining()];
                            readBuffer.get(bytes);

                            final Message response = new Message(bytes);
                            log.debug("Received " + response + " on " + address);
                            listener.accept(response);
                        }
                    } catch (final IOException e) {
                        log.warn("I/O error while receiving DNS message", e);
                    }
                });
    }

    /**
     * Create UDP Datagram Channels on specified Interfaces
     */
    private void assignNetworks() {
        final List<NetworkInterface> allNetworkInterfaces;
        try {
            allNetworkInterfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
        } catch (SocketException e) {
            log.warn("Unable to retrieve network interfaces! Clearing channels", e);
            return;
        }

        // Find Lost networkInterfaces and close and delete channels
        @SuppressWarnings("unchecked")
        final Collection<NetworkInterface> lostNetworkInterfaces =
                (Collection<NetworkInterface>) CollectionUtils.subtract(this.channelsMap.keySet(), allNetworkInterfaces);
        lostNetworkInterfaces.forEach(networkInterface -> {
            final List<mDNSChannel> removedChannels = this.channelsMap.remove(networkInterface);
            if (removedChannels != null) {
                removedChannels.forEach(mDNSChannel::close);
            }
        });

        // Find new / updated networkInterfaces
        @SuppressWarnings("unchecked")
        final Collection<NetworkInterface> newNetworkInterfaces =
                (Collection<NetworkInterface>) CollectionUtils.subtract(allNetworkInterfaces, this.channelsMap.keySet());

        for (final NetworkInterface networkInterface : newNetworkInterfaces) {
            final boolean isInterfaceUp;
            final boolean isPointToPoint;

            try {
                isInterfaceUp = networkInterface.isUp();
                isPointToPoint = networkInterface.isPointToPoint();
            } catch (SocketException e) {
                // We ignore faulty network interfaces
                log.error(e.getMessage(), e);
                return;
            }

            // No discovery possible in p2p interface or if offline
            if (isInterfaceUp && !isPointToPoint) {
                final List<mDNSChannel> mDNSChannelList = new ArrayList<>();
                if (hasIpv(networkInterface, Inet4Address.class)) {
                    openChannel(networkInterface, IPV4_ADDR).ifPresent(datagramChannel ->
                            mDNSChannelList.add(new mDNSChannel(datagramChannel, IPV4_SOA))
                    );
                }
                if (hasIpv(networkInterface, Inet6Address.class)) {
                    openChannel(networkInterface, IPV6_ADDR).ifPresent(datagramChannel ->
                            mDNSChannelList.add(new mDNSChannel(datagramChannel, IPV6_SOA))
                    );
                }
                channelsMap.put(networkInterface, mDNSChannelList);
            }
        }
    }

    /**
     * Open and Configuring the Datagram Channel
     * @param networkInterface Network Interface to Bind to
     * @param inetAddress Inet Address to bind to
     * @return Optional Return
     */
    private static Optional<DatagramChannel> openChannel(
            @NonNull final NetworkInterface networkInterface,
            @NonNull final InetAddress inetAddress
    ) {
        DatagramChannel channel = null;
        try {
            if (networkInterface.isUp() && (networkInterface.supportsMulticast() || networkInterface.isLoopback())) {
                final ProtocolFamily family = (inetAddress instanceof Inet6Address)
                        ? StandardProtocolFamily.INET6 : StandardProtocolFamily.INET;
                channel = DatagramChannel.open(family);
                channel.configureBlocking(false);
                channel.setOption(StandardSocketOptions.SO_REUSEADDR, true);
                channel.setOption(StandardSocketOptions.IP_MULTICAST_TTL, 255);
                channel.bind(new InetSocketAddress(MDNS_PORT));

                channel.setOption(StandardSocketOptions.IP_MULTICAST_IF, networkInterface);
                channel.join(inetAddress, networkInterface);
                log.debug("Joined networking address " + inetAddress + " on " + networkInterface);
                return Optional.of(channel);
            }
        } catch (IOException e) {
            log.warn("Error while opening channel on interface {} for {}", networkInterface, inetAddress, e);
            if (channel != null) {
                try {
                    channel.close();
                } catch (IOException ex) {
                    log.warn("Exception while closing channel", ex);
                }
            }
        }
        return Optional.empty();
    }

    /**
     * Check if the Network Interface has any InetAdress from the given Class
     */
    private static boolean hasIpv(@NonNull final NetworkInterface ni, @NonNull final Class<? extends InetAddress> ipv) {
        for (InetAddress inetAddress : Collections.list(ni.getInetAddresses())) {
            if (inetAddress.getClass().isAssignableFrom(ipv)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Sends given datagram to given channel an address
     *
     * @param mDNSChannel the channel on which the target is connected
     * @param src the buffer containing the datagram to be sent
     */
    private static void send(@NonNull final mDNSChannel mDNSChannel, @NonNull final ByteBuffer src) {
        final DatagramChannel datagramChannel = mDNSChannel.getDatagramChannel();
        final InetSocketAddress target = mDNSChannel.getTarget();

        if (!datagramChannel.isOpen()) {
            return;
        }

        final int position = src.position();
        try {
            datagramChannel.send(src, target);
            log.debug("Sent DNS message to " + target);
        } catch (final IOException e) {
            log.debug("I/O error while sending DNS message to " + target);
        } finally {
            src.position(position);
        }
    }
}
