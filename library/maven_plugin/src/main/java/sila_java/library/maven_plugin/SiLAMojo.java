package sila_java.library.maven_plugin;

import com.google.protobuf.Descriptors;
import org.apache.maven.execution.MavenSession;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.BuildPluginManager;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.*;
import org.apache.maven.project.MavenProject;
import org.apache.tools.ant.DirectoryScanner;
import sila_java.library.core.sila.mapping.feature.MalformedSiLAFeature;
import sila_java.library.core.sila.mapping.grpc.ProtoMapper;
import sila_java.library.core.utils.FileUtils;
import sila_java.library.sila_base.EmptyClass;

import java.io.*;
import java.net.URL;
import java.nio.file.*;

import static org.twdata.maven.mojoexecutor.MojoExecutor.*;
import static sila_java.library.core.sila.mapping.grpc.DynamicProtoBuilder.generateProtoFile;

/**
 * @implNote Basically search for Feature Definitions and create both proto files and the descendants :-)
 *
 * @implNote Only supports generated protos in ${basedir}/src/main/proto
 */
@Mojo( name = "run", defaultPhase = LifecyclePhase.GENERATE_SOURCES)
public class SiLAMojo extends AbstractMojo {
    private static final String DEFAULT_GRPC_VERSION = "1.61.1";
    private static final String DEFAULT_PROTOBUF_VERSION = "3.25.1";
    private static final String DEFAULT_PROTOC_MAVEN_PLUGIN_VERSION = "3.11.4";
    private static final String DEFAULT_FEATURES_GLOB = "/**/*.sila.xml";
    private static final String DEFAULT_FEATURES_PROJECT_RELATIVE_PATH = "/src/main/resources/";

    /**
     * The protobuf version for the protoc compiler
     */
    @Parameter(defaultValue = DEFAULT_PROTOBUF_VERSION)
    private String protobufVersion;

    /**
     * The protoc version for the protoc compiler
     */
    @Parameter(defaultValue = DEFAULT_PROTOC_MAVEN_PLUGIN_VERSION)
    private String protocMavenPluginVersion;

    /**
     * The grpc version to build the stubs for
     */
    @Parameter(defaultValue = DEFAULT_GRPC_VERSION)
    private String grpcVersion;

    /**
     * Input Features that conform with SiLA
     */
    @Parameter
    private String[] features;

    /**
     * Additional folders to generate proto files into
     */
    @Parameter
    private File[] protoGenerationFolders;

    /**
     * Maven project
     */
    @Parameter( defaultValue = "${project}", readonly = true )
    private MavenProject mavenProject;

    /**
     * Maven session
     */
    @Parameter( defaultValue = "${session}", readonly = true )
    private MavenSession mavenSession;

    /**
     * Components are necessary to provide context to other plugins
     */
    @Component
    private BuildPluginManager pluginManager;

    /**
     * @inheritDoc
     */
    @Override
    public void execute() throws MojoExecutionException {
        if (features == null) {
            final Path defaultProjectRelativePath = new File(mavenProject.getBasedir(), DEFAULT_FEATURES_PROJECT_RELATIVE_PATH).toPath();
            features = new String[]{defaultProjectRelativePath + DEFAULT_FEATURES_GLOB};
        }
        if (features.length > 0) {
            final DirectoryScanner directoryScanner = new DirectoryScanner();
            directoryScanner.setIncludes(features);
            directoryScanner.scan();
            features = directoryScanner.getIncludedFiles();
        } else {
            getLog().info( "SiLA Maven Plugin did not find any SiLA Features (what else should the plugin be for?).");
        }

        final boolean generateAdditionalProtos = (protoGenerationFolders != null);
        if (generateAdditionalProtos && (protoGenerationFolders.length == 0)) {
            throw new MojoExecutionException("If additional proto folders are specified, at least one has to be defined!");
        }

        // Show user configuration
        getLog().info( "Executing Custom SiLA Maven Plugin" );
        getLog().info("Found protobuf version: " + protobufVersion);
        getLog().info("Found maven protoc plugin version: " + protocMavenPluginVersion);
        getLog().info("Found gRPC version: " + grpcVersion);
        getLog().info("Found " + features.length + " SiLA Features.");

        // Copy the proto into the target folder
        final String frameworkProtoName = "SiLAFramework.proto";

        final URL frameworkLocation = EmptyClass.class.getResource("/sila_base/protobuf/" + frameworkProtoName);
        if (frameworkLocation == null) {
            throw new MojoExecutionException("Can not find " + frameworkProtoName);
        }

        final String targetClassesLocation = mavenProject.getBuild().getDirectory() +
                File.separator + "generated-sources" + File.separator + "sila";
        getLog().info("Generating classes into: " + targetClassesLocation);

        final String frameworkProtoLocation = targetClassesLocation + File.separator + frameworkProtoName;
        try {
            Files.createDirectories(Paths.get(targetClassesLocation));

            if (generateAdditionalProtos) {
                for (final File folder : protoGenerationFolders) {
                    Files.createDirectories(folder.toPath());
                }
            }
        } catch (IOException e) {
            throw new MojoExecutionException("Could not create directories");
        }

        try {
            Files.copy(
                    frameworkLocation.openStream(),
                    Paths.get(frameworkProtoLocation),
                    StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            throw new MojoExecutionException("Could not copy " + frameworkProtoName);
        }

        // Create the proto files from feature definitions
        for (final String file : features) {
            try {
                final ProtoMapper protoMapper = ProtoMapper.usingFeature(
                        FileUtils.getFileContent(new File(file).getAbsolutePath())
                );

                final Descriptors.FileDescriptor protoFile = protoMapper.generateProto();
                final String featureName = protoFile.getName();
                getLog().info("Generating Classes for Feature: " + featureName);

                final String protoFileContent = generateProtoFile(protoFile);

                final String packageName = protoFile.getPackage().replace(".", File.separator);

                String targetFilePath = targetClassesLocation + File.separator + packageName + File.separator + featureName + ".proto";
                createFile(targetFilePath, protoFileContent);

                if (generateAdditionalProtos) {
                    for (File folder : protoGenerationFolders) {
                        targetFilePath = folder.getPath() + File.separator + packageName + File.separator + featureName + ".proto";
                        createFile(targetFilePath, protoFileContent);
                    }
                }
            } catch (IOException | MalformedSiLAFeature e) {
                throw new MojoExecutionException(e.getClass().getName() + " : Could not parse " + file + "\n" +
                        e.getMessage());
            }
        }

        executeMojo(
                plugin(
                        groupId("com.github.os72"),
                        artifactId("protoc-jar-maven-plugin"),
                        version(protocMavenPluginVersion)
                ),
                goal("run"),
                configuration(
                        element(
                                name("protocArtifact"),
                                "com.google.protobuf:protoc:" + protobufVersion
                        ),
                        element(
                                name("addProtoSources"),
                                "all"
                        ),
                        element(
                               name("includeDirectories"),
                                element(name("include"),
                                        targetClassesLocation)
                        ),
                        element(name("inputDirectories"),
                                element(name("include"),
                                        targetClassesLocation)),
                        element(name("outputTargets"),
                                element(name("outputTarget"),
                                        element(name("type"),
                                                "java")
                                ),
                                element(name("outputTarget"),
                                        element(name("type"),
                                                "grpc-java"),
                                        element(name("pluginArtifact"),
                                                "io.grpc:protoc-gen-grpc-java:" + grpcVersion)
                                )
                        )
                ),
                executionEnvironment(
                        mavenProject,
                        mavenSession,
                        pluginManager
                )
        );
    }

    /**
     * Create a file at the specified absolute path and write content inside
     * @param absoluteFilePath the absolute path
     * @param content the content
     * @throws IOException if unable to create or write the content to the file
     */
    private void createFile(final String absoluteFilePath, final String content) throws IOException {
        final File file = new File(absoluteFilePath);
        file.getParentFile().mkdirs();
        try (FileWriter writer = new FileWriter(file)) {
            writer.write(content);
        }
        getLog().info("Generated " + absoluteFilePath);
    }
}
