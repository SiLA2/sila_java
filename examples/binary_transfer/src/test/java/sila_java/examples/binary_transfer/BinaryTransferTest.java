package sila_java.examples.binary_transfer;

import io.grpc.Channel;
import io.grpc.Status;
import io.grpc.StatusRuntimeException;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.function.Executable;
import sila2.org.silastandard.SiLABinaryTransfer;
import sila_java.library.core.sila.binary_transfer.BinaryTransferErrorHandler;
import sila_java.library.core.sila.binary_transfer.BinaryUploadHelper;
import sila_java.library.core.sila.clients.ChannelFactory;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.Optional;
import java.util.Random;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class BinaryTransferTest {
    private Server server;
    private Client client;
    private Channel channel;

    @BeforeAll
    void beforeAll() throws IOException {
        String host = "127.0.0.1";
        int port = 50052;
        this.server = new Server(host, port);
        this.channel = ChannelFactory.getUnencryptedChannel(host, port);
        this.client = new Client(channel);
    }

    @AfterAll
    void afterAll() {
        this.server.close();
    }

    @Test
    void testDirectTransfer() {
        Assertions.assertEquals("SiLA2_Test_String_Value",
                                new String(client.getBinaryValueDirectly(), StandardCharsets.UTF_8)
        );
    }

    @Test
    void testDownload() {
        String expected = String.join("",
                                      Collections.nCopies(100000,
                                                          "A_slightly_longer_SiLA2_Test_String_Value_used_to_demonstrate_the_binary_download"
                                      )
        );
        Assertions.assertEquals(expected, new String(client.getBinaryValueDownload(), StandardCharsets.UTF_8));
    }

    @Test
    void testRoundtripWithLargeBinary() {
        byte[] bytesToUpload = new byte[3 * 987 * 1024];  // > 2 MB, not a multiple of the 1 MB chunk size
        new Random().nextBytes(bytesToUpload);  // fill with random bytes

        byte[] receivedBytes = client.echoBinaryValue(bytesToUpload);
        Assertions.assertArrayEquals(bytesToUpload, receivedBytes);
    }

    @Test
    void testRoundtripWithSmallBinary() {
        byte[] bytesToUpload = new byte[3 * 1024];  // < 2 MB
        new Random().nextBytes(bytesToUpload);  // fill with random bytes

        byte[] receivedBytes = client.echoBinaryValue(bytesToUpload);
        Assertions.assertArrayEquals(bytesToUpload, receivedBytes);
    }

    @Test
    void testUploadIsRejectedForNotWhitelistedParameter() {
        final BinaryUploadHelper uploadHelper = new BinaryUploadHelper(channel);

        // existing parameter
        assertThrowsBinaryUploadFailedError(() -> uploadHelper.upload(new byte[100],
                                                                      "org.silastandard/examples/GreetingProvider/v1/Command/SayHello/Parameter/Name"
        ));
        // not existing parameter
        assertThrowsBinaryUploadFailedError(() -> uploadHelper.upload(
                new byte[100],
                "org.silastandard/examples/GreetingProvider/v1/Command/SayHello/Parameter/UnknownParameter"
        ));
        // not existing command
        assertThrowsBinaryUploadFailedError(() -> uploadHelper.upload(new byte[100],
                                                                      "org.silastandard/examples/GreetingProvider/v1/Command/UnknownCommand/Parameter/Name"
        ));
        // not implemented feature
        assertThrowsBinaryUploadFailedError(() -> uploadHelper.upload(new byte[100],
                                                                      "org.silastandard/examples/UnknownFeature/v1/Command/SayHello/Parameter/Name"
        ));
    }

    private void assertThrowsBinaryUploadFailedError(Executable call) {
        StatusRuntimeException ex = Assertions.assertThrows(StatusRuntimeException.class, call);
        Assertions.assertEquals(Status.Code.ABORTED, ex.getStatus().getCode());
        Optional<SiLABinaryTransfer.BinaryTransferError> maybeErr
                = BinaryTransferErrorHandler.retrieveBinaryTransferError(ex);
        Assertions.assertTrue(maybeErr.isPresent());
        Assertions.assertEquals(SiLABinaryTransfer.BinaryTransferError.ErrorType.BINARY_UPLOAD_FAILED,
                                maybeErr.get().getErrorType()
        );
    }
}
