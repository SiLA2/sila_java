package sila_java.examples.binary_transfer;

import sila_java.library.server_base.SiLAServer;
import sila_java.library.server_base.binary_transfer.upload.BinaryUploadParameterWhitelistInterceptor;
import sila_java.library.server_base.identification.ServerInformation;

import java.io.IOException;
import java.nio.file.Path;

import static sila_java.library.core.utils.FileUtils.getResourceContent;

/**
 * Basic server application implementing the features Greeting Provider and Authorization Service
 *
 * This is not intended to be a best practice example for server development. It just provides the necessary server
 * scaffold to showcase metadata handling.
 */
public class Server implements AutoCloseable {
    private final SiLAServer silaServer;

    public Server(String host, int port) throws IOException {
        ServerInformation serverInformation =
                new ServerInformation(
                        "MetadataExampleServer",
                        "Server to demonstrate SiLA Client Metadata using the Authorization Service feature",
                        "https://gitlab.com/SiLA2/sila_java",
                        "0.1"
                );

        this.silaServer = SiLAServer.Builder
                .newBuilder(serverInformation)
                .addFeature(getResourceContent("BinaryTransferTest-v1_0.sila.xml"), new BinaryTransferTestImpl())
                .withBinaryTransferSupport()
                .withBinaryDatabaseInjector()
                .addInterceptor(new BinaryUploadParameterWhitelistInterceptor(
                        "org.silastandard/test/BinaryTransferTest/v1/Command/EchoBinaryValue/Parameter/BinaryValue"))
                .withUnsafeCommunication(true)
                .withPort(port)
                .withHost(host)
                .start();
    }

    @Override
    public void close() {
        this.silaServer.close();
    }
}
