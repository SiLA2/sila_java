package sila_java.examples.thermostat;

import com.google.common.util.concurrent.AtomicDouble;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.TimeUnit;

@Slf4j
public class ThermostatSimulation {
    public static final double KELVIN_PER_SECONDS = 1; // [K/s] Update ramp
    public static final double UPDATE_INTERVAL = 1; // [s] Interval temperature is updated
    public static final double KELVIN_ACCURACY = 1; // [K] When control is assumed to be finished
    public static final double KELVIN_RAMP = KELVIN_PER_SECONDS * UPDATE_INTERVAL; // [Kelvin / step]
    private final AtomicDouble currentTemperature = new AtomicDouble(293d); // [K]
    private final AtomicDouble targetTemperature = new AtomicDouble(293d); // [K]
    private final List<TemperatureListener> temperatureListenerList = new CopyOnWriteArrayList<>();
    private final Thread agentThread;

    public ThermostatSimulation() {
        agentThread = new Thread(
                new TemperatureChangeAgent(),
                ThermostatSimulation.class.getName() + "_" + TemperatureChangeAgent.class.getName()
        );
        //terminate the thread with the VM.
        agentThread.setDaemon(true);
        agentThread.start();
    }

    public interface TemperatureListener {
        void onChange(double temperature);
    }

    private class TemperatureChangeAgent implements Runnable {
        @Override
        public void run() {
            while (!Thread.interrupted()) {
                final long startTime = System.currentTimeMillis();
                // Linear Increase to target temperature until accuracy reached
                final double temperature = currentTemperature.get();
                final double temperatureDifference = (targetTemperature.get() - temperature);
                final double absoluteTemperatureDifference = Math.abs(temperatureDifference);

                if (absoluteTemperatureDifference >= KELVIN_ACCURACY) {
                    if (KELVIN_RAMP > absoluteTemperatureDifference) {
                        currentTemperature.set(targetTemperature.get());
                    } else {
                        currentTemperature.getAndAdd(Math.signum(temperatureDifference) * KELVIN_RAMP);
                    }

                    ThermostatSimulation.this.temperatureListenerList.forEach(l -> l.onChange(temperature));
                }

                // Best effort to keep the update interval
                final long remainingTime =  TimeUnit.SECONDS.toMillis((long) UPDATE_INTERVAL) -
                        (System.currentTimeMillis() - startTime);
                try {
                    if (remainingTime > 0)
                        Thread.sleep(remainingTime);
                } catch (final InterruptedException e) {
                    Thread.currentThread().interrupt();
                }
            }
            log.info("End of temperature change agent");
        }
    }

    public void addListener(final @NonNull TemperatureListener listener) {
        temperatureListenerList.add(listener);
    }

    public void removeListener(final @NonNull TemperatureListener listener) {
        temperatureListenerList.remove(listener);
    }

    public void setTargetTemperature(final double targetTemperature) {
        this.targetTemperature.set(targetTemperature);
    }

    public double getCurrentTemperature() {
        return this.currentTemperature.get();
    }
}
