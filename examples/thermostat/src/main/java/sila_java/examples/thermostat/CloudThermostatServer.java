package sila_java.examples.thermostat;

import com.google.common.collect.ImmutableMap;
import io.grpc.ManagedChannel;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.CloudClientEndpointGrpc;
import sila2.org.silastandard.SiLAFramework;
import sila2.org.silastandard.examples.temperaturecontroller.v1.TemperatureControllerOuterClass;
import sila_java.library.cloudier.server.CloudierConnectionConfigurationService;
import sila_java.library.cloudier.server.CloudierServerEndpoint;
import sila_java.library.cloudier.server.CloudierSiLAService;
import sila_java.library.cloudier.server.MessageCaseHandler;
import sila_java.library.core.sila.clients.ChannelFactory;
import sila_java.library.sila_base.EmptyClass;

import java.io.IOException;
import java.util.HashMap;
import java.util.Objects;
import java.util.UUID;

import static sila_java.library.core.utils.FileUtils.getFileContent;
import static sila_java.library.core.utils.Utils.blockUntilShutdown;

@Slf4j
public class CloudThermostatServer implements AutoCloseable {
    private final CloudierSiLAService cloudierSiLAService;
    private final CloudierConnectionConfigurationService cloudierConnectionConfigurationService;
    private final ThermostatSimulation thermostatSimulation = new ThermostatSimulation();
    private final ThermostatServer.TemperatureControlImpl temperatureControl = new ThermostatServer.TemperatureControlImpl(thermostatSimulation);
    private CloudClientEndpointGrpc.CloudClientEndpointStub clientEndpoint;
    private CloudierServerEndpoint cloudServerEndpointService;
    private ManagedChannel channel;


    public static void main(String[] args) throws IOException {
        final UUID serverUUID = UUID.randomUUID();
        final CloudThermostatServer cloudThermostatServer = new CloudThermostatServer(serverUUID);
        log.info("To stop the server press CTRL + C.");
        blockUntilShutdown(cloudThermostatServer::close);
    }

    public CloudThermostatServer(UUID serverUUID) throws IOException {
        final ImmutableMap<String, String> immutableMap = ImmutableMap.<String, String>builder().put(
                "SiLAService", getFileContent(
                        Objects.requireNonNull(
                                EmptyClass.class.getResourceAsStream("/sila_base/feature_definitions/org/silastandard/core/SiLAService-v1_0.sila.xml")
                        )
                )
        ).put(
                "ConnectionConfigurationService", getFileContent(
                        Objects.requireNonNull(
                                EmptyClass.class.getResourceAsStream("/sila_base/feature_definitions/org/silastandard/core/ConnectionConfigurationService-v1_0.sila.xml")
                        )
                )
        ).put(
                "TemperatureController", getFileContent(
                        Objects.requireNonNull(
                                EmptyClass.class.getResourceAsStream("/sila_base/feature_definitions/org/silastandard/examples/TemperatureController-v1_0.sila.xml")
                        )
                )
        ).build();
        this.cloudierSiLAService = new CloudierSiLAService(
                "Cloud Thermostat Server",
                ThermostatServer.serverInfo.getType(),
                serverUUID.toString(),
                ThermostatServer.serverInfo.getVersion(),
                ThermostatServer.serverInfo.getDescription(),
                ThermostatServer.serverInfo.getVendorURL(),
                immutableMap
        );
        this.cloudierConnectionConfigurationService = new CloudierConnectionConfigurationService(
                false, (connectionMode) -> {}
        );
        this.startServerInitiatedConnection();
    }

    private void startServerInitiatedConnection() {
        this.channel = ChannelFactory.getTLSEncryptedChannel("127.0.0.1", 50051);
        this.clientEndpoint = CloudClientEndpointGrpc.newStub(this.channel);
        final HashMap<String, MessageCaseHandler> callForwarderMap = new HashMap<String, MessageCaseHandler>() {{
            put("org.silastandard/examples/TemperatureController/v1/Command/ControlTemperature", new MessageCaseHandler()
                    .withObservableCommand(TemperatureControllerOuterClass.ControlTemperature_Parameters.parser(), temperatureControl::controlTemperature)
                    .withExecInfo(SiLAFramework.CommandExecutionUUID.parser(), temperatureControl::controlTemperatureInfo)
                    .withResult(SiLAFramework.CommandExecutionUUID.parser(), temperatureControl::controlTemperatureResult)
            );
            put("org.silastandard/examples/TemperatureController/v1/Property/CurrentTemperature", new MessageCaseHandler()
                    .withObservableProperty(TemperatureControllerOuterClass.Subscribe_CurrentTemperature_Parameters.parser(), temperatureControl::subscribeCurrentTemperature)
            );
        }};
        this.cloudServerEndpointService = new CloudierServerEndpoint(
                this.cloudierSiLAService,
                this.cloudierConnectionConfigurationService,
                this.clientEndpoint,
                callForwarderMap
        );
    }

    @Override
    public void close() {
        this.channel.shutdown();
        this.temperatureControl.temperatureManager.close();
    }
}
