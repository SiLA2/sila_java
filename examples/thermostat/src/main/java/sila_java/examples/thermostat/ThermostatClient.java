package sila_java.examples.thermostat;

import io.grpc.Context;
import io.grpc.ManagedChannel;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.SiLAFramework;
import sila2.org.silastandard.examples.temperaturecontroller.v1.TemperatureControllerGrpc;
import sila2.org.silastandard.examples.temperaturecontroller.v1.TemperatureControllerOuterClass;
import sila_java.library.core.sila.types.SiLAReal;
import sila_java.library.manager.ServerFinder;
import sila_java.library.manager.ServerManager;
import sila_java.library.manager.models.Server;
import sila_java.library.core.sila.clients.ChannelFactory;

import java.time.Duration;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;

@Slf4j
public class ThermostatClient {
    @SneakyThrows
    public static void main(String[] args) throws InterruptedException {
        // Create Manager for clients and start discovery
        try (final ServerManager serverManager = ServerManager.getInstance()) {

            final Server server = ServerFinder
                    .filterBy(ServerFinder.Filter.type(ThermostatServer.SERVER_TYPE))
                    .scanAndFindOne(Duration.ofMinutes(1))
                    .orElseThrow(() -> new RuntimeException("No Thermostat server found within time"));
            log.info("Found Server!");
            final ManagedChannel serviceChannel = ChannelFactory.getTLSEncryptedChannel(
                    server.getHost(), server.getPort(), server.getCertificateAuthority());
            try {

                final TemperatureControllerGrpc.TemperatureControllerBlockingStub blockingStub =
                        TemperatureControllerGrpc.newBlockingStub(serviceChannel);

                // Set Temperature
                final SiLAFramework.CommandConfirmation commandConfirmation =
                        blockingStub.controlTemperature(TemperatureControllerOuterClass.ControlTemperature_Parameters
                                .newBuilder()
                                .setTargetTemperature(SiLAReal.from(313))
                                .build()
                        );

                // Example of subscription of normal operation
                final Context.CancellableContext cancellableContext = Context.current().withCancellation();

                cancellableContext.run(() -> {
                    final Iterator<TemperatureControllerOuterClass.Subscribe_CurrentTemperature_Responses> temperatureIterator =
                            blockingStub.subscribeCurrentTemperature(
                                    TemperatureControllerOuterClass.Subscribe_CurrentTemperature_Parameters
                                            .newBuilder()
                                            .build()
                            );

                    final Iterator<SiLAFramework.ExecutionInfo> ExecutionInfoIterator =
                            blockingStub.controlTemperatureInfo(commandConfirmation.getCommandExecutionUUID());

                    // Get Execution State
                    SiLAFramework.ExecutionInfo ExecutionInfo = ExecutionInfoIterator.next();
                    System.out.println("Initial Execution State: " + ExecutionInfo.toString());


                    // Show subscription value until finished
                    while (ExecutionInfo.getCommandStatus().equals(SiLAFramework.ExecutionInfo.CommandStatus.running)) {
                        ExecutionInfo = ExecutionInfoIterator.next();
                        System.out.println("Got Command Status: " + ExecutionInfo.toString());
                        System.out.println("Got temperature " + temperatureIterator.next());
                    }
                });

                // This cancels the subscriptions from the client side
                cancellableContext.cancel(null);
                System.out.println("Stopping...");
            } finally {
                serviceChannel.shutdown().awaitTermination(5, TimeUnit.SECONDS);
            }
        }
        System.out.println("Stopped");
    }
}
