# Hello SiLA Java Implementation
The goal is to show how a SiLA server can be implemented in Java. This is a example server and is not intended for any practical use.

Additionally, the features in `sila_base` should normally be referenced from the `sila_base` repository, 
for the ease of introduction, the feature was copied for this example but should be avoided on real implementation.

To understand the build process and source code, check `pom.xml` for a minimum build configuration that uses
maven and `src/main/java` for the reference Java classes.