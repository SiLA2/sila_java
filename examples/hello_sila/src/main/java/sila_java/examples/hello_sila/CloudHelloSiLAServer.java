package sila_java.examples.hello_sila;

import com.google.common.collect.ImmutableMap;
import io.grpc.ManagedChannel;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.CloudClientEndpointGrpc;
import sila2.org.silastandard.examples.greetingprovider.v1.GreetingProviderOuterClass;
import sila_java.library.cloudier.server.CloudierConnectionConfigurationService;
import sila_java.library.cloudier.server.CloudierServerEndpoint;
import sila_java.library.cloudier.server.CloudierSiLAService;
import sila_java.library.cloudier.server.MessageCaseHandler;
import sila_java.library.core.sila.clients.ChannelFactory;
import sila_java.library.sila_base.EmptyClass;

import java.io.IOException;
import java.util.HashMap;
import java.util.Objects;
import java.util.UUID;

import static sila_java.library.core.utils.FileUtils.getFileContent;
import static sila_java.library.core.utils.Utils.blockUntilShutdown;

@Slf4j
public class CloudHelloSiLAServer implements AutoCloseable {
    private final CloudierSiLAService cloudierSiLAService;
    private final CloudierConnectionConfigurationService cloudierConnectionConfigurationService;
    private CloudClientEndpointGrpc.CloudClientEndpointStub clientEndpoint;
    private CloudierServerEndpoint cloudServerEndpointService;
    private ManagedChannel channel;


    public static void main(String[] args) throws IOException {
        final UUID serverUUID = UUID.randomUUID();
        final CloudHelloSiLAServer cloudHelloSiLAServer = new CloudHelloSiLAServer(serverUUID);
        log.info("To stop the server press CTRL + C.");
        blockUntilShutdown(cloudHelloSiLAServer::close);
    }

    public CloudHelloSiLAServer(UUID serverUUID) throws IOException {
        final ImmutableMap<String, String> immutableMap = ImmutableMap.<String, String>builder().put(
                "SiLAService", getFileContent(
                        Objects.requireNonNull(
                                EmptyClass.class.getResourceAsStream("/sila_base/feature_definitions/org/silastandard/core/SiLAService-v1_0.sila.xml")
                        )
                )
        ).put(
                "ConnectionConfigurationService", getFileContent(
                        Objects.requireNonNull(
                                EmptyClass.class.getResourceAsStream("/sila_base/feature_definitions/org/silastandard/core/ConnectionConfigurationService-v1_0.sila.xml")
                        )
                )
        ).put(
                "GreetingProvider",
                getFileContent(EmptyClass.class.getResourceAsStream("/sila_base/feature_definitions/org/silastandard/examples/GreetingProvider-v1_0.sila.xml"))
        ).build();
        this.cloudierSiLAService = new CloudierSiLAService(
                "Cloud Hello SiLA Server",
                HelloSiLAServer.serverInfo.getType(),
                serverUUID.toString(),
                HelloSiLAServer.serverInfo.getVersion(),
                HelloSiLAServer.serverInfo.getDescription(),
                HelloSiLAServer.serverInfo.getVendorURL(),
                immutableMap
        );
        this.cloudierConnectionConfigurationService = new CloudierConnectionConfigurationService(
                false, (connectionMode) -> {}
        );
        this.startServerInitiatedConnection();
    }

    private void startServerInitiatedConnection() {
        this.channel = ChannelFactory.getTLSEncryptedChannel("127.0.0.1", 50051);
        this.clientEndpoint = CloudClientEndpointGrpc.newStub(this.channel);
        final SayHelloCommand helloCommand = new SayHelloCommand();
        final HashMap<String, MessageCaseHandler> callForwarderMap = new HashMap<String, MessageCaseHandler>() {{
            put("org.silastandard/examples/GreetingProvider/v1/Command/SayHello", new MessageCaseHandler()
                    .withUnobservableCommand(GreetingProviderOuterClass.SayHello_Parameters.parser(), helloCommand::sayHello)
            );
            put("org.silastandard/examples/GreetingProvider/v1/Property/StartYear", new MessageCaseHandler()
                    .withUnobservableProperty(GreetingProviderOuterClass.Get_StartYear_Parameters.parser(), helloCommand::getStartYear)
            );
        }};
        this.cloudServerEndpointService = new CloudierServerEndpoint(
                this.cloudierSiLAService,
                this.cloudierConnectionConfigurationService,
                this.clientEndpoint,
                callForwarderMap
        );
    }

    @Override
    public void close() {
        this.channel.shutdown();
    }
}
