package sila_java.examples.test_server;

import com.google.protobuf.ByteString;
import io.grpc.stub.StreamObserver;
import junit.framework.Assert;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import sila2.org.silastandard.SiLABinaryTransfer;
import sila2.org.silastandard.test.complexdatatypetest.v1.ComplexDataTypeTestOuterClass;
import sila_java.examples.test_server.utils.Bytes;
import sila_java.examples.test_server.utils.Interval;
import sila_java.examples.test_server.utils.Intervals;
import sila_java.library.core.sila.types.SiLABinary;
import sila_java.library.core.sila.types.SiLAInteger;

import javax.xml.bind.DatatypeConverter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

@Slf4j
class TestSiLABinary extends TestBase {
    private static final int BINARY_CHUNK_LENGTH = (int) Math.pow(2, 20); // 1048576 ~1mb
    private static final int CIPHER_XOR_KEY = 42;

    @Getter
    @AllArgsConstructor
    private static final class UploadResponse {
        private final UUID binaryId;
        private final List<String> checksums;
    }

    public static String getChecksum(byte[] bytes) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("MD5");
        md.update(bytes);
        return DatatypeConverter.printHexBinary(md.digest()).toUpperCase();
    }

    @Test
    void testUploadDownload() {
        final int nbChunks = 16;
        final int chunkLength = BINARY_CHUNK_LENGTH;
        final UploadResponse response = uploadTest(nbChunks, chunkLength);
        final List<String> downloadChecksums = downloadTest(response.getBinaryId(), nbChunks, chunkLength);
        Assert.assertTrue("Checksums differ", response.getChecksums().containsAll(downloadChecksums));
    }

    @Test
    void testLargeBinaryXorCipher() {
        final int nbChunks = 10;

        // Upload some binary data
        final UploadResponse uploadResponse = uploadTest(nbChunks, BINARY_CHUNK_LENGTH);

        // Encipher data
        final ComplexDataTypeTestOuterClass.XOREncipher_Responses encipherResponse = this.complexDataTypeTestBlockingStub.xOREncipher(
                ComplexDataTypeTestOuterClass.XOREncipher_Parameters
                        .newBuilder()
                        .setData(SiLABinary.fromBinaryTransferUUID(uploadResponse.binaryId))
                        .setCipherKey(SiLAInteger.from(CIPHER_XOR_KEY))
                        .build()
        );

        // Retrieve enciphered data checksum
        final List<String> encipheredDataChecksum = downloadTest(
                UUID.fromString(encipherResponse.getData().getBinaryTransferUUID()), nbChunks, BINARY_CHUNK_LENGTH
        );
        Assert.assertNotSame(uploadResponse.getChecksums(), encipheredDataChecksum);
        Assert.assertNotSame(uploadResponse.getBinaryId(), encipherResponse.getData().getBinaryTransferUUID());

        // Decipher data by enciphering with the same key
        final ComplexDataTypeTestOuterClass.XOREncipher_Responses decipherDataChecksum = this.complexDataTypeTestBlockingStub.xOREncipher(
                ComplexDataTypeTestOuterClass.XOREncipher_Parameters
                        .newBuilder()
                        .setData(SiLABinary.fromBinaryTransferUUID(encipherResponse.getData().getBinaryTransferUUID()))
                        .setCipherKey(SiLAInteger.from(CIPHER_XOR_KEY))
                        .build()
        );

        // Retrieve deciphered data checksum
        final List<String> decipheredDataChecksum = downloadTest(
                UUID.fromString(decipherDataChecksum.getData().getBinaryTransferUUID()), nbChunks, BINARY_CHUNK_LENGTH
        );

        Assert.assertEquals(uploadResponse.getChecksums(), decipheredDataChecksum);
        Assert.assertNotSame(uploadResponse.getBinaryId(), decipherDataChecksum.getData().getBinaryTransferUUID());
    }

    @Test
    void testSmallBinaryXorCipher() {
        final byte[] randomBytes = getRandomBytes(BINARY_CHUNK_LENGTH);

        // Encipher data
        final ComplexDataTypeTestOuterClass.XOREncipher_Responses encipherResponse = this.complexDataTypeTestBlockingStub.xOREncipher(
                ComplexDataTypeTestOuterClass.XOREncipher_Parameters
                        .newBuilder()
                        .setData(SiLABinary.fromBytes(randomBytes))
                        .setCipherKey(SiLAInteger.from(CIPHER_XOR_KEY))
                        .build()
        );
        final byte[] encipheredData = encipherResponse.getData().getValue().toByteArray();
        final byte[] manuallyEncipheredData = Arrays.copyOf(encipheredData, encipheredData.length);
        Bytes.XOREncipherBytes(manuallyEncipheredData, (byte)CIPHER_XOR_KEY, 0, 0);
        Assert.assertFalse(Arrays.equals(encipheredData, randomBytes));
        Assert.assertTrue(Arrays.equals(encipheredData, manuallyEncipheredData));

        // Decipher data
        final ComplexDataTypeTestOuterClass.XOREncipher_Responses decipherResponse = this.complexDataTypeTestBlockingStub.xOREncipher(
                ComplexDataTypeTestOuterClass.XOREncipher_Parameters
                        .newBuilder()
                        .setData(SiLABinary.fromBytes(encipheredData))
                        .setCipherKey(SiLAInteger.from(CIPHER_XOR_KEY))
                        .build()
        );
        Assert.assertTrue(Arrays.equals(decipherResponse.getData().getValue().toByteArray(), randomBytes));
    }

    @SneakyThrows
    private UploadResponse uploadTest(final int nbChunks, final int chunkLength) {
        final long nbBytes = (long) nbChunks * chunkLength;
        final List<String> checksums = new ArrayList<>();
        final Set<Integer> chunksUploaded = ConcurrentHashMap.newKeySet();
        final CompletableFuture<Void> voidCompletableFuture = new CompletableFuture<>();
        final SiLABinaryTransfer.CreateBinaryResponse binary = uploadBlockingStub.createBinary(
                SiLABinaryTransfer.CreateBinaryRequest
                        .newBuilder()
                        .setBinarySize(nbBytes)
                        .setParameterIdentifier("org.silastandard/test/ComplexDataTypeTest/v1/Command/XOREncipher/Parameter/Data")
                        .setChunkCount(nbChunks)
                        .build()
        );
        final UUID binaryId = UUID.fromString(binary.getBinaryTransferUUID());
        final StreamObserver<SiLABinaryTransfer.UploadChunkRequest> requestStreamObserver =
                uploadStreamStub.uploadChunk(new StreamObserver<SiLABinaryTransfer.UploadChunkResponse>() {
                    @Override
                    public void onNext(SiLABinaryTransfer.UploadChunkResponse uploadChunkResponse) {
                        Assert.assertEquals(binaryId, UUID.fromString(uploadChunkResponse.getBinaryTransferUUID()));
                        chunksUploaded.add(uploadChunkResponse.getChunkIndex());
                        log.info("Server received {}/{} chunks", chunksUploaded.size(), nbChunks);
                        if (chunksUploaded.size() == nbChunks) {
                            voidCompletableFuture.complete(null);
                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        log.warn("Upload chunk stream exception: {}", t.getMessage(), t);
                        voidCompletableFuture.completeExceptionally(t);
                    }

                    @Override
                    public void onCompleted() {
                        log.info("Upload complete!");
                        voidCompletableFuture.complete(null);
                    }
                });

        for (int i = 0; i < nbChunks; ++i) {
            log.info("Uploading Chunk {}/{}", i + 1, nbChunks);
            final byte[] randomBytes = getRandomBytes(chunkLength);
            checksums.add(getChecksum(randomBytes));
            requestStreamObserver.onNext(
                    SiLABinaryTransfer.UploadChunkRequest
                            .newBuilder()
                            .setChunkIndex(i)
                            .setBinaryTransferUUID(binaryId.toString())
                            .setPayload(ByteString.copyFrom(randomBytes))
                            .build()
            );
        }
        voidCompletableFuture.join();
        requestStreamObserver.onCompleted();
        return new UploadResponse(binaryId, checksums);
    }

    /**
     * Allocate a byte array with random bytes
     * @param nbBytes The number of random bytes to allocate
     * @return a new byte array containing random bytes
     * @throws IllegalArgumentException if the number of bytes is below 1
     */
    private static byte[] getRandomBytes(final int nbBytes) {
        if (nbBytes < 1)
            throw new IllegalArgumentException("Number of bytes must be greater than or equal to 1");
        final byte[] random = new byte[nbBytes];
        new Random().nextBytes(random);
        return random;
    }

    private List<String> downloadTest(final UUID binaryId, final int nbChunks, final int chunkLength) {
        final CompletableFuture<Void> voidCompletableFuture = new CompletableFuture<>();
        final Intervals downloadCompletion = new Intervals();
        final List<String> checksums = new CopyOnWriteArrayList<>();
        final long bytesToDownload = (long)nbChunks * chunkLength;
        final StreamObserver<SiLABinaryTransfer.GetChunkRequest> requestStreamObserver =
                downloadStreamStub.getChunk(new StreamObserver<SiLABinaryTransfer.GetChunkResponse>() {
                    @SneakyThrows
                    @Override
                    public void onNext(SiLABinaryTransfer.GetChunkResponse getChunkResponse) {
                        downloadCompletion.add(new Interval(getChunkResponse.getOffset(), getChunkResponse.getOffset() + getChunkResponse.getPayload().size()));
                        checksums.add(getChecksum(getChunkResponse.getPayload().toByteArray()));
                        final long bytesDownloaded = downloadCompletion.getIntervals()
                                .stream()
                                .map(i -> i.getEnd() - i.getBegin())
                                .reduce(Long::sum)
                                .orElse(0L);
                        log.info("Download {}% complete", (double)bytesDownloaded / bytesToDownload * 100L);
                        if (bytesDownloaded == bytesToDownload) {
                            voidCompletableFuture.complete(null);
                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        log.warn("Download chunk stream exception: {}", t.getMessage(), t);
                        voidCompletableFuture.completeExceptionally(t);
                    }

                    @Override
                    public void onCompleted() {
                        log.info("Download complete!");
                        voidCompletableFuture.complete(null);
                    }
                });

        for (int i = 0; i < nbChunks; ++i) {
            log.info("Sending chunk request {}/{}", i + 1, nbChunks);
            requestStreamObserver.onNext(
                    SiLABinaryTransfer.GetChunkRequest
                            .newBuilder()
                            .setBinaryTransferUUID(binaryId.toString())
                            .setOffset((long)i * chunkLength)
                            .setLength(chunkLength)
                            .build()
            );
        }
        voidCompletableFuture.join();
        requestStreamObserver.onCompleted();
        return checksums;
    }
}