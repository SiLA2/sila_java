package sila_java.examples.test_server;

import com.google.common.base.Verify;
import io.grpc.ManagedChannel;
import lombok.NonNull;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import sila2.ch.unitelabs.test.observablecommandtest.v1.ObservableCommandTestOuterClass;
import sila2.ch.unitelabs.test.observablepropertytest.v1.ObservablePropertyTestOuterClass;
import sila2.ch.unitelabs.test.parameterconstraintsprovidertest.v1.ParameterConstraintsProviderTestOuterClass;
import sila2.ch.unitelabs.test.unobservablecommandtest.v1.UnobservableCommandTestOuterClass;
import sila2.org.silastandard.CloudClientEndpointGrpc;
import sila2.org.silastandard.SiLAFramework;
import sila2.org.silastandard.core.commands.parameterconstraintsprovider.v1.ParameterConstraintsProviderGrpc;
import sila2.org.silastandard.core.commands.parameterconstraintsprovider.v1.ParameterConstraintsProviderOuterClass;
import sila_java.examples.test_server.impl.*;
import sila_java.examples.test_server.sila_base.impl.*;
import sila_java.library.cloudier.server.CloudierConnectionConfigurationService;
import sila_java.library.cloudier.server.CloudierServerEndpoint;
import sila_java.library.cloudier.server.CloudierSiLAService;
import sila_java.library.cloudier.server.MessageCaseHandler;
import sila_java.library.core.sila.clients.ChannelFactory;
import sila_java.library.core.utils.Utils;
import sila_java.library.server_base.SiLAServer;
import sila_java.library.server_base.binary_transfer.database.BinaryDatabase;
import sila_java.library.server_base.binary_transfer.database.impl.H2BinaryDatabase;
import sila_java.library.server_base.config.IServerConfigWrapper;
import sila_java.library.server_base.identification.ServerInformation;
import sila_java.library.server_base.standard_features.v1.AuthorizationController;
import sila_java.library.server_base.utils.ArgumentHelper;
import sila_java.library.sila_base.EmptyClass;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import static sila_java.library.core.utils.FileUtils.getFileContent;
import static sila_java.library.core.utils.FileUtils.getResourceContent;

/**
 * SiLA Server for test purposes
 */
@Slf4j
public class TestServer implements AutoCloseable {
    static final String SERVER_TYPE = "TestServer";
    public static final ServerInformation SERVER_INFORMATION = new ServerInformation(
            SERVER_TYPE,
            "Server for test purposes",
            "https://www.sila-standard.org",
            "0.0"
    );
    private final ObservableCommand observableCommandService = new ObservableCommand();
    private final ObservableProperty observablePropertyService = new ObservableProperty();
    private final ParameterConstraintsProvider parameterConstraintsProviderService;
    private final sila_java.library.server_base.standard_features.v1.ParameterConstraintsProvider parameterConstraintsProvider;
    private final CloudierConnectionConfigurationService connectionConfigurationService =
            new CloudierConnectionConfigurationService(
                    false, this::onServerConnectionModeChange
            );
    private final SiLAServer.Builder builder;
    private final BinaryDatabase binaryDatabase;
    private final IServerConfigWrapper configuration;
    private final ComplexDatatypeTest complexDataTypeTest;
    private final BasicDatatypeTest basicDatatypeTest;
    private final ListDatatypeTest listDatatypeTest;
    private final StructureDatatypeTest structureDatatypeTest;
    private final ObservableCommandTest observableCommandTestService;
    private final ObservablePropertyTest observablePropertyTestService;
    private final UnobservablePropertyTest unobservablePropertyTestService;
    private final UnobservableCommand unobservableCommand;
    private SiLAServer clientInitiatedServer;
    // server initiated variables
    private CloudierSiLAService cloudierSiLAService;
    private CloudClientEndpointGrpc.CloudClientEndpointStub clientEndpoint;
    private CloudierServerEndpoint cloudServerEndpointService;
    private ManagedChannel channel;
    private Thread serverInitiatedServerThread;

    private HashMap<String, MessageCaseHandler> getCallForwarderMap() {
        return new HashMap<String, MessageCaseHandler>() {{
            put("ch.unitelabs/test/ObservableCommandTest/v1/Command/RestartDevice", new MessageCaseHandler()
                    .withObservableCommand(ObservableCommandTestOuterClass.RestartDevice_Parameters.parser(), observableCommandService::restartDevice)
                    .withExecInfo(SiLAFramework.CommandExecutionUUID.parser(), observableCommandService::restartDeviceInfo)
                    .withIntermediate(SiLAFramework.CommandExecutionUUID.parser(), observableCommandService::restartDeviceIntermediate)
                    .withResult(SiLAFramework.CommandExecutionUUID.parser(), observableCommandService::restartDeviceResult)
            );
            put("ch.unitelabs/test/ObservablePropertyTest/v1/Property/ListString", new MessageCaseHandler()
                    .withObservableProperty(ObservablePropertyTestOuterClass.Subscribe_ListString_Parameters.parser(), observablePropertyService::subscribeListString)
            );
            put("ch.unitelabs/test/ObservablePropertyTest/v1/Property/SingletonListString", new MessageCaseHandler()
                    .withObservableProperty(ObservablePropertyTestOuterClass.Subscribe_SingletonListString_Parameters.parser(), observablePropertyService::subscribeSingletonListString)
            );
            put("ch.unitelabs/test/ParameterConstraintsProviderTest/v1/Command/GoToPos", new MessageCaseHandler()
                    .withUnobservableCommand(ParameterConstraintsProviderTestOuterClass.GoToPos_Parameters.parser(), parameterConstraintsProviderService::goToPos)
            );
            put("ch.unitelabs/test/ParameterConstraintsProviderTest/v1/Command/AddPosition", new MessageCaseHandler()
                    .withUnobservableCommand(ParameterConstraintsProviderTestOuterClass.AddPosition_Parameters.parser(), parameterConstraintsProviderService::addPosition)
            );
            put("ch.unitelabs/test/ParameterConstraintsProviderTest/v1/Command/RemovePosition", new MessageCaseHandler()
                    .withUnobservableCommand(ParameterConstraintsProviderTestOuterClass.RemovePosition_Parameters.parser(), parameterConstraintsProviderService::removePosition)
            );
            put("ch.unitelabs/test/ParameterConstraintsProviderTest/v1/Property/Positions", new MessageCaseHandler()
                    .withUnobservableProperty(ParameterConstraintsProviderTestOuterClass.Get_Positions_Parameters.parser(), parameterConstraintsProviderService::getPositions)
            );
            put("ch.unitelabs/test/UnobservableCommandTest/v1/Command/MakeCoffee", new MessageCaseHandler()
                    .withUnobservableCommand(UnobservableCommandTestOuterClass.MakeCoffee_Parameters.parser(), unobservableCommand::makeCoffee)
            );
            put("ch.unitelabs/test/UnobservableCommandTest/v1/Command/Sleep", new MessageCaseHandler()
                    .withUnobservableCommand(UnobservableCommandTestOuterClass.Sleep_Parameters.parser(), unobservableCommand::sleep)
            );
            // todo add all sila_base.impl
            final ParameterConstraintsProviderGrpc.ParameterConstraintsProviderImplBase s = (ParameterConstraintsProviderGrpc.ParameterConstraintsProviderImplBase) parameterConstraintsProvider.getService();
            put("org.silastandard/core.commands/ParameterConstraintsProvider/v1/Property/ParametersConstraints", new MessageCaseHandler()
                    .withObservableProperty(ParameterConstraintsProviderOuterClass.Subscribe_ParametersConstraints_Parameters.parser(), s::subscribeParametersConstraints)
            );
        }};
    }

    @SneakyThrows
    private static String getCoreFeatureContent(@NonNull final String coreFeaturePath) {
        return getFileContent(Objects.requireNonNull(EmptyClass.class.getResourceAsStream(
                "/sila_base/feature_definitions/org/silastandard/core/" + coreFeaturePath
        )));
    }

    public TestServer(@NonNull final ArgumentHelper argumentHelper) {
        try {
            this.builder = SiLAServer.Builder.newBuilder(SERVER_INFORMATION);

            this.builder.withPersistentConfig(argumentHelper.getConfigFile().isPresent());

            argumentHelper.getConfigFile().ifPresent(this.builder::withPersistentConfigFile);

            this.builder.withPersistentTLS(
                    argumentHelper.getPrivateKeyFile(),
                    argumentHelper.getCertificateFile(),
                    argumentHelper.getCertificatePassword()
            );

            argumentHelper.getPort().ifPresent(this.builder::withPort);
            argumentHelper.getInterface().ifPresent(this.builder::withNetworkInterface);

            if (argumentHelper.useUnsafeCommunication()) {
                this.builder.withUnsafeCommunication(true);
            }

            this.builder.withMetadataExtractingInterceptor();
            this.builder.withBinaryTransferSupport(true);
            this.configuration = builder.getNewServerConfigurationWrapper();
            this.builder.withCustomConfigWrapperProvider((configPath, serverConfiguration) -> this.configuration);
            this.binaryDatabase = new H2BinaryDatabase(this.configuration.getCacheConfig().getUuid());
            this.builder.withCustomBinaryDatabaseProvider((uuid) -> this.binaryDatabase);
            this.complexDataTypeTest = new ComplexDatatypeTest(this.binaryDatabase);
            this.basicDatatypeTest = new BasicDatatypeTest();
            this.listDatatypeTest = new ListDatatypeTest();
            this.structureDatatypeTest = new StructureDatatypeTest();
            this.unobservablePropertyTestService = new UnobservablePropertyTest();
            this.observableCommandTestService = new ObservableCommandTest();
            this.observablePropertyTestService = new ObservablePropertyTest();
            this.unobservableCommand = new UnobservableCommand();

            final AuthorizationController.Authorize authorize = this.builder.getAuthorize() == null ? AuthorizationController.DEFAULT_AUTHORIZE : this.builder.getAuthorize();

            this.parameterConstraintsProvider = new sila_java.library.server_base.standard_features.v1.ParameterConstraintsProvider(authorize);
            this.parameterConstraintsProviderService = new ParameterConstraintsProvider(parameterConstraintsProvider);

            this.builder.addFeature(
                    getFileContent(EmptyClass.class.getResourceAsStream("/sila_base/feature_definitions/org/silastandard/test/BasicDataTypesTest-v1_0.sila.xml")),
                    this.basicDatatypeTest
            ).addFeature(
                    getFileContent(EmptyClass.class.getResourceAsStream("/sila_base/feature_definitions/org/silastandard/test/ListDataTypeTest-v1_0.sila.xml")),
                    this.listDatatypeTest
            ).addFeature(
                    getFileContent(EmptyClass.class.getResourceAsStream("/sila_base/feature_definitions/org/silastandard/test/StructureDataTypeTest-v1_0.sila.xml")),
                    this.structureDatatypeTest
            ).addFeature(
                    getFileContent(EmptyClass.class.getResourceAsStream("/sila_base/feature_definitions/org/silastandard/test/ObservableCommandTest-v1_0.sila.xml")),
                    this.observableCommandTestService
            ).addFeature(
                    getFileContent(EmptyClass.class.getResourceAsStream("/sila_base/feature_definitions/org/silastandard/test/ObservablePropertyTest-v1_0.sila.xml")),
                    this.observablePropertyTestService
            ).addFeature(
                    getFileContent(EmptyClass.class.getResourceAsStream("/sila_base/feature_definitions/org/silastandard/test/UnobservablePropertyTest-v1_0.sila.xml")),
                    this.unobservablePropertyTestService
            ).addFeature(
                    getResourceContent("features/ComplexDataTypeTest.sila.xml"),
                    this.complexDataTypeTest
            ).addFeature(
                    getResourceContent("features/ObservablePropertyTest.sila.xml"),
                    this.observablePropertyService
            ).addFeature(
                    getResourceContent("features/UnobservableCommandTest.sila.xml"),
                    this.unobservableCommand
            ).addFeature(
                    getResourceContent("features/ObservableCommandTest.sila.xml"),
                    this.observableCommandService
            ).addFeature(
                    getResourceContent("features/ParameterConstraintsProviderTest.sila.xml"),
                    this.parameterConstraintsProviderService
            ).addFeature(getCoreFeatureContent("ConnectionConfigurationService-v1_0.sila.xml"),
                    this.connectionConfigurationService
            ).addFeature(
                    this.parameterConstraintsProvider
            );

        } catch (IOException | SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void startClientInitiatedServer() throws IOException {
        log.info("Starting server in client initiated mode");
        this.clientInitiatedServer = this.builder.start();
    }

    private void onServerConnectionModeChange(boolean serverInitiatedConnectionEnabled) {
        log.info("Server connection mode switching to {}", (serverInitiatedConnectionEnabled) ? "Server Initiated" : "Client Initiated");
        // cleanup
        if (serverInitiatedConnectionEnabled) {
            try {
                this.clientInitiatedServer.close();
            } catch (Exception e) {
                log.warn("Exception occurred while closing client initiated server");
            } finally {
                this.clientInitiatedServer = null;
            }
        } else {
            try {
                this.channel.shutdown();
                try {
                    this.channel.awaitTermination(1, TimeUnit.SECONDS);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                    // Allow thread to exit.
                } finally {
                    this.channel.shutdownNow();
                }
                Verify.verify(this.channel.isShutdown());
                this.serverInitiatedServerThread.interrupt();
                int i = 0;
                while (this.serverInitiatedServerThread.isAlive() && i < 50) {
                    Thread.sleep(100);
                    ++i;
                }
                if (this.serverInitiatedServerThread.isAlive()) {
                    this.serverInitiatedServerThread.stop();
                }
            } catch (Exception e) {
                log.warn("Exception occurred while closing server initiated server!");
            } finally {
                this.channel = null;
                this.cloudServerEndpointService = null; // todo check that garbage collected once null
                this.clientEndpoint = null;
                this.cloudierSiLAService = null;
                this.serverInitiatedServerThread = null;
            }
        }
        // start (any exception occurring here should be fatal for the server execution
        if (serverInitiatedConnectionEnabled) {
            this.startServerInitiatedConnection(this.configuration.getCacheConfig().getUuid());
        } else {
            try {
                this.startClientInitiatedServer();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        log.info("Server connection mode switched to {}", (serverInitiatedConnectionEnabled) ? "Server Initiated" : "Client Initiated");
    }

    private void startServerInitiatedConnection(@NonNull final UUID serverUUID) {
        this.serverInitiatedServerThread = new Thread(() -> {
            this.cloudierSiLAService = new CloudierSiLAService(
                    this.configuration.getCacheConfig().getName(),
                    TestServer.SERVER_INFORMATION.getType(),
                    serverUUID.toString(),
                    TestServer.SERVER_INFORMATION.getVersion(),
                    TestServer.SERVER_INFORMATION.getDescription(),
                    TestServer.SERVER_INFORMATION.getVendorURL(),
                    this.builder.getFeatureDefinitions()
            );
            // todo iterate on clients and create a channel for each
            this.channel = ChannelFactory.getTLSEncryptedChannel("127.0.0.1", 50051);
            this.clientEndpoint = CloudClientEndpointGrpc.newStub(this.channel);
            this.cloudServerEndpointService = new CloudierServerEndpoint(
                    this.cloudierSiLAService,
                    this.connectionConfigurationService,
                    this.clientEndpoint,
                    this.getCallForwarderMap(),
                    null,
                    null
                    //this.clientInitiatedServer.getUploadService().orElse(null), // todo fix nullptr exception since clientInitiatedServer is null
                    //this.clientInitiatedServer.getDownloadService().orElse(null)
            );
        });
        this.serverInitiatedServerThread.start();
    }

    @SneakyThrows
    @Override
    public void close() {
        if (this.clientInitiatedServer != null) {
            this.clientInitiatedServer.close();
        }
        if (this.channel != null) {
            this.channel.shutdown().awaitTermination(5, TimeUnit.SECONDS);
        }
        this.observableCommandService.close();
        this.binaryDatabase.close();
    }

    @SneakyThrows
    public static void main(final String[] args) {
        final ArgumentHelper argumentHelper = new ArgumentHelper(args, SERVER_TYPE);
        final TestServer server = new TestServer(argumentHelper);
        server.startClientInitiatedServer();
        log.info("To stop the server press CTRL + C.");
        Utils.blockUntilShutdown(server::close);
        log.info("Server closed.");
    }
}
