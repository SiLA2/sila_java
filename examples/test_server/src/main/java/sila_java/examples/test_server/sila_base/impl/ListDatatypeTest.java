package sila_java.examples.test_server.sila_base.impl;

import com.google.common.collect.Lists;
import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.SiLAFramework;
import sila2.org.silastandard.test.listdatatypetest.v1.ListDataTypeTestGrpc;
import sila2.org.silastandard.test.listdatatypetest.v1.ListDataTypeTestOuterClass;
import sila_java.library.core.sila.types.*;

import java.util.Collections;
import java.util.List;

@Slf4j
public class ListDatatypeTest extends ListDataTypeTestGrpc.ListDataTypeTestImplBase {

    @Override
    public void echoStringList(ListDataTypeTestOuterClass.EchoStringList_Parameters request, StreamObserver<ListDataTypeTestOuterClass.EchoStringList_Responses> responseObserver) {
        responseObserver.onNext(ListDataTypeTestOuterClass.EchoStringList_Responses.newBuilder().addAllReceivedValues(request.getStringListList()).build());
        responseObserver.onCompleted();
    }

    @Override
    public void echoIntegerList(ListDataTypeTestOuterClass.EchoIntegerList_Parameters request, StreamObserver<ListDataTypeTestOuterClass.EchoIntegerList_Responses> responseObserver) {
        responseObserver.onNext(ListDataTypeTestOuterClass.EchoIntegerList_Responses.newBuilder().addAllReceivedValues(request.getIntegerListList()).build());
        responseObserver.onCompleted();
    }

    @Override
    public void getStringList(ListDataTypeTestOuterClass.Get_StringList_Parameters request, StreamObserver<ListDataTypeTestOuterClass.Get_StringList_Responses> responseObserver) {
        final List<SiLAFramework.String> list = Lists.newArrayList(SiLAString.from("SiLA 2"), SiLAString.from("is"), SiLAString.from("great"));
        responseObserver.onNext(ListDataTypeTestOuterClass.Get_StringList_Responses.newBuilder().addAllStringList(list).build());
        responseObserver.onCompleted();
    }

    @Override
    public void getIntegerList(ListDataTypeTestOuterClass.Get_IntegerList_Parameters request, StreamObserver<ListDataTypeTestOuterClass.Get_IntegerList_Responses> responseObserver) {
        final List<SiLAFramework.Integer> list = Lists.newArrayList(SiLAInteger.from(1), SiLAInteger.from(2), SiLAInteger.from(3));
        responseObserver.onNext(ListDataTypeTestOuterClass.Get_IntegerList_Responses
                .newBuilder().
                addAllIntegerList(
                        list
                )
                .build()
        );
        responseObserver.onCompleted();
    }

    @Override
    public void echoStructureList(ListDataTypeTestOuterClass.EchoStructureList_Parameters request, StreamObserver<ListDataTypeTestOuterClass.EchoStructureList_Responses> responseObserver) {
        responseObserver.onNext(
                ListDataTypeTestOuterClass.EchoStructureList_Responses
                        .newBuilder()
                        .addAllReceivedValues(request.getStructureListList())
                        .build()
        );
        responseObserver.onCompleted();
    }

    @Override
    public void getEmptyStringList(ListDataTypeTestOuterClass.Get_EmptyStringList_Parameters request, StreamObserver<ListDataTypeTestOuterClass.Get_EmptyStringList_Responses> responseObserver) {
        responseObserver.onNext(
                ListDataTypeTestOuterClass.Get_EmptyStringList_Responses
                        .newBuilder()
                        .addAllEmptyStringList(Collections.emptyList())
                        .build()
        );
        responseObserver.onCompleted();
    }

    @Override
    public void getStructureList(ListDataTypeTestOuterClass.Get_StructureList_Parameters request, StreamObserver<ListDataTypeTestOuterClass.Get_StructureList_Responses> responseObserver) {
        // todo implement
        super.getStructureList(request, responseObserver);
    }
}
