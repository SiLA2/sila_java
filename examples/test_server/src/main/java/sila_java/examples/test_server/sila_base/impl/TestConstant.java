package sila_java.examples.test_server.sila_base.impl;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import sila2.org.silastandard.SiLAFramework;
import sila_java.library.core.sila.types.*;

import java.nio.charset.StandardCharsets;
import java.time.OffsetDateTime;
import java.time.OffsetTime;
import java.time.ZoneOffset;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public abstract class TestConstant {
    public static final SiLAFramework.String string = SiLAString.from("SiLA2_Test_String_Value");
    public static final SiLAFramework.Integer integer = SiLAInteger.from(5124);
    public static final SiLAFramework.Real real = SiLAReal.from(3.1415926);
    public static final SiLAFramework.Boolean bool  = SiLABoolean.from(true);
    public static final SiLAFramework.Binary binary = SiLABinary.fromBytes("SiLA2_Binary_String_Value".getBytes(StandardCharsets.UTF_8));
    public static final SiLAFramework.Date date = SiLADate.from(OffsetDateTime.of(2022, 8, 5, 0, 0, 0, 0, ZoneOffset.UTC));
    public static final SiLAFramework.Time time = SiLATime.from(OffsetTime.of(12, 34, 56, 789, ZoneOffset.UTC));
    public static final SiLAFramework.Timestamp timestamp = SiLATimestamp.from(OffsetDateTime.of(2022, 8, 5, 12, 34, 56, 789, ZoneOffset.UTC));
    public static final SiLAFramework.Any any = SiLAAny.from("<DataType><Basic>String</Basic></DataType>", SiLAString.from("SiLA2_Any_Type_String_Value"));
}
