package sila_java.examples.test_server.sila_base.impl;

import io.grpc.StatusRuntimeException;
import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.SiLAFramework;
import sila2.org.silastandard.test.observablecommandtest.v1.ObservableCommandTestGrpc;
import sila2.org.silastandard.test.observablecommandtest.v1.ObservableCommandTestOuterClass;
import sila_java.library.core.sila.errors.SiLAErrors;
import sila_java.library.core.sila.types.SiLAInteger;
import sila_java.library.server_base.command.observable.ObservableCommandManager;
import sila_java.library.server_base.command.observable.ObservableCommandTaskRunner;
import sila_java.library.server_base.command.observable.ObservableCommandWrapper;
import sila_java.library.server_base.command.observable.RunnableCommandTask;

import java.time.Duration;
import java.util.UUID;

@Slf4j
public class ObservableCommandTest extends ObservableCommandTestGrpc.ObservableCommandTestImplBase implements AutoCloseable {
    private final ObservableCommandManager<
            ObservableCommandTestOuterClass.Count_Parameters,
            ObservableCommandTestOuterClass.Count_Responses
            > countCommandManager = new ObservableCommandManager<>(
            new ObservableCommandTaskRunner(1, 8),
            new RunnableCommandTask<ObservableCommandTestOuterClass.Count_Parameters, ObservableCommandTestOuterClass.Count_Responses>() {
                @Override
                public ObservableCommandTestOuterClass.Count_Responses run(ObservableCommandWrapper<ObservableCommandTestOuterClass.Count_Parameters, ObservableCommandTestOuterClass.Count_Responses> command) throws StatusRuntimeException {
                    final double delay = command.getParameter().getDelay().getValue();;
                    final long nbIterations = command.getParameter().getN().getValue();
                    try {
                        for (int i = 0; i < nbIterations; ++i) {
                            Thread.sleep((long) (1000 * delay));
                            command.setExecutionInfoAndNotify(
                                    i / (nbIterations - 1d),
                                    Duration.ofSeconds((long) (delay * (nbIterations - i - 1)))
                            );
                            command.notifyIntermediateResponse(ObservableCommandTestOuterClass.Count_IntermediateResponses
                                    .newBuilder()
                                    .setCurrentIteration(SiLAInteger.from(i))
                                    .build());
                        }
                    } catch (InterruptedException e) {
                        Thread.currentThread().interrupt();
                    }
                    return ObservableCommandTestOuterClass.Count_Responses
                            .newBuilder()
                            .setIterationResponse(SiLAInteger.from(nbIterations - 1))
                            .build();
                }

                @Override
                public void validate(ObservableCommandTestOuterClass.Count_Parameters parameters) throws StatusRuntimeException {
                    if (!parameters.hasDelay()) {
                        throw SiLAErrors.generateValidationError("org.silastandard/test/ObservableCommandTest/v1/Command/Count/Parameter/Delay", "Missing parameter.");
                    }
                    if (!parameters.hasN()) {
                        throw SiLAErrors.generateValidationError("org.silastandard/test/ObservableCommandTest/v1/Command/Count/Parameter/N", "Missing parameter.");
                    }
                }
            },
            Duration.ofSeconds(60)
    );

    private final ObservableCommandManager<
            ObservableCommandTestOuterClass.EchoValueAfterDelay_Parameters,
            ObservableCommandTestOuterClass.EchoValueAfterDelay_Responses
            > echoValueCommandManager = new ObservableCommandManager<>(
            new ObservableCommandTaskRunner(1, 8),
            new RunnableCommandTask<ObservableCommandTestOuterClass.EchoValueAfterDelay_Parameters, ObservableCommandTestOuterClass.EchoValueAfterDelay_Responses>() {
                @Override
                public ObservableCommandTestOuterClass.EchoValueAfterDelay_Responses run(ObservableCommandWrapper<ObservableCommandTestOuterClass.EchoValueAfterDelay_Parameters, ObservableCommandTestOuterClass.EchoValueAfterDelay_Responses> command) throws StatusRuntimeException {
                    return ObservableCommandTestOuterClass.EchoValueAfterDelay_Responses
                            .newBuilder()
                            .setReceivedValue(command.getParameter().getValue())
                            .build();
                }

                @Override
                public void preRun(ObservableCommandTestOuterClass.EchoValueAfterDelay_Parameters parameters, UUID commandExecutionUUID) {
                    final double delay = parameters.getDelay().getValue();
                    try {
                        Thread.sleep((long) (delay));
                    } catch (InterruptedException e) {
                        Thread.currentThread().interrupt();
                    }
                }

                @Override
                public void validate(ObservableCommandTestOuterClass.EchoValueAfterDelay_Parameters parameters) throws StatusRuntimeException {
                    if (!parameters.hasDelay()) {
                        throw SiLAErrors.generateValidationError("org.silastandard/test/ObservableCommandTest/v1/Command/EchoValueAfterDelay/Parameter/Delay", "Missing parameter.");
                    }
                    if (!parameters.hasValue()) {
                        throw SiLAErrors.generateValidationError("org.silastandard/test/ObservableCommandTest/v1/Command/EchoValueAfterDelay/Parameter/Value", "Missing parameter.");
                    }
                }
            },
            Duration.ofSeconds(60)
    );

    @Override
    public void count(ObservableCommandTestOuterClass.Count_Parameters request, StreamObserver<SiLAFramework.CommandConfirmation> responseObserver) {
        this.countCommandManager.addCommand(request, responseObserver);
    }

    @Override
    public void countInfo(SiLAFramework.CommandExecutionUUID request, StreamObserver<SiLAFramework.ExecutionInfo> responseObserver) {
        this.countCommandManager.get(request).addStateObserver(responseObserver);
    }

    @Override
    public void countIntermediate(SiLAFramework.CommandExecutionUUID request, StreamObserver<ObservableCommandTestOuterClass.Count_IntermediateResponses> responseObserver) {
        this.countCommandManager.get(request).addIntermediateResponseObserver(responseObserver);
    }

    @Override
    public void countResult(SiLAFramework.CommandExecutionUUID request, StreamObserver<ObservableCommandTestOuterClass.Count_Responses> responseObserver) {
        this.countCommandManager.get(request).sendResult(responseObserver);
    }

    @Override
    public void echoValueAfterDelay(ObservableCommandTestOuterClass.EchoValueAfterDelay_Parameters request, StreamObserver<SiLAFramework.CommandConfirmation> responseObserver) {
        this.echoValueCommandManager.addCommand(request, responseObserver);
    }

    @Override
    public void echoValueAfterDelayInfo(SiLAFramework.CommandExecutionUUID request, StreamObserver<SiLAFramework.ExecutionInfo> responseObserver) {
        this.echoValueCommandManager.get(request).addStateObserver(responseObserver);
    }

    @Override
    public void echoValueAfterDelayResult(SiLAFramework.CommandExecutionUUID request, StreamObserver<ObservableCommandTestOuterClass.EchoValueAfterDelay_Responses> responseObserver) {
        this.echoValueCommandManager.get(request).sendResult(responseObserver);
    }

    @Override
    public void close() {
        this.echoValueCommandManager.close();
        this.countCommandManager.close();
    }
}
