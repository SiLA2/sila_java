package sila_java.examples.test_server.impl;

import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;
import sila2.ch.unitelabs.test.unobservablecommandtest.v1.UnobservableCommandTestGrpc;
import sila2.ch.unitelabs.test.unobservablecommandtest.v1.UnobservableCommandTestOuterClass;
import sila_java.library.core.sila.errors.SiLAErrors;
import sila_java.library.core.sila.types.SiLAString;
import sila_java.library.server_base.metadata.ServerMetadataContainer;

import static sila_java.library.server_base.metadata.MetadataExtractingInterceptor.SILA_METADATA_KEY;

@Slf4j
public class UnobservableCommand extends UnobservableCommandTestGrpc.UnobservableCommandTestImplBase {
    private static final String FULLY_QUALIFIED_FEATURE_IDENTIFIER = "ch.unitelabs/test/UnobservableCommand/v1";

    public static final UnobservableCommandTestOuterClass.MakeCoffee_Responses MAKE_COFFEE_RESPONSE = UnobservableCommandTestOuterClass.MakeCoffee_Responses
            .newBuilder()
            .setResult(SiLAString.from("Your coffee is ready!"))
            .build();

    @Override
    public void makeCoffee(UnobservableCommandTestOuterClass.MakeCoffee_Parameters request, StreamObserver<UnobservableCommandTestOuterClass.MakeCoffee_Responses> responseObserver) {
        final ServerMetadataContainer serverMetadataContainer = SILA_METADATA_KEY.get();
        final UnobservableCommandTestOuterClass.Metadata_User metadataUser = serverMetadataContainer.get(UnobservableCommandTestOuterClass.Metadata_User.class);
        if (metadataUser != null && metadataUser.hasUser()) {
            log.info("received metadata user: " + metadataUser.getUser().getIdentifier());
        } else {
            log.info("User metadata was not provided");
        }
        if (request.getSugar().getValue()) {
            responseObserver.onError(SiLAErrors.generateDefinedExecutionError(
                    FULLY_QUALIFIED_FEATURE_IDENTIFIER + "/DefinedExecutionError/" + "SugarException",
                    "Sorry, there is no more sugar! Buy some sugar or take your coffee without sugar"
            ));
            return;
        }
        responseObserver.onNext(UnobservableCommand.MAKE_COFFEE_RESPONSE);
        responseObserver.onCompleted();
    }


    @Override
    public void sleep(
            final UnobservableCommandTestOuterClass.Sleep_Parameters request,
            final StreamObserver<UnobservableCommandTestOuterClass.Sleep_Responses> responseObserver
    ) {
        try {
            Thread.sleep(request.getSecondsToSleep().getValue() * 1000);
        } catch (InterruptedException e) {
            log.warn("sleep interrupted", e);
            Thread.currentThread().interrupt();
        }
        responseObserver.onNext(UnobservableCommandTestOuterClass.Sleep_Responses.newBuilder().build());
        responseObserver.onCompleted();
    }


    @Override
    public void getFCPAffectedByMetadataUser(
            UnobservableCommandTestOuterClass.Get_FCPAffectedByMetadata_User_Parameters request,
            StreamObserver<UnobservableCommandTestOuterClass.Get_FCPAffectedByMetadata_User_Responses> responseObserver
    ) {
        responseObserver.onNext(
                UnobservableCommandTestOuterClass.Get_FCPAffectedByMetadata_User_Responses
                        .newBuilder()
                        .addAffectedCalls(SiLAString.from("ch.unitelabs/test/UnobservableCommandTest/v1/Command/MakeCoffee"))
                        .build()
        );
        responseObserver.onCompleted();
    }
}
