package sila_java.examples.test_server.sila_base.impl;

import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.test.observablepropertytest.v1.ObservablePropertyTestGrpc;
import sila2.org.silastandard.test.observablepropertytest.v1.ObservablePropertyTestOuterClass;
import sila_java.library.core.sila.errors.SiLAErrors;
import sila_java.library.core.sila.types.SiLABoolean;
import sila_java.library.core.sila.types.SiLAInteger;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

@Slf4j
public class ObservablePropertyTest extends ObservablePropertyTestGrpc.ObservablePropertyTestImplBase implements AutoCloseable {
    private static final int FIXED_VALUE = 42;
    private final Set<Runnable> editableListeners = new HashSet<>();
    private final Set<Runnable> alternatingListeners = new HashSet<>();
    private final Set<StreamObserver<ObservablePropertyTestOuterClass.Subscribe_FixedValue_Responses>> fixedListeners = new HashSet<>();
    private final ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);

    private final AtomicLong editableValue = new AtomicLong(0);
    private final AtomicBoolean alternatingValue = new AtomicBoolean(false);

    public ObservablePropertyTest() {
        this.executor.scheduleAtFixedRate(() -> {
            ObservablePropertyTest.this.alternatingValue.set(!ObservablePropertyTest.this.alternatingValue.get());
            this.notifyAlternatingListeners();
        }, 0, 1, TimeUnit.SECONDS);
    }

    private void notifyEditableListeners() {
        final Set<Runnable> toRemove = new HashSet<>();
        this.editableListeners.forEach(listener -> {
            try {
                listener.run();
            } catch (Exception e) {
                log.debug("Remove editable observable property listener because of exception", e);
                toRemove.add(listener);
            }
        });
        this.editableListeners.removeAll(toRemove);
        log.debug("Editable observable property listeners count {}", this.editableListeners.size());
    }

    private void notifyAlternatingListeners() {
        final Set<Runnable> toRemove = new HashSet<>();
        this.alternatingListeners.forEach(listener -> {
            try {
                listener.run();
            } catch (Exception e) {
                log.debug("Remove alternating observable property listener because of exception", e);
                toRemove.add(listener);
            }
        });
        this.alternatingListeners.removeAll(toRemove);
        log.debug("Alternating observable property listeners count {}", this.alternatingListeners.size());
    }

    @Override
    public void setValue(
            ObservablePropertyTestOuterClass.SetValue_Parameters request,
            StreamObserver<ObservablePropertyTestOuterClass.SetValue_Responses> responseObserver
    ) {
        if (!request.hasValue()) {
            throw SiLAErrors.generateValidationError("org.silastandard/test/ObservablePropertyTest/v1/Command/SetValue/Parameter/Value", "Missing parameter.");
        }
        this.editableValue.set(request.getValue().getValue());
        responseObserver.onNext(ObservablePropertyTestOuterClass.SetValue_Responses.newBuilder().build());
        notifyEditableListeners();
        responseObserver.onCompleted();
    }

    @Override
    public void subscribeFixedValue(
            ObservablePropertyTestOuterClass.Subscribe_FixedValue_Parameters request,
            StreamObserver<ObservablePropertyTestOuterClass.Subscribe_FixedValue_Responses> responseObserver
    ) {
        this.fixedListeners.add(responseObserver);
        responseObserver.onNext(
                ObservablePropertyTestOuterClass.Subscribe_FixedValue_Responses
                        .newBuilder()
                        .setFixedValue(SiLAInteger.from(FIXED_VALUE))
                        .build()
        );
    }

    @Override
    public void subscribeAlternating(
            ObservablePropertyTestOuterClass.Subscribe_Alternating_Parameters request,
            StreamObserver<ObservablePropertyTestOuterClass.Subscribe_Alternating_Responses> responseObserver
    ) {
        final Runnable callback = () -> {
            try {
                responseObserver.onNext(ObservablePropertyTestOuterClass.Subscribe_Alternating_Responses.newBuilder()
                        .setAlternating(SiLABoolean.from(this.alternatingValue.get()))
                        .build()
                );
            } catch (Exception e) {
                responseObserver.onCompleted();
                throw e;
            }
        };
        callback.run(); // send initial state
        this.alternatingListeners.add(callback);
    }

    @Override
    public void subscribeEditable(
            ObservablePropertyTestOuterClass.Subscribe_Editable_Parameters request,
            StreamObserver<ObservablePropertyTestOuterClass.Subscribe_Editable_Responses> responseObserver
    ) {
        final Runnable callback = () -> {
            try {
                responseObserver.onNext(ObservablePropertyTestOuterClass.Subscribe_Editable_Responses.newBuilder()
                        .setEditable(SiLAInteger.from(this.editableValue.get()))
                        .build()
                );
            } catch (Exception e) {
                responseObserver.onCompleted();
                throw e;
            }
        };
        callback.run(); // send initial state
        this.editableListeners.add(callback);
    }

    @Override
    public void close() {
        this.executor.shutdownNow();
        this.alternatingListeners.clear();
        this.editableListeners.clear();
        this.fixedListeners.clear();
    }
}
