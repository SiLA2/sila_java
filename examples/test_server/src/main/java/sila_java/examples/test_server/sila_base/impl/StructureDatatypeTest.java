package sila_java.examples.test_server.sila_base.impl;

import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.test.structuredatatypetest.v1.StructureDataTypeTestGrpc;
import sila2.org.silastandard.test.structuredatatypetest.v1.StructureDataTypeTestOuterClass;
import sila_java.library.core.sila.types.*;

@Slf4j
public class StructureDatatypeTest extends StructureDataTypeTestGrpc.StructureDataTypeTestImplBase {


    @Override
    public void echoStructureValue(StructureDataTypeTestOuterClass.EchoStructureValue_Parameters request, StreamObserver<StructureDataTypeTestOuterClass.EchoStructureValue_Responses> responseObserver) {
        responseObserver.onNext(
                StructureDataTypeTestOuterClass.EchoStructureValue_Responses
                        .newBuilder()
                        .setReceivedValues(request.getStructureValue())
                        .build()
        );
        responseObserver.onCompleted();
    }

    @Override
    public void echoDeepStructureValue(StructureDataTypeTestOuterClass.EchoDeepStructureValue_Parameters request, StreamObserver<StructureDataTypeTestOuterClass.EchoDeepStructureValue_Responses> responseObserver) {
        responseObserver.onNext(
                StructureDataTypeTestOuterClass.EchoDeepStructureValue_Responses
                        .newBuilder()
                        .setReceivedValues(request.getDeepStructureValue())
                        .build()
        );
        responseObserver.onCompleted();
    }

    @Override
    public void getStructureValue(StructureDataTypeTestOuterClass.Get_StructureValue_Parameters request, StreamObserver<StructureDataTypeTestOuterClass.Get_StructureValue_Responses> responseObserver) {
        responseObserver.onNext(
                StructureDataTypeTestOuterClass.Get_StructureValue_Responses
                        .newBuilder()
                        .setStructureValue(
                                StructureDataTypeTestOuterClass.DataType_TestStructure
                                        .newBuilder()
                                        .setTestStructure(
                                                StructureDataTypeTestOuterClass.DataType_TestStructure.TestStructure_Struct
                                                        .newBuilder()
                                                        .setBinaryTypeValue(TestConstant.binary)
                                                        .setBooleanTypeValue(TestConstant.bool)
                                                        .setDateTypeValue(TestConstant.date)
                                                        .setIntegerTypeValue(TestConstant.integer)
                                                        .setRealTypeValue(TestConstant.real)
                                                        .setTimestampTypeValue(TestConstant.timestamp)
                                                        .setTimeTypeValue(TestConstant.time)
                                                        .setStringTypeValue(TestConstant.string)
                                                        .setAnyTypeValue(TestConstant.any)
                                                        .build()
                                        )
                                        .build()
                        )
                        .build()
        );
        responseObserver.onCompleted();
    }

    @Override
    public void getDeepStructureValue(StructureDataTypeTestOuterClass.Get_DeepStructureValue_Parameters request, StreamObserver<StructureDataTypeTestOuterClass.Get_DeepStructureValue_Responses> responseObserver) {
        responseObserver.onNext(
                StructureDataTypeTestOuterClass.Get_DeepStructureValue_Responses
                        .newBuilder()
                        .setDeepStructureValue(
                                StructureDataTypeTestOuterClass.DataType_DeepStructure
                                        .newBuilder()
                                        .setDeepStructure(
                                                StructureDataTypeTestOuterClass.DataType_DeepStructure.DeepStructure_Struct
                                                        .newBuilder()
                                                        .setOuterStringTypeValue(SiLAString.from("Outer_Test_String"))
                                                        .setOuterIntegerTypeValue(SiLAInteger.from(1111))
                                                        .setMiddleStructure(StructureDataTypeTestOuterClass.DataType_DeepStructure.DeepStructure_Struct.MiddleStructure_Struct
                                                                .newBuilder()
                                                                .setMiddleStringTypeValue(SiLAString.from("Middle_Test_String"))
                                                                .setMiddleIntegerTypeValue(SiLAInteger.from(2222))
                                                                .setInnerStructure(
                                                                        StructureDataTypeTestOuterClass.DataType_DeepStructure.DeepStructure_Struct.MiddleStructure_Struct.InnerStructure_Struct
                                                                                .newBuilder()
                                                                                .setInnerStringTypeValue(SiLAString.from("Inner_Test_String"))
                                                                                .setInnerIntegerTypeValue(SiLAInteger.from(3333))
                                                                                .build()
                                                                )
                                                                .build()
                                                        )
                                                        .build()
                                        )
                                        .build()
                        )
                        .build()
        );
        responseObserver.onCompleted();
    }
}
