package sila_java.examples.test_server.sila_base.impl;

import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.test.basicdatatypestest.v1.BasicDataTypesTestGrpc;
import sila2.org.silastandard.test.basicdatatypestest.v1.BasicDataTypesTestOuterClass;

@Slf4j
public class BasicDatatypeTest extends BasicDataTypesTestGrpc.BasicDataTypesTestImplBase {

    
    @Override
    public void echoStringValue(BasicDataTypesTestOuterClass.EchoStringValue_Parameters request, StreamObserver<BasicDataTypesTestOuterClass.EchoStringValue_Responses> responseObserver) {
        responseObserver.onNext(BasicDataTypesTestOuterClass.EchoStringValue_Responses.newBuilder().setReceivedValue(request.getStringValue()).build());
        responseObserver.onCompleted();
    }

    @Override
    public void echoIntegerValue(BasicDataTypesTestOuterClass.EchoIntegerValue_Parameters request, StreamObserver<BasicDataTypesTestOuterClass.EchoIntegerValue_Responses> responseObserver) {
        responseObserver.onNext(BasicDataTypesTestOuterClass.EchoIntegerValue_Responses.newBuilder().setReceivedValue(request.getIntegerValue()).build());
        responseObserver.onCompleted();
    }

    @Override
    public void echoRealValue(BasicDataTypesTestOuterClass.EchoRealValue_Parameters request, StreamObserver<BasicDataTypesTestOuterClass.EchoRealValue_Responses> responseObserver) {
        responseObserver.onNext(BasicDataTypesTestOuterClass.EchoRealValue_Responses.newBuilder().setReceivedValue(request.getRealValue()).build());
        responseObserver.onCompleted();
    }

    @Override
    public void echoBooleanValue(BasicDataTypesTestOuterClass.EchoBooleanValue_Parameters request, StreamObserver<BasicDataTypesTestOuterClass.EchoBooleanValue_Responses> responseObserver) {
        responseObserver.onNext(BasicDataTypesTestOuterClass.EchoBooleanValue_Responses.newBuilder().setReceivedValue(request.getBooleanValue()).build());
        responseObserver.onCompleted();
    }

    @Override
    public void echoDateValue(BasicDataTypesTestOuterClass.EchoDateValue_Parameters request, StreamObserver<BasicDataTypesTestOuterClass.EchoDateValue_Responses> responseObserver) {
        responseObserver.onNext(BasicDataTypesTestOuterClass.EchoDateValue_Responses.newBuilder().setReceivedValue(request.getDateValue()).build());
        responseObserver.onCompleted();
    }

    @Override
    public void echoTimeValue(BasicDataTypesTestOuterClass.EchoTimeValue_Parameters request, StreamObserver<BasicDataTypesTestOuterClass.EchoTimeValue_Responses> responseObserver) {
        responseObserver.onNext(BasicDataTypesTestOuterClass.EchoTimeValue_Responses.newBuilder().setReceivedValue(request.getTimeValue()).build());
        responseObserver.onCompleted();
    }

    @Override
    public void echoTimestampValue(BasicDataTypesTestOuterClass.EchoTimestampValue_Parameters request, StreamObserver<BasicDataTypesTestOuterClass.EchoTimestampValue_Responses> responseObserver) {
        responseObserver.onNext(BasicDataTypesTestOuterClass.EchoTimestampValue_Responses.newBuilder().setReceivedValue(request.getTimestampValue()).build());
        responseObserver.onCompleted();
    }

    @Override
    public void getStringValue(BasicDataTypesTestOuterClass.Get_StringValue_Parameters request, StreamObserver<BasicDataTypesTestOuterClass.Get_StringValue_Responses> responseObserver) {
        responseObserver.onNext(BasicDataTypesTestOuterClass.Get_StringValue_Responses.newBuilder().setStringValue(TestConstant.string).build());
        responseObserver.onCompleted();
    }

    @Override
    public void getIntegerValue(BasicDataTypesTestOuterClass.Get_IntegerValue_Parameters request, StreamObserver<BasicDataTypesTestOuterClass.Get_IntegerValue_Responses> responseObserver) {
        responseObserver.onNext(BasicDataTypesTestOuterClass.Get_IntegerValue_Responses.newBuilder().setIntegerValue(TestConstant.integer).build());
        responseObserver.onCompleted();
    }

    @Override
    public void getRealValue(BasicDataTypesTestOuterClass.Get_RealValue_Parameters request, StreamObserver<BasicDataTypesTestOuterClass.Get_RealValue_Responses> responseObserver) {
        responseObserver.onNext(BasicDataTypesTestOuterClass.Get_RealValue_Responses.newBuilder().setRealValue(TestConstant.real).build());
        responseObserver.onCompleted();
    }

    @Override
    public void getBooleanValue(BasicDataTypesTestOuterClass.Get_BooleanValue_Parameters request, StreamObserver<BasicDataTypesTestOuterClass.Get_BooleanValue_Responses> responseObserver) {
        responseObserver.onNext(BasicDataTypesTestOuterClass.Get_BooleanValue_Responses.newBuilder().setBooleanValue(TestConstant.bool).build());
        responseObserver.onCompleted();
    }

    @Override
    public void getDateValue(BasicDataTypesTestOuterClass.Get_DateValue_Parameters request, StreamObserver<BasicDataTypesTestOuterClass.Get_DateValue_Responses> responseObserver) {
        responseObserver.onNext(BasicDataTypesTestOuterClass.Get_DateValue_Responses.newBuilder().setDateValue(TestConstant.date).build());
        responseObserver.onCompleted();
    }

    @Override
    public void getTimeValue(BasicDataTypesTestOuterClass.Get_TimeValue_Parameters request, StreamObserver<BasicDataTypesTestOuterClass.Get_TimeValue_Responses> responseObserver) {
        responseObserver.onNext(BasicDataTypesTestOuterClass.Get_TimeValue_Responses.newBuilder().setTimeValue(TestConstant.time).build());
        responseObserver.onCompleted();
    }

    @Override
    public void getTimestampValue(BasicDataTypesTestOuterClass.Get_TimestampValue_Parameters request, StreamObserver<BasicDataTypesTestOuterClass.Get_TimestampValue_Responses> responseObserver) {
        responseObserver.onNext(BasicDataTypesTestOuterClass.Get_TimestampValue_Responses.newBuilder().setTimestampValue(TestConstant.timestamp).build());
        responseObserver.onCompleted();
    }
}
