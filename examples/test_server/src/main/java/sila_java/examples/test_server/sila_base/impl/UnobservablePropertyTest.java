package sila_java.examples.test_server.sila_base.impl;


import io.grpc.stub.StreamObserver;
import sila2.org.silastandard.test.unobservablepropertytest.v1.UnobservablePropertyTestGrpc;
import sila2.org.silastandard.test.unobservablepropertytest.v1.UnobservablePropertyTestOuterClass;
import sila_java.library.core.sila.types.SiLAInteger;

import java.time.Instant;

public class UnobservablePropertyTest extends UnobservablePropertyTestGrpc.UnobservablePropertyTestImplBase {
    @Override
    public void getAnswerToEverything(
            UnobservablePropertyTestOuterClass.Get_AnswerToEverything_Parameters request,
            StreamObserver<UnobservablePropertyTestOuterClass.Get_AnswerToEverything_Responses> responseObserver
    ) {
        responseObserver.onNext(
                UnobservablePropertyTestOuterClass.Get_AnswerToEverything_Responses
                        .newBuilder()
                        .setAnswerToEverything(SiLAInteger.from(42))
                        .build()
        );
        responseObserver.onCompleted();
    }

    @Override
    public void getSecondsSince1970(
            UnobservablePropertyTestOuterClass.Get_SecondsSince1970_Parameters request,
            StreamObserver<UnobservablePropertyTestOuterClass.Get_SecondsSince1970_Responses> responseObserver
    ) {
        responseObserver.onNext(
                UnobservablePropertyTestOuterClass.Get_SecondsSince1970_Responses
                        .newBuilder()
                        .setSecondsSince1970(SiLAInteger.from(Instant.now().getEpochSecond()))
                        .build()
        );
        responseObserver.onCompleted();
    }

}
