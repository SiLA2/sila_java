package sila_java.examples.test_server.impl;

import io.grpc.stub.StreamObserver;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import sila2.ch.unitelabs.test.parameterconstraintsprovidertest.v1.ParameterConstraintsProviderTestGrpc;
import sila2.ch.unitelabs.test.parameterconstraintsprovidertest.v1.ParameterConstraintsProviderTestOuterClass;
import sila2.org.silastandard.SiLAFramework;
import sila_java.library.core.models.Constraints;
import sila_java.library.core.sila.errors.SiLAErrors;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
public class ParameterConstraintsProvider extends ParameterConstraintsProviderTestGrpc.ParameterConstraintsProviderTestImplBase {
    private static final String FULLY_QUALIFIED_POSITION_PARAMETER =
            "ch.unitelabs/test/ParameterConstraintsProviderTest/v1/Command/GoToPos/Parameter/PositionIdentifier";
    private final Map<String, Position> positions = new HashMap<>();
    private final sila_java.library.server_base.standard_features.v1.ParameterConstraintsProvider parameterConstraintProvider;

    @Getter
    @AllArgsConstructor
    private static class Position {
        private int x;
        private int y;
    }

    public ParameterConstraintsProvider(@NonNull final sila_java.library.server_base.standard_features.v1.ParameterConstraintsProvider parameterConstraintProvider) {
        this.parameterConstraintProvider = parameterConstraintProvider;
        loadPositions();
    }

    @Override
    public void goToPos(
            final ParameterConstraintsProviderTestOuterClass.GoToPos_Parameters request,
            final StreamObserver<ParameterConstraintsProviderTestOuterClass.GoToPos_Responses> responseObserver
    ) {
        final Constraints constraints = parameterConstraintProvider
                .getParametersConstraints()
                .get(FULLY_QUALIFIED_POSITION_PARAMETER);
        final List<String> allowedPos = constraints.getSet().getValue();
        final boolean match = allowedPos.stream().anyMatch(s -> s.equals(request.getPositionIdentifier().getValue()));
        if (!match) {
            throw SiLAErrors.generateUndefinedExecutionError("Incorrect position");
        }

        responseObserver.onNext(ParameterConstraintsProviderTestOuterClass.GoToPos_Responses.newBuilder().build());
        responseObserver.onCompleted();
    }

    @Override
    public void addPosition(
            final ParameterConstraintsProviderTestOuterClass.AddPosition_Parameters request,
            final StreamObserver<ParameterConstraintsProviderTestOuterClass.AddPosition_Responses> responseObserver
    ) {
        val position = request.getPosition().getPosition();
        if (this.positions.containsKey(position.getIdentifier().getValue())) {
            throw SiLAErrors.generateUndefinedExecutionError("Position already exist");
        }
        this.positions.put(
                position.getIdentifier().getValue(),
                new Position((int)position.getX().getValue(), (int)position.getY().getValue())
        );
        parameterConstraintProvider.putConstraint(FULLY_QUALIFIED_POSITION_PARAMETER, getConstraints());
        responseObserver.onNext(ParameterConstraintsProviderTestOuterClass.AddPosition_Responses.newBuilder().build());
        responseObserver.onCompleted();
    }

    @Override
    public void removePosition(
            final ParameterConstraintsProviderTestOuterClass.RemovePosition_Parameters request,
            final StreamObserver<ParameterConstraintsProviderTestOuterClass.RemovePosition_Responses> responseObserver
    ) {
        this.positions.remove(request.getIdentifier().getValue());
        parameterConstraintProvider.putConstraint(FULLY_QUALIFIED_POSITION_PARAMETER, getConstraints());
        responseObserver.onNext(ParameterConstraintsProviderTestOuterClass.RemovePosition_Responses.newBuilder().build());
        responseObserver.onCompleted();
    }

    @Override
    public void getPositions(
            final ParameterConstraintsProviderTestOuterClass.Get_Positions_Parameters request,
            final StreamObserver<ParameterConstraintsProviderTestOuterClass.Get_Positions_Responses> responseObserver
    ) {
        final Set<ParameterConstraintsProviderTestOuterClass.DataType_Position> positionSet = this.positions
                .entrySet()
                .stream()
                .map((e) -> ParameterConstraintsProviderTestOuterClass.DataType_Position.Position_Struct
                        .newBuilder()
                        .setIdentifier(SiLAFramework.String.newBuilder().setValue(e.getKey()).build())
                        .setX(SiLAFramework.Integer.newBuilder().setValue(e.getValue().getX()).build())
                        .setY(SiLAFramework.Integer.newBuilder().setValue(e.getValue().getY()).build())
                        .build())
                .map(p -> ParameterConstraintsProviderTestOuterClass.DataType_Position.newBuilder().setPosition(p).build())
                .collect(Collectors.toSet());
        responseObserver.onNext(
                ParameterConstraintsProviderTestOuterClass.Get_Positions_Responses
                        .newBuilder()
                        .addAllPositions(positionSet)
                        .build()
        );
        responseObserver.onCompleted();
    }

    private Constraints getConstraints() {
        final Constraints constraints = new Constraints();
        constraints.setSet(new Constraints.Set());
        constraints.getSet().getValue().addAll(this.positions.keySet());
        return constraints;
    }

    private void loadPositions() {
        log.info("Loading positions...");
        this.positions.put("pos1", new Position(42, 1337));
        this.positions.put("pos2", new Position(101, 7));
        parameterConstraintProvider.putConstraint(FULLY_QUALIFIED_POSITION_PARAMETER, getConstraints());
        log.info("{} positions loaded!", this.positions.size());
    }
}
