package sila_java.examples.test_server.impl;

import com.google.common.collect.Lists;
import com.google.protobuf.Descriptors;
import com.google.protobuf.DynamicMessage;
import io.grpc.stub.StreamObserver;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import sila2.org.silastandard.SiLAFramework;
import sila2.org.silastandard.test.complexdatatypetest.v1.ComplexDataTypeTestGrpc;
import sila2.org.silastandard.test.complexdatatypetest.v1.ComplexDataTypeTestOuterClass;
import sila_java.examples.test_server.utils.ByteArrayProcessedInputStream;
import sila_java.examples.test_server.utils.Bytes;
import sila_java.library.core.models.BasicType;
import sila_java.library.core.models.DataTypeType;
import sila_java.library.core.models.SiLAElement;
import sila_java.library.core.models.StructureType;
import sila_java.library.core.sila.errors.SiLAErrors;
import sila_java.library.core.sila.mapping.feature.MalformedSiLAFeature;
import sila_java.library.core.sila.mapping.grpc.ProtoMapper;
import sila_java.library.core.sila.types.*;
import sila_java.library.server_base.binary_transfer.Binary;
import sila_java.library.server_base.binary_transfer.database.BinaryDatabase;
import sila_java.library.server_base.binary_transfer.database.BinaryDatabaseException;

import java.io.IOException;
import java.sql.SQLException;
import java.util.*;

@Slf4j
public class ComplexDatatypeTest extends ComplexDataTypeTestGrpc.ComplexDataTypeTestImplBase {
    private static final String FULLY_QUALIFIED_FEATURE_IDENTIFIER = "org.silastandard/test/ComplexDatatypeTest/v1";

    public static final ComplexDataTypeTestOuterClass.WhyMakeItSimpleWhenYouCanMakeItComplicated_Responses COMPLICATED_RESPONSES =
            ComplexDataTypeTestOuterClass.WhyMakeItSimpleWhenYouCanMakeItComplicated_Responses
                    .newBuilder()
                    .setFirstResponse(SiLAString.from("Dead"))
                    .setSecondResponse(SiLAString.from("Alive"))
                    .build();
    public static final ComplexDataTypeTestOuterClass.ThreeDimensionalStruct_Responses THREE_DIMENSIONAL_STRUCT_RESPONSES =
            ComplexDataTypeTestOuterClass.ThreeDimensionalStruct_Responses
                    .newBuilder()
                    .setThreeDimensionalListResult(ComplexDataTypeTestOuterClass.DataType_ThreeDimensionalString
                            .newBuilder()
                            .addThreeDimensionalString(
                                    ComplexDataTypeTestOuterClass.DataType_TwoDimensionalString
                                            .newBuilder()
                                            .addTwoDimensionalString(ComplexDataTypeTestOuterClass.DataType_OneDimensionalString
                                                    .newBuilder()
                                                    .addOneDimensionalString(SiLAString.from("000"))
                                                    .build()
                                            ).build()
                            )
                    ).build();
    public static final ComplexDataTypeTestOuterClass.ThreeDimensionalList_Responses THREE_DIMENSIONAL_LIST_RESPONSES =
            ComplexDataTypeTestOuterClass.ThreeDimensionalList_Responses
                    .newBuilder()
                    .setThreeDimensionalStructResult(ComplexDataTypeTestOuterClass.ThreeDimensionalList_Responses.ThreeDimensionalStructResult_Struct
                            .newBuilder()
                            .setFirstDimension(ComplexDataTypeTestOuterClass.ThreeDimensionalList_Responses.ThreeDimensionalStructResult_Struct.FirstDimension_Struct
                                    .newBuilder()
                                    .setSecondDimension(ComplexDataTypeTestOuterClass.ThreeDimensionalList_Responses.ThreeDimensionalStructResult_Struct.FirstDimension_Struct.SecondDimension_Struct
                                            .newBuilder()
                                            .setThirdDimension(SiLAString.from("111"))
                                            .build())
                                    .build())
                            .build())
                    .build();
    public static final int NB_ELEMENT_HUGE_LIST = 1000;
    public static final String[] STRING_ARRAY = {
            "Switzerland", "Germany", "France", "Italy", "Finland", "Russia", "Latvia", "Lithuania", "Poland",
            "Portugal", "Follow", "the White", "Rabbit"
    };
    public static final List<SiLAFramework.String> HUGE_LIST;

    static {
        HUGE_LIST = new ArrayList<>(NB_ELEMENT_HUGE_LIST);
        for (int i = 0; i < NB_ELEMENT_HUGE_LIST; i++) {
            final StringBuilder stringBuilder = new StringBuilder();
            for (int j = 0; j < i % 5; j++) {
                stringBuilder.append(STRING_ARRAY[(i >> j) % STRING_ARRAY.length]).append(" ");
            }
            HUGE_LIST.add(SiLAString.from(stringBuilder.toString()));
        }
    }

    public static final ComplexDataTypeTestOuterClass.Get_ListString_Responses LIST_STRING_RESPONSE =
            ComplexDataTypeTestOuterClass.Get_ListString_Responses.newBuilder()
                    .addListString(SiLAString.from("White"))
                    .addListString(SiLAString.from("Rabbit"))
                    .build();

    public static final ComplexDataTypeTestOuterClass.Get_ListStructPairString_Responses LIST_STRUCT_PAIR_STRING_RESPONSE = ComplexDataTypeTestOuterClass.Get_ListStructPairString_Responses.newBuilder()
            .addListStructPairString(
                    ComplexDataTypeTestOuterClass.Get_ListStructPairString_Responses.ListStructPairString_Struct
                            .newBuilder()
                            .setKey(SiLAString.from("Unite"))
                            .setValue(SiLAString.from("Labs"))
                            .build()
            ).addListStructPairString(
                    ComplexDataTypeTestOuterClass.Get_ListStructPairString_Responses.ListStructPairString_Struct
                            .newBuilder()
                            .setKey(SiLAString.from("SiLA"))
                            .setValue(SiLAString.from("Standard"))
                            .build()
            ).build();

    public static final ComplexDataTypeTestOuterClass.Get_StructPairString_Responses STRUCT_PAIR_STRING_RESPONSE = ComplexDataTypeTestOuterClass.Get_StructPairString_Responses
            .newBuilder()
            .setStructPairString(ComplexDataTypeTestOuterClass.Get_StructPairString_Responses.StructPairString_Struct
                    .newBuilder()
                    .setPairString(ComplexDataTypeTestOuterClass.Get_StructPairString_Responses.StructPairString_Struct.PairString_Struct
                            .newBuilder()
                            .setKey(SiLAString.from("AKey"))
                            .setValue(SiLAString.from("AValue"))
                            .build())
                    .build())
            .build();

    public static final ComplexDataTypeTestOuterClass.Get_DataType_Responses DATA_TYPE_RESPONSE = ComplexDataTypeTestOuterClass.Get_DataType_Responses
            .newBuilder()
            .setDataType(ComplexDataTypeTestOuterClass.DataType_DataTypeString
                    .newBuilder()
                    .setDataTypeString(SiLAString.from("blah"))
                    .build())
            .build();

    private final BinaryDatabase binaryDatabase;

    public ComplexDatatypeTest(@NonNull final BinaryDatabase binaryDatabase) {
        this.binaryDatabase = binaryDatabase;
    }

    @Override
    public void listProvider(ComplexDataTypeTestOuterClass.ListProvider_Parameters request, StreamObserver<ComplexDataTypeTestOuterClass.ListProvider_Responses> responseObserver) {
        final long listLength = request.getListLength().getValue();
        final long elementLength = request.getElementLength().getValue();
        final List<SiLAFramework.String> list = new ArrayList<>((int) listLength);

        for (int i = 0; i < listLength; ++i) {
            list.add(i, SiLAString.from(RandomStringUtils.randomAlphanumeric((int) elementLength)));
        }
        responseObserver.onNext(ComplexDataTypeTestOuterClass.ListProvider_Responses
                .newBuilder()
                .addAllList(list)
                .build());
        responseObserver.onCompleted();
    }

    @Override
    public void threeDimensionalStruct(ComplexDataTypeTestOuterClass.ThreeDimensionalStruct_Parameters request, StreamObserver<ComplexDataTypeTestOuterClass.ThreeDimensionalStruct_Responses> responseObserver) {
        responseObserver.onNext(ComplexDatatypeTest.THREE_DIMENSIONAL_STRUCT_RESPONSES);
        responseObserver.onCompleted();
    }

    @Override
    public void threeDimensionalList(ComplexDataTypeTestOuterClass.ThreeDimensionalList_Parameters request, StreamObserver<ComplexDataTypeTestOuterClass.ThreeDimensionalList_Responses> responseObserver) {
        responseObserver.onNext(ComplexDatatypeTest.THREE_DIMENSIONAL_LIST_RESPONSES);
        responseObserver.onCompleted();
    }

    @Override
    public void whyMakeItSimpleWhenYouCanMakeItComplicated(ComplexDataTypeTestOuterClass.WhyMakeItSimpleWhenYouCanMakeItComplicated_Parameters request, StreamObserver<ComplexDataTypeTestOuterClass.WhyMakeItSimpleWhenYouCanMakeItComplicated_Responses> responseObserver) {
        responseObserver.onNext(ComplexDatatypeTest.COMPLICATED_RESPONSES);
        responseObserver.onCompleted();
    }

    @Override
    public void xOREncipher(ComplexDataTypeTestOuterClass.XOREncipher_Parameters request, StreamObserver<ComplexDataTypeTestOuterClass.XOREncipher_Responses> responseObserver) {
        final byte key = (byte)request.getCipherKey().getValue();
        if (!request.getData().getBinaryTransferUUID().isEmpty()) {
            try {
                final UUID uuid = XOREncipherSiLABinary(UUID.fromString(request.getData().getBinaryTransferUUID()), key);
                responseObserver.onNext(
                        ComplexDataTypeTestOuterClass.XOREncipher_Responses
                                .newBuilder()
                                .setData(SiLABinary.fromBinaryTransferUUID(uuid))
                                .build()
                );
            } catch (BinaryDatabaseException | SQLException e) {
                log.error(
                        "Error happened while applying XOR Cipher on SiLA Binary {}",
                        request.getData().getBinaryTransferUUID(),
                        e
                );
                responseObserver.onError(e);
                return;
            }
        } else {
            final byte[] data = request.getData().getValue().toByteArray();
            Bytes.XOREncipherBytes(data, key, 0, data.length);
            responseObserver.onNext(
                    ComplexDataTypeTestOuterClass.XOREncipher_Responses
                            .newBuilder()
                            .setData(SiLABinary.fromBytes(data))
                            .build()
            );
        }
        responseObserver.onCompleted();
    }

    @Override
    public void valueForTypeProvider(ComplexDataTypeTestOuterClass.ValueForTypeProvider_Parameters request, StreamObserver<ComplexDataTypeTestOuterClass.ValueForTypeProvider_Responses> responseObserver) {
        final DataTypeType integerType = new DataTypeType();
        integerType.setBasic(BasicType.INTEGER);

        final DataTypeType anonymousStructureType = new DataTypeType();
        final StructureType structureType = new StructureType();
        final SiLAElement element = new SiLAElement();
        element.setDataType(integerType);
        element.setDescription("X");
        element.setDisplayName("X");
        element.setIdentifier("X");
        structureType.getElement().add(element);
        anonymousStructureType.setStructure(structureType);

        final Descriptors.Descriptor anonymousStructureDescriptor;
        final DynamicMessage anonymousStructureMessage;
        try {
            anonymousStructureDescriptor = ProtoMapper.dataTypeToDescriptor(anonymousStructureType);
            anonymousStructureMessage = DynamicMessage
                    .newBuilder(anonymousStructureDescriptor)
                    .setField(anonymousStructureDescriptor.findFieldByName("X"), SiLAInteger.from(101))
                    .build();
        } catch (MalformedSiLAFeature e) {
            throw new RuntimeException(e);
        }

        final SiLAFramework.Any any;
        switch (request.getType().getValue()) {
            case "Integer":
                any = SiLAAny.from(integerType, SiLAInteger.from(1337));
                break;
            case "String":
                any = SiLAAny.from("<DataType><Basic>String</Basic></DataType>", SiLAString.from("test"));
                break;
            case "ConstrainedReal":
                any = SiLAAny.from(
                        "<DataType><Constrained><DataType><Basic>Real</Basic></DataType><Constraints><MinimalInclusive>4.2</MinimalInclusive><MaximalInclusive>133.7</MaximalInclusive></Constraints></Constrained></DataType>",
                        SiLAReal.from(7.7)
                );
                break;
            case "AnonymousStructure":
                any = SiLAAny.from(anonymousStructureType, anonymousStructureMessage);
                break;
            case "AnonymousList":
                any = SiLAAny.fromList(
                        "<DataType><List><DataType><Basic>Any</Basic></DataType></List></DataType>",
                        Lists.newArrayList(
                                SiLAAny.from(integerType, SiLAInteger.from(777)),
                                        SiLAAny.from("<DataType><Basic>String</Basic></DataType>", SiLAString.from("SiLA"))
                        )
                );
                break;
            default:
                responseObserver.onError(
                        SiLAErrors.generateUndefinedExecutionError("Unsupported type: " + request.getType().getValue())
                );
                return;
        }
        responseObserver.onNext(
                ComplexDataTypeTestOuterClass.ValueForTypeProvider_Responses
                        .newBuilder()
                        .setAny(any)
                        .build()
        );
        responseObserver.onCompleted();
    }

    @Override
    public void getHugeList(ComplexDataTypeTestOuterClass.Get_HugeList_Parameters request, StreamObserver<ComplexDataTypeTestOuterClass.Get_HugeList_Responses> responseObserver) {
        responseObserver.onNext(
                ComplexDataTypeTestOuterClass.Get_HugeList_Responses
                        .newBuilder()
                        .addAllHugeList(HUGE_LIST)
                        .build()
        );
        responseObserver.onCompleted();
    }

    @Override
    public void getBoolean(ComplexDataTypeTestOuterClass.Get_Boolean_Parameters request, StreamObserver<ComplexDataTypeTestOuterClass.Get_Boolean_Responses> responseObserver) {
        responseObserver.onNext(ComplexDataTypeTestOuterClass.Get_Boolean_Responses.newBuilder().setBoolean(SiLABoolean.from(Math.random() < 0.5)).build());
        responseObserver.onCompleted();
    }

    @Override
    public void getListString(ComplexDataTypeTestOuterClass.Get_ListString_Parameters request, StreamObserver<ComplexDataTypeTestOuterClass.Get_ListString_Responses> responseObserver) {
        responseObserver.onNext(ComplexDatatypeTest.LIST_STRING_RESPONSE);
        responseObserver.onCompleted();
    }

    @Override
    public void getRandomChangingListString(ComplexDataTypeTestOuterClass.Get_RandomChangingListString_Parameters request, StreamObserver<ComplexDataTypeTestOuterClass.Get_RandomChangingListString_Responses> responseObserver) {
        final ComplexDataTypeTestOuterClass.Get_RandomChangingListString_Responses.Builder newListRes =
                ComplexDataTypeTestOuterClass.Get_RandomChangingListString_Responses.newBuilder();

        getNewList().forEach(item -> newListRes.addRandomChangingListString(SiLAString.from(item)));
        responseObserver.onNext(newListRes.build());
        responseObserver.onCompleted();
    }

    @Override
    public void getListStructPairString(ComplexDataTypeTestOuterClass.Get_ListStructPairString_Parameters request, StreamObserver<ComplexDataTypeTestOuterClass.Get_ListStructPairString_Responses> responseObserver) {
        responseObserver.onNext(ComplexDatatypeTest.LIST_STRUCT_PAIR_STRING_RESPONSE);
        responseObserver.onCompleted();
    }

    @Override
    public void getStructPairString(ComplexDataTypeTestOuterClass.Get_StructPairString_Parameters request, StreamObserver<ComplexDataTypeTestOuterClass.Get_StructPairString_Responses> responseObserver) {
        responseObserver.onNext(ComplexDatatypeTest.STRUCT_PAIR_STRING_RESPONSE);
        responseObserver.onCompleted();
    }

    @Override
    public void getDataType(ComplexDataTypeTestOuterClass.Get_DataType_Parameters request, StreamObserver<ComplexDataTypeTestOuterClass.Get_DataType_Responses> responseObserver) {
        responseObserver.onNext(ComplexDatatypeTest.DATA_TYPE_RESPONSE);
        responseObserver.onCompleted();
    }

    @Override
    public void getThrowException(ComplexDataTypeTestOuterClass.Get_ThrowException_Parameters request, StreamObserver<ComplexDataTypeTestOuterClass.Get_ThrowException_Responses> responseObserver) {
        responseObserver.onError(
                SiLAErrors.generateDefinedExecutionError(
                        FULLY_QUALIFIED_FEATURE_IDENTIFIER + "/DefinedExecutionError/" + "TestException",
                        "Nothing to do, this will always throw an exception"
                )
        );
    }

    @Override
    public void getFCPAffectedByMetadataUser(ComplexDataTypeTestOuterClass.Get_FCPAffectedByMetadata_User_Parameters request, StreamObserver<ComplexDataTypeTestOuterClass.Get_FCPAffectedByMetadata_User_Responses> responseObserver) {
        responseObserver.onNext(
                ComplexDataTypeTestOuterClass.Get_FCPAffectedByMetadata_User_Responses
                        .newBuilder()
                        .addAffectedCalls(SiLAString.from(FULLY_QUALIFIED_FEATURE_IDENTIFIER + "/Command/XOREncipher"))
                        .build()
        );
        responseObserver.onCompleted();
    }

    /**
     * XOR encipher a SiLA Binary and returns a Binary Transfer UUID to the new enciphered SiLA Binary
     * @param binaryIdentifier The Binary Transfer UUID of the SiLA Binary to encipher
     * @param key The key to encipher the data with
     * @return a Binary Transfer UUID to the new enciphered SiLA Binary
     * @throws BinaryDatabaseException If the SiLA Binary does not exist or if the enciphered SiLA Binary cannot be created
     * @throws SQLException If an error occurs while reading the SiLA Binary blob from the database
     */
    private UUID XOREncipherSiLABinary(@NonNull final UUID binaryIdentifier, final byte key)
            throws BinaryDatabaseException, SQLException {
        final Binary binary = this.binaryDatabase.getBinary(binaryIdentifier);
        final UUID xorBinaryId = UUID.randomUUID();
        try(ByteArrayProcessedInputStream byteArrayProcessedInputStream = new ByteArrayProcessedInputStream(
                binary.getData().getBinaryStream(),
                (bytes, off, len) -> Bytes.XOREncipherBytes(bytes, key, off, len)
        )) {
            this.binaryDatabase.addBinary(xorBinaryId, byteArrayProcessedInputStream, "ch.unitelabs/test/UnobservableCommandTest/v1/Command/XOREncipher/Parameter/Data");
        } catch (IOException e) {
            // should never happen
            throw new RuntimeException(e);
        }
        return xorBinaryId;
    }

    /**
     * getNewList()
     * Randomly generates a list of strings, based on our STRING_ARRAY
     * @return List of Strings
     */
    private static List<String> getNewList() {
        final List<String> listString = new ArrayList<>();
        final Random random = new Random();
        final int listLength = random.nextInt(STRING_ARRAY.length);
        for (int i = 0; i < listLength; i++) {
            listString.add(STRING_ARRAY[random.nextInt(STRING_ARRAY.length)]);
        }
        return listString;
    }
}
