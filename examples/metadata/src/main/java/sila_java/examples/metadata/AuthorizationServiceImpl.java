package sila_java.examples.metadata;

import io.grpc.Context;
import io.grpc.ServerCall;
import io.grpc.stub.StreamObserver;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.SiLAFramework;
import sila2.org.silastandard.core.authorizationservice.v1.AuthorizationServiceGrpc;
import sila2.org.silastandard.core.authorizationservice.v1.AuthorizationServiceOuterClass;
import sila_java.library.core.sila.errors.SiLAErrors;
import sila_java.library.core.sila.types.SiLAString;
import sila_java.library.server_base.metadata.MetadataInterceptor;
import sila_java.library.server_base.metadata.ServerMetadataContainer;

import java.util.List;

/**
 * Dummy implementation of the Authorization Service feature
 */
@Slf4j
class AuthorizationServiceImpl extends AuthorizationServiceGrpc.AuthorizationServiceImplBase {
    private final String validAccessToken;
    private final List<String> affectedCalls;

    /**
     * Interceptor to add to the server
     */
    public final MetadataInterceptor accessTokenInterceptor;

    /**
     * Create an Authorization Service implementation which accepts exactly one access token
     *
     * @param validAccessToken The only valid access token
     * @param affectedCalls Fully qualified identifiers of all affected features, commands, and properties
     */
    AuthorizationServiceImpl(final String validAccessToken, final List<String> affectedCalls) {
        this.validAccessToken = validAccessToken;
        this.affectedCalls = affectedCalls;
        this.accessTokenInterceptor = new AccessTokenInterceptor(affectedCalls);
    }

    @Override
    public void getFCPAffectedByMetadataAccessToken(
            final AuthorizationServiceOuterClass.Get_FCPAffectedByMetadata_AccessToken_Parameters request,
            final StreamObserver<AuthorizationServiceOuterClass.Get_FCPAffectedByMetadata_AccessToken_Responses> responseObserver
    ) {
        final AuthorizationServiceOuterClass.Get_FCPAffectedByMetadata_AccessToken_Responses.Builder responseBuilder
                = AuthorizationServiceOuterClass.Get_FCPAffectedByMetadata_AccessToken_Responses.newBuilder();

        this.affectedCalls.forEach(affectedCall -> responseBuilder.addAffectedCalls(SiLAString.from(affectedCall)));

        responseObserver.onNext(responseBuilder.build());
        responseObserver.onCompleted();
    }

    private class AccessTokenInterceptor extends MetadataInterceptor {

        public AccessTokenInterceptor(@NonNull final List<String> affectedCalls) {
            super(affectedCalls);
        }

        public AccessTokenInterceptor(@NonNull final String affectedCall) {
            super(affectedCall);
        }

        @Override
        public <ReqT, RespT> Context intercept(
                final ServerCall<ReqT, RespT> call,
                final ServerMetadataContainer metadata
        ) {
            // get metadata message
            final AuthorizationServiceOuterClass.Metadata_AccessToken accessTokenMessage = metadata.get(
                    AuthorizationServiceOuterClass.Metadata_AccessToken.class
            );

            // validate message
            if (accessTokenMessage == null) {
                throw SiLAErrors.generateFrameworkError(
                        SiLAFramework.FrameworkError.ErrorType.INVALID_METADATA,
                        "No access token metadata received (org.silastandard/core/AuthorizationService/v1/Metadata/AccessToken)"
                );
            }

            if (!accessTokenMessage.hasAccessToken()) {
                throw SiLAErrors.generateFrameworkError(
                        SiLAFramework.FrameworkError.ErrorType.INVALID_METADATA,
                        "Received lock identifier message was empty (org.silastandard/core/AuthorizationService/v1/Metadata/AccessToken)"
                );
            }

            // validate token
            String accessToken = accessTokenMessage.getAccessToken().getValue();
            if (!accessToken.equals(validAccessToken)) {
                throw SiLAErrors.generateDefinedExecutionError(
                        "org.silastandard/core/AuthorizationService/v1/DefinedExecutionError/InvalidAccessToken",
                        "Invalid access token"
                );
            }

            // return context for downstream call handlers
            return Context.current();
        }
    }
}
