package sila_java.examples.metadata;

import io.grpc.stub.StreamObserver;
import lombok.NonNull;
import sila2.org.silastandard.examples.greetingprovider.v1.GreetingProviderGrpc;
import sila2.org.silastandard.examples.greetingprovider.v1.GreetingProviderOuterClass;
import sila_java.library.core.sila.types.SiLAInteger;
import sila_java.library.core.sila.types.SiLAString;

import java.time.LocalDateTime;

/**
 * Implementation of the Greeting Provider example feature
 */
public class GreetingProviderImpl extends GreetingProviderGrpc.GreetingProviderImplBase {
    private final int startYear = LocalDateTime.now().getYear();

    @Override
    public void sayHello(
            @NonNull final GreetingProviderOuterClass.SayHello_Parameters request,
            @NonNull final StreamObserver<GreetingProviderOuterClass.SayHello_Responses> responseObserver
    ) {
        final String greeting = "Hello SiLA 2 " + request.getName().getValue();

        responseObserver.onNext(
                GreetingProviderOuterClass.SayHello_Responses.newBuilder()
                        .setGreeting(SiLAString.from(greeting))
                        .build()
        );
        responseObserver.onCompleted();
    }

    @Override
    public void getStartYear(
            @NonNull final GreetingProviderOuterClass.Get_StartYear_Parameters request,
            @NonNull final StreamObserver<GreetingProviderOuterClass.Get_StartYear_Responses> responseObserver
    ) {
        responseObserver.onNext(
                GreetingProviderOuterClass.Get_StartYear_Responses.newBuilder()
                        .setStartYear(SiLAInteger.from(startYear))
                        .build()
        );
        responseObserver.onCompleted();
    }
}
