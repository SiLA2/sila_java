package sila_java.examples.metadata;

import io.grpc.StatusRuntimeException;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import sila2.org.silastandard.SiLAFramework;
import sila_java.library.core.sila.errors.SiLAErrors;

import java.io.IOException;
import java.util.Optional;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class MetadataTest {
    private Server server;
    private Client client;

    @BeforeAll
    void beforeAll() throws IOException {
        this.server = new Server(50052);
        this.client = new Client(50052);
    }

    @Test
    void testMissingMetadataThrowsInvalidMetadata() {
        final StatusRuntimeException e = Assertions.assertThrows(
                StatusRuntimeException.class,
                () -> this.client.sayHello("World")
        );

        final Optional<SiLAFramework.SiLAError> siLAError = SiLAErrors.retrieveSiLAError(e);
        Assertions.assertTrue(siLAError.isPresent());

        Assertions.assertTrue(siLAError.get()
                                       .hasFrameworkError());
        Assertions.assertEquals(
                SiLAFramework.FrameworkError.ErrorType.INVALID_METADATA,
                siLAError.get()
                         .getFrameworkError()
                         .getErrorType()
        );
    }

    @Test
    void testInvalidTokenThrowsDefinedExecutionError() {
        final StatusRuntimeException e = Assertions.assertThrows(
                StatusRuntimeException.class,
                () -> this.client.sayHelloWithAccessToken("World", "invalid-token")
        );

        final Optional<SiLAFramework.SiLAError> siLAError = SiLAErrors.retrieveSiLAError(e);
        Assertions.assertTrue(siLAError.isPresent());

        Assertions.assertTrue(siLAError.get().hasDefinedExecutionError());
        Assertions.assertEquals(
                "org.silastandard/core/AuthorizationService/v1/DefinedExecutionError/InvalidAccessToken",
                siLAError.get().getDefinedExecutionError().getErrorIdentifier()
        );
    }

    @Test
    void testValidAccessTokenProvidesAccess() {
        Assertions.assertEquals(
                "Hello SiLA 2 World",
                this.client.sayHelloWithAccessToken("World", "secret-access-token")
        );
    }

    @AfterAll
    void afterAll() {
        this.server.close();
    }
}
