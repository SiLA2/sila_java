# SiLA Java Release Process

### Local setup
#### Create a key
Linux:
- Create a new key (if you don't already have one) `gpg2 --gen-key`

Windows:
- Download Gpg4win https://gpg4win.org/download.html
- Install and open Kleopatra
- Create a new key pair
#### Publish a key
Linux:
- List the keys `gpg2 --list-keys`
- Publish the key (replace `<KEY-ID>` by yours) `gpg2 --keyserver hkp://ipv4.pool.sks-keyservers.net --send-keys <KEY-ID>`

Windows:
- Click on the key pair you generated and export (the public key)
- Upload the exported key here: https://keys.openpgp.org/upload/submit
#### Configure Maven
- Create / edit the file `~/.m2/settings.xml`and add the following lines into it :
```xml
<settings>
  <servers>
    <server>
      <id>ossrh</id>
      <username><USERNAME></username>
      <password><PASSWORD></password>
    </server>
  </servers>
  <profiles>
    <profile>
      <id>ossrh</id>
      <activation>
        <activeByDefault>true</activeByDefault>
      </activation>
      <properties>
        <gpg.executable>gpg2</gpg.executable>
        <gpg.passphrase><PASS_PHRASE></gpg.passphrase>
      </properties>
    </profile>
  </profiles>
</settings>
```
- Replace `<USERNAME>` with your SonaType username
- Replace `<PASSWORD>` with your SonaType password
- Replace `<PASS_PHRASE>` with your key passphrase (if any)
### Deploy artifacts
- If deploying a release, upgrade the version of the artifacts in the poms
- `mvn clean deploy`
- Go to `https://oss.sonatype.org/#stagingRepositories` and login into your SonaType account
- Select the deployed repository, then you can either `release` if everything is correct or `drop`.
### Use of released artifacts
Simply add the dependencies like you would normally do in your project pom like so:
```xml
<dependency>
    <groupId>org.sila-standard.sila_java.library</groupId>
    <artifactId>manager</artifactId>
    <version>0.0.2</version>
</dependency>
```