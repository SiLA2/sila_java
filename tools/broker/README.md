# SiLA Broker
Providing utilities for late binded SiLA calls. Also providing a REST layer for other applications
or debugging purposes.

Note: As this tool is currently only for accessibility and debugging purposes, the error messages
are not very descriptive.

## Installation
First install the module with maven: `mvn clean install`, then you can execute
the JAR with `java -jar target/broker.jar` (depending on the version).

For Windows Machines, there is also a stand-alone executable `broker.exe` generated.

# REST Interface
To check how to use the provided jar, simply execute `java -jar target/broker.jar -h`.
By default, all calls can be accessed through `localhost:8000/v1`

Test the broker for example by starting the `HelloSiLAServer` and using Postman to issue the REST Calls.

The following explanations assume you started the broker with the default configuration.

## Handling Servers
## Trigger Discovery
To trigger discovery with the broker, one has to issue a POST request without payload to `/v1/scan_network`.

## Manual add and remove to Cache
In addition to automatic discovery, Servers can be added "manually" by letting the broker resolve the 
SiLA Server at a host and port combination. Issue a POST Request to `/v1/servers` with the payload:

```bash
{
    "host": "127.0.0.1",
    "port": "50052"
}
```

If you want to delete a server from the cache, you simply issue a DELETE Request to `/v1/server/:id`,
with the id being a unique id assigned in the cache.

## Get All Servers
Simply issue a GET Request to `/v1/servers` to retrieve all servers

## Get More Information about specific Server
You will get more Information about the a specific server, its meta info and features, by issuing
a GET Request to `/v1/server/:id`, with the id being a unique id assigned in the cache.

## Calling SiLA Features
### Get Property of Server
Properties are retrieved via POST Requests to `/v1/property` and a JSON Object as body identifying
the property, by example:

```json
{
    "serverIdentifier": "85b2a6d2-8ec6-4a5e-bed9-c7a620fa1f36",
    "featureIdentifier": "GreetingProvider",
    "propertyIdentifier": "StartYear"
}
```

For observable properties, simply use the `/v1/observable_property` endpoint with the same behaviour, 
in the Broker Application it simply returns the first value.

### Call the Command of a Server
Commands are called via POST Requests to `/v1/command` and a JSON Object as body identifying the command.
The parameters itself are expected to be a JSON body that can be serialised into the correct protobuf message
for the SiLA Parameters, according to the 
[Proto 3 JSON specification](https://developers.google.com/protocol-buffers/docs/proto3#json).

HelloSiLA Example Body:

```json
{
    "serverIdentifier": "85b2a6d2-8ec6-4a5e-bed9-c7a620fa1f36",
    "featureIdentifier": "GreetingProvider",
    "commandIdentifier": "SayHello",
    "parameters": {"Name": {"value": "SiLA"}}
}
```

For observable commands, simply use the `/v1/observable_command` endpoint with the same behaviour,
in the Broker Application it simply blocks until the command is finished.

# Know issues

## JSON Representation
The representation of a message has 3 different behaviors:

- A message without value : 
```json
{
}
```
- A nessage with a default value (see the default value of types: https://developers.google.com/protocol-buffers/docs/proto3#default): 
```json
{
  "StartYear": {
  }
}
```
- A message with a non default value:
```json
{
  "StartYear": {
    "value": "1"
  }
}
```