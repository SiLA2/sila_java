package sila_java.library.tools.broker;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.grpc.Status;
import io.javalin.http.Context;
import io.javalin.http.Handler;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import sila_java.library.manager.server_management.ServerConnectionException;
import sila_java.library.manager.ServerFinder;
import sila_java.library.manager.ServerManager;
import sila_java.library.manager.executor.ExecutableServerCall;
import sila_java.library.manager.models.SiLACall;
import sila_java.library.manager.models.Server;

import java.io.IOException;
import java.util.*;

/**
 * SilaActions - Contains the http methods of the broker.
 */
@Slf4j
class SiLAActions {
    private static final String OK_MESSAGE = new JSONObject().put("result", "OK").toString();

    final Handler getServers = this::getServers;
    final Handler getServer = this::getServer;
    final Handler addServer = this::addServer;
    final Handler deleteServer = this::deleteServer;
    final Handler executeUnobservableCommand = this::executeUnobservableCommand;
    final Handler executeObservableCommand = this::executeObservableCommand;
    final Handler getUnobservableProperty = this::getUnobservableProperty;
    final Handler getObservableProperty = this::getObservableProperty;
    final Handler scanNetwork = this::scanNetwork;

    private final String apiPrefix;

    /**
     * @param apiPrefix API Prefix for REST Calls
     */
    SiLAActions(String apiPrefix) {
        this.apiPrefix = apiPrefix;
    }

    /**
     * @implNote Parameters are nested JSON Objects
     */
    @JsonIgnoreProperties(ignoreUnknown = false)
    private static class AddRequest {
        @JsonProperty("host")
        String host;
        @JsonProperty("port")
        int port;
    }

    @JsonIgnoreProperties(ignoreUnknown = false)
    private static class CommandRequest {
        @JsonProperty("serverIdentifier")
        UUID serverId;
        @JsonProperty("featureIdentifier")
        String featureId;
        @JsonProperty("commandIdentifier")
        String commandId;
        @JsonProperty("parameters")
        JsonNode parameters;
    }

    @JsonIgnoreProperties(ignoreUnknown = false)
    private static class PropertyRequest {
        @JsonProperty("serverIdentifier")
        UUID serverId;
        @JsonProperty("featureIdentifier")
        String featureId;
        @JsonProperty("propertyIdentifier")
        String propertyId;
    }

    /**
     * Get a single server
     */
    private void getServer(final Context ctx) {
        final String id = ctx.pathParam("id");

        if (id == null) {
            respondError(ctx, new NullPointerException("Id must be set to get Server"));
            return;
        }

        log.info(ctx.req.getRemoteAddr() + " requested /servers/" + id);
        final UUID serverId = UUID.fromString(id);
        final Optional<Server> optionalServer = ServerFinder.filterBy(ServerFinder.Filter.uuid(serverId)).findOne();
        if (!optionalServer.isPresent()) {
            respondError(ctx, new IllegalArgumentException("No server found with id: " + id));
            return;
        }

        ctx.json(optionalServer.get());
    }

    /**
     * Get the list of connected servers
     */
    private void getServers(final Context ctx) {
        final Set<String> serverUrls = new TreeSet<>();
        ServerManager.getInstance().getServers().keySet().forEach(serverId ->
                serverUrls.add(this.getBaseUrl(ctx.host()) + "/servers/" + serverId)
        );
        log.info(ctx.req.getRemoteAddr() + " requested /servers");
        ctx.json(serverUrls);
    }

    /**
     * Add Server manually
     */
    private void addServer(final Context ctx) {
        final String jsonString = ctx.body();
        log.info("Command body: " + jsonString);
        final AddRequest addRequest = getRequestObject(ctx, jsonString, AddRequest.class);

        if (addRequest == null) return;

        try {
            ServerManager.getInstance().addServer(addRequest.host, addRequest.port);
        } catch (final ServerConnectionException e) {
            respondError(ctx, e);
            return;
        }
        log.info(ctx.req.getRemoteAddr() + " requested /servers add");
    }

    /**
     * Delete Server from cache
     */
    private void deleteServer(final Context ctx) {
        final String id = ctx.pathParam("id");

        if (id == null) {
            respondError(ctx, new NullPointerException("Id must be set to delete Server"));
            return;
        }

        ServerManager.getInstance().removeServer(UUID.fromString(id));
        log.info(ctx.req.getRemoteAddr() + " requested /servers delete");
    }

    /**
     * Issue a new scan to the network
     */
    private void scanNetwork(Context ctx) {
        ServerManager.getInstance().getDiscovery().scanNetwork();
        log.info(ctx.req.getRemoteAddr() + " requested /scan_network");
    }

    /**
     * Execute a single command
     */
    private void executeUnobservableCommand(final Context ctx) {
        this.executeCommand(ctx, SiLACall.Type.UNOBSERVABLE_COMMAND);
    }

    /**
     * Execute an observable command
     */
    private void executeObservableCommand(final Context ctx) {
        this.executeCommand(ctx, SiLACall.Type.OBSERVABLE_COMMAND);
    }

    private void executeCommand(final Context ctx, SiLACall.Type type) {
        final String jsonString = ctx.body();
        log.info("Command body: " + jsonString);
        final CommandRequest cmdReq = getRequestObject(ctx, jsonString, CommandRequest.class);
        if (cmdReq == null)
            return;
        final String parameterBody = SiLAActions.parametersToString(cmdReq.parameters);
        if (parameterBody == null)
            return;

        log.info("Got parameters body: " + parameterBody);

        final SiLACall siLACall = new SiLACall(
                cmdReq.serverId,
                cmdReq.featureId,
                cmdReq.commandId,
                type,
                parameterBody
        );

        String result = this.doCall(ctx, siLACall);
        if (result == null)
            return;

        if (result.isEmpty())
            result = OK_MESSAGE;

        log.info(ctx.req.getRemoteAddr() + " requested /call/ >server:" +
                cmdReq.serverId + " >feature:" +
                cmdReq.featureId + " >command:" + cmdReq.commandId);
        ctx.result(result);
    }

    /**
     * Get unobservable Property
     */
    private void getUnobservableProperty(final Context ctx) {
        this.getProperty(ctx, SiLACall.Type.UNOBSERVABLE_PROPERTY);
    }

    /**
     * Get Observable Property
     */
    private void getObservableProperty(final Context ctx) {
        this.getProperty(ctx, SiLACall.Type.OBSERVABLE_PROPERTY);
    }

    private void getProperty(final Context ctx, SiLACall.Type type) {
        final String jsonString = ctx.body();
        log.info("Property body: " + jsonString);
        final PropertyRequest propReq = getRequestObject(ctx, jsonString, PropertyRequest.class);
        if (propReq == null)
            return;

        final SiLACall siLACall = new SiLACall(
                propReq.serverId,
                propReq.featureId,
                propReq.propertyId,
                type
        );

        final String result = this.doCall(ctx, siLACall);
        if (result == null)
            return;

        log.info(ctx.req.getRemoteAddr() + " requested /properties/ >server:" +
                propReq.serverId + " >feature:" +
                propReq.featureId + " >property:" + propReq.propertyId);
        ctx.result(result);
    }

    private String getBaseUrl(String host) {
        return "http://" + host + apiPrefix;
    }

    private String doCall(final Context ctx, final SiLACall siLACall) {
        try {
            return ServerManager.getInstance().getServerCallManager()
                    .runAsync(ExecutableServerCall.newBuilder(siLACall).build())
                    .get();
        } catch (Throwable e) {
            log.error(e.getMessage());
            respondError(ctx, e);
        }
        return null;
    }

    private static <T> T getRequestObject(final Context ctx, final String jsonString, Class<T> clazz) {
        try {
            return new ObjectMapper().readValue(jsonString, clazz);
        } catch (IOException e) {
            log.error(e.getMessage());
            respondError(ctx, e);
        }
        return null;
    }

    private static String parametersToString(final JsonNode parameters) {
        final ObjectMapper objectMapper = new ObjectMapper();
        try {
            return (objectMapper.writeValueAsString(parameters));
        } catch (JsonProcessingException e) {
            log.error("Invalid 'parameters' JSON format", e);
        }
        return (null);
    }

    private static void respondError(Context ctx, Throwable e) {
        final Status st = Status.fromThrowable(e);
        final String description = st.getDescription();
        ctx.status(st.getCode().value());
        ctx.header("X-SiLA-Grpc-Status-Code", String.valueOf(st.getCode().value()));

        // server crashes with 500 Internal Server error if we try to
        // send respondJsonText() with null || undefined variable
        if (description != null) {
            ctx.json(description);
        } else {
            log.info("[respondError] status.toString:" + st.toString());
            log.info("[respondError] status.description:" + st.getDescription());
            log.info("[respondError] status.cause:" + st.getCause());
            log.info("[respondError] status.code:" + st.getCode());

            String identifier = st.getCode().toString();
            String message = "Unknown error";

            JSONObject jsonErrorMessage = new JSONObject();
            jsonErrorMessage.put("Identifier", identifier);
            jsonErrorMessage.put("Message", message);
            ctx.result(jsonErrorMessage.toString());
        }
    }
}
