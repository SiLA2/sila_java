package sila_java.library.tools.broker;

import io.javalin.Javalin;
import lombok.extern.slf4j.Slf4j;
import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.Namespace;
import org.apache.commons.configuration2.Configuration;
import sila_java.library.manager.ServerManager;

import static sila_java.library.core.utils.FileUtils.getLocalConfiguration;

/**
 * Main Entry Point for Broker
 */
@Slf4j
public class BrokerApplication {
    private Javalin server;

    public static void main(String[] args) {
        final String propFileName = "broker.properties";
        final Configuration config = getLocalConfiguration(propFileName);

        // Argument Parser
        ArgumentParser parser = ArgumentParsers.newFor("broker").build()
                .defaultHelp(true)
                .description("Broker service to register SiLA Servers "
                        + "and serve REST about them");
        parser.addArgument("-p", "--port")
                .type(Integer.class)
                .setDefault(config.getInt("restPort"))
                .help("Specify port to use.");
        parser.addArgument("-n", "--network_interface")
                .type(String.class)
                .help("Specify internet interface. Check ifconfig (LNX & MAC)" + " and for windows, ask us for a tiny java app.");
        parser.addArgument("-a", "--api-path")
                .type(String.class)
                .setDefault(config.getString("apiPrefix"))
                .help("Specify desired API Path. ex.: /v1");

        Namespace ns = null;
        try {
            ns = parser.parseArgs(args);
        } catch (ArgumentParserException e) {
            parser.handleError(e);
            System.exit(1);
        }

        // Start Application
        final BrokerApplication app = new BrokerApplication();

        // Graceful aborting
        Runtime.getRuntime().addShutdownHook(
                new Thread(app::stop)
        );
        app.start(
                ns.getInt("port"),
                ns.getString("api_path")
        );
    }

    /**
     * Start REST Server exposing SiLA calls
     *
     * @param port for HTTP Server
     * @param apiPrefix Prefix for API calls
     */
    private void start(int port, String apiPrefix) {
        final SiLAActions siLAActions = new SiLAActions(apiPrefix);

        // Create HTTP Server
        this.server = Javalin.create((cfg) -> {
            cfg.enableCorsForAllOrigins();
            //cfg.enableDevLogging();
        })
                .events(eventListener -> {
                    eventListener.serverStarted(() -> {
                        log.info("SiLA Broker started.");
                    });
                    eventListener.serverStartFailed(() -> {
                        log.error("Start of HTTP Server failed");
                        throw new RuntimeException("Could not start HTTP Server, aborting...");
                    });
                })
                .exception(Exception.class, (e, ctx) -> {
                    log.error("Unexpected exception", e);
                    ctx.result(e.toString());
                })
                .get(apiPrefix + "/servers", siLAActions.getServers)
                .get(apiPrefix + "/servers/{id}", siLAActions.getServer)
                .post(apiPrefix + "/servers", siLAActions.addServer)
                .delete(apiPrefix + "/servers/{id}", siLAActions.deleteServer)
                .post(apiPrefix + "/scan_network", siLAActions.scanNetwork)
                .post(apiPrefix + "/property", siLAActions.getUnobservableProperty)
                .post(apiPrefix + "/observable_property", siLAActions.getObservableProperty)
                .post(apiPrefix + "/command", siLAActions.executeUnobservableCommand)
                .post(apiPrefix + "/observable_command", siLAActions.executeObservableCommand)
                .start(port);
        log.info("Performing initial network scan...");
        ServerManager.getInstance().getDiscovery().scanNetwork();
        log.info("Initial network scan done.");
    }

    private void stop() {
        if(server != null)
            server.stop();
        ServerManager.getInstance().close();
    }
}
