# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### New Features

### Improvements

### Bug Fixes

### Removals

## [0.14.0] - 2024-09-25

### Improvements
- Implemented cached mDNS answers to improve network discovery with sila_python servers #69
- Added listeners for server discovery and removal events

## [0.13.0] - 2024-02-27

### New Features

- Server Call Enhancements:
  - `Binary Transfer`: Added the ability to download and upload binary data using the following SiLA features:
  - DOWNLOAD_BINARY (`org.silastandard/test/BinaryTransferTest/v1/Command/DownloadBinary`)
  - UPLOAD_BINARY (`org.silastandard/test/BinaryTransferTest/v1/Command/UploadBinary`)
  - Refined error handling and timeout management for server calls.
- Feature Management:
  - Added the ability to load features directly from a Server object.
- SiLAAny:
  - Add Void type
  - Support lists
### Improvements
- Libraries updates:
  - argparse4j: Updated from 0.8.1 to 0.9.0
  - commons-io: Updated from 2.11.0 to 2.15.1
  - grpc: Updated from 1.50.2 to 1.60.0
  - gson: Updated from 2.9.1 to 2.10.1
  - guava: Updated from 31.1-jre to 32.1.3-jre
  - junit: Updated from 3.8.1 to 4.13.2 (test scope)
  - lombok: Updated from 1.18.20 to 1.18.30
  - protobuf: Updated from 3.21.8 to 3.25.1
  - maven-antrun-plugin: Updated from 1.8 to 3.1.0
  - maven-clean-plugin: Updated from 3.1.0 to 3.3.2
  - maven-source-plugin: Updated from 3.0.1 to 3.3.0
   -maven-javadoc-plugin: Updated from 3.1.0 to 3.6.3
  - maven-gpg-plugin: Updated from 1.6 to 3.1.0
  - nexus-staging-maven-plugin: Updated from 1.6.8 to 1.6.13
  - git-commit-id-plugin: Updated from 3.0.1 to 4.9.10
  - argparse4j:0.8.1
  - protobuf-java-util:3.5.1
  - jakarta.activation:1.2.1
  - jSerialComm:2.0.2
  - netty-tcnative-boringssl-static:2.0.54.Final
- README Updates:
  - Docker Usage: Updated the README with more comprehensive Docker usage instructions including:
  - Clear documentation on exposing container ports (e.g., 50053 and 5353 for mDNS discovery).
  - Guidance on using the docker run command to pass additional arguments or environment variables to the application.
  - Components Section: Added a new "Components" section to the README, with placeholders for eventual component-specific READMEs.
- Code:
  - Self-Signed Certificate Generation: Added logic to generate a self-signed certificate if no certificate is provided.
  - SiLA Error Handling: Improved error handling for SiLA calls, including generating more informative ValidationErrors and UndefinedExecutionErrors.
  - Internal Refactoring: Refactored various parts of the codebase for maintainability and potential performance optimizations.
  - Improved Fully Qualified Identifier validation
  - Certificate generation:
    - Add localhost and docker internal host in alternative names such as discovery works within docker
    - Add default IP if none are provided/found
### Bug Fixes
  - Fix server removing all binaries stored in the DB when closing. Now binaries are removed only when they are expired.
  - Fix SiLAAny serialization and deserialization  
  - Binary Database race condition
### Removals

## [0.12.0] - 2023-06-13

### New Features
- Introduced support for the `Authorization Controller` feature.
- Integrated support for feature versioning.
- Enhanced `test_server` to support various test features from `sila_base`, including basic, list, and structure data types, as well as observable and unobservable properties/commands.
- Constructed a custom `docker image` specifically to test `sila_java`.
- Created custom `docker images` for all servers and tools, encompassing `hello_sila_server`, `metadata_server`, `test_server_server`, `thermostat_server`, `code_generator`, and `broker`.

### Improvements
- Updated `sila_base` to align with the current latest master version.
- Upgraded the `interoperability server and client`, successfully completing 100% of the new version tests.
- Enhanced the efficiency of `CI` tests and significantly reduced step duration.
- Replaced deprecated/duplicated feature versions with the latest ones from `sila_base`.

### Bug Fixes
- Resolved a race condition in the `Large Binary Upload` process with the final chunk.
- Updated the `Maven plugin` to support features with identical simple identifiers.
- Ensured proper setup for Observable Property in the metadata context.

### Removals
- Removed the `sila_interoperability` submodule, as its features have been migrated to `sila_base`.
- Eliminated the `ConstraintProvider` feature from `test_server`.

## [0.11.0] - 2023-02-21

### New Features
- Utils module to reduce core module size

### Improvements
- Move `sila_java.library.core.utils.WinRegistryUtility.java` to `sila_java.library.utils.WinRegistryUtility.java`
- Move `sila_java.library.core.utils.ProcessUtil.java` to `sila_java.library.utils.ProcessUtil.java`
- `SimulationController.ContextSwitcher` now use `java.util.function.Supplier` instead of `javax.inject.Provider` 
- Improve log messages
- `server_management.Connection.loadFeaturesFromServer` to public to allow server features update
- `manager.ServerManager.addServer` now returns the added server
- `manager.ServerManager.setAllowUnsecureConnection` to `manager.ServerManager.setAllowInsecureConnection`
- Client now rejects servers containing invalid SiLA Feature definitions.
- `core.utils.Utils.validateFeatureXML` validates feature against `sila_base/xslt/fdl-validation.xsl`
- Replace `javax.annotation.Nonnull` with `lombok.NonNull`

### Bug Fixes
- Defined error identifier to be fully qualified
- Typos (unsecure  -> insecure)
- `ObservableCommandManager` raise an undefined error instead of a defined one when the command does not return a result
- Log level ('Info' logs when performing network discovery, failing to add a server, ...)
- Server (infos, features, config) are properly updated when becoming offline and back online
- Large Binary Upload no longer send one extra chunk
- Made `manager.ServerManager.close` thread safe
- `Core` test feature `TestFeature.sila.xml`
- `Core` test feature `NestedDataTypesFeature.sila.xml`
- `TestServer` feature `ParameterConstraintsTest.sila.xml`
- `TestServer` and `HelloSiLA` Server implementation to use fully qualified identifier when raising SiLA Errors
- `core.sila.errors.ExceptionGeneration.generateMessage` catch and consider `InterruptedException` like `CancellationException`

### Removals
- Global dependency to `org.springframework.boot` to reduce library size

## [0.10.0] - 2022-12-20

### New Features
- `Interoperability` `repository` & `client` WIP & `server` & test reporting in CI
- `SiLA Service`:
  - Early stage API to support FCP affected by metadata
- `SiLABinaryTransferUUID` SiLA Type utils

### Improvements
- Move `sila_java.library.manager.ServerAdditionException` to `sila_java.library.manager.server_management.ServerConnectionException`
- `SiLA Service`:
  - Rejects presence of any metadata
  - Feature and server name validation
- `Binary Upload/Download/Database/Tests`
  - Update large binary APIs and database to support parameter identifier
  - Store parameter identifier in database
  - Validate parameter type to be a binary
  - Validate downloaded byte length
  - Validate presence of metadata (FCP affected by)
  - Allow client to upload less data than requested
- `Observable command manager`:
  - Use fully qualified identifier instead of simple feature identifier 
  - Add parameter validation and pre-run callback
  - Rework disposal of expired observable commands
  - Add Metadata, UUID, execution state validation
  - Allow SiLACall without feature identifier for `BINARY_DOWNLOAD`
- Optimize CI page generation

### Bug Fixes
- Duplicate log configuration in classpath
- `ServerManager` no longer requires to provide a certificate for trusted certificate to connect to a server
- Connection to insecure server when allowed
- `Self-signed certificat`e generation to have `127.0.0.1` as a default host when none provided
- `Observable command manager`:
  - Race conditions
- `Binary Download` and `Binary Upload`
  - Race conditions
  - Sending excessive chunks
  - Missing chunks
- ThermostatServer host argument 

### Removals
- `rootCertificate` from `ServerManager`
- `ServerAdditionException` replaced by `ServerConnectionException`

## [0.9.0] - 2022-10-26

### New Features

### Improvements
- Replace default server host from `localhost` to `127.0.0.1`
- Update protobuf to `3.21.8`
- Update grpc to `1.50.2`
- `SiLMojo`: 
  - Support pattern globing like `${project.basedir}/src/main/resources/**/*.sila.xml`
  - Add default parameters to reduce the minimum plugin configuration

### Bug Fixes
- `SiLAServer` BinaryDatabase ownership when a custom database provider is provided
- Allows `Binary Length Constraint` in code generator  

### Removals


## [0.8.0] - 2022-10-12

### New Features
- `BinaryUploadParameterWhitelistInterceptor`
- `BinaryDatabaseInjector`: Inject `BinaryDatabase` instance into gRPC Context
- `BinaryDatabase.current`: Retrieve database from gRPC Context
- `BinaryTransferTest`
- `dnsjava`: Library to replace previous home-made DNS implementation.

### Improvements
- `MetadataInterceptor`: Validate Binary affected Parameter Identifier and call handler delegation
- `ListNetworkInterfaces`: Migrated under `sila_java.library.core.utils`
- `BinaryTransferErrorHandler`: Migrated under `sila_java.library.core.sila.binary_transfer`
- `ByteArrayProcessedInputStream`: Migrated under `sila_java.examples.test_server.utils`
- `sila_java.library.core.communication.*`: Migrated under `sila_java.library.communication.*`
- Utils
  - `blockUntilShutdown`: Allow to block current thread while underlying gRPC Server is running
  - `blockUntilStop`: Deprecated over Utils.blockUntilShutdown and Server.awaitTermination
- SiLAServer 
  - `withPersistedTLS`: Renamed to `withPersistentTLS`
  - `withoutDiscovery`: Disable network discovery
  - `withBinaryDatabaseInjector`: Add BinaryDatabase instance in gRPC Context

### Bug Fixes
- `BinaryUpload`: raise proper BinaryTransferError
- `H2BinaryDatabase`: close method can be called twice without raising an exception

### Removals
- `sila_java.library.core.discovery.networking.dns.*`: Replaced by `dnsjava` library

## [0.7.0] - 2022-07-09

### New Features
- Server CLI arguments
  - `-d` / `--discovery`: Enable or disable network discovery.
- SiLAServer
  - `getBinaryDatabase`:
- SiLAServer.Builder
  - `ConfigWrapperProvider`: Interface to provide a custom config wrapper
  - `BinaryDatabaseProvider`: Interface to provide a custom binary database
  - `getCustomServerConfigProvider`
  - `getCustomBinaryDatabaseProvider`
  - `withDiscovery`: Enable or disable network discovery
  - `withNetworkInterface`: Specify a network interface to provide network discovery on
  - `withHost`: Specify a network to provide network discovery o,
  - `withName`: Override server name
  - `withUUID`: Override server UUID
  - `withDefaultServerConfiguration`: Provide a default server configuration
  - `withBinaryTransferSupport`: Enable or disable binary transfer support
  - `withCustomBinaryDatabaseProvider`: Provide a custom binary database
  - `withCustomConfigWrapperProvider`: Provide a custom config wrapper
  - `withPersistentConfig`: Enable or disable persistent config
  - `withPersistentConfigFile`: File to persist the config
  - `getNewServerConfigurationWrapper`: Get a new server configuration wrapper instance
- `H2BinaryDatabase` test

### Improvements
- `ServerConfiguration` API (breaking)
- `SiLAServerRegistration` API (breaking): support interface and host based network discovery
  - `SiLAServerRegistration` Constructor replaced by newInstanceFromInterface & newInstanceFromHost
- `EncryptionUtils`: Add malformed private key check
- `SiLAServer.Builder`: 
  - `withoutConfig` & `withConfig` replaced by `newBuilder` & `withPersistentConfig`
  - `withBinaryTransferSupport` now takes a boolean and does not return a database

### Bug Fixes

- `SiLA Service` -> `Get Feature Definition` raise correct DefinedExecutionError on unimplemented features
- `ServerInformation` & `ServerConfiguration`: Reject values which would violate SiLAService constraints
- Self Signed Certificate generation
- `UnobservableCommand` & `UploadService` stream Memory leak
- `H2BinaryDatabase` binary size (used to be utf-8 length instead of octet length)

### Removals
- `BinaryDatabase.reserveBytes`

## [0.6.0] - 2022-08-08

### New Features

- Server CLI arguments
    - `-u` / `--unsafeCommunication`: allow unsafe (plain-text) communications
    - `-k` / `--privateKey`: file path to private key
    - `-cr` / `--certificate`: file path to certificate
    - `-crp` / `--certificatePassword`: certificate password
- Javadoc for most of the code base
- New metadata API
  - `ServerMetadataContainer`
  - `MetadataInterceptor`
  - `MetadataExtractingInterceptor`
  - `MetadataUtils`
  - `FullyQualifiedIdentifierUtils`
  - `ServerMetadataContainer`
- Add `SiLAServer.Builer` API:
    - `withPersistedTLS`
    - `addInterceptor`
    - `withMetadataExtractingInterceptor`
    - `withPersistedTLS`
- Add `ServerManager` API:
    - `allowUnsecureonnection`: setter & getter
    - `rootCertificate `: setter & getter
- Add `EncryptionUtils` API:
    - `readPrivateKey`
    - `readCertificate` (from File/String)
    - `writeCertificateToFile`
    - `writePrivateKeyToFile`
    - `writePEMObjToFile`
    - `writeCertificateToString`
- Add metadata server and test 

### Improvements

- Update most dependencies with vulnerabilities [Lift SonaType Report](https://sbom.lift.sonatype.com/report/T1-a0368c8f29fdaa555824-8712395c006910-1659841342-40509c7f189044f48a428942c1557946)
- Replace deprecated AdoptOpenJdk by Adoptium (Eclispe Temurin™)
- Replace UniteLabs reference by SiLA
- Insecure (plain-text) communication are deprecated and not enabled by default
- `SiLAServer` now automatically load or create certificate and private key 
- `ServerLoading.attemptConnectionWithServer` now takes a certificate and boolean option to accept insecure connection
- `ServerManager` does not blindly allow non-standard untrusted certificates
- `ChannelFactory`:
  - Add `getTLSEncryptedChannel` (with and without certificate)
  - Remove `withClientInterceptor`
- `ChannelBuilder` :
  - Add `withTLSEncryption` (with and without certificate)
  - Add `withoutEncryption`
  - Remove `builderWithoutEncryption`
  - Remove `builderWithEncryption`
- Fix `SelfSignedCertificate` to generate SiLA standard compliant self signed certificate
- `SiLAServerRegistration` supports certificate and send certificate in the mdns answers
- `SiLADiscovery` supports certificate provided in the mdns answers
- `ServerListener.serverAdded` optionally provided a certificate

### Bug Fixes

- Fix maximum mdns response size with custom forked jmdns library
- Fix not working insecure communication (`SiLAServer`, `ServerLoading`)
- Fix transitive dependencies issues (Dependency Hell) when using the library

### Removals

- Removed `FullQualifierUtils` replaced by `FullyQualifiedIdentifierUtils`
- Removed `SSLContextFactory`
- Removed `ChannelUtils`
  
## [0.5.0] - 2022-03-22

## [Template]

### New Features

- What has been Added

### Improvements

- What has been changed

### Bug Fixes

- What has been fixed

### Removals

- What has been removed

[unreleased]: https://gitlab.com/SiLA2/sila_java/-/compare/v0.14.0...master?from_project_id=4205706
[0.14.0]: https://gitlab.com/SiLA2/sila_java/-/compare/v0.13.0...v0.14.0?from_project_id=4205706
[0.13.0]: https://gitlab.com/SiLA2/sila_java/-/compare/v0.12.0...v0.13.0?from_project_id=4205706
[0.12.0]: https://gitlab.com/SiLA2/sila_java/-/compare/v0.11.0...v0.12.0?from_project_id=4205706
[0.11.0]: https://gitlab.com/SiLA2/sila_java/-/compare/v0.10.0...v0.11.0?from_project_id=4205706
[0.10.0]: https://gitlab.com/SiLA2/sila_java/-/compare/v0.9.0...v0.10.0?from_project_id=4205706
[0.9.0]: https://gitlab.com/SiLA2/sila_java/-/compare/v0.8.0...v0.9.0?from_project_id=4205706
[0.8.0]: https://gitlab.com/SiLA2/sila_java/-/compare/v0.7.0...v0.8.0?from_project_id=4205706
[0.7.0]: https://gitlab.com/SiLA2/sila_java/-/compare/v0.6.0...v0.7.0?from_project_id=4205706
[0.6.0]: https://gitlab.com/SiLA2/sila_java/-/compare/v0.5.0...v0.6.0?from_project_id=4205706
[0.5.0]: https://gitlab.com/SiLA2/sila_java/-/compare/v0.1.0...v0.5.0?from_project_id=4205706
