package sila_java.interoperability.client.features;


import com.google.common.collect.Lists;
import com.google.protobuf.util.JsonFormat;
import io.grpc.ManagedChannel;
import lombok.NonNull;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.SiLABinaryTransfer;
import sila2.org.silastandard.SiLAFramework;
import sila2.org.silastandard.test.binarytransfertest.v1.BinaryTransferTestGrpc;
import sila2.org.silastandard.test.binarytransfertest.v1.BinaryTransferTestOuterClass;
import sila_java.library.core.sila.types.SiLABinary;
import sila_java.library.core.sila.types.SiLAString;
import sila_java.library.manager.ServerManager;
import sila_java.library.manager.executor.BinaryDownloader;
import sila_java.library.manager.executor.BinaryUploader;
import sila_java.library.manager.executor.ExecutableServerCall;
import sila_java.library.manager.models.SiLACall;

import java.io.*;
import java.util.Iterator;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import static sila_java.library.core.sila.mapping.grpc.ProtoMapper.serializeToJson;

@Slf4j
public class BinaryTransferTest {
    private final UUID serverUUID;
    private BinaryTransferTestGrpc.BinaryTransferTestBlockingStub blockingStub;

    public BinaryTransferTest(final ManagedChannel channel, @NonNull final UUID serverUUID) {
        this.blockingStub = BinaryTransferTestGrpc.newBlockingStub(channel);
        this.serverUUID = serverUUID;
    }

    public void getFCPAffectedByMetadataString() {
        this.blockingStub.getFCPAffectedByMetadataString(BinaryTransferTestOuterClass.Get_FCPAffectedByMetadata_String_Parameters.newBuilder().build());
    }

    @SneakyThrows
    public void echoBinaryValue(byte[] bytes) {
        SiLAFramework.Binary binary = SiLABinary.fromBytes(bytes);
        if (bytes.length >= 2097152) {
            try (final InputStream targetStream = new ByteArrayInputStream(bytes)) {
                final BinaryUploader binaryUploader = new BinaryUploader(targetStream, bytes.length, "org.silastandard/test/BinaryTransferTest/v1/Command/EchoBinaryValue/Parameter/BinaryValue");
                final SiLACall siLACall = new SiLACall.Builder(
                        serverUUID, "org.silastandard/test/BinaryTransferTest/v1", "EchoBinaryValue", SiLACall.Type.UPLOAD_BINARY
                ).build();
                final String createBinaryResponse = ServerManager.getInstance().getServerCallManager()
                        .runAsync(ExecutableServerCall
                                .newBuilder(siLACall)
                                .withBinaryUploader(binaryUploader)
                                .build()
                        )
                        .get(20, TimeUnit.SECONDS);
                SiLABinaryTransfer.CreateBinaryResponse.Builder builder = SiLABinaryTransfer.CreateBinaryResponse.newBuilder();
                JsonFormat.parser().merge(createBinaryResponse, builder);
                binary = SiLABinary.fromBinaryTransferUUID(builder.getBinaryTransferUUID());
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }

        BinaryTransferTestOuterClass.EchoBinaryValue_Responses echoBinaryValueResponses = this.blockingStub.echoBinaryValue(
                BinaryTransferTestOuterClass.EchoBinaryValue_Parameters
                        .newBuilder()
                        .setBinaryValue(binary)
                        .build()
        );
        if (echoBinaryValueResponses.getReceivedValue().hasBinaryTransferUUID()) {
            try (final ByteArrayOutputStream os = new ByteArrayOutputStream()) {
                BinaryDownloader binaryDownloader = new BinaryDownloader(os, UUID.fromString(echoBinaryValueResponses.getReceivedValue().getBinaryTransferUUID()));
                final String binaryInfo = ServerManager.getInstance().getServerCallManager()
                        .runAsync(ExecutableServerCall
                                .newBuilder(SiLACall.fromBinaryDownload(serverUUID))
                                .withBinaryDownloader(binaryDownloader)
                                .build()
                        )
                        .get(20, TimeUnit.SECONDS);
                binaryDownloader.isValidAndCompleteOrThrow();
                SiLABinaryTransfer.GetBinaryInfoResponse.Builder builder = SiLABinaryTransfer.GetBinaryInfoResponse.newBuilder();
                JsonFormat.parser().merge(binaryInfo, builder);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    @SneakyThrows
    public void echoBinariesObservably(byte[] abcBytes, byte[] largeBinaryBytes, byte[] smallBinaryBytes) {
        SiLAFramework.Binary largeBinary;
        SiLAFramework.Binary smallBinary = SiLABinary.fromBytes(smallBinaryBytes);
        SiLAFramework.Binary abcBinary = SiLABinary.fromBytes(abcBytes);
        try (final InputStream targetStream = new ByteArrayInputStream(largeBinaryBytes)) {
            final BinaryUploader binaryUploader = new BinaryUploader(targetStream, largeBinaryBytes.length, "org.silastandard/test/BinaryTransferTest/v1/Command/EchoBinariesObservably/Parameter/Binaries");
            final SiLACall siLACall = new SiLACall.Builder(
                    serverUUID, "org.silastandard/test/BinaryTransferTest/v1", "EchoBinariesObservably", SiLACall.Type.UPLOAD_BINARY
            ).build();
            final String createBinaryResponse = ServerManager.getInstance().getServerCallManager()
                    .runAsync(ExecutableServerCall
                            .newBuilder(siLACall)
                            .withBinaryUploader(binaryUploader)
                            .build()
                    )
                    .get(20, TimeUnit.SECONDS);
            SiLABinaryTransfer.CreateBinaryResponse.Builder builder = SiLABinaryTransfer.CreateBinaryResponse.newBuilder();
            JsonFormat.parser().merge(createBinaryResponse, builder);
            largeBinary = SiLABinary.fromBinaryTransferUUID(builder.getBinaryTransferUUID());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        SiLAFramework.CommandConfirmation commandConfirmation = this.blockingStub.echoBinariesObservably(
                BinaryTransferTestOuterClass.EchoBinariesObservably_Parameters
                        .newBuilder()
                        .addAllBinaries(
                                Lists.newArrayList(
                                        abcBinary,
                                        largeBinary,
                                        smallBinary
                                )
                        ).build()
        );
        Iterator<BinaryTransferTestOuterClass.EchoBinariesObservably_IntermediateResponses> intermediateIterator
                = this.blockingStub.echoBinariesObservablyIntermediate(commandConfirmation.getCommandExecutionUUID());
        Iterator<SiLAFramework.ExecutionInfo> infoIterator =
                this.blockingStub.echoBinariesObservablyInfo(commandConfirmation.getCommandExecutionUUID());
        while (intermediateIterator.hasNext()) {
            BinaryTransferTestOuterClass.EchoBinariesObservably_IntermediateResponses next = intermediateIterator.next();
            if (next.getBinary().hasBinaryTransferUUID()) {
                try (final ByteArrayOutputStream os = new ByteArrayOutputStream()) {
                    BinaryDownloader binaryDownloader = new BinaryDownloader(os, UUID.fromString(next.getBinary().getBinaryTransferUUID()));
                    final String binaryInfo = ServerManager.getInstance().getServerCallManager()
                            .runAsync(ExecutableServerCall
                                    .newBuilder(SiLACall.fromBinaryDownload(serverUUID))
                                    .withBinaryDownloader(binaryDownloader)
                                    .build()
                            )
                            .get(20, TimeUnit.SECONDS);
                    binaryDownloader.isValidAndCompleteOrThrow();
                    SiLABinaryTransfer.GetBinaryInfoResponse.Builder builder = SiLABinaryTransfer.GetBinaryInfoResponse.newBuilder();
                    JsonFormat.parser().merge(binaryInfo, builder);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        }
        while (infoIterator.hasNext()) {
            SiLAFramework.ExecutionInfo.CommandStatus commandStatus = infoIterator.next().getCommandStatus();
            if (commandStatus == SiLAFramework.ExecutionInfo.CommandStatus.finishedSuccessfully || commandStatus == SiLAFramework.ExecutionInfo.CommandStatus.finishedWithError) {
                break;
            }
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
        BinaryTransferTestOuterClass.EchoBinariesObservably_Responses result =
                this.blockingStub.echoBinariesObservablyResult(commandConfirmation.getCommandExecutionUUID());
        if (result.getJointBinary().hasBinaryTransferUUID()) {
            try (final ByteArrayOutputStream os = new ByteArrayOutputStream()) {
                BinaryDownloader binaryDownloader = new BinaryDownloader(os, UUID.fromString(result.getJointBinary().getBinaryTransferUUID()));
                final String binaryInfo = ServerManager.getInstance().getServerCallManager()
                        .runAsync(ExecutableServerCall
                                .newBuilder(SiLACall.fromBinaryDownload(serverUUID))
                                .withBinaryDownloader(binaryDownloader)
                                .build()
                        )
                        .get(20, TimeUnit.SECONDS);
                binaryDownloader.isValidAndCompleteOrThrow();
                SiLABinaryTransfer.GetBinaryInfoResponse.Builder builder = SiLABinaryTransfer.GetBinaryInfoResponse.newBuilder();
                JsonFormat.parser().merge(binaryInfo, builder);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    @SneakyThrows
    public void binaryValueDirectly() {
        BinaryTransferTestOuterClass.Get_BinaryValueDirectly_Responses binaryValueDirectly = this.blockingStub.getBinaryValueDirectly(
                BinaryTransferTestOuterClass.Get_BinaryValueDirectly_Parameters.newBuilder().build()
        );
        if (binaryValueDirectly.getBinaryValueDirectly().hasBinaryTransferUUID()) {
            try (final ByteArrayOutputStream os = new ByteArrayOutputStream()) {
                BinaryDownloader binaryDownloader = new BinaryDownloader(os, UUID.fromString(binaryValueDirectly.getBinaryValueDirectly().getBinaryTransferUUID()));
                final String binaryInfo = ServerManager.getInstance().getServerCallManager()
                        .runAsync(ExecutableServerCall
                                .newBuilder(SiLACall.fromBinaryDownload(serverUUID))
                                .withBinaryDownloader(binaryDownloader)
                                .build()
                        )
                        .get(20, TimeUnit.SECONDS);
                binaryDownloader.isValidAndCompleteOrThrow();
                SiLABinaryTransfer.GetBinaryInfoResponse.Builder builder = SiLABinaryTransfer.GetBinaryInfoResponse.newBuilder();
                JsonFormat.parser().merge(binaryInfo, builder);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    @SneakyThrows
    public void binaryValueDownload() {
        BinaryTransferTestOuterClass.Get_BinaryValueDownload_Responses binaryValueDownload = this.blockingStub.getBinaryValueDownload(
                BinaryTransferTestOuterClass.Get_BinaryValueDownload_Parameters.newBuilder().build()
        );
        if (binaryValueDownload.getBinaryValueDownload().hasBinaryTransferUUID()) {
            try (final ByteArrayOutputStream os = new ByteArrayOutputStream()) {
                BinaryDownloader binaryDownloader = new BinaryDownloader(os, UUID.fromString(binaryValueDownload.getBinaryValueDownload().getBinaryTransferUUID()));
                final String binaryInfo = ServerManager.getInstance().getServerCallManager()
                        .runAsync(ExecutableServerCall
                                .newBuilder(SiLACall.fromBinaryDownload(serverUUID))
                                .withBinaryDownloader(binaryDownloader)
                                .build()
                        )
                        .get(20, TimeUnit.SECONDS);
                binaryDownloader.isValidAndCompleteOrThrow();
                SiLABinaryTransfer.GetBinaryInfoResponse.Builder builder = SiLABinaryTransfer.GetBinaryInfoResponse.newBuilder();
                JsonFormat.parser().merge(binaryInfo, builder);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    @SneakyThrows
    public void echoBinaryAndMetadataString(byte[] bytes) {
        final String metadatum = "{\"org.silastandard/test/BinaryTransferTest/v1/Metadata/String\":" +
                serializeToJson(
                        BinaryTransferTestOuterClass.Metadata_String
                                .newBuilder()
                                .setString(SiLAString.from("abc"))
                                .build()
                ) + "}";
        SiLAFramework.Binary binary = SiLABinary.fromBytes(bytes);
        if (bytes.length >= 2097152) {
            try (final InputStream targetStream = new ByteArrayInputStream(bytes)) {
                final BinaryUploader binaryUploader = new BinaryUploader(targetStream, bytes.length, "org.silastandard/test/BinaryTransferTest/v1/Command/EchoBinaryAndMetadataString/Parameter/Binary");
                final SiLACall siLACall = new SiLACall.Builder(
                        serverUUID, "org.silastandard/test/BinaryTransferTest/v1", "EchoBinaryAndMetadataString", SiLACall.Type.UPLOAD_BINARY
                ).withMetadata(metadatum).build();
                final String createBinaryResponse = ServerManager.getInstance().getServerCallManager()
                        .runAsync(ExecutableServerCall
                                .newBuilder(siLACall)
                                .withBinaryUploader(binaryUploader)
                                .build()
                        )
                        .get(20, TimeUnit.SECONDS);
                SiLABinaryTransfer.CreateBinaryResponse.Builder builder = SiLABinaryTransfer.CreateBinaryResponse.newBuilder();
                JsonFormat.parser().merge(createBinaryResponse, builder);
                binary = SiLABinary.fromBinaryTransferUUID(builder.getBinaryTransferUUID());
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        BinaryTransferTestOuterClass.EchoBinaryAndMetadataString_Parameters parameters = BinaryTransferTestOuterClass.EchoBinaryAndMetadataString_Parameters
                .newBuilder()
                .setBinary(binary)
                .build();
        final SiLACall siLACall = new SiLACall.Builder(
                serverUUID, "org.silastandard/test/BinaryTransferTest/v1", "EchoBinaryAndMetadataString", SiLACall.Type.UNOBSERVABLE_COMMAND
        ).withParameters(serializeToJson(parameters)).withMetadata(metadatum).build();
        final String response = ServerManager.getInstance().getServerCallManager()
                .runAsync(ExecutableServerCall
                        .newBuilder(siLACall)
                        .build()
                )
                .get(20, TimeUnit.SECONDS);

        BinaryTransferTestOuterClass.EchoBinaryAndMetadataString_Responses.Builder parsedResponse = BinaryTransferTestOuterClass.EchoBinaryAndMetadataString_Responses.newBuilder();
        JsonFormat.parser().merge(response, parsedResponse);
        if (parsedResponse.getBinary().hasBinaryTransferUUID()) {
            try (final ByteArrayOutputStream os = new ByteArrayOutputStream()) {
                BinaryDownloader binaryDownloader = new BinaryDownloader(os, UUID.fromString(parsedResponse.getBinary().getBinaryTransferUUID()));
                final String binaryInfo = ServerManager.getInstance().getServerCallManager()
                        .runAsync(ExecutableServerCall
                                .newBuilder(SiLACall.fromBinaryDownload(serverUUID))
                                .withBinaryDownloader(binaryDownloader)
                                .build()
                        )
                        .get(20, TimeUnit.SECONDS);
                binaryDownloader.isValidAndCompleteOrThrow();
                SiLABinaryTransfer.GetBinaryInfoResponse.Builder builder = SiLABinaryTransfer.GetBinaryInfoResponse.newBuilder();
                JsonFormat.parser().merge(binaryInfo, builder);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
