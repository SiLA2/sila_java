package sila_java.interoperability.client.features;

import io.grpc.ManagedChannel;
import sila2.org.silastandard.core.authenticationservice.v1.AuthenticationServiceGrpc;
import sila2.org.silastandard.core.authenticationservice.v1.AuthenticationServiceOuterClass;
import sila_java.library.core.sila.types.SiLAString;

import java.util.Collections;
import java.util.UUID;

public class Authentication {
    private final UUID serverUUID;
    private AuthenticationServiceGrpc.AuthenticationServiceBlockingStub blockingStub;

    public Authentication(final ManagedChannel channel, final UUID serverUUID) {
        this.blockingStub = AuthenticationServiceGrpc.newBlockingStub(channel);
        this.serverUUID = serverUUID;
    }

    public String login(String user, String password) {
        AuthenticationServiceOuterClass.Login_Responses login = this.blockingStub.login(
                AuthenticationServiceOuterClass.Login_Parameters
                        .newBuilder()
                        .setUserIdentification(SiLAString.from(user))
                        .setPassword(SiLAString.from(password))
                        .setRequestedServer(SiLAString.from(serverUUID.toString()))
                        .addAllRequestedFeatures(Collections.singleton(SiLAString.from("org.silastandard/test/AuthenticationTest/v1")))
                        .build()
        );
        return login.getAccessToken().getValue();
    }

    public void logout(String token) {
        AuthenticationServiceOuterClass.Logout_Responses logout = this.blockingStub.logout(
                AuthenticationServiceOuterClass.Logout_Parameters
                        .newBuilder()
                        .setAccessToken(SiLAString.from(token))
                        .build()
        );
    }
}
