package sila_java.interoperability.client.features;

import lombok.NonNull;
import lombok.SneakyThrows;
import sila2.org.silastandard.test.metadataprovider.v1.MetadataProviderOuterClass;
import sila_java.library.core.sila.types.SiLAInteger;
import sila_java.library.core.sila.types.SiLAString;
import sila_java.library.manager.ServerManager;
import sila_java.library.manager.executor.ExecutableServerCall;
import sila_java.library.manager.models.SiLACall;

import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import static sila_java.library.core.sila.mapping.grpc.ProtoMapper.serializeToJson;

public class MetadataConsumerTest {


    private final UUID serverUUID;

    public MetadataConsumerTest(@NonNull final UUID serverUUID) {
        this.serverUUID = serverUUID;
    }

    @SneakyThrows
    public void echoStringMetadata() {
        final String metadatum = "{\"org.silastandard/test/MetadataProvider/v1/Metadata/StringMetadata\":" +
                serializeToJson(
                        MetadataProviderOuterClass.Metadata_StringMetadata
                                .newBuilder()
                                .setStringMetadata(SiLAString.from("abc"))
                                .build()
                ) + "}";
        final SiLACall siLACall = new SiLACall.Builder(
                serverUUID, "org.silastandard/test/MetadataConsumerTest/v1", "EchoStringMetadata", SiLACall.Type.UNOBSERVABLE_COMMAND
        ).withMetadata(metadatum).build();
        final String createBinaryResponse = ServerManager.getInstance().getServerCallManager()
                .runAsync(ExecutableServerCall
                        .newBuilder(siLACall)
                        .build()
                )
                .get(20, TimeUnit.SECONDS);
    }

    @SneakyThrows
    public void getReceivedStringMetadata() {
        final String metadatum = "{\"org.silastandard/test/MetadataProvider/v1/Metadata/StringMetadata\":" +
                serializeToJson(
                        MetadataProviderOuterClass.Metadata_StringMetadata
                                .newBuilder()
                                .setStringMetadata(SiLAString.from("abc"))
                                .build()
                ) + "}";
        final SiLACall siLACall = new SiLACall.Builder(
                serverUUID, "org.silastandard/test/MetadataConsumerTest/v1", "ReceivedStringMetadata", SiLACall.Type.UNOBSERVABLE_PROPERTY
        ).withMetadata(metadatum).build();
        final String createBinaryResponse = ServerManager.getInstance().getServerCallManager()
                .runAsync(ExecutableServerCall
                        .newBuilder(siLACall)
                        .build()
                )
                .get(20, TimeUnit.SECONDS);
    }

    @SneakyThrows
    public void subscribeReceivedStringMetadataAsCharacters() {
        final String metadatum = "{\"org.silastandard/test/MetadataProvider/v1/Metadata/StringMetadata\":" +
                serializeToJson(
                        MetadataProviderOuterClass.Metadata_StringMetadata
                                .newBuilder()
                                .setStringMetadata(SiLAString.from("abc"))
                                .build()
                ) + "}";
        final SiLACall siLACall = new SiLACall.Builder(
                serverUUID, "org.silastandard/test/MetadataConsumerTest/v1", "ReceivedStringMetadataAsCharacters", SiLACall.Type.OBSERVABLE_PROPERTY
        ).withMetadata(metadatum).build();
        final String createBinaryResponse = ServerManager.getInstance().getServerCallManager()
                .runAsync(ExecutableServerCall
                        .newBuilder(siLACall)
                        .build()
                ).get(20, TimeUnit.SECONDS);
    }

    @SneakyThrows
    public void unpackMetadata() {
        final String metadatum = "{\"org.silastandard/test/MetadataProvider/v1/Metadata/TwoIntegersMetadata\":" +
                serializeToJson(
                        MetadataProviderOuterClass.Metadata_TwoIntegersMetadata
                                .newBuilder()
                                .setTwoIntegersMetadata(
                                        MetadataProviderOuterClass.Metadata_TwoIntegersMetadata.TwoIntegersMetadata_Struct
                                                .newBuilder()
                                                .setFirstInteger(SiLAInteger.from(123))
                                                .setSecondInteger(SiLAInteger.from(456))
                                                .build()
                                )
                                .build()
                ) + ",\n" +
                " \"org.silastandard/test/MetadataProvider/v1/Metadata/StringMetadata\":" +
                serializeToJson(
                        MetadataProviderOuterClass.Metadata_StringMetadata
                                .newBuilder()
                                .setStringMetadata(SiLAString.from("abc"))
                                .build()
                ) +
                "}";
        final SiLACall siLACall = new SiLACall.Builder(
                serverUUID, "org.silastandard/test/MetadataConsumerTest/v1", "UnpackMetadata", SiLACall.Type.UNOBSERVABLE_COMMAND
        ).withMetadata(metadatum).build();
        final String createBinaryResponse = ServerManager.getInstance().getServerCallManager()
                .runAsync(ExecutableServerCall
                        .newBuilder(siLACall)
                        .build()
                )
                .get(20, TimeUnit.SECONDS);
    }
}
