package sila_java.interoperability.client.features;

import io.grpc.ManagedChannel;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.test.observablepropertytest.v1.ObservablePropertyTestGrpc;
import sila2.org.silastandard.test.observablepropertytest.v1.ObservablePropertyTestOuterClass;
import sila_java.library.core.sila.types.SiLAInteger;

import java.util.Iterator;


@Slf4j
public class ObservablePropertyTest {
    private final ObservablePropertyTestGrpc.ObservablePropertyTestBlockingStub blockingStub;

    public ObservablePropertyTest(final ManagedChannel channel) {
        this.blockingStub = ObservablePropertyTestGrpc.newBlockingStub(channel);
    }

    public void setValue(long value) {
        log.info("Setting observable property value to {}", value);
        ObservablePropertyTestOuterClass.SetValue_Responses responses = this.blockingStub.setValue(
                ObservablePropertyTestOuterClass.SetValue_Parameters
                        .newBuilder()
                        .setValue(SiLAInteger.from(value))
                        .build()
        );
    }

    public void subscribeAlternating() {
        Iterator<ObservablePropertyTestOuterClass.Subscribe_Alternating_Responses> iterator =
                this.blockingStub.subscribeAlternating(ObservablePropertyTestOuterClass.Subscribe_Alternating_Parameters.newBuilder().build());
        if (iterator.next() != iterator.next()) {
            log.info("Observable property is alternating");
        } else {
            throw new RuntimeException("Observable property not alternating");
        }
    }

    public void subscribeEditable() {
        Iterator<ObservablePropertyTestOuterClass.Subscribe_Editable_Responses> iterator =
                this.blockingStub.subscribeEditable(ObservablePropertyTestOuterClass.Subscribe_Editable_Parameters.newBuilder().build());
        final long nextValue = iterator.next().getEditable().getValue() - 1;
        setValue(nextValue);
        if (iterator.next().getEditable().getValue() == nextValue) {
            log.info("Observable property is editable");
        } else {
            throw new RuntimeException("Observable property is not editable");

        }

    }

    public void subscribeFixedValue() {
        Iterator<ObservablePropertyTestOuterClass.Subscribe_FixedValue_Responses> iterator =
                this.blockingStub.subscribeFixedValue(ObservablePropertyTestOuterClass.Subscribe_FixedValue_Parameters.newBuilder().build());
        if (iterator.next().getFixedValue().getValue() == 42) {
            log.info("Observable property is fix");
        } else {
            throw new RuntimeException("Observable property is not fix");
        }
    }
}
