package sila_java.interoperability.client.features;

import io.grpc.ManagedChannel;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.core.silaservice.v1.SiLAServiceGrpc;
import sila2.org.silastandard.core.silaservice.v1.SiLAServiceOuterClass;
import sila_java.library.core.sila.types.SiLAString;


@Slf4j
public class SiLAServiceTest {
    private final SiLAServiceGrpc.SiLAServiceBlockingStub blockingStub;

    public SiLAServiceTest(@NonNull final ManagedChannel serviceChannel) {
        this.blockingStub = SiLAServiceGrpc.newBlockingStub(serviceChannel);
    }

    public void setServerName(@NonNull final String newServerName) {
        log.info("Setting server name to {}", newServerName);
        this.blockingStub.setServerName(
                SiLAServiceOuterClass.SetServerName_Parameters
                        .newBuilder()
                        .setServerName(SiLAString.from(newServerName))
                        .build()
        );

    }
}
