package sila_java.interoperability.client.features;


import io.grpc.ManagedChannel;
import sila2.org.silastandard.test.metadataprovider.v1.MetadataProviderGrpc;
import sila2.org.silastandard.test.metadataprovider.v1.MetadataProviderOuterClass;

public class MetadataProvider {

    private final MetadataProviderGrpc.MetadataProviderBlockingStub blockingStub;

    public MetadataProvider(final ManagedChannel channel) {
        this.blockingStub = MetadataProviderGrpc.newBlockingStub(channel);
    }

    public void getFCPAffectedByMetadataStringMetadata() {
        this.blockingStub.getFCPAffectedByMetadataStringMetadata(
                MetadataProviderOuterClass.Get_FCPAffectedByMetadata_StringMetadata_Parameters
                        .newBuilder()
                        .build()
        );
    }

    public void getFCPAffectedByMetadataTwoIntegersMetadata() {
        this.blockingStub.getFCPAffectedByMetadataTwoIntegersMetadata(
                MetadataProviderOuterClass.Get_FCPAffectedByMetadata_TwoIntegersMetadata_Parameters
                        .newBuilder()
                        .build()
        );
    }
}
