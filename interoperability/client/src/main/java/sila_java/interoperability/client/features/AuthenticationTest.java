package sila_java.interoperability.client.features;

import com.google.protobuf.util.JsonFormat;
import io.grpc.ManagedChannel;
import lombok.SneakyThrows;
import sila2.org.silastandard.SiLABinaryTransfer;
import sila2.org.silastandard.SiLAFramework;
import sila2.org.silastandard.core.authorizationservice.v1.AuthorizationServiceOuterClass;
import sila2.org.silastandard.test.authenticationtest.v1.AuthenticationTestGrpc;
import sila2.org.silastandard.test.authenticationtest.v1.AuthenticationTestOuterClass;
import sila_java.library.core.sila.types.SiLABinary;
import sila_java.library.core.sila.types.SiLAString;
import sila_java.library.manager.ServerManager;
import sila_java.library.manager.executor.BinaryUploader;
import sila_java.library.manager.executor.ExecutableServerCall;
import sila_java.library.manager.models.SiLACall;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import static sila_java.library.core.sila.mapping.grpc.ProtoMapper.serializeToJson;

public class AuthenticationTest {
    private final UUID serverUUID;

    private AuthenticationTestGrpc.AuthenticationTestBlockingStub blockingStub;

    public AuthenticationTest(final ManagedChannel channel, final UUID serverUUID) {
        this.blockingStub = AuthenticationTestGrpc.newBlockingStub(channel);
        this.serverUUID = serverUUID;
    }

    @SneakyThrows
    public void requiresToken(String token) {
        final String metadatum = "{\"org.silastandard/core/AuthorizationService/v1/Metadata/AccessToken\":" +
                serializeToJson(
                        AuthorizationServiceOuterClass.Metadata_AccessToken
                                .newBuilder()
                                .setAccessToken(SiLAString.from(token))
                                .build()
                ) + "}";
        final SiLACall siLACall = new SiLACall.Builder(
                serverUUID, "org.silastandard/test/AuthenticationTest/v1", "RequiresToken", SiLACall.Type.UNOBSERVABLE_COMMAND
        ).withMetadata(metadatum).build();
        final String response = ServerManager.getInstance().getServerCallManager()
                .runAsync(ExecutableServerCall
                        .newBuilder(siLACall)
                        .build()
                )
                .get(20, TimeUnit.SECONDS);
    }

    @SneakyThrows
    public void requiresTokenForBinaryUpload(String token, byte[] bytes) {
        final String metadatum = "{\"org.silastandard/core/AuthorizationService/v1/Metadata/AccessToken\":" +
                serializeToJson(
                        AuthorizationServiceOuterClass.Metadata_AccessToken
                                .newBuilder()
                                .setAccessToken(SiLAString.from(token))
                                .build()
                ) + "}";
        SiLAFramework.Binary binary = SiLABinary.fromBytes(bytes);
        if (bytes.length >= 2097152) {
            try (final InputStream targetStream = new ByteArrayInputStream(bytes)) {
                final BinaryUploader binaryUploader = new BinaryUploader(targetStream, bytes.length, "org.silastandard/test/AuthenticationTest/v1/Command/RequiresTokenForBinaryUpload/Parameter/BinaryToUpload");
                final SiLACall siLACall = new SiLACall.Builder(
                        serverUUID, "org.silastandard/test/AuthenticationTest/v1", "RequiresTokenForBinaryUpload", SiLACall.Type.UPLOAD_BINARY
                ).withMetadata(metadatum).build();
                final String createBinaryResponse = ServerManager.getInstance().getServerCallManager()
                        .runAsync(ExecutableServerCall
                                .newBuilder(siLACall)
                                .withBinaryUploader(binaryUploader)
                                .build()
                        )
                        .get(20, TimeUnit.SECONDS);
                SiLABinaryTransfer.CreateBinaryResponse.Builder builder = SiLABinaryTransfer.CreateBinaryResponse.newBuilder();
                JsonFormat.parser().merge(createBinaryResponse, builder);
                binary = SiLABinary.fromBinaryTransferUUID(builder.getBinaryTransferUUID());
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        Thread.sleep(1000);
        AuthenticationTestOuterClass.RequiresTokenForBinaryUpload_Parameters parameters = AuthenticationTestOuterClass.RequiresTokenForBinaryUpload_Parameters
                .newBuilder()
                .setBinaryToUpload(binary)
                .build();
        final SiLACall siLACall = new SiLACall.Builder(
                serverUUID, "org.silastandard/test/AuthenticationTest/v1", "RequiresTokenForBinaryUpload", SiLACall.Type.UNOBSERVABLE_COMMAND
        ).withParameters(serializeToJson(parameters)).withMetadata(metadatum).build();
        final String response = ServerManager.getInstance().getServerCallManager()
                .runAsync(ExecutableServerCall
                        .newBuilder(siLACall)
                        .build()
                )
                .get(20, TimeUnit.SECONDS);
    }
}
