package sila_java.interoperability.client.features;

import io.grpc.ManagedChannel;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.SiLAFramework;
import sila2.org.silastandard.test.observablecommandtest.v1.ObservableCommandTestGrpc;
import sila2.org.silastandard.test.observablecommandtest.v1.ObservableCommandTestOuterClass;
import sila_java.library.core.sila.types.SiLAInteger;
import sila_java.library.core.sila.types.SiLAReal;

import java.util.Iterator;

@Slf4j
public class ObservableCommandTest {

    private final ObservableCommandTestGrpc.ObservableCommandTestBlockingStub blockingStub;

    public ObservableCommandTest(final ManagedChannel channel) {
        this.blockingStub = ObservableCommandTestGrpc.newBlockingStub(channel);
    }

    public void count(double seconds, int nb) {
        SiLAFramework.CommandConfirmation count = this.blockingStub.count(
                ObservableCommandTestOuterClass.Count_Parameters
                        .newBuilder()
                        .setDelay(SiLAReal.from(seconds))
                        .setN(SiLAInteger.from(nb))
                        .build()
        );
        Iterator<ObservableCommandTestOuterClass.Count_IntermediateResponses> intermediateIterator
                = this.blockingStub.countIntermediate(count.getCommandExecutionUUID());
        Iterator<SiLAFramework.ExecutionInfo> infoIterator =
                this.blockingStub.countInfo(count.getCommandExecutionUUID());
        SiLAFramework.ExecutionInfo.CommandStatus commandStatus = infoIterator.next().getCommandStatus();
        if (commandStatus == SiLAFramework.ExecutionInfo.CommandStatus.waiting || commandStatus == SiLAFramework.ExecutionInfo.CommandStatus.running) {
            log.info("Command request has been received {}", count.getCommandExecutionUUID().getValue());
        } else {
            throw new RuntimeException("Invalid observable command status");
        }
        long it = 0;
        while (it < (nb - 1)) {
            it = intermediateIterator.next().getCurrentIteration().getValue();
        }
        while (commandStatus != SiLAFramework.ExecutionInfo.CommandStatus.finishedSuccessfully && commandStatus != SiLAFramework.ExecutionInfo.CommandStatus.finishedWithError) {
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            commandStatus = infoIterator.next().getCommandStatus();
        }
        ObservableCommandTestOuterClass.Count_Responses result =
                this.blockingStub.countResult(count.getCommandExecutionUUID());
        if (result.getIterationResponse().getValue() != (nb - 1)) {
            throw new RuntimeException("Invalid count result");
        } else {
            log.info("Received valid count result {}", result.getIterationResponse().getValue());
        }
    }

    public void echo(double seconds, long value) {
        SiLAFramework.CommandConfirmation echo = this.blockingStub.echoValueAfterDelay(
                ObservableCommandTestOuterClass.EchoValueAfterDelay_Parameters
                        .newBuilder()
                        .setValue(SiLAInteger.from(value))
                        .setDelay(SiLAReal.from(seconds))
                        .build()
        );
        Iterator<SiLAFramework.ExecutionInfo> infoIterator =
                this.blockingStub.echoValueAfterDelayInfo(echo.getCommandExecutionUUID());
        SiLAFramework.ExecutionInfo.CommandStatus commandStatus = infoIterator.next().getCommandStatus();
        if (commandStatus == SiLAFramework.ExecutionInfo.CommandStatus.waiting || commandStatus == SiLAFramework.ExecutionInfo.CommandStatus.running) {
            log.info("Command request has been received {}", echo.getCommandExecutionUUID().getValue());
        } else {
            throw new RuntimeException("Invalid observable command status");
        }
        while (commandStatus != SiLAFramework.ExecutionInfo.CommandStatus.finishedSuccessfully && commandStatus != SiLAFramework.ExecutionInfo.CommandStatus.finishedWithError) {
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            commandStatus = infoIterator.next().getCommandStatus();
        }
        ObservableCommandTestOuterClass.EchoValueAfterDelay_Responses result =
                this.blockingStub.echoValueAfterDelayResult(echo.getCommandExecutionUUID());
        if (result.getReceivedValue().getValue() != value) {
            throw new RuntimeException("Invalid echo result");
        } else {
            log.info("Received valid echo result {}", result.getReceivedValue().getValue());
        }
    }
}
