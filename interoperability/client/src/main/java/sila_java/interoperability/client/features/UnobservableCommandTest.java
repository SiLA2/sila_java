package sila_java.interoperability.client.features;


import io.grpc.ManagedChannel;
import io.grpc.Status;
import io.grpc.StatusRuntimeException;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.test.unobservablecommandtest.v1.UnobservableCommandTestGrpc;
import sila2.org.silastandard.test.unobservablecommandtest.v1.UnobservableCommandTestOuterClass;
import sila_java.library.core.sila.types.SiLAInteger;
import sila_java.library.core.sila.types.SiLAString;

@Slf4j
public class UnobservableCommandTest {

    private final UnobservableCommandTestGrpc.UnobservableCommandTestBlockingStub blockingStub;

    public UnobservableCommandTest(final ManagedChannel channel) {
        this.blockingStub = UnobservableCommandTestGrpc.newBlockingStub(channel);
    }

    public void commandWithParametersAndResponses() {
        try {
            UnobservableCommandTestOuterClass.CommandWithoutParametersAndResponses_Responses responses =
                    this.blockingStub.commandWithoutParametersAndResponses(
                            UnobservableCommandTestOuterClass.CommandWithoutParametersAndResponses_Parameters.newBuilder().build()
                    );
        } catch (StatusRuntimeException e) {
            if (e.getStatus().getCode() == Status.Code.CANCELLED) {
                log.info("Command without responses");
                return;
            }
            throw e;
        }
    }

    public String convertIntegerToString(final int integer) {
        UnobservableCommandTestOuterClass.ConvertIntegerToString_Responses responses = this.blockingStub.convertIntegerToString(
                UnobservableCommandTestOuterClass.ConvertIntegerToString_Parameters
                        .newBuilder()
                        .setInteger(SiLAInteger.from(integer))
                        .build()
        );
        log.info("String representation of {} is: {}", integer, responses.getStringRepresentation().getValue());
        return responses.getStringRepresentation().getValue();
    }

    public String joinIntegerAndString(final int integer, final String str) {
        UnobservableCommandTestOuterClass.JoinIntegerAndString_Responses responses = this.blockingStub.joinIntegerAndString(
                UnobservableCommandTestOuterClass.JoinIntegerAndString_Parameters
                        .newBuilder()
                        .setInteger(SiLAInteger.from(integer))
                        .setString(SiLAString.from(str))
                        .build()
        );
        log.info("Joined integer {} and string {} is: {}", integer, str, responses.getJoinedParameters().getValue());
        return responses.getJoinedParameters().getValue();
    }

    public String[] splitStringAfterFirstChar(final String str) {
        UnobservableCommandTestOuterClass.SplitStringAfterFirstCharacter_Responses responses = this.blockingStub.splitStringAfterFirstCharacter(
                UnobservableCommandTestOuterClass.SplitStringAfterFirstCharacter_Parameters
                        .newBuilder()
                        .setString(SiLAString.from(str))
                        .build()
        );
        log.info("String {} split after first char is: {} and reminder: {}", str, responses.getFirstCharacter().getValue(), responses.getRemainder());
        return new String[]{responses.getFirstCharacter().getValue(), responses.getRemainder().getValue()};
    }
}
