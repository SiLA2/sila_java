package sila_java.interoperability.client.features;


import io.grpc.ManagedChannel;
import io.grpc.StatusRuntimeException;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.SiLAFramework;
import sila2.org.silastandard.test.errorhandlingtest.v1.ErrorHandlingTestGrpc;
import sila2.org.silastandard.test.errorhandlingtest.v1.ErrorHandlingTestOuterClass;

import java.util.Iterator;

@Slf4j
public class ErrorHandlingTest {

    private final ErrorHandlingTestGrpc.ErrorHandlingTestBlockingStub blockingStub;

    public ErrorHandlingTest(final ManagedChannel channel) {
        this.blockingStub = ErrorHandlingTestGrpc.newBlockingStub(channel);
    }


    ///         defined error
    ///////////////////////////////////

    @SneakyThrows
    public void raiseDefinedExecutionErrorObservably() {
        SiLAFramework.CommandConfirmation commandConfirmation = this.blockingStub.raiseDefinedExecutionErrorObservably(
                ErrorHandlingTestOuterClass.RaiseDefinedExecutionErrorObservably_Parameters.newBuilder().build()
        );
        Thread.sleep(1000);
        try {
            this.blockingStub.raiseDefinedExecutionErrorObservablyResult(commandConfirmation.getCommandExecutionUUID());
        } catch (StatusRuntimeException e) {
            log.info("Received a defined execution error observably as expected");
        }
    }

    public void raiseDefinedExecutionErrorOnSubscribe() {
        SiLAFramework.CommandConfirmation commandConfirmation = this.blockingStub.raiseDefinedExecutionErrorObservably(
                ErrorHandlingTestOuterClass.RaiseDefinedExecutionErrorObservably_Parameters.newBuilder().build()
        );
        try {
            this.blockingStub.subscribeRaiseDefinedExecutionErrorOnSubscribe(
                    ErrorHandlingTestOuterClass.Subscribe_RaiseDefinedExecutionErrorOnSubscribe_Parameters
                            .newBuilder()
                            .build()
            );
        } catch (StatusRuntimeException e) {
            log.info("Received a defined execution error on subscribe as expected");
        }
    }

    public void raiseDefinedExecutionErrorAfterValueWasSent() {
        try {
            Iterator<ErrorHandlingTestOuterClass.Subscribe_RaiseDefinedExecutionErrorAfterValueWasSent_Responses> response =
                    this.blockingStub.subscribeRaiseDefinedExecutionErrorAfterValueWasSent(
                            ErrorHandlingTestOuterClass.Subscribe_RaiseDefinedExecutionErrorAfterValueWasSent_Parameters.newBuilder().build()
                    );
            response.next();
        } catch (StatusRuntimeException e) {
            log.info("Received a defined execution error after value was sent as expected");
        }
    }

    public void raiseDefinedExecutionError() {
        try {
            this.blockingStub.raiseDefinedExecutionError(
                    ErrorHandlingTestOuterClass.RaiseDefinedExecutionError_Parameters.newBuilder().build()
            );
        } catch (StatusRuntimeException e) {
            log.info("Received a defined execution error as expected");
        }
    }

    public void raiseDefinedExecutionErrorOnGet() {
        try {
            this.blockingStub.getRaiseDefinedExecutionErrorOnGet(
                    ErrorHandlingTestOuterClass.Get_RaiseDefinedExecutionErrorOnGet_Parameters.newBuilder().build()
            );
        } catch (StatusRuntimeException e) {
            log.info("Received a defined execution error on get as expected");
        }
    }

    ///         undefined error
    ///////////////////////////////////

    @SneakyThrows
    public void raiseUndefinedExecutionErrorObservably() {
        SiLAFramework.CommandConfirmation commandConfirmation = this.blockingStub.raiseUndefinedExecutionErrorObservably(
                ErrorHandlingTestOuterClass.RaiseUndefinedExecutionErrorObservably_Parameters.newBuilder().build()
        );
        Thread.sleep(1000);
        try {
            this.blockingStub.raiseUndefinedExecutionErrorObservablyResult(commandConfirmation.getCommandExecutionUUID());
        } catch (StatusRuntimeException e) {
            log.info("Received a undefined execution error observably as expected");
        }
    }
    public void raiseUndefinedExecutionErrorOnSubscribe() {
        SiLAFramework.CommandConfirmation commandConfirmation = this.blockingStub.raiseUndefinedExecutionErrorObservably(
                ErrorHandlingTestOuterClass.RaiseUndefinedExecutionErrorObservably_Parameters.newBuilder().build()
        );
        try {
            this.blockingStub.subscribeRaiseUndefinedExecutionErrorOnSubscribe(
                    ErrorHandlingTestOuterClass.Subscribe_RaiseUndefinedExecutionErrorOnSubscribe_Parameters
                            .newBuilder()
                            .build()
            );
        } catch (StatusRuntimeException e) {
            log.info("Received a undefined execution error on subscribe as expected");
        }
    }
    public void raiseUndefinedExecutionErrorAfterValueWasSent() {
        try {
            Iterator<ErrorHandlingTestOuterClass.Subscribe_RaiseUndefinedExecutionErrorAfterValueWasSent_Responses> response =
                    this.blockingStub.subscribeRaiseUndefinedExecutionErrorAfterValueWasSent(
                            ErrorHandlingTestOuterClass.Subscribe_RaiseUndefinedExecutionErrorAfterValueWasSent_Parameters.newBuilder().build()
                    );
            response.next();
        } catch (StatusRuntimeException e) {
            log.info("Received a undefined execution error after value was sent as expected");
        }
    }

    public void raiseUndefinedExecutionError() {
        try {
            this.blockingStub.raiseUndefinedExecutionError(
                    ErrorHandlingTestOuterClass.RaiseUndefinedExecutionError_Parameters.newBuilder().build()
            );
        } catch (StatusRuntimeException e) {
            log.info("Received a undefined execution error as expected");
        }
    }

    public void raiseUndefinedExecutionErrorOnGet() {
        try {
            this.blockingStub.getRaiseUndefinedExecutionErrorOnGet(
                    ErrorHandlingTestOuterClass.Get_RaiseUndefinedExecutionErrorOnGet_Parameters.newBuilder().build()
            );
        } catch (StatusRuntimeException e) {
            log.info("Received a undefined execution error on get as expected");
        }
    }
}
