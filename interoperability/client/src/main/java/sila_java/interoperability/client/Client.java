package sila_java.interoperability.client;

import io.grpc.ManagedChannel;
import lombok.NonNull;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import sila_java.interoperability.client.features.*;
import sila_java.library.manager.ServerManager;
import sila_java.library.manager.models.Server;
import sila_java.library.manager.server_management.Connection;

import java.nio.charset.StandardCharsets;
import java.util.UUID;

@Slf4j
public class Client {
    public static final String SERVER_TYPE = "TestServer";
    private final UnobservablePropertyTest unobservablePropertyTest;
    private final UnobservableCommandTest unobservableCommandTest;
    private final ObservablePropertyTest observablePropertyTest;
    private final ObservableCommandTest observableCommandTest;
    private final MetadataProvider metadataProvider;
    private final MetadataConsumerTest metadataConsumerTest;
    private final ErrorHandlingTest errorHandlingTest;
    private final BinaryTransferTest binaryTransferTest;
    private final SiLAServiceTest silaServiceTest;
    private final Authentication authentication;
    private final AuthenticationTest authenticationTest;
    private final AuthorizationTest authorizationTest;

    public Client(@NonNull final UUID serverUUID) {
        final Connection connection = ServerManager.getInstance().getConnections().get(serverUUID);
        final ManagedChannel serviceChannel = connection.getManagedChannel();
        this.silaServiceTest = new SiLAServiceTest(serviceChannel);
        this.unobservablePropertyTest = new UnobservablePropertyTest(serviceChannel);
        this.unobservableCommandTest = new UnobservableCommandTest(serviceChannel);
        this.observablePropertyTest = new ObservablePropertyTest(serviceChannel);
        this.observableCommandTest = new ObservableCommandTest(serviceChannel);
        this.metadataProvider = new MetadataProvider(serviceChannel);
        this.metadataConsumerTest = new MetadataConsumerTest(serverUUID);
        this.errorHandlingTest = new ErrorHandlingTest(serviceChannel);
        this.binaryTransferTest = new BinaryTransferTest(serviceChannel, serverUUID);
        this.authentication = new Authentication(serviceChannel, serverUUID);
        this.authenticationTest = new AuthenticationTest(serviceChannel, serverUUID);
        this.authorizationTest = new AuthorizationTest(serviceChannel);
    }

    /**
     * Simple Client that stops after using the GreetingProvider Feature
     */
    @SneakyThrows
    public static void main(String[] args) {
        try (final ServerManager serverManager = ServerManager.getInstance()) {
            serverManager.setAllowInsecureConnection(true);

            // Create Manager for clients and start discovery
            //final Server server = ServerFinder
            //        .filterBy(ServerFinder.Filter.type(SERVER_TYPE))
            //        .scanAndFindOne(Duration.ofMinutes(1))
            //        .orElseThrow(() -> new RuntimeException("No " + SERVER_TYPE + " found within time"));

            ServerManager.getInstance().addServer("127.0.0.1", 50053);
            final Server server = ServerManager.getInstance().getServers().values()
                    .stream()
                    .filter(s -> s.getInformation().getType().equals(SERVER_TYPE))
                    .findAny()
                    .orElseThrow(RuntimeException::new);

            log.info("Found Server!");

            try {
                final Client client = new Client(server.getConfiguration().getUuid());
                client.runSiLAServiceTest();
                client.runUnobservablePropertyTest();
                client.runUnobservableCommandTest();
                client.runObservablePropertyTest();
                client.runObservableCommandTest();
                client.runMetadataProvider();
                client.runMetadataConsumerTest();
                client.runErrorHandlingTest();
                client.authorizationTest();
                client.authentication();
                client.authenticationTest();
                client.runBinaryTransferTest();
            } finally {
                //ServerManager.getInstance().close();
            }
        }
    }

    public void runBinaryTransferTest() {
        binaryTransferTest.echoBinaryValue(StringUtils.repeat("abc", 1).getBytes(StandardCharsets.UTF_8));
        binaryTransferTest.echoBinaryValue(StringUtils.repeat("SiLA2_Test_String_Value", 1).getBytes(StandardCharsets.UTF_8));
        binaryTransferTest.echoBinaryValue(StringUtils.repeat("abc", 1000000).getBytes(StandardCharsets.UTF_8));

        binaryTransferTest.echoBinariesObservably(
            StringUtils.repeat("abc", 1).getBytes(StandardCharsets.UTF_8),
            StringUtils.repeat("abc", 1000000).getBytes(StandardCharsets.UTF_8),
            StringUtils.repeat("SiLA2_Test_String_Value", 1).getBytes(StandardCharsets.UTF_8)
        );

        binaryTransferTest.echoBinaryAndMetadataString(StringUtils.repeat("abc", 1).getBytes(StandardCharsets.UTF_8));
        binaryTransferTest.echoBinaryAndMetadataString(StringUtils.repeat("abc", 1000000).getBytes(StandardCharsets.UTF_8));
        binaryTransferTest.binaryValueDownload();
        binaryTransferTest.binaryValueDirectly();
        binaryTransferTest.getFCPAffectedByMetadataString();
    }

    public void runErrorHandlingTest() {
        errorHandlingTest.raiseDefinedExecutionError();
        errorHandlingTest.raiseDefinedExecutionErrorObservably();
        errorHandlingTest.raiseUndefinedExecutionError();
        errorHandlingTest.raiseUndefinedExecutionErrorObservably();
        errorHandlingTest.raiseDefinedExecutionErrorOnGet();
        errorHandlingTest.raiseDefinedExecutionErrorOnSubscribe();
        errorHandlingTest.raiseUndefinedExecutionErrorOnGet();
        errorHandlingTest.raiseUndefinedExecutionErrorOnSubscribe();
        errorHandlingTest.raiseDefinedExecutionErrorAfterValueWasSent();
        errorHandlingTest.raiseUndefinedExecutionErrorAfterValueWasSent();
    }

    public void runMetadataConsumerTest() {
        metadataConsumerTest.echoStringMetadata();
        metadataConsumerTest.unpackMetadata();
        metadataConsumerTest.getReceivedStringMetadata();
        metadataConsumerTest.subscribeReceivedStringMetadataAsCharacters();
    }

    public void runMetadataProvider() {
        metadataProvider.getFCPAffectedByMetadataStringMetadata();
        metadataProvider.getFCPAffectedByMetadataTwoIntegersMetadata();
    }

    public void runObservableCommandTest() {
        observableCommandTest.count(1, 5);
        observableCommandTest.echo(5, 3);
    }

    public void runObservablePropertyTest() {
        observablePropertyTest.subscribeFixedValue();
        observablePropertyTest.subscribeAlternating();
        observablePropertyTest.subscribeEditable();
        observablePropertyTest.setValue(1);
        observablePropertyTest.setValue(2);
        observablePropertyTest.setValue(3);
    }

    public void runUnobservableCommandTest() {
        unobservableCommandTest.commandWithParametersAndResponses();
        unobservableCommandTest.convertIntegerToString(12345);
        unobservableCommandTest.joinIntegerAndString(123, "abc");
        unobservableCommandTest.splitStringAfterFirstChar("");
        unobservableCommandTest.splitStringAfterFirstChar("a");
        unobservableCommandTest.splitStringAfterFirstChar("ab");
        unobservableCommandTest.splitStringAfterFirstChar("abcde");
    }
    public void runUnobservablePropertyTest() {
        unobservablePropertyTest.answerToEverything();
        unobservablePropertyTest.secondsSince1970();
    }

    public void authentication() {
        String token = authentication.login("test", "test");
        authentication.logout(token);
    }

    public void authenticationTest() {
        String token = authentication.login("test", "test");
        authenticationTest.requiresToken(token);
        authenticationTest.requiresTokenForBinaryUpload(token, StringUtils.repeat("abc", 1).getBytes(StandardCharsets.UTF_8));
        authenticationTest.requiresTokenForBinaryUpload(token, StringUtils.repeat("abc", 1000000).getBytes(StandardCharsets.UTF_8));
        authentication.logout(token);
    }

    public void authorizationTest() {
        authorizationTest.getFCPAffectedByMetadataAccessToken();
    }

    @SneakyThrows
    public void runSiLAServiceTest() {
        silaServiceTest.setServerName("SiLA is Awesome");
    }
}