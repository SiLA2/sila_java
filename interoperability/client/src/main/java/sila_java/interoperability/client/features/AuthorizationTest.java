package sila_java.interoperability.client.features;

import io.grpc.ManagedChannel;
import sila2.org.silastandard.core.authorizationservice.v1.AuthorizationServiceGrpc;
import sila2.org.silastandard.core.authorizationservice.v1.AuthorizationServiceOuterClass;

public class AuthorizationTest {
    private AuthorizationServiceGrpc.AuthorizationServiceBlockingStub blockingStub;

    public AuthorizationTest(final ManagedChannel channel) {
        this.blockingStub = AuthorizationServiceGrpc.newBlockingStub(channel);
    }

    public void getFCPAffectedByMetadataAccessToken() {
        AuthorizationServiceOuterClass.Get_FCPAffectedByMetadata_AccessToken_Responses affected = this.blockingStub.getFCPAffectedByMetadataAccessToken(
                AuthorizationServiceOuterClass.Get_FCPAffectedByMetadata_AccessToken_Parameters
                        .newBuilder()
                        .build()
        );
    }

}
