package sila_java.interoperability.client.features;

import io.grpc.ManagedChannel;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.test.unobservablepropertytest.v1.UnobservablePropertyTestGrpc;
import sila2.org.silastandard.test.unobservablepropertytest.v1.UnobservablePropertyTestOuterClass;

@Slf4j
public class UnobservablePropertyTest {
    private UnobservablePropertyTestGrpc.UnobservablePropertyTestBlockingStub blockingStub;

    public UnobservablePropertyTest(final ManagedChannel channel) {
        this.blockingStub = UnobservablePropertyTestGrpc.newBlockingStub(channel);
    }

    public long secondsSince1970() {
        UnobservablePropertyTestOuterClass.Get_SecondsSince1970_Responses answer = this.blockingStub.getSecondsSince1970(
                UnobservablePropertyTestOuterClass.Get_SecondsSince1970_Parameters.newBuilder().build()
        );
        log.info("Seconds since 1970 is: {}", answer.getSecondsSince1970().getValue());
        return answer.getSecondsSince1970().getValue();
    }

    public long answerToEverything() {
        UnobservablePropertyTestOuterClass.Get_AnswerToEverything_Responses answer = this.blockingStub.getAnswerToEverything(
                UnobservablePropertyTestOuterClass.Get_AnswerToEverything_Parameters.newBuilder().build()
        );
        log.info("Answer to everything is: {}", answer.getAnswerToEverything().getValue());
        return answer.getAnswerToEverything().getValue();
    }
}
