package sila_java.interoperability.server.features;

import com.google.common.base.Strings;
import com.google.protobuf.ByteString;
import io.grpc.StatusRuntimeException;
import io.grpc.stub.StreamObserver;
import sila2.org.silastandard.SiLABinaryTransfer;
import sila2.org.silastandard.SiLAFramework;
import sila2.org.silastandard.test.binarytransfertest.v1.BinaryTransferTestGrpc;
import sila2.org.silastandard.test.binarytransfertest.v1.BinaryTransferTestOuterClass;
import sila_java.library.core.sila.binary_transfer.BinaryInfo;
import sila_java.library.core.sila.binary_transfer.BinaryTransferErrorHandler;
import sila_java.library.core.sila.errors.SiLAErrors;
import sila_java.library.core.sila.types.SiLABinary;
import sila_java.library.core.sila.types.SiLABinaryTransferUUID;
import sila_java.library.core.sila.types.SiLAString;
import sila_java.library.server_base.binary_transfer.Binary;
import sila_java.library.server_base.binary_transfer.database.BinaryDatabase;
import sila_java.library.server_base.binary_transfer.database.BinaryDatabaseException;
import sila_java.library.server_base.command.observable.ObservableCommandManager;
import sila_java.library.server_base.command.observable.ObservableCommandTaskRunner;
import sila_java.library.server_base.command.observable.ObservableCommandWrapper;
import sila_java.library.server_base.command.observable.RunnableCommandTask;
import sila_java.library.server_base.metadata.ServerMetadataContainer;

import java.io.IOException;
import java.io.InputStream;
import java.io.SequenceInputStream;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.time.Duration;
import java.util.*;

import static sila_java.library.server_base.metadata.MetadataExtractingInterceptor.SILA_METADATA_KEY;

public class BinaryTransferTest extends BinaryTransferTestGrpc.BinaryTransferTestImplBase {
    private static final int MAX_CHUNK_SIZE = 2097152;

    private final BinaryDatabase binaryDatabase;

    private final ObservableCommandManager<
            BinaryTransferTestOuterClass.EchoBinariesObservably_Parameters,
            BinaryTransferTestOuterClass.EchoBinariesObservably_Responses
            > echoBinaryCommandManager;

    public BinaryTransferTest(BinaryDatabase binaryDatabase) {
        this.binaryDatabase = binaryDatabase;
        this.echoBinaryCommandManager = new ObservableCommandManager<>(
                new ObservableCommandTaskRunner(1, 8),
                new RunnableCommandTask<BinaryTransferTestOuterClass.EchoBinariesObservably_Parameters, BinaryTransferTestOuterClass.EchoBinariesObservably_Responses>() {
                    @Override
                    public BinaryTransferTestOuterClass.EchoBinariesObservably_Responses run(
                            ObservableCommandWrapper<
                                    BinaryTransferTestOuterClass.EchoBinariesObservably_Parameters,
                                    BinaryTransferTestOuterClass.EchoBinariesObservably_Responses
                                    > command
                    ) throws StatusRuntimeException {
                        final List<SiLAFramework.Binary> binaries = command.getParameter().getBinariesList();
                        long totalSize = 0;
                        for (final SiLAFramework.Binary binary : binaries) {
                            if (binary.hasValue()) {
                                totalSize += binary.getValue().size();
                                continue;
                            }
                            try {
                                final BinaryInfo info = binaryDatabase.getBinaryInfo(SiLABinaryTransferUUID.from(binary));
                                totalSize += info.getLength();
                            } catch (BinaryDatabaseException e) {
                                throw BinaryTransferErrorHandler.generateBinaryTransferError(SiLABinaryTransfer.BinaryTransferError.ErrorType.UNRECOGNIZED, "");
                            }
                        }

                        final ArrayList<InputStream> inputStreams = new ArrayList<>(binaries.size());
                        try {
                            for (final SiLAFramework.Binary binary : binaries) {
                                if (binary.hasValue()) {
                                    inputStreams.add(binary.getValue().newInput());
                                } else {
                                    try {
                                        final Binary largeBinary = binaryDatabase.getBinary(SiLABinaryTransferUUID.from(binary));
                                        inputStreams.add(largeBinary.getData().getBinaryStream());
                                    } catch (BinaryDatabaseException | SQLException e) {
                                        throw BinaryTransferErrorHandler.generateBinaryTransferError(SiLABinaryTransfer.BinaryTransferError.ErrorType.UNRECOGNIZED, "");
                                    }
                                }
                            }
                            final SequenceInputStream sequenceInputStream = new SequenceInputStream(Collections.enumeration(inputStreams));
                            final BinaryTransferTestOuterClass.EchoBinariesObservably_Responses response;
                            if (totalSize <= MAX_CHUNK_SIZE) {
                                try {
                                    response =
                                            BinaryTransferTestOuterClass.EchoBinariesObservably_Responses
                                                    .newBuilder()
                                                    .setJointBinary(SiLABinary.fromBytes(ByteString.readFrom(sequenceInputStream)))
                                                    .build();
                                } catch (IOException e) {
                                    throw BinaryTransferErrorHandler.generateBinaryTransferError(SiLABinaryTransfer.BinaryTransferError.ErrorType.UNRECOGNIZED, "");
                                }
                            } else {
                                final UUID newBinaryUUID = UUID.randomUUID();
                                try {
                                    binaryDatabase.addBinary(
                                            newBinaryUUID,
                                            sequenceInputStream,
                                            "org.silastandard/test/BinaryTransferTest/v1/Command/EchoBinariesObservably/Parameter/Binaries"
                                    );
                                    response = BinaryTransferTestOuterClass.EchoBinariesObservably_Responses
                                            .newBuilder()
                                            .setJointBinary(SiLABinary.fromBinaryTransferUUID(newBinaryUUID))
                                            .build();
                                } catch (BinaryDatabaseException e) {
                                    throw BinaryTransferErrorHandler.generateBinaryTransferError(
                                            SiLABinaryTransfer.BinaryTransferError.ErrorType.UNRECOGNIZED,
                                            "org.silastandard/test/BinaryTransferTest/v1/Command/EchoBinariesObservably/Parameter/Binaries"
                                    );
                                }
                            }
                            for (int i = 0; i < binaries.size(); i++) {
                                final SiLAFramework.Binary binary = binaries.get(i);
                                command.setExecutionInfoAndNotify(
                                        i / (binaries.size() - 1d),
                                        Duration.ofSeconds(4)
                                );
                                command.notifyIntermediateResponse(BinaryTransferTestOuterClass.EchoBinariesObservably_IntermediateResponses
                                        .newBuilder()
                                        .setBinary(binary)
                                        .build());
                                try {
                                    Thread.sleep(1000);
                                } catch (InterruptedException e) {
                                    throw new RuntimeException(e);
                                }
                            }
                            return response;
                        } finally {
                            silentlyCloseInputStreams(inputStreams);
                        }
                    }

                    @Override
                    public void validate(BinaryTransferTestOuterClass.EchoBinariesObservably_Parameters parameters) throws StatusRuntimeException {
                        if (parameters.getBinariesList().isEmpty()) {
                            throw SiLAErrors.generateValidationError(
                                    "org.silastandard/test/BinaryTransferTest/v1/Command/EchoBinariesObservably/Parameter/Binaries",
                                    "Empty Binaries parameter."
                            );
                        }
                        parameters.getBinariesList().forEach((b) -> {
                            if (b.hasBinaryTransferUUID()) {
                                try {
                                    if (!binaryDatabase.hasBinary(UUID.fromString(b.getBinaryTransferUUID()))) {
                                        throw SiLAErrors.generateValidationError(
                                                "org.silastandard/test/BinaryTransferTest/v1/Command/EchoBinariesObservably/Parameter/Binaries",
                                                "Invalid Binary Transfer UUID"
                                        );
                                    }
                                } catch (Exception e) {
                                    throw SiLAErrors.generateValidationError(
                                            "org.silastandard/test/BinaryTransferTest/v1/Command/EchoBinariesObservably/Parameter/Binaries",
                                            "Invalid Binary Transfer UUID"
                                    );
                                }
                            } else if (!b.hasValue()) {
                                throw SiLAErrors.generateValidationError(
                                        "org.silastandard/test/BinaryTransferTest/v1/Command/EchoBinariesObservably/Parameter/Binaries",
                                        "Empty Binary value."
                                );
                            }
                        });
                    }
                },
                Duration.ofSeconds(60)
        );
    }

    @Override
    public void echoBinaryValue(
            BinaryTransferTestOuterClass.EchoBinaryValue_Parameters request,
            StreamObserver<BinaryTransferTestOuterClass.EchoBinaryValue_Responses> responseObserver
    ) {
        if (!request.hasBinaryValue()) {
            throw SiLAErrors.generateValidationError(
                    "org.silastandard/test/BinaryTransferTest/v1/Command/EchoBinaryValue/Parameter/BinaryValue",
                    "Missing BinaryValue parameter."
            );
        }
        if (request.getBinaryValue().hasBinaryTransferUUID()) {
            try {
                if (!this.binaryDatabase.hasBinary(UUID.fromString(request.getBinaryValue().getBinaryTransferUUID()))) {
                    throw SiLAErrors.generateValidationError(
                            "org.silastandard/test/BinaryTransferTest/v1/Command/EchoBinaryValue/Parameter/BinaryValue",
                            "Invalid Binary Transfer UUID"
                    );
                }
            } catch (Exception e) {
                throw SiLAErrors.generateValidationError(
                        "org.silastandard/test/BinaryTransferTest/v1/Command/EchoBinaryValue/Parameter/BinaryValue",
                        "Invalid Binary Transfer UUID"
                );
            }
        } else if (!request.getBinaryValue().hasValue()) {
            throw SiLAErrors.generateValidationError(
                    "org.silastandard/test/BinaryTransferTest/v1/Command/EchoBinaryValue/Parameter/BinaryValue",
                    "Empty BinaryValue parameter."
            );
        }

        responseObserver.onNext(
                BinaryTransferTestOuterClass.EchoBinaryValue_Responses
                        .newBuilder()
                        .setReceivedValue(request.getBinaryValue())
                        .build()
        );
        responseObserver.onCompleted();
    }

    @Override
    public void echoBinariesObservably(
            BinaryTransferTestOuterClass.EchoBinariesObservably_Parameters request,
            StreamObserver<SiLAFramework.CommandConfirmation> responseObserver
    ) {
        this.echoBinaryCommandManager.addCommand(request, responseObserver);
    }

    @Override
    public void echoBinariesObservablyInfo(
            SiLAFramework.CommandExecutionUUID request,
            StreamObserver<SiLAFramework.ExecutionInfo> responseObserver
    ) {
        this.echoBinaryCommandManager.get(request).addStateObserver(responseObserver);
    }

    @Override
    public void echoBinariesObservablyIntermediate(
            SiLAFramework.CommandExecutionUUID request,
            StreamObserver<BinaryTransferTestOuterClass.EchoBinariesObservably_IntermediateResponses> responseObserver
    ) {
        this.echoBinaryCommandManager.get(request).addIntermediateResponseObserver(responseObserver);
    }

    @Override
    public void echoBinariesObservablyResult(
            SiLAFramework.CommandExecutionUUID request,
            StreamObserver<BinaryTransferTestOuterClass.EchoBinariesObservably_Responses> responseObserver
    ) {
        this.echoBinaryCommandManager.get(request).sendResult(responseObserver);
    }

    @Override
    public void echoBinaryAndMetadataString(
            BinaryTransferTestOuterClass.EchoBinaryAndMetadataString_Parameters request,
            StreamObserver<BinaryTransferTestOuterClass.EchoBinaryAndMetadataString_Responses> responseObserver
    ) {
        final ServerMetadataContainer serverMetadataContainer = SILA_METADATA_KEY.get();

        final BinaryTransferTestOuterClass.Metadata_String metadataString;
        try {
            metadataString = serverMetadataContainer.get(BinaryTransferTestOuterClass.Metadata_String.class);
        } catch (Exception e) {
            throw SiLAErrors.generateFrameworkError(SiLAFramework.FrameworkError.ErrorType.INVALID_METADATA, "Malformed metadata.");
        }
        if (metadataString == null || !metadataString.hasString()) {
            throw SiLAErrors.generateFrameworkError(SiLAFramework.FrameworkError.ErrorType.INVALID_METADATA, "Expect a metadata.");
        }

        if (request.hasBinary() && request.getBinary().hasBinaryTransferUUID()) {
            try {
                if (!binaryDatabase.hasBinary(UUID.fromString(request.getBinary().getBinaryTransferUUID()))) {
                    throw SiLAErrors.generateValidationError(
                            "org.silastandard/test/BinaryTransferTest/v1/Command/EchoBinaryAndMetadataString/Parameter/Binary",
                            "Invalid Binary Transfer UUID"
                    );
                }
            } catch (Exception e) {
                throw SiLAErrors.generateValidationError(
                        "org.silastandard/test/BinaryTransferTest/v1/Command/EchoBinaryAndMetadataString/Parameter/Binary",
                        "Invalid Binary Transfer UUID"
                );
            }
        } else if (!request.hasBinary() || !request.getBinary().hasValue()) {
            throw SiLAErrors.generateValidationError(
                    "org.silastandard/test/BinaryTransferTest/v1/Command/EchoBinaryAndMetadataString/Parameter/Binary",
                    "Empty Binary value."
            );
        }

        responseObserver.onNext(
                BinaryTransferTestOuterClass.EchoBinaryAndMetadataString_Responses
                        .newBuilder()
                        .setBinary(request.getBinary())
                        .setStringMetadata(metadataString.getString())
                        .build()
        );
        responseObserver.onCompleted();
    }

    @Override
    public void getBinaryValueDirectly(
            BinaryTransferTestOuterClass.Get_BinaryValueDirectly_Parameters request,
            StreamObserver<BinaryTransferTestOuterClass.Get_BinaryValueDirectly_Responses> responseObserver
    ) {
        responseObserver.onNext(
                BinaryTransferTestOuterClass.Get_BinaryValueDirectly_Responses
                        .newBuilder()
                        .setBinaryValueDirectly(
                                SiLABinary.fromBytes(
                                        ByteString.copyFrom("SiLA2_Test_String_Value", StandardCharsets.UTF_8)
                                )
                        )
                        .build()
        );
        responseObserver.onCompleted();
    }

    @Override
    public void getBinaryValueDownload(
            BinaryTransferTestOuterClass.Get_BinaryValueDownload_Parameters request,
            StreamObserver<BinaryTransferTestOuterClass.Get_BinaryValueDownload_Responses> responseObserver
    ) {
        final byte[] data = Strings.repeat(
                "A_slightly_longer_SiLA2_Test_String_Value_used_to_demonstrate_the_binary_download", 100_000
        ).getBytes(StandardCharsets.UTF_8);
        final UUID newBinaryUUID = UUID.randomUUID();
        try {
            this.binaryDatabase.addBinary(newBinaryUUID, data, "org.silastandard/test/BinaryTransferTest/v1/Command/EchoBinaryAndMetadataString/Parameter/Binary");
        } catch (BinaryDatabaseException e) {
            throw BinaryTransferErrorHandler.generateBinaryTransferError(
                    SiLABinaryTransfer.BinaryTransferError.ErrorType.INVALID_BINARY_TRANSFER_UUID, ""
            );
        }
        responseObserver.onNext(BinaryTransferTestOuterClass.Get_BinaryValueDownload_Responses
                .newBuilder()
                .setBinaryValueDownload(SiLABinary.fromBinaryTransferUUID(newBinaryUUID))
                .build()
        );
        responseObserver.onCompleted();
    }

    @Override
    public void getFCPAffectedByMetadataString(
            BinaryTransferTestOuterClass.Get_FCPAffectedByMetadata_String_Parameters request,
            StreamObserver<BinaryTransferTestOuterClass.Get_FCPAffectedByMetadata_String_Responses> responseObserver
    ) {
        responseObserver.onNext(
                BinaryTransferTestOuterClass.Get_FCPAffectedByMetadata_String_Responses
                        .newBuilder()
                        .addAllAffectedCalls(Collections.singletonList(SiLAString.from("org.silastandard/test/BinaryTransferTest/v1/Command/EchoBinaryAndMetadataString")))
                        .build()
        );
        responseObserver.onCompleted();
    }

    private static void silentlyCloseInputStreams(List<InputStream> inputStreams) {
        for (final InputStream inputStream : inputStreams) {
            try {
                inputStream.close();
            } catch (IOException ignored) {}
        }
    }
}
