package sila_java.interoperability.server.features;

import io.grpc.stub.StreamObserver;
import lombok.AllArgsConstructor;
import sila2.org.silastandard.SiLAFramework;
import sila2.org.silastandard.core.authenticationservice.v1.AuthenticationServiceGrpc;
import sila2.org.silastandard.core.authenticationservice.v1.AuthenticationServiceOuterClass;
import sila_java.interoperability.server.AuthTokens;
import sila_java.library.core.sila.errors.SiLAErrors;

import java.time.Instant;
import java.util.UUID;
import java.util.regex.Pattern;

@AllArgsConstructor
public class AuthenticationService extends AuthenticationServiceGrpc.AuthenticationServiceImplBase {
    public static final String FULLY_QUALIFIED_FEATURE_IDENTIFIER = "org.silastandard/core/AuthenticationService/v1";

    public static final String INVALID_ACCESS_TOKEN = "InvalidAccessToken";
    private static final String AUTHENTICATION_FAILED = "AuthenticationFailed";
    private static final Pattern SERVER_PATTERN = Pattern.compile("[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}");
    private static final Pattern FEATURE_PATTERN = Pattern.compile("[a-z][a-z.]*/[a-z][a-z.]*/[A-Z][a-zA-Z0-9]*/v\\d+");

    private final AuthTokens authTokens;


    @Override
    public void login(AuthenticationServiceOuterClass.Login_Parameters request, StreamObserver<AuthenticationServiceOuterClass.Login_Responses> responseObserver) {
        if (!request.hasUserIdentification()) {
            responseObserver.onError(SiLAErrors.generateValidationError(
                    "org.silastandard/core/AuthenticationService/v1/Command/Login/Parameter/UserIdentification",
                    "Missing one or more required parameters: UserIdentification"
            ));
            return;
        }
        if (!request.hasPassword()) {
            responseObserver.onError(SiLAErrors.generateValidationError(
                    "org.silastandard/core/AuthenticationService/v1/Command/Login/Parameter/Password",
                    "Missing one or more required parameters: Password"
            ));
            return;
        }

        if (!request.hasRequestedServer()) {
            responseObserver.onError(SiLAErrors.generateValidationError(
                    "org.silastandard/core/AuthenticationService/v1/Command/Login/Parameter/RequestedServer",
                    "Missing one or more required parameters: RequestedServer"
            ));
            return;
        }

        if (!SERVER_PATTERN.matcher(request.getRequestedServer().getValue()).matches()) {
            responseObserver.onError(SiLAErrors.generateValidationError(
                    "org.silastandard/core/AuthenticationService/v1/Command/Login/Parameter/RequestedServer",
                    "Parameter 'RequestedServer' does not match the required pattern"
            ));
            return;
        }

        for (SiLAFramework.String feature : request.getRequestedFeaturesList()) {
            if (!FEATURE_PATTERN.matcher(feature.getValue()).matches()) {
                responseObserver.onError(SiLAErrors.generateValidationError(
                        "org.silastandard/core/AuthenticationService/v1/Command/Login/Parameter/RequestedFeatures",
                        "Requested Feature parameter is not a fully qualified feature identifier: " + feature.getValue()
                ));
                return;
            }
        }

        if (!UUID.fromString(request.getRequestedServer().getValue()).equals(authTokens.getServerUuid())) {
            responseObserver.onError(SiLAErrors.generateValidationError(
                    "org.silastandard/core/AuthenticationService/v1/Command/Login/Parameter/RequestedFeatures",
                    "Unknown server UUID: " + request.getRequestedServer().getValue()
            ));
            return;
        }

        if (request.getRequestedFeaturesCount() != 1 || !request.getRequestedFeatures(0).getValue().equals("org.silastandard/test/AuthenticationTest/v1")) {
            responseObserver.onError(SiLAErrors.generateValidationError(
                    "org.silastandard/core/AuthenticationService/v1/Command/Login/Parameter/RequestedFeatures",
                    "Login is only possible for the 'Authentication Test' feature"
            ));
            return;
        }

        if (!request.getUserIdentification().getValue().equals("test") || !request.getPassword().getValue().equals("test")) {
            responseObserver.onError(SiLAErrors.generateValidationError(
                    "org.silastandard/core/AuthenticationService/v1/Command/Login/Parameter/RequestedFeatures",
                    "Invalid credentials"
            ));
            return;
        }

        String token = UUID.randomUUID().toString();
        int lifetimeSeconds = 60 * 10; // 10 minutes
        authTokens.getAuthTokens().put(token, Instant.now().plusSeconds(lifetimeSeconds));

        AuthenticationServiceOuterClass.Login_Responses response = AuthenticationServiceOuterClass.Login_Responses.newBuilder()
                .setAccessToken(SiLAFramework.String.newBuilder().setValue(token).build())
                .setTokenLifetime(SiLAFramework.Integer.newBuilder().setValue(lifetimeSeconds).build())
                .build();

        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }

    @Override
    public void logout(AuthenticationServiceOuterClass.Logout_Parameters request, StreamObserver<AuthenticationServiceOuterClass.Logout_Responses> responseObserver) {
        if (!request.hasAccessToken()) {
            responseObserver.onError(SiLAErrors.generateValidationError(
                    "org.silastandard/core/AuthenticationService/v1/Command/Logout/Parameter/AccessToken",
                    "Missing parameter 'AccessToken'"
            ));
            return;
        }

        Instant tokenExpiration = authTokens.getAuthTokens().get(request.getAccessToken().getValue());
        if (tokenExpiration == null || tokenExpiration.isBefore(Instant.now())) {
            responseObserver.onError(
                    SiLAErrors.generateDefinedExecutionError(
                            FULLY_QUALIFIED_FEATURE_IDENTIFIER + "/DefinedExecutionError/" + INVALID_ACCESS_TOKEN,
                            "Invalid access token"
                    )
            );
            return;
        }

        authTokens.getAuthTokens().remove(request.getAccessToken().getValue());

        AuthenticationServiceOuterClass.Logout_Responses response = AuthenticationServiceOuterClass.Logout_Responses.newBuilder().build();

        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }
}
