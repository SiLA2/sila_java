package sila_java.interoperability.server;

import java.io.IOException;
import java.sql.SQLException;
import java.time.Instant;
import java.util.Objects;
import java.util.UUID;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.SiLAFramework;
import sila2.org.silastandard.core.authorizationservice.v1.AuthorizationServiceOuterClass;
import sila_java.interoperability.server.features.*;
import sila_java.library.core.sila.errors.SiLAErrors;
import sila_java.library.server_base.SiLAServer;
import sila_java.library.server_base.binary_transfer.database.BinaryDatabase;
import sila_java.library.server_base.binary_transfer.database.impl.H2BinaryDatabase;
import sila_java.library.server_base.config.IServerConfigWrapper;
import sila_java.library.server_base.identification.ServerInformation;
import sila_java.library.server_base.metadata.ServerMetadataContainer;
import sila_java.library.server_base.standard_features.v1.AuthorizationController;
import sila_java.library.server_base.utils.ArgumentHelper;
import sila_java.library.sila_base.EmptyClass;
import static sila_java.library.core.utils.FileUtils.getFileContent;
import static sila_java.library.server_base.metadata.MetadataExtractingInterceptor.SILA_METADATA_KEY;

/**
 * Example minimal SiLA Server
 */
@Slf4j
public class Server implements AutoCloseable {
    public static final String SERVER_TYPE = "InteroperabilitySiLAServer";

    private final BinaryDatabase binaryDatabase;

    private final SiLAServer server;
    public static final ServerInformation serverInfo = new ServerInformation(
            SERVER_TYPE,
            "Interoperability SiLA Server",
            "https://www.sila-standard.org",
            "0.1"
    );
    private final IServerConfigWrapper configuration;

    private final AuthTokens authTokens;

    /**
     * Application Class using command line arguments
     *
     * @param argumentHelper Custom Argument Helper
     */
    Server(@NonNull final ArgumentHelper argumentHelper) {
        try {
            final SiLAServer.Builder builder = SiLAServer.Builder.newBuilder(serverInfo);

            builder.withPersistentConfig(argumentHelper.getConfigFile().isPresent());
            argumentHelper.getConfigFile().ifPresent(builder::withPersistentConfigFile);

            this.configuration = builder.getNewServerConfigurationWrapper();
            builder.withCustomConfigWrapperProvider((configPath, serverConfiguration) -> this.configuration);
            final UUID serverId = this.configuration.getCacheConfig().getUuid();
            this.authTokens = new AuthTokens(serverId);
            this.binaryDatabase = new H2BinaryDatabase(serverId);
            builder.withCustomBinaryDatabaseProvider((uuid) -> this.binaryDatabase);
            builder.withMetadataExtractingInterceptor();
            builder.withBinaryTransferSupport();
            builder.withUnsafeCommunication(true);

            builder.withPersistentTLS(
                    argumentHelper.getPrivateKeyFile(),
                    argumentHelper.getCertificateFile(),
                    argumentHelper.getCertificatePassword()
            );

            if (argumentHelper.useUnsafeCommunication()) {
                builder.withUnsafeCommunication(true);
            }

            argumentHelper.getHost().ifPresent(builder::withHost);
            argumentHelper.getPort().ifPresent(builder::withPort);
            argumentHelper.getInterface().ifPresent(builder::withNetworkInterface);
            builder.withDiscovery(argumentHelper.hasNetworkDiscovery());

            builder.withCustomAuthorizer((fqi) -> {
                if (!fqi.startsWith("org.silastandard/test/AuthenticationTest/v1")) {
                    return;
                }
                final ServerMetadataContainer serverMetadataContainer = SILA_METADATA_KEY.get();

                final AuthorizationServiceOuterClass.Metadata_AccessToken accessToken;
                try {
                    accessToken = serverMetadataContainer.get(AuthorizationServiceOuterClass.Metadata_AccessToken.class);
                } catch (Exception e) {
                    throw SiLAErrors.generateDefinedExecutionError(
                            AuthorizationController.FULLY_QUALIFIED_FEATURE_IDENTIFIER + "/DefinedExecutionError/" + AuthorizationController.INVALID_ACCESS_TOKEN,
                            "Invalid access token"
                    );
                }
                if (accessToken == null || !accessToken.hasAccessToken()) {
                    throw SiLAErrors.generateFrameworkError(SiLAFramework.FrameworkError.ErrorType.INVALID_METADATA, "Missing metadata 'Access Token'.");
                }

                String token = accessToken.getAccessToken().getValue();
                if (!authTokens.getAuthTokens().containsKey(token) || authTokens.getAuthTokens().get(token).isBefore(Instant.now())) {
                    throw SiLAErrors.generateDefinedExecutionError(
                            AuthorizationController.FULLY_QUALIFIED_FEATURE_IDENTIFIER + "/DefinedExecutionError/" + AuthorizationController.INVALID_ACCESS_TOKEN,
                            "Invalid access token"
                    );
                }
            });

            final String testFeaturesPath = "/sila_base/feature_definitions/org/silastandard/test/";
            final String baseFeaturesPath = "/sila_base/feature_definitions/org/silastandard/core/";

            builder.addFeature(
                    getFileContent(
                            Objects.requireNonNull(
                                    EmptyClass.class.getResourceAsStream(testFeaturesPath + "UnobservablePropertyTest-v1_0.sila.xml")
                            )
                    ),
                    new UnobservablePropertyTest()
            );
            builder.addFeature(
                    getFileContent(
                            Objects.requireNonNull(
                                    EmptyClass.class.getResourceAsStream(testFeaturesPath + "UnobservableCommandTest-v1_0.sila.xml")
                            )
                    ),
                    new UnobservableCommandTest()
            );
            builder.addFeature(
                    getFileContent(
                            Objects.requireNonNull(
                                    EmptyClass.class.getResourceAsStream(testFeaturesPath + "ObservablePropertyTest-v1_0.sila.xml")
                            )
                    ),
                    new ObservablePropertyTest()
            );
            builder.addFeature(
                    getFileContent(
                            Objects.requireNonNull(
                                    EmptyClass.class.getResourceAsStream(testFeaturesPath + "ObservableCommandTest-v1_0.sila.xml")
                            )
                    ),
                    new ObservableCommandTest()
            );
            builder.addFeature(
                    getFileContent(
                            Objects.requireNonNull(
                                    EmptyClass.class.getResourceAsStream(testFeaturesPath + "MetadataProvider-v1_0.sila.xml")
                            )
                    ),
                    new MetadataProvider()
            );
            builder.addFeature(
                    getFileContent(
                            Objects.requireNonNull(
                                    EmptyClass.class.getResourceAsStream(testFeaturesPath + "MetadataConsumerTest-v1_0.sila.xml")
                            )
                    ),
                    new MetadataConsumerTest()
            );
            builder.addFeature(
                    getFileContent(
                            Objects.requireNonNull(
                                    EmptyClass.class.getResourceAsStream(testFeaturesPath + "ErrorHandlingTest-v1_0.sila.xml")
                            )
                    ),
                    new ErrorHandlingTest()
            );
            builder.addFeature(
                    getFileContent(
                            Objects.requireNonNull(
                                    EmptyClass.class.getResourceAsStream(testFeaturesPath + "BinaryTransferTest-v1_0.sila.xml")
                            )
                    ),
                    new BinaryTransferTest(this.binaryDatabase)
            );
            builder.addFeature(
                    getFileContent(
                            Objects.requireNonNull(
                                    EmptyClass.class.getResourceAsStream(baseFeaturesPath + "AuthenticationService-v1_0.sila.xml")
                            )
                    ),
                    new AuthenticationService(this.authTokens)
            );
            builder.addFeature(
                    getFileContent(
                            Objects.requireNonNull(
                                    EmptyClass.class.getResourceAsStream(testFeaturesPath + "AuthenticationTest-v1_0.sila.xml")
                            )
                    ),
                    new AuthenticationTest(this.binaryDatabase, builder.getAuthorize())
            );
            builder.addFeature(
                    getFileContent(
                            Objects.requireNonNull(
                                    EmptyClass.class.getResourceAsStream(baseFeaturesPath + "AuthorizationService-v1_0.sila.xml")
                            )
                    ),
                    new AuthorizationService()
            );
            builder.addFeature(
                    getFileContent(
                            Objects.requireNonNull(
                                    EmptyClass.class.getResourceAsStream(testFeaturesPath + "MultiClientTest-v1_0.sila.xml")
                            )
                    ),
                    new MultiClientTest()
            );

            builder.addFCPAffectedByMetadata(
                    "org.silastandard/test/BinaryTransferTest/v1/Command/EchoBinaryAndMetadataString",
                    "org.silastandard/test/BinaryTransferTest/v1/Metadata/String"
            );

            this.server = builder.start();
        } catch (IOException | SQLException e) {
            log.error("Something went wrong when building / starting server", e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public void close() {
        this.server.close();
    }

    /**
     * Simple main function that starts the server and keeps it alive
     */
    public static void main(final String[] args) {
        final ArgumentHelper argumentHelper = new ArgumentHelper(args, SERVER_TYPE);
        final Server server = new Server(argumentHelper);
        log.info("To stop the server press CTRL + C.");
        server.server.blockUntilShutdown();
        log.info("termination complete.");
    }

}
