package sila_java.interoperability.server.features;

import io.grpc.stub.StreamObserver;
import sila2.org.silastandard.test.unobservablecommandtest.v1.UnobservableCommandTestGrpc;
import sila2.org.silastandard.test.unobservablecommandtest.v1.UnobservableCommandTestOuterClass;
import sila_java.library.core.sila.errors.SiLAErrors;
import sila_java.library.core.sila.types.SiLAString;

public class UnobservableCommandTest extends UnobservableCommandTestGrpc.UnobservableCommandTestImplBase {

    @Override
    public void commandWithoutParametersAndResponses(
            UnobservableCommandTestOuterClass.CommandWithoutParametersAndResponses_Parameters request,
            StreamObserver<UnobservableCommandTestOuterClass.CommandWithoutParametersAndResponses_Responses> responseObserver
    ) {
        responseObserver.onNext(
                UnobservableCommandTestOuterClass.CommandWithoutParametersAndResponses_Responses
                        .newBuilder()
                        .build()
        );
        responseObserver.onCompleted();
    }

    @Override
    public void convertIntegerToString(
            UnobservableCommandTestOuterClass.ConvertIntegerToString_Parameters request,
            StreamObserver<UnobservableCommandTestOuterClass.ConvertIntegerToString_Responses> responseObserver
    ) {
        if (!request.hasInteger()) {
            throw SiLAErrors.generateValidationError("org.silastandard/test/UnobservableCommandTest/v1/Command/ConvertIntegerToString/Parameter/Integer", "Missing parameter.");
        }
        responseObserver.onNext(
                UnobservableCommandTestOuterClass.ConvertIntegerToString_Responses
                        .newBuilder()
                        .setStringRepresentation(SiLAString.from(Long.toString(request.getInteger().getValue())))
                        .build()
        );
        responseObserver.onCompleted();
    }

    @Override
    public void joinIntegerAndString(
            UnobservableCommandTestOuterClass.JoinIntegerAndString_Parameters request,
            StreamObserver<UnobservableCommandTestOuterClass.JoinIntegerAndString_Responses> responseObserver
    ) {
        if (!request.hasString()) {
            throw SiLAErrors.generateValidationError("org.silastandard/test/UnobservableCommandTest/v1/Command/JoinIntegerAndString/Parameter/String", "Missing parameter.");
        }
        if (!request.hasInteger()) {
            throw SiLAErrors.generateValidationError("org.silastandard/test/UnobservableCommandTest/v1/Command/JoinIntegerAndString/Parameter/Integer", "Missing parameter.");
        }
        responseObserver.onNext(
                UnobservableCommandTestOuterClass.JoinIntegerAndString_Responses
                        .newBuilder()
                        .setJoinedParameters(
                                SiLAString.from(request.getInteger().getValue() + request.getString().getValue())
                        )
                        .build()
        );
        responseObserver.onCompleted();
    }

    @Override
    public void splitStringAfterFirstCharacter(
            UnobservableCommandTestOuterClass.SplitStringAfterFirstCharacter_Parameters request,
            StreamObserver<UnobservableCommandTestOuterClass.SplitStringAfterFirstCharacter_Responses> responseObserver
    ) {
        if (!request.hasString()) {
            throw SiLAErrors.generateValidationError("org.silastandard/test/UnobservableCommandTest/v1/Command/SplitStringAfterFirstCharacter/Parameter/String", "Missing parameter.");
        }
        final String inputStr = request.getString().getValue();
        final String firstChar = (inputStr.length() > 0) ? inputStr.substring(0, 1) : "";
        final String remainder = (inputStr.length() > 1) ? inputStr.substring(1) : "";
        responseObserver.onNext(
                UnobservableCommandTestOuterClass.SplitStringAfterFirstCharacter_Responses
                        .newBuilder()
                        .setFirstCharacter(SiLAString.from(firstChar))
                        .setRemainder(SiLAString.from(remainder))
                        .build()
        );
        responseObserver.onCompleted();
    }
}
