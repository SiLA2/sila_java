package sila_java.interoperability.server.features;

import io.grpc.stub.StreamObserver;
import sila2.org.silastandard.SiLAFramework;
import sila2.org.silastandard.test.metadataconsumertest.v1.MetadataConsumerTestGrpc;
import sila2.org.silastandard.test.metadataconsumertest.v1.MetadataConsumerTestOuterClass;
import sila2.org.silastandard.test.metadataprovider.v1.MetadataProviderOuterClass;
import sila_java.library.core.sila.errors.SiLAErrors;
import sila_java.library.core.sila.types.SiLAString;
import sila_java.library.server_base.metadata.ServerMetadataContainer;

import static sila_java.library.server_base.metadata.MetadataExtractingInterceptor.SILA_METADATA_KEY;

public class MetadataConsumerTest extends MetadataConsumerTestGrpc.MetadataConsumerTestImplBase {
    @Override
    public void echoStringMetadata(
            MetadataConsumerTestOuterClass.EchoStringMetadata_Parameters request,
            StreamObserver<MetadataConsumerTestOuterClass.EchoStringMetadata_Responses> responseObserver
    ) {
        final ServerMetadataContainer serverMetadataContainer = SILA_METADATA_KEY.get();
        final MetadataProviderOuterClass.Metadata_StringMetadata metadataString;
        try {
            metadataString = serverMetadataContainer.get(MetadataProviderOuterClass.Metadata_StringMetadata.class);
        } catch (Exception e) {
            throw SiLAErrors.generateFrameworkError(SiLAFramework.FrameworkError.ErrorType.INVALID_METADATA, "Malformed metadata.");
        }
        if (metadataString == null || !metadataString.hasStringMetadata()) {
            throw SiLAErrors.generateFrameworkError(SiLAFramework.FrameworkError.ErrorType.INVALID_METADATA, "Expect a metadata.");
        }

        responseObserver.onNext(
                MetadataConsumerTestOuterClass.EchoStringMetadata_Responses
                        .newBuilder()
                        .setReceivedStringMetadata(metadataString.getStringMetadata())
                        .build()
        );
        responseObserver.onCompleted();
    }

    @Override
    public void unpackMetadata(
            MetadataConsumerTestOuterClass.UnpackMetadata_Parameters request,
            StreamObserver<MetadataConsumerTestOuterClass.UnpackMetadata_Responses> responseObserver
    ) {
        final ServerMetadataContainer serverMetadataContainer = SILA_METADATA_KEY.get();

        final MetadataProviderOuterClass.Metadata_StringMetadata metadataString;
        try {
            metadataString = serverMetadataContainer.get(MetadataProviderOuterClass.Metadata_StringMetadata.class);
        } catch (Exception e) {
            throw SiLAErrors.generateFrameworkError(SiLAFramework.FrameworkError.ErrorType.INVALID_METADATA, "Malformed metadata.");
        }
        if (metadataString == null || !metadataString.hasStringMetadata()) {
            throw SiLAErrors.generateFrameworkError(SiLAFramework.FrameworkError.ErrorType.INVALID_METADATA, "Expect a metadata.");
        }

        final MetadataProviderOuterClass.Metadata_TwoIntegersMetadata twoIntegersMetadata;
        try {
            twoIntegersMetadata = serverMetadataContainer.get(MetadataProviderOuterClass.Metadata_TwoIntegersMetadata.class);
        } catch (Exception e) {
            throw SiLAErrors.generateFrameworkError(SiLAFramework.FrameworkError.ErrorType.INVALID_METADATA, "Malformed metadata.");
        }
        if (twoIntegersMetadata == null ||
                !twoIntegersMetadata.hasTwoIntegersMetadata() ||
                !twoIntegersMetadata.getTwoIntegersMetadata().hasFirstInteger() ||
                !twoIntegersMetadata.getTwoIntegersMetadata().hasSecondInteger()
        ) {
            throw SiLAErrors.generateFrameworkError(SiLAFramework.FrameworkError.ErrorType.INVALID_METADATA, "Expect a metadata.");
        }
        responseObserver.onNext(
                MetadataConsumerTestOuterClass.UnpackMetadata_Responses
                        .newBuilder()
                        .setFirstReceivedInteger(twoIntegersMetadata.getTwoIntegersMetadata().getFirstInteger())
                        .setSecondReceivedInteger(twoIntegersMetadata.getTwoIntegersMetadata().getSecondInteger())
                        .setReceivedString(metadataString.getStringMetadata())
                        .build()
        );
        responseObserver.onCompleted();
    }

    @Override
    public void getReceivedStringMetadata(MetadataConsumerTestOuterClass.Get_ReceivedStringMetadata_Parameters request, StreamObserver<MetadataConsumerTestOuterClass.Get_ReceivedStringMetadata_Responses> responseObserver) {
        final ServerMetadataContainer serverMetadataContainer = SILA_METADATA_KEY.get();

        final MetadataProviderOuterClass.Metadata_StringMetadata metadataString;
        try {
            metadataString = serverMetadataContainer.get(MetadataProviderOuterClass.Metadata_StringMetadata.class);
        } catch (Exception e) {
            throw SiLAErrors.generateFrameworkError(SiLAFramework.FrameworkError.ErrorType.INVALID_METADATA, "Malformed metadata.");
        }
        if (metadataString == null || !metadataString.hasStringMetadata()) {
            throw SiLAErrors.generateFrameworkError(SiLAFramework.FrameworkError.ErrorType.INVALID_METADATA, "Expect a metadata.");
        }
        responseObserver.onNext(
                MetadataConsumerTestOuterClass.Get_ReceivedStringMetadata_Responses
                        .newBuilder()
                        .setReceivedStringMetadata(metadataString.getStringMetadata())
                        .build()
        );
        responseObserver.onCompleted();
    }

    @Override
    public void subscribeReceivedStringMetadataAsCharacters(MetadataConsumerTestOuterClass.Subscribe_ReceivedStringMetadataAsCharacters_Parameters request, StreamObserver<MetadataConsumerTestOuterClass.Subscribe_ReceivedStringMetadataAsCharacters_Responses> responseObserver) {
        final ServerMetadataContainer serverMetadataContainer = SILA_METADATA_KEY.get();

        final MetadataProviderOuterClass.Metadata_StringMetadata metadataString;
        try {
            metadataString = serverMetadataContainer.get(MetadataProviderOuterClass.Metadata_StringMetadata.class);
        } catch (Exception e) {
            throw SiLAErrors.generateFrameworkError(SiLAFramework.FrameworkError.ErrorType.INVALID_METADATA, "Malformed metadata.");
        }
        if (metadataString == null || !metadataString.hasStringMetadata()) {
            throw SiLAErrors.generateFrameworkError(SiLAFramework.FrameworkError.ErrorType.INVALID_METADATA, "Expect a metadata.");
        }
        for (int i = 0; i < metadataString.getStringMetadata().getValue().length(); i++){
            final char c = metadataString.getStringMetadata().getValue().charAt(i);
            responseObserver.onNext(
                    MetadataConsumerTestOuterClass.Subscribe_ReceivedStringMetadataAsCharacters_Responses
                            .newBuilder()
                            .setReceivedStringMetadataAsCharacters(SiLAString.from(String.valueOf(c)))
                            .build()
            );
        }
        responseObserver.onCompleted();
    }
}
