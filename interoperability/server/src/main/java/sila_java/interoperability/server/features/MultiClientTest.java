package sila_java.interoperability.server.features;

import io.grpc.StatusRuntimeException;
import io.grpc.stub.StreamObserver;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.SiLAFramework;
import sila2.org.silastandard.test.multiclienttest.v1.MultiClientTestGrpc;
import sila2.org.silastandard.test.multiclienttest.v1.MultiClientTestOuterClass;
import sila_java.library.core.sila.errors.SiLAErrors;
import sila_java.library.server_base.command.observable.ObservableCommandManager;
import sila_java.library.server_base.command.observable.ObservableCommandTaskRunner;
import sila_java.library.server_base.command.observable.ObservableCommandWrapper;
import sila_java.library.server_base.command.observable.RunnableCommandTask;

@Slf4j
@AllArgsConstructor
public class MultiClientTest extends MultiClientTestGrpc.MultiClientTestImplBase {

    private final ObservableCommandManager<
            MultiClientTestOuterClass.RunInParallel_Parameters,
            MultiClientTestOuterClass.RunInParallel_Responses
            > runInParallel = new ObservableCommandManager<>(
            new ObservableCommandTaskRunner(1, 8),
            new RunnableCommandTask<MultiClientTestOuterClass.RunInParallel_Parameters, MultiClientTestOuterClass.RunInParallel_Responses>() {
                @Override
                public MultiClientTestOuterClass.RunInParallel_Responses run(ObservableCommandWrapper<MultiClientTestOuterClass.RunInParallel_Parameters, MultiClientTestOuterClass.RunInParallel_Responses> command) throws StatusRuntimeException {
                    try {
                        Thread.sleep((long)(command.getParameter().getDuration().getValue() * 1000));
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                    return MultiClientTestOuterClass.RunInParallel_Responses.newBuilder().build();
                }

                @Override
                public void validate(MultiClientTestOuterClass.RunInParallel_Parameters parameters) throws StatusRuntimeException {
                    if (!parameters.hasDuration()) {
                        throw SiLAErrors.generateValidationError("org.silastandard/test/MultiClientTest/v1/Command/RunInParallel/Parameter/Duration", "Missing parameter.");
                    }
                }
            }
    );

    private final ObservableCommandManager<
            MultiClientTestOuterClass.RunQueued_Parameters,
            MultiClientTestOuterClass.RunQueued_Responses
            > runQueued = new ObservableCommandManager<>(
            new ObservableCommandTaskRunner(8, 1),
            new RunnableCommandTask<MultiClientTestOuterClass.RunQueued_Parameters, MultiClientTestOuterClass.RunQueued_Responses>() {
                @Override
                public MultiClientTestOuterClass.RunQueued_Responses run(ObservableCommandWrapper<MultiClientTestOuterClass.RunQueued_Parameters, MultiClientTestOuterClass.RunQueued_Responses> command) throws StatusRuntimeException {
                    try {
                        Thread.sleep((long)command.getParameter().getDuration().getValue() * 1000);
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                    return MultiClientTestOuterClass.RunQueued_Responses.newBuilder().build();
                }

                @Override
                public void validate(MultiClientTestOuterClass.RunQueued_Parameters parameters) throws StatusRuntimeException {
                    if (!parameters.hasDuration()) {
                        throw SiLAErrors.generateValidationError("org.silastandard/test/MultiClientTest/v1/Command/RunQueued/Parameter/Duration", "Missing parameter.");
                    }
                }
            }
    );


    private final ObservableCommandManager<
            MultiClientTestOuterClass.RejectParallelExecution_Parameters,
            MultiClientTestOuterClass.RejectParallelExecution_Responses
            > rejectParallel = new ObservableCommandManager<>(
            new ObservableCommandTaskRunner(0, 1),
            new RunnableCommandTask<MultiClientTestOuterClass.RejectParallelExecution_Parameters, MultiClientTestOuterClass.RejectParallelExecution_Responses>() {
                @Override
                public MultiClientTestOuterClass.RejectParallelExecution_Responses run(ObservableCommandWrapper<MultiClientTestOuterClass.RejectParallelExecution_Parameters, MultiClientTestOuterClass.RejectParallelExecution_Responses> command) throws StatusRuntimeException {
                    try {
                        Thread.sleep((long)command.getParameter().getDuration().getValue() * 1000);
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                    return MultiClientTestOuterClass.RejectParallelExecution_Responses.newBuilder().build();
                }

                @Override
                public void validate(MultiClientTestOuterClass.RejectParallelExecution_Parameters parameters) throws StatusRuntimeException {
                    if (!parameters.hasDuration()) {
                        throw SiLAErrors.generateValidationError("org.silastandard/test/MultiClientTest/v1/Command/RejectParallelExecution/Parameter/Duration", "Missing parameter.");
                    }
                }
            }
    );


    @Override
    public void runInParallel(MultiClientTestOuterClass.RunInParallel_Parameters request, StreamObserver<SiLAFramework.CommandConfirmation> responseObserver) {
        this.runInParallel.addCommand(request, responseObserver);
    }

    @Override
    public void runInParallelInfo(SiLAFramework.CommandExecutionUUID request, StreamObserver<SiLAFramework.ExecutionInfo> responseObserver) {
        this.runInParallel.get(request).addStateObserver(responseObserver);
    }

    @Override
    public void runInParallelResult(SiLAFramework.CommandExecutionUUID request, StreamObserver<MultiClientTestOuterClass.RunInParallel_Responses> responseObserver) {
        this.runInParallel.get(request).sendResult(responseObserver);
    }

    @Override
    public void runQueued(MultiClientTestOuterClass.RunQueued_Parameters request, StreamObserver<SiLAFramework.CommandConfirmation> responseObserver) {
        this.runQueued.addCommand(request, responseObserver);
    }

    @Override
    public void runQueuedInfo(SiLAFramework.CommandExecutionUUID request, StreamObserver<SiLAFramework.ExecutionInfo> responseObserver) {
        this.runQueued.get(request).addStateObserver(responseObserver);
    }

    @Override
    public void runQueuedResult(SiLAFramework.CommandExecutionUUID request, StreamObserver<MultiClientTestOuterClass.RunQueued_Responses> responseObserver) {
        this.runQueued.get(request).sendResult(responseObserver);
    }

    @Override
    public void rejectParallelExecution(MultiClientTestOuterClass.RejectParallelExecution_Parameters request, StreamObserver<SiLAFramework.CommandConfirmation> responseObserver) {
        this.rejectParallel.addCommand(request, responseObserver);
    }

    @Override
    public void rejectParallelExecutionInfo(SiLAFramework.CommandExecutionUUID request, StreamObserver<SiLAFramework.ExecutionInfo> responseObserver) {
        this.rejectParallel.get(request).addStateObserver(responseObserver);
    }

    @Override
    public void rejectParallelExecutionResult(SiLAFramework.CommandExecutionUUID request, StreamObserver<MultiClientTestOuterClass.RejectParallelExecution_Responses> responseObserver) {
        this.rejectParallel.get(request).sendResult(responseObserver);
    }
}