package sila_java.interoperability.server.features;

import io.grpc.stub.StreamObserver;
import sila2.org.silastandard.SiLAFramework;
import sila2.org.silastandard.core.authorizationservice.v1.AuthorizationServiceGrpc;
import sila2.org.silastandard.core.authorizationservice.v1.AuthorizationServiceOuterClass;

public class AuthorizationService extends AuthorizationServiceGrpc.AuthorizationServiceImplBase {

    @Override
    public void getFCPAffectedByMetadataAccessToken(
            AuthorizationServiceOuterClass.Get_FCPAffectedByMetadata_AccessToken_Parameters request,
            StreamObserver<AuthorizationServiceOuterClass.Get_FCPAffectedByMetadata_AccessToken_Responses> responseObserver
    ) {
        responseObserver.onNext(
                AuthorizationServiceOuterClass.Get_FCPAffectedByMetadata_AccessToken_Responses.newBuilder()
                        .addAffectedCalls(SiLAFramework.String
                                .newBuilder()
                                .setValue("org.silastandard/test/AuthenticationTest/v1")
                                .build()
                        )
                        .build()
        );
        responseObserver.onCompleted();
    }
}