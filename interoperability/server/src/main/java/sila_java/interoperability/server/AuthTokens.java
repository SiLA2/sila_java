package sila_java.interoperability.server;

import java.time.Instant;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

public class AuthTokens {
    private final UUID serverUuid;
    private final Map<String, Instant> authTokens = new ConcurrentHashMap<>();

    public AuthTokens(final UUID serverUuid) {
        this.serverUuid = serverUuid;
    }

    public UUID getServerUuid() {
        return serverUuid;
    }

    public Map<String, Instant> getAuthTokens() {
        return authTokens;
    }
}
