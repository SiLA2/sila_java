package sila_java.interoperability.server.features;

import io.grpc.stub.StreamObserver;
import sila2.org.silastandard.SiLAFramework;
import sila2.org.silastandard.test.errorhandlingtest.v1.ErrorHandlingTestGrpc;
import sila2.org.silastandard.test.errorhandlingtest.v1.ErrorHandlingTestOuterClass;
import sila_java.library.core.sila.errors.SiLAErrors;
import sila_java.library.core.sila.types.SiLAInteger;
import sila_java.library.server_base.command.observable.ObservableCommandManager;
import sila_java.library.server_base.command.observable.ObservableCommandTaskRunner;

import java.time.Duration;

public class ErrorHandlingTest extends ErrorHandlingTestGrpc.ErrorHandlingTestImplBase {
    private final ObservableCommandManager<
            ErrorHandlingTestOuterClass.RaiseDefinedExecutionErrorObservably_Parameters,
            ErrorHandlingTestOuterClass.RaiseDefinedExecutionErrorObservably_Responses
            > definedErrorObservably = new ObservableCommandManager<>(
            new ObservableCommandTaskRunner(1, 8),
            command -> {
                throw SiLAErrors.generateDefinedExecutionError("org.silastandard/test/ErrorHandlingTest/v1/DefinedExecutionError/TestError", "SiLA2_test_error_message");
            },
            Duration.ofSeconds(59)
    );

    private final ObservableCommandManager<
            ErrorHandlingTestOuterClass.RaiseUndefinedExecutionErrorObservably_Parameters,
            ErrorHandlingTestOuterClass.RaiseUndefinedExecutionErrorObservably_Responses
            > undefinedErrorObservably = new ObservableCommandManager<>(
            new ObservableCommandTaskRunner(1, 8),
            command -> {
                throw SiLAErrors.generateUndefinedExecutionError("SiLA2_test_error_message");
            },
            Duration.ofSeconds(59)
    );
    @Override
    public void raiseDefinedExecutionError(
            ErrorHandlingTestOuterClass.RaiseDefinedExecutionError_Parameters request,
            StreamObserver<ErrorHandlingTestOuterClass.RaiseDefinedExecutionError_Responses> responseObserver
    ) {
        throw SiLAErrors.generateDefinedExecutionError("org.silastandard/test/ErrorHandlingTest/v1/DefinedExecutionError/TestError", "SiLA2_test_error_message");
    }

    @Override
    public void raiseDefinedExecutionErrorObservably(
            ErrorHandlingTestOuterClass.RaiseDefinedExecutionErrorObservably_Parameters request,
            StreamObserver<SiLAFramework.CommandConfirmation> responseObserver
    ) {
        this.definedErrorObservably.addCommand(request, responseObserver);
    }

    @Override
    public void raiseDefinedExecutionErrorObservablyInfo(
            SiLAFramework.CommandExecutionUUID request,
            StreamObserver<SiLAFramework.ExecutionInfo> responseObserver
    ) {
        this.definedErrorObservably.get(request).addStateObserver(responseObserver);
    }

    @Override
    public void raiseDefinedExecutionErrorObservablyResult(
            SiLAFramework.CommandExecutionUUID request,
            StreamObserver<ErrorHandlingTestOuterClass.RaiseDefinedExecutionErrorObservably_Responses> responseObserver
    ) {
        this.definedErrorObservably.get(request).sendResult(responseObserver);
    }

    @Override
    public void raiseUndefinedExecutionError(
            ErrorHandlingTestOuterClass.RaiseUndefinedExecutionError_Parameters request,
            StreamObserver<ErrorHandlingTestOuterClass.RaiseUndefinedExecutionError_Responses> responseObserver
    ) {
        throw SiLAErrors.generateUndefinedExecutionError("SiLA2_test_error_message");
    }

    @Override
    public void raiseUndefinedExecutionErrorObservably(
            ErrorHandlingTestOuterClass.RaiseUndefinedExecutionErrorObservably_Parameters request,
            StreamObserver<SiLAFramework.CommandConfirmation> responseObserver
    ) {
        this.undefinedErrorObservably.addCommand(request, responseObserver);
    }

    @Override
    public void raiseUndefinedExecutionErrorObservablyInfo(
            SiLAFramework.CommandExecutionUUID request,
            StreamObserver<SiLAFramework.ExecutionInfo> responseObserver
    ) {
        this.undefinedErrorObservably.get(request).addStateObserver(responseObserver);
    }

    @Override
    public void raiseUndefinedExecutionErrorObservablyResult(
            SiLAFramework.CommandExecutionUUID request,
            StreamObserver<ErrorHandlingTestOuterClass.RaiseUndefinedExecutionErrorObservably_Responses> responseObserver
    ) {
        this.undefinedErrorObservably.get(request).sendResult(responseObserver);
    }

    @Override
    public void getRaiseDefinedExecutionErrorOnGet(
            ErrorHandlingTestOuterClass.Get_RaiseDefinedExecutionErrorOnGet_Parameters request,
            StreamObserver<ErrorHandlingTestOuterClass.Get_RaiseDefinedExecutionErrorOnGet_Responses> responseObserver
    ) {
        throw SiLAErrors.generateDefinedExecutionError("org.silastandard/test/ErrorHandlingTest/v1/DefinedExecutionError/TestError", "SiLA2_test_error_message");
    }

    @Override
    public void subscribeRaiseDefinedExecutionErrorOnSubscribe(
            ErrorHandlingTestOuterClass.Subscribe_RaiseDefinedExecutionErrorOnSubscribe_Parameters request,
            StreamObserver<ErrorHandlingTestOuterClass.Subscribe_RaiseDefinedExecutionErrorOnSubscribe_Responses> responseObserver
    ) {
        throw SiLAErrors.generateDefinedExecutionError("org.silastandard/test/ErrorHandlingTest/v1/DefinedExecutionError/TestError", "SiLA2_test_error_message");
    }

    @Override
    public void getRaiseUndefinedExecutionErrorOnGet(
            ErrorHandlingTestOuterClass.Get_RaiseUndefinedExecutionErrorOnGet_Parameters request,
            StreamObserver<ErrorHandlingTestOuterClass.Get_RaiseUndefinedExecutionErrorOnGet_Responses> responseObserver
    ) {
        throw SiLAErrors.generateUndefinedExecutionError("SiLA2_test_error_message");
    }

    @Override
    public void subscribeRaiseUndefinedExecutionErrorOnSubscribe(
            ErrorHandlingTestOuterClass.Subscribe_RaiseUndefinedExecutionErrorOnSubscribe_Parameters request,
            StreamObserver<ErrorHandlingTestOuterClass.Subscribe_RaiseUndefinedExecutionErrorOnSubscribe_Responses> responseObserver
    ) {
        throw SiLAErrors.generateUndefinedExecutionError("SiLA2_test_error_message");
    }

    @Override
    public void subscribeRaiseDefinedExecutionErrorAfterValueWasSent(
            ErrorHandlingTestOuterClass.Subscribe_RaiseDefinedExecutionErrorAfterValueWasSent_Parameters request,
            StreamObserver<ErrorHandlingTestOuterClass.Subscribe_RaiseDefinedExecutionErrorAfterValueWasSent_Responses> responseObserver
    ) {
        responseObserver.onNext(
                ErrorHandlingTestOuterClass.Subscribe_RaiseDefinedExecutionErrorAfterValueWasSent_Responses
                        .newBuilder()
                        .setRaiseDefinedExecutionErrorAfterValueWasSent(SiLAInteger.from(1))
                        .build()
        );
        throw SiLAErrors.generateDefinedExecutionError("org.silastandard/test/ErrorHandlingTest/v1/DefinedExecutionError/TestError", "SiLA2_test_error_message");
    }

    @Override
    public void subscribeRaiseUndefinedExecutionErrorAfterValueWasSent(
            ErrorHandlingTestOuterClass.Subscribe_RaiseUndefinedExecutionErrorAfterValueWasSent_Parameters request,
            StreamObserver<ErrorHandlingTestOuterClass.Subscribe_RaiseUndefinedExecutionErrorAfterValueWasSent_Responses> responseObserver
    ) {
        responseObserver.onNext(
                ErrorHandlingTestOuterClass.Subscribe_RaiseUndefinedExecutionErrorAfterValueWasSent_Responses
                        .newBuilder()
                        .setRaiseUndefinedExecutionErrorAfterValueWasSent(SiLAInteger.from(1))
                        .build()
        );
        throw SiLAErrors.generateUndefinedExecutionError("SiLA2_test_error_message");
    }
}
