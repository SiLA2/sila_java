package sila_java.interoperability.server.features;

import com.google.common.collect.Lists;
import io.grpc.stub.StreamObserver;
import sila2.org.silastandard.test.metadataprovider.v1.MetadataProviderGrpc;
import sila2.org.silastandard.test.metadataprovider.v1.MetadataProviderOuterClass;
import sila_java.library.core.sila.types.SiLAString;

import java.util.Collections;

public class MetadataProvider extends MetadataProviderGrpc.MetadataProviderImplBase {
    @Override
    public void getFCPAffectedByMetadataStringMetadata(
            MetadataProviderOuterClass.Get_FCPAffectedByMetadata_StringMetadata_Parameters request,
            StreamObserver<MetadataProviderOuterClass.Get_FCPAffectedByMetadata_StringMetadata_Responses> responseObserver
    ) {
        responseObserver.onNext(
                MetadataProviderOuterClass.Get_FCPAffectedByMetadata_StringMetadata_Responses
                        .newBuilder()
                        .addAllAffectedCalls(Lists.newArrayList(
                                SiLAString.from("org.silastandard/test/MetadataConsumerTest/v1/Command/UnpackMetadata"),
                                SiLAString.from("org.silastandard/test/MetadataConsumerTest/v1/Command/EchoStringMetadata"))
                        )
                        .build()
        );
        responseObserver.onCompleted();
    }

    @Override
    public void getFCPAffectedByMetadataTwoIntegersMetadata(
            MetadataProviderOuterClass.Get_FCPAffectedByMetadata_TwoIntegersMetadata_Parameters request,
            StreamObserver<MetadataProviderOuterClass.Get_FCPAffectedByMetadata_TwoIntegersMetadata_Responses> responseObserver
    ) {
        responseObserver.onNext(
                MetadataProviderOuterClass.Get_FCPAffectedByMetadata_TwoIntegersMetadata_Responses
                        .newBuilder()
                        .addAllAffectedCalls(Collections.singleton(SiLAString.from("org.silastandard/test/MetadataConsumerTest/v1/Command/UnpackMetadata")))
                        .build()
        );
        responseObserver.onCompleted();
    }
}
