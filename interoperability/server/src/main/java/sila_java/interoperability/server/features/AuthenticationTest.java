package sila_java.interoperability.server.features;

import io.grpc.stub.StreamObserver;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.test.authenticationtest.v1.AuthenticationTestGrpc;
import sila2.org.silastandard.test.authenticationtest.v1.AuthenticationTestOuterClass;
import sila_java.library.core.sila.errors.SiLAErrorException;
import sila_java.library.core.sila.errors.SiLAErrors;
import sila_java.library.server_base.binary_transfer.Binary;
import sila_java.library.server_base.binary_transfer.database.BinaryDatabase;
import sila_java.library.server_base.binary_transfer.database.BinaryDatabaseException;
import sila_java.library.server_base.standard_features.v1.AuthorizationController;

import java.util.UUID;


@Slf4j
@AllArgsConstructor
public class AuthenticationTest extends AuthenticationTestGrpc.AuthenticationTestImplBase {

    private final BinaryDatabase binaryDatabase;
    private final AuthorizationController.Authorize authorize;

    @Override
    public void requiresToken(
            AuthenticationTestOuterClass.RequiresToken_Parameters request,
            StreamObserver<AuthenticationTestOuterClass.RequiresToken_Responses> responseObserver
    ) {
        try {
            this.authorize.authorize("org.silastandard/test/AuthenticationTest/v1");
        } catch (SiLAErrorException e) {
            responseObserver.onError(e);
            return;
        }

        responseObserver.onNext(
                AuthenticationTestOuterClass.RequiresToken_Responses
                        .newBuilder()
                        .build()
        );
        responseObserver.onCompleted();
    }

    @Override
    public void requiresTokenForBinaryUpload(
            AuthenticationTestOuterClass.RequiresTokenForBinaryUpload_Parameters request,
            StreamObserver<AuthenticationTestOuterClass.RequiresTokenForBinaryUpload_Responses> responseObserver
    ) {
        try {
            this.authorize.authorize("org.silastandard/test/AuthenticationTest/v1");
        } catch (SiLAErrorException e) {
            responseObserver.onError(e);
            return;
        }

        if (!request.hasBinaryToUpload()) {
            responseObserver.onError(SiLAErrors.generateValidationError(
                    "org.silastandard/test/AuthenticationTest/v1/Command/RequiresTokenForBinaryUpload/Parameter/BinaryToUpload",
                    "Missing parameter 'Binary To Upload'."
            ));
            return;
        }

        if (request.getBinaryToUpload().hasBinaryTransferUUID()) {
            try {
                Binary binary = binaryDatabase.getBinary(UUID.fromString(request.getBinaryToUpload().getBinaryTransferUUID()));
            } catch (BinaryDatabaseException e) {
                log.error("Error during binary transfer", e);
                responseObserver.onError(SiLAErrors.generateUndefinedExecutionError("Error during binary transfer"));
                return;
            }
        }

        responseObserver.onNext(
                AuthenticationTestOuterClass.RequiresTokenForBinaryUpload_Responses
                        .newBuilder()
                        .build()
        );
        responseObserver.onCompleted();
    }
}